<?php
/**
 * Базовый класс модели
 */
abstract class Welt_Model implements ArrayAccess
{
	
	const DEF_LANG = 'ru';
	
	/**
	 * Свойства
	 * @var array
	 */
	protected $_data = array();
	
	/**
	 * Свойства, зависящие от языка
	 * @var array 
	 */
	protected $_langDep = array();


	/**
	 * Конструктор: установка параметров
	 * @param array $data [optional]
	 * @return 
	 */
    public function __construct(array $data = null)
	{
		//Устанавливаем свойства
        if (!is_null($data))
		{
            foreach ($data as $name=>$value)
			{
                $this-> {$name} = $value;
            }
        }
    }
	
	/**
	 * Получение текущего объекта в виде массива
	 * @return 
	 */
    public function toArray()
	{
        return $this->_data;
    }
	
	/**
	 * Установка свойства
	 * @param string $name
	 * @param string $value
	 * @return 
	 */
    public function __set($name, $value)
	{
		$setter = 'set' . ucfirst($name);	
		if(method_exists($this,$setter))
		{
			return $this->$setter($value);
		}
		else
		{
			$this->_data[$name] = $value;
		}
    }
	
	/**
	 * Получение свойства
	 * @param string $name
	 * @return 
	 */
    public function __get($name)
	{
		$getter = 'get' . ucfirst($name);
		if(method_exists($this, $getter))
		{
			return $this->$getter();
		}
        elseif (array_key_exists($name, $this->_data))
		{
            return $this->_data[$name];
        }
    }
	
	/**
	 * Проверка существования свойства
	 * @param string $name
	 * @return 
	 */
    public function __isset($name)
	{
        return isset($this->_data[$name]);
    }
	
	/**
	 * Удаление свойства
	 * @param string $name
	 * @return 
	 */
    public function __unset($name)
	{
        if (isset($this->_data[$name]))
		{
            unset($this->_data[$name]);
        }
    }
	
	/**
	 * Получить поле в зависимости от языка
	 * @param string $field 
	 */
	protected function _getLangDep($field)
	{
		$lang = $this->_getCurLang();
		$langIndex = 0;
		if ($lang == 'en')
		{
			$langIndex = 1;
		}
		if (isset($this->_langDep[$field]) && isset($this->_langDep[$field][$langIndex]))
		{
			$prop = $this->_langDep[$field][$langIndex];
			return $this->$prop;
		}
		return NULL;
	}

	protected function _getCurLang()
	{
		$lang = self::DEF_LANG;
		if (Zend_Registry::isRegistered('lang'))
		{
			$lang = Zend_Registry::get('lang');
		}
		return $lang;
	}
	
	
	/**
	 * Returns whether there is an element at the specified offset.
	 * This method is required by the interface ArrayAccess.
	 * @param mixed $offset the offset to check on
	 * @return boolean
	 */
	public function offsetExists($offset)
	{
		return property_exists($this,$offset);
	}

	/**
	 * Returns the element at the specified offset.
	 * This method is required by the interface ArrayAccess.
	 * @param integer $offset the offset to retrieve element.
	 * @return mixed the element at the offset, null if no element is found at the offset
	 */
	public function offsetGet($offset)
	{
		return $this->$offset;
	}

	/**
	 * Sets the element at the specified offset.
	 * This method is required by the interface ArrayAccess.
	 * @param integer $offset the offset to set element
	 * @param mixed $item the element value
	 */
	public function offsetSet($offset,$item)
	{
		$this->$offset=$item;
	}

	/**
	 * Unsets the element at the specified offset.
	 * This method is required by the interface ArrayAccess.
	 * @param mixed $offset the offset to unset element
	 */
	public function offsetUnset($offset)
	{
		unset($this->$offset);
	}

}
