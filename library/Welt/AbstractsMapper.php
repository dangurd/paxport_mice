<?php
class Welt_AbstractsMapper extends Welt_FieldsMapper
{
	protected $_fieldsMap = array(
		'person_id' => 'personId',
		'title' => 'title',
		'form' => 'form',
		'theme' => 'theme',
		'section' => 'section',
		'title2' => 'title2',
		'authors' => 'authors',
		'authors2' => 'authors2',
		'company' => 'company',
		'company2' => 'company2',
		'city' => 'city',
		'city2' => 'city2',
		'notes' => 'notes',
		'lector' => 'lector',
		'goal_text' => 'goalText',

	);
}