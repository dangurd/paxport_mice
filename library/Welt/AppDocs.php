<?php
class Welt_AppDocs {
	/**
	 * @var Тип документа
	 */
	protected $docType;
	/**
	 * @var Таблица для работы с выбранным типом документа
	 */
	protected $docTable;
	
	protected static $avaibTypes = array(
		'confirmation',
		'invoice',
		'auth_letter'
	);
	
	protected $settings;
	
	/**
	 * Конструктор
	 * @param string $docType
	 * @return 
	 */
	public function __construct($docType)
	{
		$this->setDocType($docType);
		$this->settings = Zend_Registry::get('settings');
	}
	
	/**
	 * Сеттер для docType
	 * @return 
	 */
	public function setDocType($docType)
	{
		//Проверяем, корректен ли запрошенный тип документа
		if (!in_array($docType,self::$avaibTypes))
		{
			throw new Exception('Wrong doc type');
		}
		else
		{
			$this->docType = $docType;
			$this->setDocTable();
		}
	}
	
	
	/**
	 * В зависмости от типа документа, определяем таблицу для поиска документа
	 * @return 
	 */
	protected function setDocTable()
	{
		switch($this->docType)
		{
			case 'confirmation' : $this->docTable = new Welt_DbTable_Confirmations();
			break;
			case 'invoice' : $this->docTable = new Welt_DbTable_Invoices();
			break;
			case 'auth_letter' : $this->docTable = new Welt_DbTable_AuthLetters();
			break;
		}
	}
	
	/**
	 * Получение всех документов заданного типа для заявки
	 * @return 
	 */
	public function getAppDocs($appId)
	{
		$docs = array();
		//В зависимости от типа документа, используем требуемый метод
		switch($this->docType)
		{
			case 'invoice' : $docs = $this->docTable->getAppInvoices($appId);
			break;
			case 'auth_letter' : $docs = $this->docTable->getAppAuthLetters($appId);
			break;
		}
		return $docs;
	}
	
	/**
	 * Определение названия файла и пути к файлу с документом заданного типа в рамках указанной заявки
	 * @param int $docId
	 * @param int $appId
	 * @return 
	 */
	public function getDocLocation($docId,$appId)
	{
		//Ищем документ в БД
		$docRow = $this->docTable->find($docId)->current();
		//Проверка, принадлежит ли документ к указанной заявке
		if ($appId!=$docRow['application_id'])
		{
			return false;
		}
		//Если документ не найден
		if (!$docRow)
		{
			return false;
		}
		$docLoc = array(
			'link'		=>	$docRow->link,
			'filePath'	=>	$this->settings->services->docs_path.$docRow->link
		);
		return $docLoc;
	}
	
	
}
