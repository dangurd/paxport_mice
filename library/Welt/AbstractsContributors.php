<?php
class Welt_DbTable_AbstractsContributors extends Welt_DbTableBase
{
	protected $_name = 'abstracts_contributors';
	
	/**
	 * Получение списка соавторов для тезиса
	 * @param int $abstractId
	 * @return 
	 */
	public function getsAbstractContributors($abstractId)
	{
		$select = $this->select();
		$select->from(array($this->_name))
				->where('abstract_id = ?', $abstractId);
		$rows = $this->fetchAll($select);
		return $rows->toArray();
	}
}