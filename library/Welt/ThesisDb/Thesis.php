<?php
/**
 * Created by Reiseburo WELT.
 * User: Max Chaply
 * Date: 17.03.14
 * Time: 17:52
 * To change this template use File | Settings | File Templates.
 */


class Welt_DbTable_ThesisDb_Thesis extends Welt_DbTable_ThesisDb_Base
{
    protected $_name = 'thesis';

    public function getAll()
    {
        $select = $this->select();
        $select->from(array($this->_name));
        $rows = $this->fetchAll($select);
        return $rows->toArray();
    }


}