<?php
/**
 * Класс для работы с уcлугами в рамках заказа
 */
class Welt_Services {
	/**
	 * @var Id заявки (тура)
	 */
	protected $appId;
	/**
	 * @var Id участника
	 */
	protected $userId;
	/**
	 * @var Id услуги
	 */
	protected $serviceId;
	/**
	 * @var Массив из Id кокретных персон, для которых идёт заказ
	 */
	protected $orderPersons;
	/**
	 * @var Доступные для заказа типы услуг
	 */
	protected $servicesTypes;
	/**
	 * @var Установленный тип услуги
	 */
	protected $servicesType;
	/**
	 * @var Системные настройки
	 */
	protected $sysSettings;
	/**
	 * @var Объект класса для работы с таблицей связей персон и услуг
	 */
	protected $serviceAndPersonsTable;
	/**
	 * @var Таблица в Бд для работы с заданной услугой
	 */
	protected $serviceTable;
	
	/**
	 * Конструктор
	 * @param int $appId
	 * @param int $userId
	 * @return 
	 */
	public function __construct($appId,$userId)
	{
		$this->appId = $appId;
		$this->userId  = $userId;
		//Устанавливаем возможные типы услуг
		$this->servicesTypes = $this->setServicesTypes();
		$this->sysSettings = Zend_Registry::get('settings');
		//Объекты классов таблиц
		$this->serviceAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
	}
	
	/**
	 * Сеттеры для свойств
	 */
	public function setUser($userId)
	{
		$this->userId  = $userId;
	}
	
	public function setAppId($appId)
	{
		$this->appId = $appId;
	}
	
	/**
	 * Установка списка возможных для бронирования услуг
	 * @return array $servicesTypes
	 */
	protected function setServicesTypes()
	{
		$servicesTypesTable = new Welt_DbTable_ServicesTypes();
		$servicesTypes = $servicesTypesTable->getSysNamesArr();
		return $servicesTypes;
	}
	
	/**
	 * Получение списка услуг, доступных для заказа
	 * @return 
	 */
	public function getServicesTypes()
	{
		return $this->servicesTypes;
	}
	
	/**
	 * Добавление привязки услуги к персонам
	 * @param int $serviceId
	 * @param array $orderPersons
	 * @return bool
	 */
	protected function addServiceToPersons($serviceId,$orderPersons)
	{
		$servicesAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
		foreach ($orderPersons as $person)
		{
			$serviceData['application_id'] = $this->appId;
			$serviceData['service_sys_name'] = $this->servicesType;
			$serviceData['service_id'] = $serviceId;
			$serviceData['person_id'] = $person['id'];
			if(!$servicesAndPersonsTable->insert($serviceData))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Удаление связей между персонами и услугой
	 * @param int $serviceId
	 * @return 
	 */
	protected function deletePersonsService($serviceId)
	{
		$servicesAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
		$where[] = $servicesAndPersonsTable->getAdapter()->quoteInto('service_id = ?', $serviceId);
		$where[] = $servicesAndPersonsTable->getAdapter()->quoteInto('service_sys_name = ?', $this->servicesType);
		return $servicesAndPersonsTable->delete($where);
	}
	
	/**
	 * Проверка на предмет, установлены ли Id персоны и тип услуги.
	 * Проверяется при заказе конкретных услуг. 
	 * @return 
	 */
	protected function checkOrderReady()
	{
		if (!isset($this->orderPersons) || !isset($this->servicesType))
		{
			throw new Zend_Exception('Не установлены ли необходимые для заказа параметры: orderPersons или servicesType');
		}
	}
	
	/**
	 * Проверка на предмет, установлен ли тип услуги.
	 * Проверяется при удалении заказов услуг, получении информации по услугам. 
	 * @return 
	 */
	protected function checkServicesType()
	{
		if (!isset($this->servicesType))
		{
			throw new Zend_Exception('Не установлен тип услуги servicesType');
		}
	}
	
	/**
	 * Установка таблицы для работы с услугой в зависимости от типа услуги
	 * @return 
	 */
	protected function setServiceTable()
	{
		switch($this->servicesType)
		{
			case 'hotel' : 
				$this->serviceTable = new Welt_DbTable_HotelOrders();
			break;
			case 'transport' : 
				$this->serviceTable = new Welt_DbTable_TransportOrders();
			break;
			case 'excursion' : 
				$this->serviceTable = new Welt_DbTable_ExcursionOrders();
			break;
			case 'meal' : 
				$this->serviceTable = new Welt_DbTable_MealOrders();
			break;
			case 'reg_fee' :
				$this->serviceTable = new Welt_DbTable_RegFeeOrders();
			break;
			case 'visa' :
				$this->serviceTable = new Welt_DbTable_VisaOrders();
			break;
			case 'other' :
				$this->serviceTable = new Welt_DbTable_OtherOrders();
			break;
		}
	}

	/**
	 * Установка конкретной услуги
	 * @param int $serviceId
	 * @return 
	 */
	public function setService($serviceId)
	{
		$this->serviceId = $serviceId;
	}
	/**
	 * Получение установленной услуги
	 * @return 
	 */
	public function getService()
	{
		return $this->serviceId;
	}
	
	/**
	 * Установка выбранного типа услуги
	 * @param string $serviceSysName
	 * @return 
	 */
	public function setServiceType($serviceSysName)
	{
		//Проверяем, корректный ли задан тип услуги
		if (in_array($serviceSysName,$this->servicesTypes))
		{
			$this->servicesType = $serviceSysName;
		}
		else
		{
			throw new Zend_Exception('Установлен не корректный тип услуги servicesType');
		}
	}
	/**
	 * Получение установленного типа услуги
	 * @return 
	 */
	public function getServiceType()
	{
		return $this->servicesType;
	}
	
	/**
	 * Установка персон для заказа услуги
	 * @param int $orderPersons
	 * @return 
	 */
	public function setPersons($orderPersons)
	{
		$this->orderPersons = $orderPersons;
	} 
	/**
	 * Получение установленных персон
	 * @return 
	 */
	public function getPersons()
	{
		return $this->orderPersons;
	} 
	
	
	
	/**
	 * Заказ услуги Визовая поддержка
	 * @param mixed $serviceData [optional]
	 * @return bool
	 */
	public function addVisaOrder($serviceData='')
	{
		//Проверка, установлены ли необходимые для заказа параметры
		$this->checkOrderReady();
		//Устанавливаем тип заказываемой услуги
		$this->setServiceType('visa');
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		/* 
		 * В зависимости от типа визы, определяем её стоимость
		*/
		$visaTypesTable = new Welt_DbTable_VisaTypes();
		$data['summ']  = $visaTypesTable->getVisaTypePrice($serviceData['visaTypeId']);
		//Добавляем заказ услуги Визовая поддержка
		$data['application_id'] = $this->appId;
		$data['visa_type_id'] = $serviceData['visaTypeId'];
		//Статус услуги
		if (isset($serviceData) && isset($serviceData['status']))
		{
			$data['status'] = $serviceData['status'];
		}
		if (!$this->serviceTable->insert($data))
		{
			return false;
		}
		//Получаем id заказанной услуги
		$curServiceId = $this->serviceTable->getAdapter()->lastInsertId();
		//Добавляем привязку заказанной услуги к персоне
		if (!$this->addServiceToPersons($curServiceId,$this->orderPersons))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Заказ услуги регистрационный сбор
	 * @param mixed $serviceData
	 * @return bool
	 */
	public function addRegFeeOrder($serviceData)
	{
		//Проверка, установлены ли необходимые для заказа параметры
		$this->checkOrderReady();
		//Устанавливаем тип заказываемой услуги
		$this->setServiceType('reg_fee');
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		/* 
		 * В зависимости от типа персоны (участник/сопровождающее лицо/..),
		 * определяем стоимость регистрационного сбора
		*/
		$regFeePricesTable = new Welt_DbTable_RegFeePrices();
		$regFeePrice = $regFeePricesTable->getPriceByPersonType($serviceData['person_type_id']);
		//Устанавливаем параметры для заказанной услуги и сохраняем заказ в БД
		$data['application_id'] = $this->appId;
		$data['reg_fee_type_id'] = $regFeePrice ['reg_fee_type_id'];
		$data['summ'] = $regFeePrice['price'];
		//Статус услуги
		if (isset($serviceData['status']))
		{
			$data['status'] = $serviceData['status'];
		}
		if (!$this->serviceTable->insert($data))
		{
			return false;
		}
		//Получаем id заказанной услуги
		$curServiceId = $this->serviceTable->getAdapter()->lastInsertId();
		//Добавляем привязку заказанной услуги к персоне
		if (!$this->addServiceToPersons($curServiceId,$this->orderPersons))
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Заказ услуги Проживание
	 * @param mixed $serviceData
	 * @return mixed
	 */
	public function addHotelOrder($serviceData)
	{
		//Проверка, установлены ли необходимые для заказа параметры
		$this->checkOrderReady();
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		$data = array();
		//Добавляем заказ услуги Проживание
		$data['application_id'] = $this->appId;
		$data['hotel_id'] = $serviceData['hotelId'];
		$data['room_id'] = $serviceData['roomId'];
		$data['accommodation'] = $serviceData['accommodation'];
		$data['breakfast'] = $serviceData['breakfast'];
		$data['extra_bed'] = $serviceData['extraBed'];
		$data['bed_type'] = $serviceData['bedType'];
		$data['start_date'] = Welt_Functions::dateToMysql($serviceData['startDate']);
		$data['end_date'] = Welt_Functions::dateToMysql($serviceData['endDate']);
		//Если требуется завтрак и он не включён, то добавляем его к стоимости
		if ($serviceData['breakfast']==1 && $serviceData['breakfastIncl']!=1)
		{
			$breakfastSumm  = sizeof($this->orderPersons) * $serviceData['breakfastPrice'];
			$serviceData['summ'] = $serviceData['summ'] + $breakfastSumm;
		}
		$data['summ'] = $serviceData['summ'];
		$data['comment'] = Welt_Functions::filterComment($serviceData['comment']);
		if (!$this->serviceTable->insert($data))
		{
			return false;
		}
		//Получаем id заказанной услуги
		$curServiceId = $this->serviceTable->getAdapter()->lastInsertId();
		//Добавляем привязку заказанной услуги к персонам
		$this->addServiceToPersons($curServiceId,$this->orderPersons);
		//Формируем результат с параметрами добавленной услуги
		$result = array('id'=>$curServiceId);
		return $result;
	}
	
	/**
	 * Заказ услуги Транспорт
	 * @param mixed $serviceData
	 * @return mixed
	 */
	public function addTransportOrder($serviceData)
	{
		//Проверка, установлены ли необходимые для заказа параметры
		$this->checkOrderReady();
		//Устанавливаем тип заказываемой услуги
		$this->setServiceType('transport');
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		$data = array();
		if ($serviceData['isFree'] != 1)
		{
			//Определяем стоимость заказа, если услуга платная
			$transportPricesTable = new Welt_DbTable_TransportPrices();
			$data['summ'] = $transportPricesTable->getCurPrice($serviceData['transpServiceId'],$serviceData['transpTypeId']);
			//Для Спб в ночное время увеличиваем стоимость на 1.5
			if (isset($serviceData['cityId']) && $serviceData['cityId'] == Welt_DbTable_Cities::SPB_ID)
			{
				//Получаем даты действия ночного коэффициента
				$orderDate = new Zend_Date($serviceData['date'] . ' ' .$serviceData['time'], 'dd.MM.yyyy HH:mm');
				$specRateStart = new Zend_Date(
					$serviceData['date'] . ' ' . Welt_DbTable_TransportPrices::$spbPricesRatio['sTime'],
					'dd.MM.yyyy HH:mm'
				);
				$specRateEnd = new Zend_Date(
					$serviceData['date'] . ' ' . Welt_DbTable_TransportPrices::$spbPricesRatio['eTime'],
					'dd.MM.yyyy HH:mm'
				);
				//Если заказ попадает на время действия ночного коэффициента, то увеличиваем стоимость
				if ($orderDate->isEarlier($specRateStart) || $orderDate->equals($specRateStart) || $orderDate->isLater($specRateEnd) || $orderDate->equals($specRateEnd))
				{
					$data['summ'] *= Welt_DbTable_TransportPrices::$spbPricesRatio['ratio'];
				}
			}
			//Назначаем для услуги переданный тип транспорта
			$data['transp_type_id'] = $serviceData['transpTypeId'];
		}
		else
		{
			//Для бесплатной услуги стоимость 0
			$data['summ'] = 0;
			//Для бесплатной услуги устанавливаем определённый вид тип транспорта
			$data['transp_type_id'] = Welt_DbTable_TransportTypes::freeTransportTypeId;
		}
		//Добавляем заказ услуги Транспорт
		$data['application_id'] = $this->appId;
		$data['transp_service_id'] = $serviceData['transpServiceId'];
		$data['date'] = Welt_Functions::dateToMysql($serviceData['date']);
		$data['time'] = $serviceData['time'];
		if (isset($serviceData['stationId']))
		{
			$data['station_id'] = $serviceData['stationId'];
		}
		$data['number'] = $serviceData['number'];
		$data['train_car'] = $serviceData['train_car'];
		$data['comment'] = Welt_Functions::filterComment($serviceData['comment']);
		//Статус услуги
		if (isset($serviceData['status']))
		{
			$data['status'] = $serviceData['status'];
		}
		if(!$this->serviceTable->insert($data))
		{
			return false;
		}
		//Получаем id заказанной услуги
		$curServiceId = $this->serviceTable->getAdapter()->lastInsertId();
		//Добавляем привязку заказанной услуги к персонам
		$this->addServiceToPersons($curServiceId,$this->orderPersons);
		return array('id'=>$curServiceId,'summ'=>$data['summ']);
	}
	
	/**
	 * Заказ услуги Экскурсия
	 * @param object $serviceData
	 * @return mixed
	 */
	public function addExcursionOrder($serviceData)
	{
		//Проверка, установлены ли необходимые для заказа параметры
		$this->checkOrderReady();
		//Устанавливаем тип заказываемой услуги
		$this->setServiceType('excursion');
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		$data = array();
		//Определяем стоимость заказа
		$excursionsTable = new Welt_DbTable_Excursions();
		$excursionInfo = $excursionsTable->getInfo($serviceData);
		$personsCount = sizeof($this->orderPersons);
		$data['comment'] = '';
		//Если экскурсия с размещением и передан тип размещения
		if ($excursionInfo['with_acc'] == 1 && $serviceData['withAcc'] == 1 && isset($serviceData['accommodation']))
		{
			$data['comment'] = 'Тип размещения: ';
			//Считаем стоимость
			if ($personsCount == 1)
			{
				$price = $excursionInfo['price1'];
			}
			elseif ($personsCount == 2)
			{
				$price = $excursionInfo['price2'];
			}
			elseif ($personsCount == 3)
			{
				$price = $excursionInfo['price3'];
			}
			//Добавляем в комментарий текст о выбранном типе размещения
			if ($serviceData['accommodation'] == 1)
			{
				$data['comment'] .= 'Одноместное';
			}
			elseif ($serviceData['accommodation'] == 2)
			{
				$data['comment'] .= 'Двухместное, тип кровати: ' . $serviceData['bedType'];
			}
			elseif ($serviceData['accommodation'] == 3)
			{
				$data['comment'] .= 'Двухместное с дополнительной кроватью, тип кровати: ' . $serviceData['bedType'];
			}
			$data['comment'] .= '.<br>';
		}
		//Если экскурсия без размещения
		else
		{
			$price = $excursionInfo['price1'];
		}
		//Умножаем стоимость на количество персон
		$data['summ'] = $price * $personsCount;
		//Получаем даты и время для выбранного периода
		$excursionPeriodsTable = new Welt_DbTable_ExcursionPeriods();
		$periodDetails = $excursionPeriodsTable->getPeriodDetails($serviceData['periodId']);
		$data['start_date']= Welt_Functions::dateToMysql($periodDetails['start_date']);
		$data['end_date'] = Welt_Functions::dateToMysql($periodDetails['end_date']);
		$data['start_time'] = $periodDetails['start_time'];
		$data['end_time'] = $periodDetails['end_time'];
		//Остальные параметры
		$data['excursion_id'] = $serviceData['excursionId'];
		$data['application_id'] = $this->appId;
		$data['comment'] .= Welt_Functions::filterComment($serviceData['comment']);
		//Статус услуги
		if (isset($serviceData['status']))
		{
			$data['status'] = $serviceData['status'];
		}
		//Добавляем заказ услуги	
		if(!$this->serviceTable->insert($data))
		{
			return false;
		}
		//Получаем id заказанной услуги
		$curServiceId = $this->serviceTable->getAdapter()->lastInsertId();
		//Добавляем привязку заказанной услуги к персонам
		$this->addServiceToPersons($curServiceId,$this->orderPersons);
		return array(
			'id'			=>	$curServiceId,
			'summ'			=>	$data['summ'],
			'start_date'	=>	$periodDetails['start_date'],
			'end_date'		=>	$periodDetails['end_date'],
			'start_time'	=>	$periodDetails['start_time'],
			'end_time'		=>	$periodDetails['end_time']
		);
	}
	
	/**
	 * Заказ услуги банкет
	 * @param mixed $serviceData
	 * @return bool
	 */
	public function addMealOrder($serviceData)
	{
		//Устанавливаем тип заказываемой услуги
		$this->setServiceType('meal');
		//Проверка, установлены ли необходимые для заказа параметры
		$this->checkOrderReady();
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		$data = array();
		$data['application_id'] = $this->appId;
		$data['comment'] = Welt_Functions::filterComment($serviceData['comment']);
		$data['meal_id'] = $serviceData['mealId'];
		//Получаем данные о выбранном банкете
		$mealsTable = new Welt_DbTable_Meals();
		$mealDetails = $mealsTable->find($serviceData['mealId'])->toArray();
		$mealDetails = $mealDetails[0];
		//Получаем стоимость банкета, если он не включён в регистрационный сбор
		if ($mealDetails['incl_reg_fee']!=1)
		{
			$personsCount = sizeof($this->orderPersons);
			$data['summ'] = $mealDetails['price'] * $personsCount;
		}
		//Выбранный тип пищи
		if (isset($serviceData['mealTypeId']))
		{
			$data['meal_type_id'] = $serviceData['mealTypeId'];
		}
		//Статус услуги
		if (isset($serviceData['status']))
		{
			$data['status'] = $serviceData['status'];
		}
		//Сохраняем услугу
		if(!$this->serviceTable->insert($data))
		{
			return false;
		}
		//Получаем id заказанной услуги
		$curServiceId = $this->serviceTable->getAdapter()->lastInsertId();
		//Добавляем привязку заказанной услуги к персонам
		$this->addServiceToPersons($curServiceId,$this->orderPersons);
		return array(
			'id'				=>	$curServiceId,
			'summ'				=>	$data['summ'],
			'date'				=>	Welt_Functions::dateFromMysql($mealDetails['date']),
			'with_meal_types'	=>	$mealDetails['with_meal_types'],
			'incl_reg_fee'		=>	$mealDetails['incl_reg_fee']
		);
	}
	
	/**
	 * Добавление в заказ для заданных персон всех включённых в рег. сбор банкеты
	 * @return 
	 */
	public function addIncludedMealsToPersons()
	{
		//Получаем список всех банкетов, которые включены в регистрационный сбор
		$mealsTable = new Welt_DbTable_Meals();
		$incRegFeeMeals = $mealsTable->getIncludedMeals();
		//Проходимся по найденым банкетам
		$mealTypesTable = new Welt_DbTable_MealTypes();
		$serviceData = array();
		$persons = $this->orderPersons;
		foreach ($incRegFeeMeals as $incRegFeeMeal)
		{
			//Помечаем услугу, как добавленную из ЛК
			$serviceData['status'] = 1;
			//Формируем массив данных заказа на банкет
			$serviceData['mealId'] = $incRegFeeMeal['id'];
			$serviceData['inclRegFee'] = 1;
			//Если предусмотрен выбор типа пищи, получаем тип пищи
			if ($incRegFeeMeal['with_meal_types']==1)
			{
				$serviceData['mealTypeId'] = $mealTypesTable->find($incRegFeeMeal['id'])->current()->id;
			}
			//Добавляем заказ банкета для каждой из установленных персон
			foreach($persons as $person)
			{
				$this->setPersons(array(0=>$person));
				$this->addMealOrder($serviceData);
			}
		}
	}
    
    /**
     * Заказ услуги типа "Другое"
     * @param mixed $serviceData
     * @return bool
     */
    public function addOtherOrder($serviceData)
    {
        //Устанавливаем тип заказываемой услуги
        $this->setServiceType('other');
        //Проверка, установлены ли необходимые для заказа параметры
        $this->checkOrderReady();
        //Определяем таблицу для работы с услугой в зависисмости от типа услуги
        $this->setServiceTable();
        //Получаем данные по услуге
        $servsTable = new Welt_DbTable_OtherServices();
        $service = $servsTable->getService($serviceData['serviceId']);
        $data = array();
        $data['name'] = $service['name'];
        $data['name_eng'] = $service['name_eng'];
        $data['date'] = Welt_Functions::dateToMysql($service['date']);
        $personsCount = sizeof($this->orderPersons);
        $data['summ'] = $service['price'] * $personsCount;
        $data['application_id'] = $this->appId;
        $data['comment'] = Welt_Functions::filterComment($serviceData['comment']);
        //Статус услуги
        if (isset($serviceData['status']))
        {
            $data['status'] = $serviceData['status'];
        }
        //Сохраняем услугу
        if(!$this->serviceTable->insert($data))
        {
            return false;
        }
        //Получаем id заказанной услуги
        $curServiceId = $this->serviceTable->getAdapter()->lastInsertId();
        //Добавляем привязку заказанной услуги к персонам
        $this->addServiceToPersons($curServiceId,$this->orderPersons);
        return array(
            'id'                =>  $curServiceId,
            'name'              =>  $service['nameLng'],
            'summ'              =>  $data['summ'],
            'date'              =>  $service['date'],
        );
    }
	
	
	/**
	 * Удаление заказа на услугу
	 * @param int $serviceId
	 * @param int $delType [optional] Тип удаления. 0 - со всем прикреплёнными персонами. 1 - только сама услуга
	 * @return 
	 */
	public function deleteServiceOrder($serviceId,$delType=0)
	{
		//Проверка на установленные параметры
		$this->checkServicesType();
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		//Удаляем услугу
		$where[] = $this->serviceTable->getAdapter()->quoteInto('id = ?', $serviceId);
		$where[] = $this->serviceTable->getAdapter()->quoteInto('application_id = ?', $this->appId);
		if ($this->serviceTable->delete($where))
		{
			if ($delType!=1)
			{
				//Удаляем связи с данной услугой для персон
				return $this->deletePersonsService($serviceId);
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}	
	}
	
	/**
	 * Удаление всех услуг заданного типа в рамках заявки
	 * @return 
	 */
	public function deleteAllServicesByType()
	{
		//Проверка на установленные параметры
		$this->checkServicesType();
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		//Получаем все услуги, которые необходимо удалить и удаляем их
		$servicesToDel = $this->serviceTable->getUserOrdersIds($this->appId);
		if(is_array($servicesToDel) && sizeof($servicesToDel)>0)
		{
			foreach($servicesToDel as $service)
			{
				if(!$this->deleteServiceOrder($service['id']))
				{
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Получение информации заказанным участником услугам в рамках данной заявки для заданного ранее типа услуги
	 * @param mixed $serviceStatuses [optional] Масив статусов услуг для отбора
	 * @param mixed $options [optional] Массив дополнительных параметров
	 * @return 
	 */
	public function getUserServiceOrders($serviceStatuses=array(),$options=array())
	{
		//Проверка на установленные параметры
		$this->checkServicesType();
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		switch($this->servicesType)
		{
			case 'hotel' : 
				$orders = $this->serviceTable->getUserHotelOrders($this->appId,$serviceStatuses,$options);
			break;
			case 'transport' : 
				$orders = $this->serviceTable->getUserTransportOrders($this->appId,$serviceStatuses,$options);
			break;
			case 'excursion' : 
				$orders = $this->serviceTable->getUserExcursionOrders($this->appId,$serviceStatuses,$options);
			break;
			case 'meal' : 
				$orders = $this->serviceTable->getUserMealOrders($this->appId,$serviceStatuses,$options);
			break;
			case 'reg_fee' :
				$orders = $this->serviceTable->getUserRegFeeOrders($this->appId,$serviceStatuses,$options);
			break;
			case 'visa' :
				$orders = $this->serviceTable->getUserVisaOrders($this->appId,$serviceStatuses,$options);
			break;
			case 'other' :
				$orders = $this->serviceTable->getUserOtherOrders($this->appId,$serviceStatuses,$options);
			break;
		}
		//Получаем список персон для каждой из заказанных услуг
		$ordersSize = sizeof($orders);
		for ($i=0;$i<$ordersSize;$i++)
		{
			$orders[$i]['persons'] = $this->getServicePersons($orders[$i]['id']);
		}
		return $orders;
	}
	
	
	/**
	 * Получение информации по всем заказам в рамках заданной заявки
	 * @param int $type [optional] Определяет формат результирующего массива 
	 * 0 - в формате array([service_sys_name]=>array(0=>service1))
	 * 1 - в формате array(0=>array(service_sys_name,service_name,oders=>array()) - с названиями типов услуг
	 * @param mixed $serviceStatuses [optional] Массив статусов для отбора заказанных услуг
	 * @param mixed $options [optional] Массив дополнительных параметров
	 * @return mixed
	 */
	public function getAllUserServicesOrders($type=0,$serviceStatuses=array(),$options=array())
	{
		$orders = array();
		//Проходимся по всем доступным типам услуг и получаем заказы участника по каждой
		//Список всех заказанных услуг с группировкой по типу
		if ($type==1)
		{
			//Получаем все типы услуг
			$servicesTypesTable = new Welt_DbTable_ServicesTypes();
			$servicesTypes = $servicesTypesTable->getAllServiceTypes();
			$servicesTypesSize = sizeof($servicesTypes);
			$j=0;
			//Получаем заказы по всем типам услуг
			for($i=0;$i<$servicesTypesSize;$i++)
			{
				$this->setServiceType($servicesTypes[$i]['sys_name']);
				$curServiceOrders = $this->getUserServiceOrders($serviceStatuses,$options);
				if (is_array($curServiceOrders) && sizeof($curServiceOrders>0))
				{
					//Помещаем все заказы в общий массив и добавляем информацию об услуге
					foreach($curServiceOrders as $curServiceOrder)
					{
						$orders[$j] = $curServiceOrder;
						$orders[$j]['service_sys_name'] = $servicesTypes[$i]['sys_name'];
						$orders[$j]['service_name'] = $servicesTypes[$i]['name'];
						//Описание услуги в зависмости от типа
						$orders[$j]['caption'] = $this->generateServiceCapt($servicesTypes[$i]['sys_name'],$orders[$j]);
						/*
						 * Ставим для каждой услуги флаги, определяющие: возможно ли наличие подтверждения по данной услуге, 
						 * возможен ли просмотр деталей и редактирование (на основе типа услуги)
						 */
						$orders[$j]['specParams'] = $this->getServiceSpecParams($servicesTypes[$i]['sys_name'],$orders[$j]);
						$j++;
					}
				}
			}
		}
		//Для вывода таблицы по всем услугам без группировки по типу
		elseif ($type==0)
		{
			foreach ($this->servicesTypes as $serviceType)
			{
				$this->setServiceType($serviceType);
				$orders[$serviceType] = $this->getUserServiceOrders($serviceStatuses,$options);
			}
		}
		return $orders;
	}
	
	
	/**
	 * Генерация описания для заказанной услуги в зависмости от типа и на основе переданных параметров заказа
	 * @param string $serviceType
	 * @param mixed $serviceData
	 * @return 
	 */
	public function generateServiceCapt($serviceType,$serviceOrder)
	{
		$translator = Zend_Registry::get('Zend_Translate');
		$caption = '';
		switch($serviceType)
		{
			case 'excursion' : $caption = $translator->translate('Cultural program').' : '.$serviceOrder['name'];
			break;
			case 'hotel' :
				$caption = $translator->translate('Accommodation in hotel').' '.$serviceOrder['hotelName'];
				if ($serviceOrder['is_extra_bed'] == 1)
				{
					$caption .= ' ('.$translator->translate('extra bed').')';
				}
			break;
			case 'meal' : $caption = $serviceOrder['mealName'];
			break;
			case 'reg_fee' : 
				$caption = $serviceOrder['service_name'];
			break;
			case 'visa' : 
				$caption = $serviceOrder['service_name'].' ('.$serviceOrder['visaTypeName'].')';
			break;
			case 'transport' : $caption = $serviceOrder['transpServiceName'];
			break;
			case 'other' : $caption = $serviceOrder['name'];
			break;
		}
		return $caption;
	}
	
	/**
	 * Получение массива параметров, характерных для указанного типа услуги в зависимости от параметров заказа
	 * @param string $serviceType
	 * @param mixed $serviceData
	 * @return 
	 */
	public function getServiceSpecParams($serviceType,$serviceOrder)
	{
		$params = array();
		//Устанавливаем параметры в зависимости от типа и деталей заказа
		switch($serviceType)
		{
			case 'excursion' : 
				$params = array('canBeFree'=>0,'haveDetails'=>1,'haveConfirmation'=>1,'canBeEdit'=>1,'canBeCancel'=>1);
			break;
			case 'hotel' :
				$params = array('canBeFree'=>0,'haveDetails'=>1,'haveConfirmation'=>1,'canBeEdit'=>1,'canBeCancel'=>1);
				//Если заказ - доп. кровать
				if ($serviceOrder['is_extra_bed'] == 1)
				{
					$params['haveDetails'] = 0;
					$params['canBeEdit'] = 0;
				}
			break;
			case 'meal' : 
				$params = array('canBeFree'=>0,'haveDetails'=>1,'haveConfirmation'=>0,'canBeEdit'=>1,'canBeCancel'=>1);
				//Если банкет включён в рег. сбор
				if ($serviceOrder['incl_reg_fee'] == 1)
				{
					$params['canBeFree'] = 1; 
					$params['canBeCancel'] = 0;
				}
				//Если доступен выбор типа пищи
				if ($serviceOrder['with_meal_types'] == 1)
				{
					$params['canBeEdit'] = 1; 
				}
			break;
			case 'reg_fee' : 
				$params = array('canBeFree'=>0,'haveDetails'=>0,'haveConfirmation'=>0,'canBeEdit'=>0,'canBeCancel'=>0);
			break;
			case 'visa' : 
				$params = array('canBeFree'=>1,'haveDetails'=>0,'haveConfirmation'=>1,'canBeEdit'=>0,'canBeCancel'=>0);
			break;
			case 'transport' : 
				$params = array('canBeFree'=>1,'haveDetails'=>1,'haveConfirmation'=>1,'canBeEdit'=>1,'canBeCancel'=>1);
			break;
			case 'other' : 
				$params = array('canBeFree'=>1,'haveDetails'=>0,'haveConfirmation'=>1,'canBeEdit'=>0,'canBeCancel'=>0);
			break;
		}
		return $params;
	}
	
	/**
	 * Поиск всех услуг с заданным статусом и изменение его на указанный
	 * @param int $findStatus
	 * @param int $newStatus
	 * @return 
	 */
	public function setAppServicesOrdersStatus($findStatus,$newStatus)
	{
		$this->setServiceTable();
		$this->serviceTable->setAppServicesOrdersStatus($this->appId,$findStatus,$newStatus);
	}
	
	/**
	 * Установка статуса для услуги
	 * @param int $status
	 * @return 
	 */
	public function setServiceStatus($serviceId,$status)
	{
		//Проверка на установленные параметры
		$this->checkServicesType();
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		//Обновляем статус
		$where[] = $this->serviceTable->getAdapter()->quoteInto('id = ?', $serviceId);
		$data = array('status'=>$status);
		return $this->serviceTable->update($data,$where);
	}
	
	
	/**
	 * Завершение процесса добавления услуг из ЛК, изменение статуса добавленных услуг на 1
	 * @return 
	 */
	public function servicesOrderFromPpcComplete()
	{
		//Находим в рамках заявки для каждого типа услуги все услуги, которые имею статус 0 и меняем его на 1
		foreach ($this->servicesTypes as $serviceType)
		{
			$this->setServiceType($serviceType);
			$this->setAppServicesOrdersStatus(0,1);
		}
	}
	
	
	/**
	 * Получение услуг, заказанных только на указанную персону (те, которые можно удалять, если удаляется персона)
	 * @param int $personId
	 * @return 
	 */
	public function getOnlyPersonServicesOrders($personId)
	{
		//Получаем все связи услуг с персонами
		$servicesAndPersons = $this->serviceAndPersonsTable->getAppServicesAndPersons($this->appId);
		//Отбираем услуги, заказанные для конкретной персоны
		$personServices = array();
		$servicesAndPersonsSize = sizeof($servicesAndPersons);
		$j=0;
		for($i=0;$i<$servicesAndPersonsSize;$i++)
		{
			if ($servicesAndPersons[$i]['person_id']==$personId)
			{
				$personServices[$j]['service_id'] = $servicesAndPersons[$i]['service_id']; 
				$personServices[$j]['service_type'] = $servicesAndPersons[$i]['service_sys_name']; 
				$j++;
			}
		}
		//Проверяем, заказана ли данная услуга ещё на какие-то персоны
		$servicesToDelete = array();
		$personServicesSize = sizeof($personServices);
		foreach($servicesAndPersons as $record)
		{
			//Если персона отличная от заданной
			if ($record['person_id']!=$personId)
			{
				//Проходимся по всем заказанным услугам для заданной персоны
				foreach ($personServices as $key=>$value)
				{
					//Если услуга заказана ещё для какой-то персоны, то удаляем её из списка на удаление
					if ($personServices[$key]['service_id']==$record['service_id'])
					{
						unset($personServices[$key]);
					}
				}
			}
		}
		return $personServices;
	}
	
	/**
	 * Получение детальной информации по указанной услуге
	 * @param int $serviceId
	 * @return 
	 */
	public function getServiceDetails($serviceId)
	{
		//Проверка на установленные параметры
		$this->checkServicesType();
		//Определяем таблицу для работы с услугой в зависисмости от типа услуги
		$this->setServiceTable();
		$serviceDetails = $this->serviceTable->getOrderDetails($serviceId);
		return $serviceDetails;
	}
	
	/**
	 * Получения деталей по услуге, нужных для редактирования услуги
	 * @param int $serviceId
	 * @return 
	 */
	public function getServiceDetailsForEdit($serviceId)
	{
		//Получаем детали по услуге
		$serviceDetails = $this->getServiceDetails($serviceId);
		/*
		 * В зависимости от типа услуги, получаем информацию для редактирования
		 */
		switch($this->servicesType)
		{
			case 'excursion' : 
				$excursionPeriodsTable = new Welt_DbTable_ExcursionPeriods();
				$serviceDetails['excursionPeriods'] = $excursionPeriodsTable->getExcursionPeriods($serviceDetails['excursion_id']);
			break;
			case 'meal' : 
				$mealTypesTable = new Welt_DbTable_MealTypes();
				$serviceDetails['mealTypes'] = $mealTypesTable->getMealTypes($serviceDetails['meal_id']);
			break;
			case 'transport' :
				$transportTypesTable = new Welt_DbTable_TransportTypes();
				$serviceDetails['transportTypes'] = $transportTypesTable->getTransportTypes();
			break;	
		}
		return $serviceDetails;
	}
	
	/**
	 * Подача запроса на аннуляцию услуги
	 * @param int $serviceId
	 * @return 
	 */
	public function serviceCancelRequest($serviceId)
	{
		//Проверка на установленные параметры
		$this->checkServicesType();
		//Добавляем запрос на изменение услуги в БД
		$servicesCancellationsTable = new Welt_DbTable_ServicesCancellations();
		$serviceCancelData = array (
			'service_id'		=>	$serviceId,
			'service_sys_name'	=>	$this->servicesType,
			'application_id'	=>	$this->appId,
			'status'			=>	0,
			'created'			=>	Welt_Functions::getDateTimeMysql()
		);
		if(!$servicesCancellationsTable->insert($serviceCancelData))
		{
			return false;
		}
		//Изменяем статус услуги
		return $this->setServiceStatus($serviceId,8);
	}
	
	
	
	/**
	 * Получение персон, для которых заказана услуга с указанного типа с указанным Id
	 * @param int $serviceId
	 * @return 
	 */
	public function getServicePersons($serviceId)
	{
		//Проверка на установленные параметры
		$this->checkServicesType();
		$persons = $this->serviceAndPersonsTable->getServicePersons($serviceId,$this->servicesType);
		return $persons;
	}
	
	
	/**
	 * Рассчёт стоимости для услуги при заданной стоимости и датах начала и окончания периода
	 * @param mixed $data
	 * @return int
	 */
	public function calcAccPrice($data)
	{
		$zendStartDate = new Zend_Date($data['startDate'],'dd.MM.yyyy');
		$zendEndDate = new Zend_Date($data['endDate'],'dd.MM.yyyy');
		$zendStartDateUnix = (int) $zendStartDate->toString('U');
		$zendEndDateUnix = (int) $zendEndDate->toString('U');
		$daysDiff = floor(($zendEndDateUnix-$zendStartDateUnix)/86400);
		$summ = $daysDiff*$data['price'];
		return $summ;
	}
	
	/**
	 * Проверка, есть ли заказы услуг (всех  типов или заданного типа) на указанную персону
	 * @param int $personId
	 * @param string $serviceType
	 * @return 
	 */
	public static function checkPersonServicesOrders($personId,$serviceType='all')
	{
		$serviceAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
		return $serviceAndPersonsTable->checkPersonServicesOrders($personId,$serviceType);
	}
	
	/**
	 * Получение списка заявок, в которых есть изменённые услуги
	 * @param string $sDate
	 * @param string $eDate
	 * @return 
	 */
	public function geUpdatedApps($sDate, $eDate)
	{
		$apps = array();
		foreach ($this->servicesTypes as $serviceType)
		{
			if ($serviceType != 'other')
			{
				$this->servicesType = $serviceType;
				$this->setServiceTable();
				$apps = array_merge($apps, $this->serviceTable->geUpdatedApps($sDate, $eDate));
			}
		}
		return $apps;
	}
}
