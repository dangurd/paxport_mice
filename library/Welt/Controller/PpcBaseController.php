<?php
/**
 * Базовый котроллер для модуля Ppc
 */
class Welt_Controller_PpcBaseController extends Zend_Controller_Action
{
	protected $sessionData;
	
	protected $settings;
	
	protected $eventParams;
	
	protected $appId;
	
	protected $userId;
	
	protected $appObj;
	
	protected $servicesObj;
	
    public function init()
    {
    	//Получаем данные из сессии
		$auth = Zend_Auth::getInstance();
		$auth->setStorage(new Zend_Auth_Storage_Session('ppc'));
		$this->sessionData = $auth->getIdentity();
		$this->appId = $this->sessionData['appId'];
		$this->userId = $this->sessionData['userId'];
		//Создаём объекты классов для работы с заявками и услугами
		$this->appObj = new Welt_Applications($this->appId,$this->userId);
		$this->servicesObj = new Welt_Services($this->appId,$this->userId);
		//Получаем системные настройки
		$this->settings = Zend_Registry::get('settings');
		$this->view->settings = $this->settings;
		$this->eventParams = Zend_Registry::get('eventParams');
		$this->view->eventParams = $this->eventParams;
		$this->view->lang = Zend_Registry::get('lang');
		//Флаг отображения визы
		$servicesTypes = $this->servicesObj->getServicesTypes();
		if (in_array('visa',$servicesTypes) && ($this->view->lang == 'en' || $this->settings->show_visa == 1))
		{
			$this->view->visaDisabled = FALSE;
		}
		else
		{
			$this->view->visaDisabled = TRUE;
		}
        //Передаём во view флаг, определяющий, доступна ли смена языка
        if ($this->settings->system->lang->switch->ppc == 1 && $this->eventParams['multilang'] == 1)
        {
            $this->view->langSw = TRUE;
        }
        else
        {
            $this->view->langSw = FALSE;    
        }
    }
	
}
