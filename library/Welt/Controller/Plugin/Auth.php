<?php
class Welt_Controller_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
 
 	/**
 	 * @var array $ppcNoAuth Определение перехода для незалогиненных участников (ЛК участника)
 	 */
 	private static $ppcNoAuth = array(
		'module'	 => 'ppc',
 		'controller' => 'user',
 		'action'     => 'login'
		);
		
	/**
 	 * @var array $ppcIndex Определение перехода для залогиненных участников при 
 	 * попытке доступа к страницам авторизации и восстановления пароля (ЛК участника)
 	 */
	private static $ppcIndex = array(
		'module'	 => 'ppc',
 		'controller' => 'index',
 		'action'     => 'index'
		); 	
	 

	/**
	 * @var mixed $acl Привелегии пользователей 
	 */
 	protected $acl;
	
	public function __construct($acl)
	{
		$this->acl=$acl;
	}
 	/**
 	 * Проверяем залогинненость пользователя, если не залогинен, то отправляем на на страницу авторизации
 	 */
 	public function preDispatch(Zend_Controller_Request_Abstract $request)
 	{
		//Если не было брошено исключений
 		if (!$this->_response->isException())
		{
	 	 	// Определяем параметры запроса
			$module		 = $request->module;
	   		$controller  = $request->controller;
			$action      = $request->action;		
			//Параметры для редиректа
			$redirectParams = array();
			//Если модуль - регистрационная форма
			if ($module=='default')
			{
				$settings = Zend_Registry::get('settings');
				//Если установлен режим предварительной регистрации
				if ($settings->reg->type == 1)
				{
					//Проверяем, создана ли сессия, если нет, то редирект на шаг авторизации
					$this->userAppNamespace = new Zend_Session_Namespace('user');
					if (!isset($this->userAppNamespace->appId) || !isset($this->userAppNamespace->userId))
					{
						$userRole = 'guest';
					}
					else
					{
						$userRole = 'participant';
					}
					$redirectParams = self::$ppcNoAuth;
				}
				//Если регистрация открытая
				else
				{
					return true;
				}
				
			}
			//Если модуль - ЛК Участника
			elseif ($module=='ppc')
			{
				/*
				 * Если переданы email и пароль в get запросе (для доступа менеджеров в ЛК из ТМ, то очищаем данные сессии
				 */
				if ($controller == 'user' && $action == 'login' && $request->isGet() && $request->getParam('email')!='' && $request->getParam('password')!='')
				{
					$authObj = new Auth();
					$authObj->logout();
				}
				//Проверяем, залогинился ли участник
				$auth = Zend_Auth::getInstance();
				$auth->setStorage(new Zend_Auth_Storage_Session('ppc'));
				if ($auth->hasIdentity())
				{
					$userRole = 'participant';
				}
				else
				{
					$userRole = 'guest';
				}
				$redirectParams = self::$ppcNoAuth;
			}
			$resource = $module.':'.$controller;
			//Если ресурс существует
			if ($this->acl->has($resource)) 
			{
				//Проверка прав доступа
				if (!$this->acl->isAllowed($userRole,$resource,$action))
				{
					/*
					 * Если зарегистрированный пользователь, то закрываем от него
					 * страницу авторизации и восстановления пароля (ЛК участника)
					*/
					if ($userRole=='participant')
					{
						$redirectParams = self::$ppcIndex;
					}
					//Перенаправление
					$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
					$redirector->gotoSimple($redirectParams['action'],$redirectParams['controller'],$redirectParams['module']);
				}
			}
		}
 	}
	
}