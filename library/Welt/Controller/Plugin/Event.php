<?php
class Welt_Controller_Plugin_Event extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$serverParams = $this->_request->getServer();
		//Если не было брошено исключений
 		if (!$this->_response->isException()) // && !preg_match('/^192\.168\.(2|200)\.[0-9]{2,3}$/si', $serverParams['REMOTE_ADDR'])
		{
	 	 	// Определяем параметры запроса
			$module		 = $request->module;
	   		$controller  = $request->controller;
			$action      = $request->action;
			$eventParams = Zend_Registry::get('eventParams');
			if (!($module=='default' && $controller=='index' && $action=='deadline') && !($module=='default' && $controller=='kcaptcha'))
			{
				$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
				$curDate = new Zend_Date();
				//Если модуль - рег. форма и регистрация окончена, то редирект на специальную страницу
				if ($module == 'default' && $eventParams['regFinished'])
				{
					$redirector->gotoSimple('deadline','index','default');
				}
				//Если модуль - ЛК и доступ к ЛК уже закрыт, то редирект на специальную страницу
				elseif ($module == 'ppc' && $eventParams['ppcClosed'])
				{
					$redirector->gotoSimple('deadline','index','default');
				}
			}
		}
		//Добавляем для ie специальный заголовок, чтобы форма корректно работала в iframe
		if ($request->module == 'default')
		{	
			$this->getResponse()->setHeader('P3P','CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
			$this->getResponse()->setHeader('P3P', 'CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
		}
	}
}			