<?
/**
 * Плагин для обеспечения мультиязычности
 */
class Welt_Controller_Plugin_Lang extends Zend_Controller_Plugin_Abstract
{
	/**
	 * @var Пространство имён сессии
	 */
	protected $appNamespace;
	/**
	 * @var Настройки системы
	 */
	protected $settings;
	
	public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
    	//Определяем текущий url
    	$serverParams = $request->getServer();
		$curUrl = $serverParams['REQUEST_URI'];
		//Простаранство имён в зависимости от модуля
		if (strpos($curUrl,'ppc'))
		{
			$this->appNamespace = new Zend_Session_Namespace('ppc');
		}
		else
		{
			$this->appNamespace = new Zend_Session_Namespace('user');
		}
		$eventParams= Zend_Registry::get('eventParams');
		if (preg_match('/^.*\/syslang\/(ru|en)\/.*$/',$curUrl,$matches))
		{
			//Параметр, определяющий, идёт ли смена языка
			$sysLang = $matches[1];
		}
		$langSwitch = 0;
		//Если в url передан параметр syslang, то меняем язык системы
		if (isset($sysLang) && $sysLang!='')
		{
			$langSwitch = 1;
			$lang = $sysLang;
			$this->appNamespace->lang = $sysLang;
			/*
			 * Если пользователь изменил язык по умолчанию, то ставим cookie на год для того, чтобы язык сохранился 
			 * при дальнейшей работе пользователя с системой
			 */
			setcookie('lang',$sysLang,time()+29030400,'/');
		}
		//Если параметр syslang не передан, проверяем установленных в сессии язык или ставим язык по умолчанию
		else
		{
			//Если запись о языке есть в cookie
			$cookies = $request->getCookie();
			if (isset($cookies['lang']) && $cookies['lang']!='')
			{
				$lang = $cookies['lang'];
				$this->appNamespace->lang = $lang;
			}
			else
			{
				//Язык из сессии
				if (isset($this->appNamespace->lang) && $this->appNamespace->lang!='')
				{
					$lang = $this->appNamespace->lang;
				}
				//Язык по умолчанию
				else
				{
					$lang = $eventParams['def_lang'];
					$this->appNamespace->lang = $lang;
				}
			}
		}
		Zend_Registry::set('lang', $lang);
		/*
		 * Определяем, какие файлы с переводом нужно игнорировать в зависимости от текущего модуля.
		 */
		if (strpos($curUrl,'ppc'))
		{
			$ignore = array('default');
		}
		else
		{
			$ignore = array('ppc');
		}
		//Создаём объект Zend_Translate
		$translator = new Zend_Translate (
			array (
				'adapter' 	=> 	'array',
				'content' 	=> 	APPLICATION_PATH.'/languages/',
				'locale'  	=> 	$lang,
				'scan' 		=>	Zend_Translate::LOCALE_DIRECTORY,
				'ignore'	=>	$ignore
			)
		);
		// Устанавливаем локаль в объект Zend_Translate
		Zend_Locale::setDefault($lang);
		$locale = new Zend_Locale($lang);
        $translator->setLocale($lang);
		//Устанваливаем перевод сообщений для Zend_Form и валидаторов
		Zend_Form::setDefaultTranslator($translator);
		Zend_Validate_Abstract::setDefaultTranslator($translator);
        //Сохраняем в реестре объекты
        Zend_Registry::set('Zend_Locale', $locale);
        Zend_Registry::set('Zend_Translate', $translator);
		//Устанавливаем tranlator для помощника headTitle
		$layout = Zend_Layout::getMvcInstance();
		$view = $layout->getView();
		$view->headTitle()->enableTranslation();
		$view->headTitle()->setTranslator($translator);
		/*
		 * Если идёт смена языка, то редирект на тот же раздел системы, но без параметра syslang
		 */
		if ($langSwitch==1)
		{
			$curUrl = str_replace('/syslang/'.$lang.'/','',$curUrl);
			$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
			$redirector->gotoUrl($curUrl);
		}
	}
}