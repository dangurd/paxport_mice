<?php
class Welt_Controller_Plugin_EventParams extends Zend_Controller_Plugin_Abstract
{
    
	public function routeStartup(Zend_Controller_Request_Abstract $request)
	{
	    $eventParams = NULL;
		$miceSysName = Zend_Registry::get('settings')->mice->sys_name;
		if (is_null($miceSysName) || $miceSysName == '')
		{
		    throw new Zend_Exception('Please set correct mice sys_name in config');
		}
        $miceOrganizersGw = new Welt_DbTable_ExpoMice_MiceOrganizers();
        $miceData = $miceOrganizersGw->getBySysName($miceSysName);
        if (is_null($miceData))
        {
            throw new Zend_Exception('Mice with sys_name="' . $miceSysName . '" not found');
        }
        $eventParamsGateway = new Welt_DbTable_ExpoMice_EventParams();
        $eventParams = $eventParamsGateway->getEventParams($miceData['id_1c']);
        // Дополняем параметры данными по организатору и прочими настройками
        $eventParams = array_merge($eventParams, $miceData);
        $eventParams = $this->completeEvParams($eventParams);
		// Создаём объект настроек и пишем егов  реестр
		$eventParamsObj = new Welt_Model_EventParams($eventParams);
		Zend_Registry::set('eventParams', $eventParamsObj);
	}


    protected function completeEvParams($eventParams)
    {
        $checkDate = function($date)
        {
            return preg_match('/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/', $date);
        };
		$curDate = new Zend_Date();
        // Даты закрытия доступа к форме ЛК по умолчанию (есди не заданы)
        $evStopDate = new Zend_Date($eventParams['event_edate'], 'dd.MM.yyyy');
        if (is_null($eventParams['form_close']) || $eventParams['form_close'] == '' || !$checkDate($eventParams['form_close']))
        {
        	$formClose = clone $evStopDate;
			$formClose->add(1, Zend_Date::DAY);
            $eventParams['form_close'] = $formClose->toString('dd.MM.yyyy');
        }
		if (is_null($eventParams['lk_close']) || $eventParams['lk_close'] == '' || !$checkDate($eventParams['lk_close']))
        {
        	$ppcCloseDate = clone $evStopDate;
        	$ppcCloseDate->add(1, Zend_Date::MONTH);
            $eventParams['lk_close'] = $ppcCloseDate->toString('dd.MM.yyyy');
        }
        // Флаги, обозначающие, закончилась ли уже регистрация и закрыт ли доступ к ЛК
        $eventParams['regFinished'] = FALSE;
		$eventParams['ppcClosed'] = FALSE;
        $regStopDate = new Zend_Date($eventParams['form_close'], 'dd.MM.yyyy'); 
		$ppcCloseDate = new Zend_Date($eventParams['lk_close'], 'dd.MM.yyyy');            
        if ($curDate >= $regStopDate)
        {
            $eventParams['regFinished'] = TRUE;
        }
		if ($curDate >= $ppcCloseDate)
		{
			$eventParams['ppcClosed'] = TRUE;
		}
        return $eventParams;
    }

}