<?php
/**
 * Плагин, который контрлирует ситуацию, когда в сессии у пользователя остался id заявки, а сама заявка уже удалена
 */
class Welt_Controller_Plugin_CheckAppExist extends Zend_Controller_Plugin_Abstract
{

 	public function preDispatch(Zend_Controller_Request_Abstract $request)
 	{
 		if (!$this->_response->isException())
		{
	 	 	// Определяем параметры запроса
			$module  = $request->module;
	   		$controller  = $request->controller;
			$action      = $request->action;
			$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
			//Создаём объект класса для работы с заявками
			$appObj = new Welt_Applications();
			//Если модуль - Регистрация
			if ($module=='default')
			{
				$userAppNamespace = new Zend_Session_Namespace('user');
				//Если заявка ещё не записана в сессию
				if (!isset($userAppNamespace->appId))
				{
					return true;
				}
				$appObj->setAppId($userAppNamespace->appId);
				//Если заявка не найдена, удаляем информацию в сессии и перенаправляем пользователя на 1 шаг
				if (!$appObj->checkAppExist())
				{
					Zend_Session::namespaceUnset('user');
					$redirector->gotoSimple('index','index','default',array());
				}
			}
			//Если модуль - ЛК Участника
			elseif ($module=='ppc')
			{
				$auth = Zend_Auth::getInstance();
				$auth->setStorage(new Zend_Auth_Storage_Session('ppc'));
				//Если пользователь не залогинен
				if (!$auth->hasIdentity())
				{
					return true;
				}
				$this->sessionData = $auth->getIdentity();
				$appObj->setAppId($this->sessionData['appId']);
				//Если заявка не найдена, удаляем информацию в сессии и перенаправляем пользователя на шаг авторизации
				if (!$appObj->checkAppExist())
				{
					$auth->clearIdentity();
					Zend_Session::namespaceUnset('ppc');
					Zend_Session::forgetMe();
					$redirector->gotoSimple('index','index','ppc');
				}
			}
			
		}
	}
}