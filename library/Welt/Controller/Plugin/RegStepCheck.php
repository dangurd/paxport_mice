<?php
/**
 * Плагин для проверки шага регистрации, на котором находится пользователь на основании данных сессии.
 * Если сессия не создана, то шаг 1.
 */
class Welt_Controller_Plugin_RegStepCheck extends Zend_Controller_Plugin_Abstract
{
	private static $stepsArr = array('index','step1','step2','step3','step4','step5');
 
 	/**
 	 * Проверяем залогинненость пользователя, если не залогинен, то отправляем на главную
 	 */
 	public function preDispatch(Zend_Controller_Request_Abstract $request)
 	{
 		if (!$this->_response->isException())
		{
	 	 	// Определяем параметры запроса
			$module  = $request->module;
	   		$controller  = $request->controller;
			$action      = $request->action;
			$userAppNamespace = new Zend_Session_Namespace('user');
			//Если запрашивается какой-либо шаг регистрации
			if ($module=="default" && $controller=="index" && in_array($action,self::$stepsArr))
			{
				$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
				//Если есть созданная сессия и установлена переменная с номером шага регистрации
				if (Zend_Session::sessionExists() && isset($userAppNamespace->step) && $userAppNamespace->step!=1)
				{
					/* Если пользователь запрашивает шаг, отличный от доступного ему,
					 * то перенправляем его на нужный шаг
					 */
					$actionStep = (int) str_replace('step','',$action);
					if ($actionStep>$userAppNamespace->step)
					{
						$redirRoute = 'step'.$userAppNamespace->step; 
						$redirector->gotoRoute(array(''),$redirRoute);
					}
					
				}
				//Если нет сессии
				else
				{
					//Если пользователь уже не на 0 или 1 шаге, то перенаправляем на 1 шаг
					if ($action!='index' && $action!='step1')
					{
						$redirector->gotoRoute(array(''),'step1');
					}
				}
			}
		}
 	}
	
}