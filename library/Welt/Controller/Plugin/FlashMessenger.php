<?php
class Welt_Controller_Plugin_FlashMessenger extends Zend_Controller_Plugin_Abstract
{
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        // Инициализируем помощник FlashMessenger и получаем сообщения
        $actionHelperFlashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
        $messages = $actionHelperFlashMessenger->setNamespace('ErrorMessages')->getMessages();

        // Если сообщений нет, или процес диспетчеризации не закончен успешно, просто выходим из плагина
        if (empty($messages) || !$request->isDispatched()) 
		{
        	return;
        }
    }
}