<?php

// ���������� ������ � �������� ������� ����
if ($cur_type == 'ru') $cur_title = '���.'; elseif($cur_type == 'en') $cur_title = '$'; elseif($cur_type == 'eu') $cur_title = '�';
$cur_rate = 1;
if ($cur_type != 'ru')
{
	$sql = "SELECT rate FROM currency WHERE cur_name=". my_quote_smart($cur_type);
	$results = $mysql->ql_select($sql);
	$cur_rate = 0; // �������� �� ���� � ��������
	if (sizeof($results)) $cur_rate = $results[0]['rate'];
}

// ������ ������ �������, ��������� ������ � ���� ��� ���������
$hotelService = new HotelService($cur_hotel);
$hotelService->setRate($cur_rate);
$hotelService->loadRooms();
$hotelService->loadRoomsAccPrice($sdateF, $edateF);
$hotelData = $hotelService->getHotel();
?>
<table id="tabdesign" width="90%" cellspacing="2" cellpadding="5" border="0">
<tr>
    <th>��������� ������</th>
    <th>����������� ����������<br /> (<?php echo $cur_title; ?>)</th>
    <th>����������� ����������<br /> (<?php echo $cur_title; ?>)</th>
    <th>�������<br />(��������� �� ���. � �����) </th>
    <th></th>
</tr>  
<?php foreach ($hotelData ['rooms'] as $room) : ?>
<tr class="room">
	<td class="name">
       <?php echo $room['name']; ?>
	</td>
	<td class="price">
		<?php echo formatSum($room['price']['summ1'], FALSE); ?>
	</td>
	<td class="price">
		<?php if ($room['accom'] == 1) : ?>
			-
		<?php else : ?>
			<?php echo formatSum($room['price']['summ2'], FALSE); ?>
		<?php endif; ?>
	</td>
    <td class="breakfast">
		<?php echo dispBreakf($room, TRUE, $cur_title); ?>
	</td>
	<td class="action">
		<?php
        ($room['price']['summ1'] == 0 && $room['price']['summ2'] == 0) ? $byReq = TRUE : $byReq = FALSE;
        if ($byReq === TRUE)
		{
			($hotelData['show_bar_only'] != 1) ? $reqFunc = "prices_req" : $reqFunc = "goto_price_req"; 
			$orderText = '��������� ������ �� ����<img id="room-req-loader-'.$room['id'].'" class="room-loader" src="/img/loader16.gif" />';
			$orderLink = "<a href='#' onclick='".$reqFunc."(null, null, \"".$room['id']."\"); return false;'>".$orderText."</a>";
		}
		else
		{
			$orderText = '������������� �����';
			$orderLink = "<a class='order-link' target='_blank' href='". makeBookLink(array('curr' => $cur_type, 'hotel' => $hotelData['id'], 'room' => $room["id"], 'sdate' => $sdateF, 'edate' => $edateF)). "'>".$orderText."</a>";
		}
        echo $orderLink;
        ?>
	</td>
</tr>
<?php endforeach; ?>
</table>
