<?php
/**
 * Класс для работы с персонами
 */
class Welt_Persons {
	/**
	 * @var Id участника
	 */
	protected $userId;
	/**
	 * @var Id персоны
	 */
	protected $personId;
	/**
	 * @var Объект класса для работы с таблицей персон
	 */
	protected $personsTable;
	/**
	 * @var Объект класса для работы с таблицей загруженных для персон документов
	 */
	protected $personsDocsTable;
	/**
	 * @var Системные настройки из конфига
	 */
	protected $sysSettings;
	
	/**
	 * Конструктор
	 * @param int $userId
	 * @param int $personId [optional]
	 * @return 
	 */
	public function __construct($userId,$personId=0)
	{
		$this->userId = $userId;
		if (isset($personId)) $this->personId = $personId;
		$this->personsTable = new Welt_DbTable_Persons();
		$this->personsDocsTable = new Welt_DbTable_PersonsDocuments();
		$this->sysSettings = Zend_Registry::get('settings');
	}
	
	/**
	 * Установка персоны
	 * @param int $personId
	 * @return 
	 */
	public function setPerson($personId)
	{
		$this->personId = $personId;
	}
	
	/**
	 * Добавление новой персоны
	 * @param array $personData
	 * @return 
	 */
	public function addPerson($personData)
	{
		//Добавляем id участника
		if (!isset($personData['user_id']))
		{
			$personData['user_id'] = $this->userId;
		}
		if($this->personsTable->insert($personData))
		{
			return $this->personsTable->getAdapter()->lastInsertId();
		}
		else 
		{
			return false;
		}	
	}
	
	/**
	 * Редактирование персоны
	 * @param array $personData
	 * @return 
	 */
	public function updatePerson($personData)
	{
		//Если есть элемент с ключом id, удаляем его, чтобы не было проблем с записью в БД
		if (isset($personData['id'])) unset($personData['id']);
		//Тип персоны нельзя изменять
		if (isset($personData['person_type_id'])) unset($personData['person_type_id']);
		//Обновляем запись
		$where = $this->personsTable->getAdapter()->quoteInto('id=?',$this->personId);
		return $this->personsTable->update($personData,$where);
	}
	
	/**
	 * Получение списка документов по персоне
	 * @return 
	 */
	public function getPersonDocs()
	{
		return $this->personsDocsTable->getPersonDocsList($this->personId);
	}
	
	/**
	 * Получение информации по персонам (+ список документов для каждой персоны, параметр, определяющий, 
	 * заказана ли визовая поддержка)
	 * @return mixed
	 */
	public function getPersonsList()
	{
		//Получаем данные о персонах
		$persons = $this->personsTable->getUserPersonsDetail($this->userId,1,array(1,2));
		//Получаем данные о загруженных для персон материалах
		$personsSize = sizeof($persons);
		for($i=0;$i<$personsSize;$i++)
		{
			$persons[$i]['docs'] = $this->personsDocsTable->getPersonDocsList($persons[$i]['id']);
			$persons[$i]['visa'] = (int) Welt_Services::checkPersonServicesOrders($persons[$i]['id'],'visa');
		}
		return $persons;
	}
	
	/**
	 * Загрузка скана документа для персоны (запись в БД + копирование файла)
	 * @param string $fileKey Ключ в массиве $_FILES, в значении которого содержится информация о загруженном файле
	 * @return 
	 */
	public function uploadPersonDoc($fileKey)
	{
		//Временный файл, с которым работаем
		$tempFile = $_FILES[$fileKey];
		if (is_uploaded_file($tempFile['tmp_name']))
		{
			//Подготавливаем данные для записи в БД
			$docData = array();
			$docData['person_id'] = $this->personId;
			$docData['file_length'] = $tempFile['size'];
			//Определяем id заявки
			$appsTable = new Welt_DbTable_Applications();
			$appData = $appsTable->getUserApp($this->userId);
			$docData['application_id'] = $appData['id'];
			//Определяем Content-Type и расширение файла 
			$docData['extension'] = strtolower(array_pop(explode('.',$tempFile['name'])));
			$docData['type'] = Welt_Functions::getContentType($tempFile['name']);
			//Добавляем запись о загруженном файле в БД
			if($this->personsDocsTable->insert($docData))
			{
				//Получаем id добавленной записи для формирования имени файла
				$docId = $this->personsDocsTable->getAdapter()->lastInsertId();
				//Копируем файл
				$newFileName = $this->sysSettings->ppc->persons->docs_path.$docId.'.'.$docData['extension'];
				//Если файл успешно скопирован
				if(move_uploaded_file($_FILES['docFile']['tmp_name'],$newFileName))
				{
					//Возвращаем данные о загруженном файле
					$curDate = new Zend_Date();
					$result = array(
						'docId'			=>	$docId,
						'docName'		=>	$docId.'.'.$docData['extension'],
						'docDate'		=>	$curDate->toString('dd.MM.yyyy HH:mm')
					);
					return $result;
				}
				return false;
			}
			return false;
		}
		return false;
	}
	
	/**
	 * Удаление скана документа для персоны
	 * @param int $docFileId
	 * @return 
	 */
	public function deletePersonDoc($docFileId)
	{
		//Получаем данные по удаляемому файлу
		$docFile = $this->personsDocsTable->find($docFileId)->current();
		//Удаляем запись из БД
		$where = $this->personsDocsTable->getAdapter()->quoteInto('id=?',$docFileId);
		//Если запись успешно удалена
		if ($this->personsDocsTable->delete($where))
		{
			//Формируем путь к файлу
			$fullFileName = $this->sysSettings->ppc->persons->docs_path.$docFile->id.'.'.$docFile->extension;
			//Удаляем сам файл
			return unlink($fullFileName);
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Определение, соответствует ли данная персона участнику
	 * @param int $personId [optional]
	 * @return 
	 */
	public function checkIsUserPerson($personId=0)
	{
		if ($personId==0)
		{
			$personId = $this->personId;
		}
		//Получаем данные по персоне
		$personData = $this->personsTable->find($this->personId)->current();
		//Проверяем, соответствует ли персона текущему участнику
		if ($personData->user_id==$this->userId)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Смена статуса для указанной персоны
	 * @param int $personId
	 * @param int $status
	 * @return 
	 */
	public function changeStatusForPerson($personId,$status)
	{
		$personRow = $this->personsTable->fetchRow($this->personsTable->select()->where('id = ?', $personId));
		$personRow->status = $status;
		$res = $personRow->save();
		return true;
	}
}