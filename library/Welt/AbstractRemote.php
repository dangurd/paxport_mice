<?php
/**
 * Данный класс копирует данные из таблицы данного приложения в таблицу
 * тезисов расположенную по другому адресу на другом сайте
 * Created by Reiseburo WELT.
 * User: Max Chaply
 * Date: 18.03.14
 * Time: 17:53
 * To change this template use File | Settings | File Templates.
 */

class Welt_AbstractRemote {

    public $user_id;

    protected $_abstracts;

    protected $_remote_thesis;

    function __construct($user_id){

        $this->user_id = $user_id;
        $this->_remote_thesis = new Welt_DbTable_ThesisDb_Thesis();
        //echo "user Number: ".$user_id;
        //exit;
    }

    // Непосредственно осуществляет заполнение данных в базе Тезисов

    public function setToRemote(){

        $this->setThesisToRemote();
        $this->setAuthorToRemote();
        $this->setContributorToRemote();
        return true;
    }


    //Получает данные из таблиц abstracts, persons и persons_details

    protected function getAbstracts(){

        $abstracts = new Welt_DbTable_Abstracts();
        return $abstracts->getAbstractsToRemote($this->user_id);
    }

    //Получает данные из таблицы Сontributors

    protected function getContributors($abstract_id){

        $contributors = new Welt_DbTable_AbstractsContributors();
        return $contributors->getsAbstractContributors($abstract_id);
    }


    //Данные вставляются в таблицу thesis

    protected function setThesisToRemote(){

        $remote_thesis = $this->_remote_thesis;
        $this->_abstracts = $this->getAbstracts();


        foreach($this->_abstracts as $abstract){


        $data = array(
            'person_id'      => $abstract['person_id'],
            'abstract_id' => $abstract['id'],
            'title'      => $abstract['title'],
            'report_form'=>$abstract['form'],
            'theme'=>$abstract['theme'],
            'section'=>$abstract['section'],
            'intro_text'=>$abstract['intro_text'],
            'goal_text'=>$abstract['goal_text'],
            'material_text'=>$abstract['mm_text'],
            'result_text'=>$abstract['result_text'],
            'conlusion_text'=>$abstract['findings_text'],
            'created_on' => new Zend_Db_Expr('CURDATE()'),

        );

            $remote_thesis->insert($data);

        }

    }


    // Данные вставляются в таблицу author

    protected function setAuthorToRemote(){

        $remote_author = new Welt_DbTable_ThesisDb_Author();
        foreach($this->_abstracts as $abstract){
            $data = array(
                'person_id'=>$abstract['person_id'],
                'first_name'=>$abstract['fname'],
                'last_name'=>$abstract['lname'],
                'second_name'=>isset($abstract['patronymic'])?$abstract['patronymic']:'',
                'company'=>isset($abstract['company'])?$abstract['company']:'',
                'position'=>isset($abstract['position'])?$abstract['position']:'',
                'degree'=>isset($abstract['degree'])?$abstract['degree']:'',
                'email'=>isset($abstract['email'])?$abstract['email']:'',
                'phone'=>isset($abstract['phone'])?$abstract['phone']:'',  
            );

            $remote_author->insert($data);
        }


    }

    //Данные вставляются в таблицу Contributor

    protected function setContributorToRemote(){

        $remote_contributor = new Welt_DbTable_ThesisDb_Contributor();

        foreach($this->_abstracts as $abstract){

            $contributors = $this->getContributors($abstract['id']);

            $thesis_id = $this->_remote_thesis->getAdapter()->lastInsertId();

            foreach($contributors as $contributor){
                if(!empty($contributor['fname'])){
                    $data = array(
                            'thesis_id'=>  $thesis_id,
                            'abstract_id'=> $contributor['abstract_id'],
                            'first_name'=>$contributor['fname'],
                            'last_name'=>$contributor['lname'],
                            'second_name'=>$contributor['patronymic'],
                            'company'=>$contributor['company'],

                    );

                $remote_contributor->insert($data);

                }

            }

        }



    }

}
