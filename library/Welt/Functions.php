<?php
class Welt_Functions {
	
	/**
	 * Смена кодировки cp1251 на utf-8
	 * @param string $str
	 * @return 
	 */
	public static function cp1251toUtf(&$str)
	{
		$str=mb_convert_encoding($str,'UTF-8','CP-1251');
		return $str;
	}
	
	/**
	 * Смена кодировки utf-8 на cp1251
	 * @param string $str
	 * @return 
	 */
	public static function UtfToCp1251(&$str)
	{
		$str=mb_convert_encoding($str,'CP-1251','UTF-8');
		return $str;
	}
	
	
	/**
	 * Функция для преобразования результатов выборки из mysql в utf-8
	 * @param object $result
	 * @return object $result
	 */
	public static function resultToUtf($result)
	{
		array_walk_recursive($result,'Welt_Functions::cp1251toUtf');
		return $result;
	}
	
	/**
	 * Проверка даты на корректность
	 * @param string $date
	 * @return 
	 */
	public static function checkDate($date)
	{
		//Корректный формат
		if (!preg_match('/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/',$date))
		{
			return false;
		}
		//Дата не в прошлом
		$zendDate = new Zend_Date($date,'dd.MM.yyyy');
		//Добавляем к проверяемой дате маскимально возможные час и минуту (для корректного сравнения дат)
		$zendDate->add('23:59:00', Zend_Date::TIMES);
		$curDate = new Zend_Date();
		$zendDateUnix = (int) $zendDate->toString('U');
		$curDateUnix = (int) $curDate->toString('U');
		if ($zendDateUnix  <= $curDateUnix)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Проверка периода на корректность
	 * @param string $startDate
	 * @param string $endDate
	 * @return 
	 */
	public static function checkPeriod($startDate,$endDate)
	{
		//Даты корректные
		if (!Welt_Functions::checkDate($startDate) || !Welt_Functions::checkDate($endDate))
		{
			return false;
		}
		//Дата начала меньше даты окончания;
		$zendStartDate = new Zend_Date($startDate,'dd.MM.yyyy');
		$zendEndDate = new Zend_Date($endDate,'dd.MM.yyyy');
		$zendStartDateUnix = (int) $zendStartDate->toString('U');
		$zendEndDateUnix = (int) $zendEndDate->toString('U');
		if ($zendStartDateUnix >= $zendEndDateUnix)
		{
			return false;
		}
		return true;
	}
	
	/**
	 * Конвертирует дату в unix-формат
	 * @param string $date Дата в формате dd.mm.yyyy
	 * @return 
	 */
	public static function convertDateUnix($date) 
	{
		list($day,$month,$year) = explode(".", $date);
		$time = mktime(0, 0, 0, $month, $day, $year);
		return $time;	
	}
	
	/**
	 * Генерация случайной строки
	 * @param int $length [optional]
	 * @return 
	 */
	public static function getUniqueCode($length = '')
	{	
		$code = md5(uniqid(rand(), true));
		if ($length != '') return substr($code, 0, $length);
		else return $code;
	}
	
	/**
	 * Получить дату = текущая дата - $tokenLifetime 
	 * Используется при восстановлении пароля
	 * @return 
	 */
	public static function expiredDate()
	{
		$tokenLifetime = Zend_Registry::get('settings')->token_lifetime;
		$curDate = new Zend_Date();
		$curDate->sub($tokenLifetime,Zend_Date::HOUR);
		return $curDate->toString('yyyy-MM-dd HH:mm:ss');
	}
	
	/**
	 * Текущая дата для записи в mysql
	 * @return 
	 */
	public static function getDateTimeMysql()
	{
		$curDate = new Zend_Date();
		return $curDate->toString('yyyy-MM-dd HH:mm:ss');
	}
	
	/**
	 * Преобразование даты для записи в mysql
	 * @param string $date
	 * @return string
	 */
	public static function dateToMysql($date)
	{
		$zendDate = new Zend_Date($date,'dd.MM.yyyy');
		return $zendDate->toString('yyyy-MM-dd');
	}
	
	/**
	 * Преобразование даты из mysql в обычный формат
	 * @param string $date
	 * @return string
	 */
	public static function dateFromMysql($date,$type=0)
	{
		if ($type==1)
		{
			if($date=='0000-00-00 00:00:00' || $date=='')
			{
				$dateFormatted = '';
			}
			else
			{
				$zendDate = new Zend_Date($date,'yyyy-MM-dd HH:mm:ss');
				$dateFormatted = $zendDate->toString('dd.MM.yyyy HH:mm');
			}
		}
		else
		{
			if($date=='0000-00-00' || $date=='')
			{
				$dateFormatted = '';
			}
			else
			{
				$zendDate = new Zend_Date($date,'yyyy-MM-dd');
				$dateFormatted = $zendDate->toString('dd.MM.yyyy');
			}
		}
		return $dateFormatted;
	}
	
	/**
	 * Преобразование массива в строку с перечислением элементов для использования в запросе вида
	 * SELECT field FROM table WHERE id IN(?)
	 * @param array $arr
	 * @return string
	 */
	public function arrayToSqlWhere($arr)
	{
		$arraySize = sizeof($arr);
		$arrStr = '';
		//Если в массиве 1 элемент
		if ($arraySize==1)
		{
			if (is_int($arr[0]))
			{
				$arrStr = $arr[0];
			}
			else
			{
				$arrStr = "'".$arr[0]."'";
			}
		}
		else
		{
			$i = 0;
			foreach ($arr as $elem)
			{
				if (is_int($elem))
				{
					$arrStr .= $elem;
				}
				else
				{
					$arrStr .= "'".$elem."'";
				}
				if ($i != ($arraySize-1))
				{
					$arrStr .= ',';
				}
				$i++;
			}
		}
		return $arrStr;
	}
	
	/**
	 * Конвертирование валют из рублей и в рубли
	 * @param string $currName
	 * @param string $price
	 * @param int $mode 1 - в рубли, 2 - из рублей
	 * @return 
	 */
	public static function convertCurrencyRur($currName,$price,$mode)
	{
		//Получаем курс валюты
		$currRate = self::getCurrency($currName);
		//Если идёт перевод из рублей	
		if ($mode==1)
		{
			$convPrice = round($price/$currRate);
		}
		//Если идёт перевод в рубли
		elseif ($mode==2)
		{
			$convPrice = round(($currRate*$price)/10)*10;
		}
		return $convPrice;
	}
	
	/**
	 * Получение курса относительно рубля для заданной валюты
	 * @param string $currName
	 * @return 
	 */
	public static function getCurrency($currName)
	{
		//Получаем курс валюты
		$dbMediumCache = Zend_Registry::get('cacheManager')->getCache('databaseMedium');
		$cacheId = 'currency_'.$currName;
		//Проверяем наличие данных в кэше
		if ($dbMediumCache->test($cacheId))
		{
			$currRate = $dbMediumCache->load($cacheId);
		}
		else
		{
			$currTable = new Welt_DbTable_WeltCurrency();
			$currRate = $currTable->getRate($currName);
			$dbMediumCache->save($currRate,$cacheId);
		}
		return $currRate;
	}
	
	
	/**
	 * Очистка комментария от html-тегов и пр. Укорачивание строки до установленного в настройках размера.
	 * @param string $comment
	 * @return 
	 */
	public static function filterComment($comment)
	{	
		$comment = stripslashes($comment);
		$comment = str_replace("&lt;", "", $comment);
		$comment = str_replace("&gt;", "", $comment);
		$comment = htmlspecialchars($comment);
		$commentMaxLen = Zend_Registry::get('settings')->services->max_comment_size;
		if (strlen($comment)>$commentMaxLen)
		{
			$comment = substr($comment,0,$commentMaxLen);
		}
		return $comment;
	}
	
	/**
	 * Определение Content-Type файла
	 * @param string $filename
	 * @return 
	 */
	public function getContentType($filename) {
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
	
	/**
	 * Определение браузера
	 * @param string $agent
	 * @return 
	 */
	public static function userBrowser($agent) 
	{
	preg_match("/(MSIE|Opera|Firefox|Chrome|Version|Opera Mini|Netscape|Konqueror|SeaMonkey|Camino|Minefield|Iceweasel|K-Meleon|Maxthon)(?:\/| )([0-9.]+)/", $agent, $browser_info); // регулярное выражение, которое позволяет отпределить 90% браузеров
        list(,$browser,$version) = $browser_info; // получаем данные из массива в переменную
        if (preg_match("/Opera ([0-9.]+)/i", $agent, $opera)) return 'Opera '.$opera[1]; // определение _очень_старых_ версий Оперы (до 8.50), при желании можно убрать
        if ($browser == 'MSIE') { // если браузер определён как IE
                preg_match("/(Maxthon|Avant Browser|MyIE2)/i", $agent, $ie); // проверяем, не разработка ли это на основе IE
                if ($ie) return $ie[1].' based on IE '.$version; // если да, то возвращаем сообщение об этом
                return 'IE '.$version; // иначе просто возвращаем IE и номер версии
        }
        if ($browser == 'Firefox') { // если браузер определён как Firefox
                preg_match("/(Flock|Navigator|Epiphany)\/([0-9.]+)/", $agent, $ff); // проверяем, не разработка ли это на основе Firefox
                if ($ff) return $ff[1].' '.$ff[2]; // если да, то выводим номер и версию
        }
        if ($browser == 'Opera' && $version == '9.80') return 'Opera '.substr($agent,-5); // если браузер определён как Opera 9.80, берём версию Оперы из конца строки
        if ($browser == 'Version') return 'Safari '.$version; // определяем Сафари
        if (!$browser && strpos($agent, 'Gecko')) return 'Browser based on Gecko'; // для неопознанных браузеров проверяем, если они на движке Gecko, и возращаем сообщение об этом
        return $browser.' '.$version; // для всех остальных возвращаем браузер и версию
    }

	
	
	/**
	 * Экранирование спецсимволов в текстовом узле xml-документа
	 * @param string $textNode
	 * @return 
	 */
	public static function xmlTextNodeEscape($textNode)
	{
		$textNode = str_replace('"','&quot;',$textNode);
		$textNode = str_replace('&','&amp;',$textNode);
		$textNode = str_replace("'","&apos;",$textNode);
		$textNode = str_replace('"','&quot;',$textNode);
		$textNode = str_replace('<','&lt;',$textNode);
		$textNode = str_replace('>','&gt;',$textNode);
		return $textNode;
	}
	
	/**
	 * Проверка на администратора по ip и вывод информации в firebug по необходимости
	 * @param object $text [optional]
	 * @return mixed
	 */
	public static function showForAdmin($text=0)
	{
		$adminIp = Zend_Registry::get('settings')->system->admin->ip;
		$curUserIp = $_SERVER['REMOTE_ADDR'];
		//Если ip совпадает с указанным в конфиге 
		if ($curUserIp==$adminIp)
		{
			//Если нужно вывести объект в firebug
			if (isset($text) && $text)
			{
				$logger = Zend_Registry::get('logger');
				$logger->log($text, Zend_Log::INFO);
			}
			//Если идёт просто проверка на валидность ip
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Добавление текстовых значений для стран в указанных полях для каджой записи из массива строк
	 */
	public function addCountriesNamesToRows($rows, $countriesFields)
	{
		$rowsSize = sizeof($rows);
		if (is_array($rows) && $rowsSize > 0)
		{
			//Формируем список из всех id стран, встречающихся в данных по персонам
			$countriesTable = new Welt_DbTable_WeltCountries();
			$countriesIds = array();
			foreach ($rows as $row)
			{
				foreach ($countriesFields as $countryIdField=>$countryNameField)
				{
					//Если id страны не пустой
					if (!is_null($row[$countryIdField]) && $row[$countryIdField] != '00000000-0000' && $row[$countryIdField] != '')
					{
						$countriesIds[] = $row[$countryIdField];
					}
				}
			}
			$countriesList = array();
			//Если данные по странам есть
			if (sizeof($countriesIds) > 0)
			{
				//Получаем данных по странам с id, найденными ранее
				$countriesIds = array_unique($countriesIds);
				$countriesList = $countriesTable->getCountries($countriesIds);
			}
			//Проходимся по персонам
			for ($i = 0; $i < $rowsSize; $i++)
			{
				//Проходимся по списку полей, для которых определяется страна по id 
				foreach ($countriesFields as $countryIdField=>$countryNameField)
				{
					//Если id страны пустой или информация по стране с данным id не найдена, то оставляем название страны пустым
					if (is_null($rows[$i][$countryIdField]) || $rows[$i][$countryIdField] == '' 
						|| $rows[$i][$countryIdField] == '00000000-0000' || !isset($countriesList[$rows[$i][$countryIdField]]))
					{
						$rows[$i][$countryNameField] = '';
					}
					//Если id страны не пустой и информация по данной стране была получена ранее 
					else
					{
						$rows[$i][$countryNameField] = $countriesList[$rows[$i][$countryIdField]];
					}
				}
			}
		}
		return $rows;
	}

}
