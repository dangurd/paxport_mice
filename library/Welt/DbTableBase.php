<?php
/**
 * Базовый класс для классов таблиц БД
 */
class Welt_DbTableBase extends Zend_Db_Table_Abstract 
{
	protected $lang;
    
    const DEF_LANG = 'ru';
	
	public function __construct($lang='')
	{
		parent::__construct();
		if ($lang=='')
		{
			//Если язык установлен в реестре
			if (Zend_Registry::isRegistered('lang'))
			{
				$lang = Zend_Registry::get('lang');
			}
			//Если не установлен, берём язык по умолчанию
			else
			{
				$lang = self::DEF_LANG;
			}
		}
		$this->lang = $lang;
	}
	
	/**
	 * Функция для преобразования результатов выборки из mysql в utf-8
	 * @param array $result
	 * @return array $result
	 */
	protected function resultToUtf($result)
	{
		array_walk_recursive($result,'Welt_Functions::cp1251toUtf');
		return $result;
	}
	
	/**
	 * Функция для преобразования информации в массиве в cp1251 для записи в БД
	 * @param array $result
	 * @return array $result
	 */
	protected function dataToCp1251($result)
	{
		array_walk_recursive($result,'Welt_Functions::UtfToCp1251');
		return $result;
	}
	
	/**
	 * Обработка элементов массива для добавления/обновления записи, содержащих даты
	 * @param array $elemsKeys Ключи элементов массива, содержащих даты 
	 * @return 
	 */
	protected function datesProcess(&$dataArr,$elemsKeys)
	{
		//Проходимся по элементам массива с указанными ключами
		foreach($elemsKeys as $key)
		{
			if (isset($dataArr[$key]))
			{
				/* Если даты нет или она в неправильном формате, то удаляем данный элемента массива 
				 * (в БД запишется NULL)
				 */
				if ($dataArr[$key]=='' || !preg_match('/^[0-9]{2}\.[0-9]{2}\.[0-9]{4}$/',$dataArr[$key]))
				{
					unset($dataArr[$key]);
				}
				//Если дата корректная, преобразуем её в нужный формат
				else
				{
					$dataArr[$key] = Welt_Functions::dateToMysql($dataArr[$key]);
				}
			}
		}
		return $dataArr;
	} 
	
	
}
