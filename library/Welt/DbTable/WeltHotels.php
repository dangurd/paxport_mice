<?php
class Welt_DbTable_WeltHotels extends Welt_DbTableBase
{
	protected $_name = 'hotels';
	
	private $nameField;
	private $addressField;
	private $captField;
	
	/**
	 * Выбор БД welt
	 * @return 
	 */
	protected function _setupDatabaseAdapter()
    {
         $this->_db = Zend_Registry::get('welt');
		 parent::_setupDatabaseAdapter();
    }
	
	/**
	 * Получение краткой информации по гостинице
	 * @param string $hotelId
	 * @return 
	 */
	public function getHotelShortInfo($hotelId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('name'=>$this->nameField,'address'=>$this->addressField))
			   ->where('full_id=?',$hotelId);
		$rows = $this->fetchRow($select)->toArray();
		return $this->resultToUtf($rows);
	}
	
	/**
	 * Получение названия гостиницы по id
	 * @param int $hotelId
	 * @return 
	 */
	public function getNameById($hotelId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('name'=>$this->nameField))
			   ->where('full_id=?',$hotelId);
		$rows = $this->fetchRow($select)->toArray();
		$rows = $this->resultToUtf($rows);
		return $rows['name'];
	}
	
	/**
	 * Получение детальной информации по указанной гостинице
	 * @param string $hotelId
	 * @return 
	 */
	public function getHotelDetails($hotelId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'id'			=>		'id',
			'1d_1c'			=>		'full_id',
			'name'			=>		$this->nameField,
			'address'		=>		$this->addressField,
			'caption'		=>		$this->captField,
			'stars'			=>		'number_of_stars',
			'link_name',
			'google_coordinates'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'hotels_metro'),'y.hotel_id=x.id',array())
			   ->joinLeft(array('z'=>'metro_station'),'z.id=y.station_id',array('metro_stations'=>'GROUP_CONCAT(DISTINCT z.name1 SEPARATOR ", ")'))
			   ->joinLeft(array('cities'),'x.city=cities.id',array('city_name_eng'=>'LOWER(city_name_eng)','city_link'=>'link_name'))
			   ->where('x.full_id=?',$hotelId)
			   ->group('x.full_id');
		$row = $this->fetchRow($select);
		//Если гостиница не найдена
		if (!$row)
		{
			return false;
		}
		$row = $row->toArray();
		$hotelInfo = $this->resultToUtf($row);
		//Если язык английский, то переводим в транслит название станций метро
		if ($this->lang == 'en')
		{
			$translitFilter = new Welt_Filter_Translit();
			$hotelInfo['metro_stations'] = $this->translateStations($hotelInfo['metro_stations'],$translitFilter);
		}
		return $hotelInfo;
	}
	
	/**
	 * Перевод строки, содержащей список станций метро в транслит
	 * @param string $metroStations
	 * @param string $filter Объекта класс-фильтра
	 * @return 
	 */
	protected function translateStations($metroStations,$filter)
	{
		if (strlen(trim($metroStations)==''))
		{
			return '';
		}
		$metroStationsArr = explode(',',$metroStations);
		$metroStationsArrSize = sizeof($metroStationsArr);
		for ($i=0; $i<$metroStationsArrSize; $i++)
		{
			$metroStationsArr[$i] = $filter->filter($metroStationsArr[$i]);
		}
		$metroStationsStr = implode(',',$metroStationsArr);
		return $metroStationsStr;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField = 'hotel_name_eng';
			$this->addressField = 'hotel_address_eng';
			$this->captField = 'description_eng';
		}
		else
		{
			$this->nameField='hotel_name';
			$this->addressField = 'hotel_address';
			$this->captField = 'description';
		}
	}
}