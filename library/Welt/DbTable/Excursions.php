<?php
class Welt_DbTable_Excursions extends Welt_DbTableBase
{
	protected $_name = 'excursions';
	private $nameField;
	private $noteField;
	
	/**
	 * Получение всех возможных экскурсий
	 * @return mixed
	 */
	public function getExcursions($cityId = NULL)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'id',
			'id_1c',
			'name'			=>	$this->nameField,
			'price1'		=>	'ROUND(price1)',
			'price2'		=>	'ROUND(price2)',
			'price3'		=>	'ROUND(price3)',
			'with_acc'		=>	'with_acc',
			'note'			=>	$this->noteField
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'excursion_periods'),'x.id=y.excursion_id',array(''))
			   ->order(array('x.' . $this->nameField . ' ASC'))
			   ->where('disabled!=?', 1)
			   ->group('x.id');
		if (!is_null($cityId))
		{
			$select->where('x.city_id=?',$cityId);
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение данных по экскурси на основе заданных параметров
	 * @param mixed $excursionData
	 * @return int
	 */
	public function getInfo($excursionData)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('price1' => 'ROUND(price1)', 'price2' => 'ROUND(price2)', 'price3' => 'ROUND(price3)','with_acc' =>	'with_acc'))
			   ->where('id=?',$excursionData['excursionId']);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
			$this->noteField='note_eng';
		}
		else
		{
			$this->nameField='name';
			$this->noteField='note';
		}
	}
	
}