<?php
class Welt_DbTable_DocsRequests extends Welt_DbTableBase
{
	protected $_name = 'docs_requests';
	
	/**
	 * Добавление нового запроса на счёт/авт. письмо
	 * @param array $requestData
	 * @return 
	 */
	public function addDocRequest($appId,$requestData)
	{
		$docRequest = array(
			'application_id'		=>	$appId,
			'doc_type'				=>	$requestData['doc_type'],
			'lang'					=>	$requestData['lang'],
			'created'				=>	Welt_Functions::getDateTimeMysql(),
			'company'				=>	$requestData['company']
		);
		return $this->insert($docRequest);
	}
}