<?php
class Welt_DbTable_Discounts extends Welt_DbTableBase
{
	protected $_name = 'discounts';
	
	public function getDiscountInfo ($discountCode)
	{
	    $select = $this->select();
	    $select->from('discounts')
	           ->where('code = ?', $discountCode)
	           ->where('active = ?', '1');
	    $rows = $this->fetchAll($select)->toArray();
	    return $rows;
	}
}