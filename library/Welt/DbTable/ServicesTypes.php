<?php
class Welt_DbTable_ServicesTypes extends Welt_DbTableBase
{
	protected $_name = 'services_types';
	
	private $nameField;
	
	/**
	 * Получение системных имён всех доступных для заказа услуг
	 * @return 
	 */
	public function getSysNamesArr()
	{
		if (Zend_Registry::isRegistered('cacheManager'))
		{
			$cacheOn = true;
			//Проверяем наличие данных в кэше
			$dbExtraLongCache = Zend_Registry::get('cacheManager')->getCache('databaseExtraLong');
			$cacheId = $this->_name.'_'.$this->lang;
		}
		else
		{
			$cacheOn = false;
		}
		if ($cacheOn === true && $dbExtraLongCache->test($cacheId))
		{
			$sysNames = $dbExtraLongCache->load($cacheId);
		}
		else
		{
			$select = $this->select();
			$select->from(array($this->_name),array('sys_name'))
				   ->where('disabled!=?',1);
			$rows = $this->fetchAll($select)->toArray();
			$sysNames = array();
			foreach ($rows as $row)
			{
				$sysNames[] = $row['sys_name'];
			}
			if ($cacheOn === true)
			{
				$dbExtraLongCache->save($sysNames,$cacheId);
			}
		}
		return $sysNames;
	}
	
	/**
	 * Получение информации по всем типам услуг
	 * @return 
	 */
	public function getAllServiceTypes()
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('sys_name','name'=>$this->nameField,'deny_user_order'))
			   ->where('disabled!=?',1);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
}