<?php
class Welt_DbTable_Persons extends Welt_DbTableBase
{
	protected $_name = 'persons';
	
	private $personTypeNameField;
	
	/**
	 * Переопределяем функцию insert
	 * @param array $data
	 * @return 
	*/
	public function insert(array $data)
    {
    	//Добавляем дату создания и статус 
        if (empty($data['created']))
		{
            $data['created'] = Welt_Functions::getDateTimeMysql();
        }
		if (empty($data['status']))
		{
			$data['status'] = 0;
		}
		//Ставим флаг: иностранец или нет
		if ($data['citizenship_id'] != Welt_DbTable_WeltCountries::russiaId)
		{
			$data['is_foreign'] = 1;
		}
		//Обрабатываем даты
		$this->datesProcess($data,array('birthday','passport_exp_date'));
		return parent::insert($data);
    }
	
	/**
	 * Переопределяем функцию update 
	 * @param array $data
	 * @param string $where
	 * @return 
	*/
	public function update(array $data, $where)
    {
    	//Добавляем по умолчанию флаг changed=1
        if (!isset($data['changed']))
		{
            $data['changed'] = 1;
        }
		//Необходимо для корректной работы движка разных версий
		if (isset($data['citizenship_id']) && $data['citizenship_id'] != '')
		{
			$data['citizenship'] = '';
		}
		//Обрабатываем даты
		$this->datesProcess($data,array('birthday','passport_exp_date'));
        return parent::update($data, $where);
    }
	
	/**
	 * Получение информации по прикпеплённым к пользователю персонам
	 * @param int $userId
	 * @param int $type [optional]
	 * @return 
	 */
	public function getUserPersons($userId,$type=0,$statuses=array())
	{
		$this->setQueryFields();
		if ($type==1)
		{
			$fields = array('x.id');
		}
		else
		{
			$fields = array('x.id','x.person_type_id','x.fname','x.lname','name'=>'CONCAT(x.fname," ",x.lname)','x.sex','x.citizenship_id','x.status');
		}
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name),$fields)
			   ->where('x.user_id=?',$userId);
		if ($type!=1)
		{
			$select->joinLeft(array('y'=>'persons_types'),'x.person_type_id=y.id',array('personTypeName'=>'y.'.$this->personTypeNameField));
		}
		//Если идёт выборка персон по заданным статусам
		if (is_array($statuses) && sizeof($statuses)>0)
		{
			$select->where('x.status IN('.Welt_Functions::arrayToSqlWhere($statuses).')');
		}
		$persons = $this->fetchAll($select)->toArray();
		//Если необходимо, по id стран в полях определяем их названия
		if ($type!=1)
		{
			$persons = Welt_Functions::addCountriesNamesToRows($persons, array('citizenship_id' => 'citizenship'));
		}
		return $persons;
	}
	
	/**
	 * Получение полной информации по персонам
	 * @param int $userId
	 * @param int $type [optional]
	 * @return mixed
	 */
	public function getUserPersonsDetail($userId,$type=0,$statuses=array())
	{
		$this->setQueryFields();
		if ($type==1)
		{
			$fields = array('id','person_type_id','fname','lname','name'=>'CONCAT(fname," ",lname)', 'sex', 'birthday','citizenship_id','passport_number','changed','passport_exp_date','status', 'updated', 'created');	
		}
		else
		{
			$fields = array('id','person_type_id','name'=>'CONCAT(fname," ",lname)','created','sex');	
		}
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'persons_types'),'x.person_type_id=y.id',array('personTypeName'=>'y.'.$this->personTypeNameField))
			   ->where('x.user_id=?',$userId)
			   ->order('x.person_type_id ASC');
		//Если идёт выборка персон по заданным статусам
		if (is_array($statuses) && sizeof($statuses)>0)
		{
			$select->where('x.status IN('.Welt_Functions::arrayToSqlWhere($statuses).')');
		}
		$persons = $this->fetchAll($select)->toArray();
		//Если необходимо, по id стран в полях определяем их названия
		if ($type==1)
		{
			$persons = Welt_Functions::addCountriesNamesToRows($persons, array('citizenship_id' => 'citizenship'));
		}
		return $persons;
	}
	
	/**
	 * Получение типа персоны по id
	 * @param int $personId
	 * @return 
	 */
	public function getPersonType($personId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name),'x.person_type_id')
			   ->joinLeft(array('y'=>'persons_types'),'x.person_type_id=y.id',array('personTypeName'=>'y.'.$this->personTypeNameField))
			   ->where('x.id=?',$personId);
		$row = $this->fetchRow($select);
		if ($row)
		{
			$row = $row->toArray();
		}
		else
		{
			$row = array();
		}
		return $row;
	}
	
	/**
	 * Смена статуса с указанного на заданный для персон для текущего пользователя
	 * @param int $userId
	 * @param int $findStatus
	 * @param int $newStatus
	 * @return 
	 */
	public function changeAppPersonsStatus($userId,$findStatus,$newStatus)
	{
		$where[] = $this->getAdapter()->quoteInto('user_id = ?', $userId);
		$where[] = $this->getAdapter()->quoteInto('status = ?', $findStatus);
		$data = array('status'=>$newStatus);
		return $this->update($data,$where);
	}
	
	/**
	 * Получение списка заявок, в которых есть изменённые персоны, за указанный период
	 * @param string $sdate Дата начала периода в формате mysql date
	 * @param string $edate Дата окончания периода в формате mysql date
	 * @return 
	 */
	public function geUpdatedApps($sdate, $edate)
	{
		$select = $this->select();
		$select->from(array('x' => $this->_name),array('DISTINCT(application_id) as app'))
			   ->joinLeft(array('y' => 'applications'), 'x.application_id=y.id', array())
			   ->where('(x.updated BETWEEN ' . $this->getAdapter()->quote($sdate) . 'AND ' . $this->getAdapter()->quote($edate) . ')')
			   ->where('x.status>?', 1)
			   //Чтобы в синхронизацию не попадали только что созданные заявки
			   ->where('ROUND(TIME_TO_SEC(TIMEDIFF(x.updated, y.finished))) > ?', Welt_Applications::appProcessTime)
			   ->where('y.data_sended = ?', 1);
		$rows = $this->fetchAll($select);
		if (is_null($rows))
		{
			return false;
		}
		$apps = array();
		foreach ($rows as $row)
		{
			$apps[] = $row['app'];
		}
		return $apps;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->personTypeNameField='name_eng';
		}
		else
		{
			$this->personTypeNameField='name';
		}
	}

	
	
}