<?php
class Welt_DbTable_PaymentMethods extends Welt_DbTableBase
{
	protected $_name = 'payment_methods';
	private $nameField;
	private $descField;
	
	/**
	 * Получения списка всех методов оплаты
	 * @return 
	 */
	public function getPaymentMethods()
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('sys_name','name'=>$this->nameField,'desc'=>$this->descField));
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
		
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
			$this->descField='desc_eng';
		}
		else
		{
			$this->nameField='name';
			$this->descField='desc';
		}
	}
}