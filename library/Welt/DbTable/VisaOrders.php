<?php
class Welt_DbTable_VisaOrders extends Welt_DbTable_BaseOrders
{
	protected $_name = 'visa_orders';
	
	private $visaTypeField;
	
	/**
	 * Получение информации о заказах на визу в рамках заданной заявки
	 * @param int $appId
	 * @param mixed $serviceStatuses Масив статусов услуг для отбора
	 * @return 
	 */
	public function getUserVisaOrders($appId, $serviceStatuses, $options)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name))
			   ->joinLeft(array('y'=>'visa_types'),'y.id=x.visa_type_id',array('visaTypeName'=>$this->visaTypeField, 'is_free'))
			   ->where('x.application_id=?',$appId);
		//Если идёт выборка заказов по заданным статусам
		if (is_array($serviceStatuses) && sizeof($serviceStatuses)>0)
		{
			$select->where('x.status IN('.Welt_Functions::arrayToSqlWhere($serviceStatuses).')');
		}
		//Если переданы доп. опции
		if (isset($options) && is_array($options) && sizeof($options)>0)
		{
			foreach ($options as $option=>$value)
			{
				switch ($option)
				{
					case 'is_free' : $select->where('y.is_free=?', $value); break;
				}
			}
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение детальной информации по заказанной услуге
	 * @param int $serviceId Id заказанной услуги
	 * @return 
	 */
	public function getOrderDetails($serviceId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name))
			   ->joinLeft(array('y'=>'visa_types'),'y.id=x.visa_type_id',array('visaTypeName'=>$this->visaTypeField, 'is_free'))
			   ->where('x.id=?',$serviceId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->visaTypeField='name_eng';
		}
		else
		{
			$this->visaTypeField='name';
		}
	}
	
}