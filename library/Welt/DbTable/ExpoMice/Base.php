<?php
abstract class Welt_DbTable_ExpoMice_Base extends Welt_DbTableBase
{
	
	/**
	 * Выбор БД expo_mice
	 * @return 
	 */
	protected function _setupDatabaseAdapter()
    {
         $this->_db = Zend_Registry::get('expoMice');
		 parent::_setupDatabaseAdapter();
    }
}
