<?php
class Welt_DbTable_ExpoMice_MiceOrganizers extends Welt_DbTable_ExpoMice_Base
{

    protected $_name = 'mice_organizers';
    
    /**
     * Получение параметров организатора по систменому имени
     * @param string $organizerId 
     */
    public function getBySysName($sysName)
    {
        $select = $this->select();
        $fields = array(
            'id',
            'id_1c',
            'database',
            'sys_name',
        );
        $select->from(array($this->_name), $fields)
                ->where('sys_name=?', $sysName);
        $row = $this->fetchRow($select);
        if ($row)
        {
            return $row->toArray();
        }
    }
    
}