<?php
class Welt_DbTable_ExpoMice_EventParams extends Welt_DbTable_ExpoMice_Base
{

	protected $_name = 'organizers_event_params';
	
	protected $_coordName;
	
	protected $_eventName;

	/**
	 * Получение параметров мероприятия
	 * @param string $organizerId 
	 */
	public function getEventParams($organizerId, $mode = 1)
	{
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
		    'organizer_id_1c',
			'event_sdate'	=>	'DATE_FORMAT(x.event_sdate, "%d.%m.%Y")',
			'event_edate'	=>	'DATE_FORMAT(x.event_edate, "%d.%m.%Y")',
			'event_name',
			'event_name_eng',
			'multilang',
			'person_types',
			'def_lang',
			'main_city',
			'form_close'    =>  'DATE_FORMAT(x.form_close, "%d.%m.%Y")',
			'lk_close'      =>  'DATE_FORMAT(x.lk_close, "%d.%m.%Y")',
		);
		$coordFields = array(
			'coord_name'		=>	'name',
			'coord_name_eng'	=>	'name_eng',
			'coord_email'		=>	'email',
			'coord_phone'		=>	'phone',
			'coord_phone_ext'	=>	'phone_ext',
		);
		$select->from(array('x' => $this->_name), $fields)
                ->joinLeft(array('y' => 'events_coordinators'), 'x.coord_id=y.id', $coordFields)
				->where('organizer_id_1c =? ', $organizerId);
		$row = $this->fetchRow($select);
		if ($row)
		{
			return $row->toArray();
		}
		else
		{
			return false;
		}	   
	}
    
    /**
     * Получить указанные тексты из параметров мероприятия
     */
    public function getTexts($organizerId, array $textsTypes)
    {
        $avTypes = array(
            'intro' => array('intro_text', 'intro_text_eng'),
            'hotel' => array('hotel_info', 'hotel_info_eng'),
            'transport' => array('transport_info', 'transport_info_eng'),
            'excursion' => array('excursion_info', 'excursion_info_eng'),
        );
        $textsFields = array();
        ($this->lang == 'en') ? $langIndex = 1 : $langIndex = 0;
        foreach($textsTypes as $type)
        {
            if (!array_key_exists($type, $avTypes))
            {
                continue;
            }
            $textsFields[$type] = $avTypes[$type][$langIndex];
        }
        $select = $this->select();
        if (empty($textsFields))
        {
            return;
        }
        $select->from(array($this->_name), $textsFields)
                ->where('organizer_id_1c =? ', $organizerId);
        $row = $this->fetchRow($select);
        if ($row)
        {
            return $row->toArray();
        }
    }
}
