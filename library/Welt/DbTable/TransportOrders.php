<?php
class Welt_DbTable_TransportOrders extends Welt_DbTable_BaseOrders
{
	protected $_name = 'transport_orders';
	
	private $serviceNameField;
	private $typeNameField;
	protected $routeField;
	protected $stationField;
	
	/**
	 * Получение информации о заказах на транспорт в рамках заданной заявки
	 * @param int $appId
	 * @param mixed $serviceStatuses Масив статусов услуг для отбора
	 * @return 
	 */
	public function getUserTransportOrders($appId,$serviceStatuses)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'id',
			'id_1c',
			'application_id',
			'status',
			'order_date',
			'transp_type_id',
			'transp_service_id',
			'date',
			'time'					=>	'TIME_FORMAT(time,"%H:%i")',
			'route'					=>	$this->routeField,
			'number',
			'train_car',
			'summ',
			'comment',
			'updated'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'transport_services'),'x.transp_service_id=y.id',array('transpServiceName'=>'y.'.$this->serviceNameField))
			   ->joinLeft(array('z'=>'transport_types'),'x.transp_type_id=z.id',array('transpTypeName'=>'z.'.$this->typeNameField))
			   ->joinLeft(array('tr'=>'train_stations'),'x.station_id=tr.id',array('station'=>'tr.'.$this->stationField))
			   ->where('x.application_id=?',$appId);
		//Если идёт выборка заказов по заданным статусам
		if (is_array($serviceStatuses) && sizeof($serviceStatuses)>0)
		{
			$select->where('x.status IN('.Welt_Functions::arrayToSqlWhere($serviceStatuses).')');
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение детальной информации по заказанной услуге
	 * @param int $serviceId Id заказанной услуги
	 * @return 
	 */
	public function getOrderDetails($serviceId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'date',
			'time'			=>	'TIME_FORMAT(time,"%H:%i")',
			'route'			=>	$this->routeField,
			'number',
			'train_car',
			'summ',
			'comment'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'transport_services'),'x.transp_service_id=y.id',array('transpServiceName'=>'y.'.$this->serviceNameField,'transport_category','station_select'))
			   ->joinLeft(array('z'=>'transport_types'),'x.transp_type_id=z.id',array('transpTypeName'=>'z.'.$this->typeNameField))
			   ->joinLeft(array('tr'=>'train_stations'),'x.station_id=tr.id',array('station'=>'tr.'.$this->stationField))
			   ->where('x.id=?',$serviceId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->serviceNameField = 'name_eng';
			$this->typeNameField = 'name_eng';
			$this->routeField = 'route_eng';
			$this->stationField = 'name_eng';
		}
		else
		{
			$this->serviceNameField='name';
			$this->typeNameField = 'name';
			$this->routeField = 'route';
			$this->stationField = 'name';
		}
	}
}