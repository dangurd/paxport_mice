<?php
class Welt_DbTable_WeltRooms extends Welt_DbTableBase
{
	protected $_name = 'hotels_rooms';
	
	private $nameField;
	
	/**
	 * Выбор БД welt
	 * @return 
	 */
	protected function _setupDatabaseAdapter()
    {
         $this->_db = Zend_Registry::get('welt');
		 parent::_setupDatabaseAdapter();
    }
	
	/**
	 * Получение краткой информации по номеру
	 * @param string $hotelId
	 * @return 
	 */
	public function getRoomShortInfo($roomId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('name'=>$this->nameField))
			   ->where('full_id=?',$roomId);
		$rows = $this->fetchRow($select)->toArray();
		return $this->resultToUtf($rows);
	}
	
	/**
	 * Получение названия номера по id
	 * @param int $roomId
	 * @return 
	 */
	public function getNameById($roomId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('name'=>$this->nameField))
			   ->where('full_id=?',$roomId);
		$rows = $this->fetchRow($select)->toArray();
		$rows = $this->resultToUtf($rows);
		return $rows['name'];
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField = 'room_eng';
		}
		else
		{
			$this->nameField='room';
		}
	}
}