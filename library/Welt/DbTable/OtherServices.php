<?php
class Welt_DbTable_OtherServices extends Welt_DbTableBase
{
    protected $_name = 'other_services';
    
    private $nameField;
    
    /**
     * Получение всех возможных типов услуг
     * @return mixed
     */
    public function getServices()
    {
        $this->setQueryFields();
        $fields = array(
            'id',
            'name'              =>      $this->nameField,
            'date'              =>      'DATE_FORMAT(date,"%d.%m.%Y")',
            'price',
        );
        $select = $this->select();
        $select->from(array($this->_name), $fields)
                ->where('disabled!=?', 1);
        $rows = $this->fetchAll($select)->toArray();
        return $rows;
    }
    
    /**
     * Получение данных по услуге
     * @return mixed
     */
    public function getService($serviceId)
    {
        $this->setQueryFields();
        $fields = array(
            'id',
            'nameLng'              =>      $this->nameField,
            'name',
            'name_eng',
            'date'              =>      'DATE_FORMAT(date,"%d.%m.%Y")',
            'price',
        );
        $select = $this->select();
        $select->from(array($this->_name), $fields)
                ->where('id=?', $serviceId);
        $row = $this->fetchRow($select);
        if ($row)
        {
            return $row->toArray();
        }
        return NULL;
    }
    
    
    /**
     * Установка полей для выборки в зависимости от языка
     */
    private function setQueryFields()
    {
        //Определяем поля для выборки из таблиц в зависимости от языка
        if($this->lang=='en') 
        {
            $this->nameField='name_eng';
        }
        else
        {
            $this->nameField='name';
        }
    }
    
}