<?php
class Welt_DbTable_TrainStations extends Welt_DbTable_BaseOrders
{
	protected $_name = 'train_stations';
	
	private $nameField;
	
	/**
	 * Получение списка ж/д вокзалов
	 * @return 
	 */
	public function getStations($city)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField))
			   ->where('city=?', $city);
			   //->where('id!=?', 14);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
}