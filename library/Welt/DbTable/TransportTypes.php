<?php
class Welt_DbTable_TransportTypes extends Welt_DbTableBase
{
	/*
	 * Id транспортного средства, на которое вешаются бесплатные услуги
	 */
	const freeTransportTypeId = 0;
	
	protected $_name = 'transport_types';
	
	private $nameField;
	private $carsField;
	
	/**
	 * Получение возможных типов транспорта
	 * @return mixed
	 */
	public function getTransportTypes($cityId = NULL)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField,'max_persons','cars'=>$this->carsField))
			   ->order(array('id ASC'));
		if (!is_null($cityId))
		{
			$select->where('city_id=?',$cityId);
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField = 'name_eng';
			$this->carsField = 'cars_eng';
		}
		else
		{
			$this->nameField='name';
			$this->carsField = 'cars';
		}
	}
}