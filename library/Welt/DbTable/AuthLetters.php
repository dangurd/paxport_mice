<?php
class Welt_DbTable_AuthLetters extends Welt_DbTableBase
{
	
	protected $_name = 'auth_letters';
	
	/**
	 * Получение списка авторизационных писем по заявке.
	 * @param int $appId
	 * @return 
	 */
	public function getAppAuthLetters($appId)
	{
		//Формируем запрос
		$select = $this->select();
		$select->from(array($this->_name))
			   ->where('application_id=?',$appId)
			   ->where('status=?',2)
			   ->order('created DESC');
		$rows = $this->fetchAll($select);
		return $rows->toArray();
	}
}
