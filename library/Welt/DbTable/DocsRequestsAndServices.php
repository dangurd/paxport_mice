<?php
class Welt_DbTable_DocsRequestsAndServices extends Welt_DbTableBase
{
	protected $_name = 'docs_requests_and_services';
	
	public function saveRequestService($appId,$requestId,$serviceData)
	{
		$data = array(
			'application_id'	=>	$appId,
			'service_id'		=>	$serviceData['id'],
			'service_sys_name'	=>	$serviceData['sys_name'],
			'request_id'		=>	$requestId,
			'created'			=>	Welt_Functions::getDateTimeMysql()
		);
		return $this->insert($data);
	}
}