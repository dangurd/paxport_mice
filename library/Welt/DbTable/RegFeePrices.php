<?php
class Welt_DbTable_RegFeePrices extends Welt_DbTableBase
{
	protected $_name = 'reg_fee_prices';
	
	private $regFeeTypeNameField;
	
	protected $priceField;
	
	
	/**
	 * Выборка цен на регистрационный сбор для заданного типа участника.
	 * Необходимо для формирования таблицы цен.
	 * @return mixed
	 */
	public function getRegFeePricesRow($personTypeId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('price' => $this->priceField))
			   ->where('person_type_id=?',$personTypeId)
			   ->order(array('person_type_id ASC'));
		$rows=$this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Определение стоимости регистрационного сбора для определённого типа участника
	 * на текущий день
	 * @param int $personTypeId
	 * @return int
	 */
	public function getPriceByPersonType($personTypeId)
	{
		$this->setQueryFields();
		$curDate = new Zend_Date();
		$curDateDb = $curDate->toString('yyyy-MM-dd');
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name),array('price' => $this->priceField, 'reg_fee_type_id'))
			   ->where('x.person_type_id=?',$personTypeId)
			   ->where('y.start_date<=?',$curDateDb)
			   ->where('y.stop_date>=?',$curDateDb)
			   ->joinLeft(array('y'=>'reg_fee_types'),'x.reg_fee_type_id=y.id',array());
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->priceField = 'price_en';
		}
		else
		{
			$this->priceField = 'price_ru';
		}
	}
	
}