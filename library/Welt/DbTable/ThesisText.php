<?php
class Welt_DbTable_ThesisText extends Welt_DbTableBase
{
    protected $_name = 'thesis_text';
    
    
    /**
     * Получение данных тезиса
     * @param string $thesisId
     */
    public function getThesisTextByThesisId($thesisId){
        $fields = array(
            'thesis_id',
            'name',
            'text',
        );
        $select = $this->select();
        $select->from('thesis_text')
        ->where('thesis_text.thesis_id = ?', $thesisId);
        
        $thesis = $this->fetchAll($select);
        return $thesis->toArray();
    }
}