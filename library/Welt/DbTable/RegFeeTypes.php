<?php
class Welt_DbTable_RegFeeTypes extends Welt_DbTableBase
{
	protected $_name = 'reg_fee_types';
	
	private $nameField;
	
	/**
	 * Получение возможных типов регистрации (ранняя, поздняя и т.д.)
	 * @return mixed
	 */
	public function getRegFeeTypes()
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField))
			   ->order(array('id ASC'));
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
}