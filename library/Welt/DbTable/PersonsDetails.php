<?php
class Welt_DbTable_PersonsDetails extends Welt_DbTableBase
{
    protected $_name = 'persons_details';

    /**
     * Получение деталей по персоне.
     * @param int $personId
     * @return array
     */
    public function getPersonDetails($personId)
    {
        $select = $this->select();
        $select->from(array($this->_name))
            ->where('person_id=?', $personId);
        return $this->fetchRow($select)->toArray();
    }

    public function getFirstPersonByUser($user_id)
    {

        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('pd'=>$this->_name))
            ->join(array('p'=>'persons'),'pd.person_id = p.id',array('fname','lname'))
            ->join(array('u'=>'users'),'u.id = p.user_id',array())
            ->where('u.id=?',$user_id)
            ->order('p.id ASC');

        $row = $this->fetchRow($select)->toArray();

        return $row;


    }


}