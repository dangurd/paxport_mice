<?php
class Welt_DbTable_OtherOrders extends Welt_DbTable_BaseOrders
{
    protected $_name = 'other_orders';
    
    private $nameField;
    
    /**
     * Получение информации о заказах на другие услуги в рамках заданной заявки
     * @param mixed $appId Id заявки
     * @param mixed $serviceStatuses Масив статусов услуг для отбора
     * @return 
     */
    public function getUserOtherOrders($appId,$serviceStatuses)
    {
        $this->setQueryFields();
        $fields = array(
            'id',
            'id_1c',
            'application_id',
            'status',
            'name'              =>  $this->nameField,
            'order_date'        => 'DATE_FORMAT(order_date,"%d.%m.%Y")',
            'date'              => 'DATE_FORMAT(date,"%d.%m.%Y")',
            'summ'
        );
        $select = $this->select();
        $select->from(array('x'=>$this->_name),$fields)
               ->where('x.application_id=?',$appId);
        //Если идёт выборка заказов по заданным статусам
        if (is_array($serviceStatuses) && sizeof($serviceStatuses)>0)
        {
            $select->where('x.status IN('.Welt_Functions::arrayToSqlWhere($serviceStatuses).')');
        }
        $rows = $this->fetchAll($select)->toArray();
        return $rows;
    }
    
    
    /**
     * Установка полей для выборки в зависимости от языка
     */
    private function setQueryFields()
    {
        //Определяем поля для выборки из таблиц в зависимости от языка
        if($this->lang=='en') 
        {
            $this->nameField = 'name_eng';
        }
        else
        {
            $this->nameField = 'name';
        }
    }
}