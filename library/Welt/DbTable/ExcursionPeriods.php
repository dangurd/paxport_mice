<?php
class Welt_DbTable_ExcursionPeriods extends Welt_DbTableBase
{
	protected $_name = 'excursion_periods';
	
	/**
	 * Получение дат (периодов), на которые возможен заказ экскурсии
	 * @param int $excursionId
	 * @return mixed
	 */
	public function getExcursionPeriods($excursionId)
	{
		$fields = array(
			'id',
			'excursion_id',
			'start_date'		=>	'DATE_FORMAT(start_date,"%d.%m.%Y")',
			'end_date'			=>	'DATE_FORMAT(end_date,"%d.%m.%Y")',
			'start_time'		=>	'TIME_FORMAT(start_time,"%H:%i")',
			'end_time'			=>	'TIME_FORMAT(end_time,"%H:%i")'
		);
		$select = $this->select();
		$select->from(array('x' => $this->_name),$fields)
			   ->where('excursion_id=?',$excursionId)
			   ->order('x.start_date ASC');
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение информации по указанному периоду
	 * @param int $periodId
	 * @return 
	 */
	public function getPeriodDetails($periodId)
	{
		$fields = array(
			'start_date'		=>	'DATE_FORMAT(start_date,"%d.%m.%Y")',
			'end_date'			=>	'DATE_FORMAT(end_date,"%d.%m.%Y")',
			'start_time'		=>	'TIME_FORMAT(start_time,"%H:%i")',
			'end_time'			=>	'TIME_FORMAT(end_time,"%H:%i")'
		);
		$select = $this->select();
		$select->from(array($this->_name),$fields)
			   ->where('id=?',$periodId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
}