<?php
class Welt_DbTable_Cities extends Welt_DbTableBase
{
	
	const MSK_ID = 'aa9c588d-b27c-11da-8d0d-505054503030';
	
	const SPB_ID = 'd2b8e814-b5b4-11da-8d0d-505054503030';
	
	protected $_name = 'cities';
	
	protected $nameField;
	
	/**
	 * Получить список городов
	 */
	public function getCities()
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id'=>'id','name'=>$this->nameField))
			   ->order($this->nameField . ' ASC');
		if ($_SERVER['REMOTE_ADDR'] != Zend_Registry::get('settings')->system->admin->ip)
		{
			$select->where('disabled!=?',1);
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
}