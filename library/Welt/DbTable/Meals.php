<?php
class Welt_DbTable_Meals extends Welt_DbTableBase
{
	protected $_name = 'meals';
	
	private $nameField;
	
	/**
	 * Получение всех возможных типов банкетов
	 * @return mixed
	 */
	public function getMeals()
	{
		$this->setQueryFields();
		$fields = array(
			'id',
			'name'				=>		$this->nameField,
			'incl_reg_fee',
			'date' 				=> 		'DATE_FORMAT(date,"%d.%m.%Y")',
			'price',
			'with_meal_types'
		);
		$select = $this->select();
		$select->from(array($this->_name), $fields);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	
	/**
	 * Получение стоимости банкета
	 * @param int $mealId Id банкета
	 * @return 
	 */
	public function getMealPrice($mealId)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('price'))
			   ->where('id=?',$mealId);
		$row = $this->fetchRow($select);
		return $row->price;
	}
	
	/**
	 * Получение всех банкетов, которые включены в регистрационный сбор
	 * @return 
	 */
	public function getIncludedMeals()
	{
		$select = $this->select();
		$select->from(array($this->_name),array('id','with_meal_types'))
			   ->where('incl_reg_fee=?',1);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
	
}