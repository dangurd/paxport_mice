<?php
class Welt_DbTable_HotelsRooms extends Welt_DbTable_BaseOrders
{
	protected $_name = 'hotels_rooms';
	
	private $nameField;
	
	/**
	 * Получение номеров для заданной гостиницы
	 * @param string $hotelId
	 * @return 
	 */
	public function getHotelsRooms($hotelId, $accomadation)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id'=>'id_1c','name'=>$this->nameField,'accomodation'))
			   ->where('hotel_id=?',$hotelId)
			   ->where('accomodation=?',$accomadation)
			   ->where('disabled!=?',1);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение названия номера по id_1c
	 * @param int $roomId
	 * @return 
	 */
	public function getNameById($roomId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('name'=>$this->nameField))
			   ->where('id_1c=?',$roomId);
		$row = $this->fetchRow($select)->toArray();
		$name = $row['name'];
		return $name;
	}
	
	/**
	 * Получение типа размещения для номера по id_1c
	 * @param int $roomId
	 * @return 
	 */
	public function getRoomAcc($roomId)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('accomodation'))
			   ->where('id_1c=?',$roomId);
		$row = $this->fetchRow($select);
		if ($row)
		{
			$row = $row->toArray();
			return $row['accomodation'];
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
	
}