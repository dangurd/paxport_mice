<?php
class Welt_DbTable_WeltHotelsAdditionalServices extends Welt_DbTableBase
{
	protected $_name = 'hotels_additional_services';
	protected $_db;
	private $serviceNameField;
	
	/**
	 * Выбор БД welt
	 * @return 
	 */
	protected function _setupDatabaseAdapter()
    {
         $this->_db = Zend_Registry::get('welt');
		 parent::_setupDatabaseAdapter();
    }
	
	/**
	 * Получение дополнительных услуг для гостиницы
	 * @param string $hotelId
	 * @return 
	 */
	public function getHotelsAdditionals($hotelId)
	{
		$this->setQueryFields();
		//Получаем укороченный id гостиницы на основе полного
		$hotelId = Welt_Hotels::getIdByFullId($hotelId);
		//Формируем запрос
		if ($this->lang == 'en')
		{
			$fields = array('service_value','service_value_eng');
		}
		else
		{
			$fields = array('service_value');
		}
		$select = $this->_db->select();
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'additional_services'),'y.id=x.service_id',array('service_name'=>$this->serviceNameField))
			   ->where('x.hotel_id=?',$hotelId)
			   ->where('x.service_value!=""')
			   ->order('y.order_id ASC');
		$rows = $this->_db->fetchAll($select);
		$services = $this->resultToUtf($rows);
		//Если язык системы - английский и нет английского значения услуги, то переводим русское значение
		if ($this->lang == 'en')
		{
			$translator = Zend_Registry::get('Zend_Translate');
			$servicesSize = sizeof($services);
			for ($i=0;$i<$servicesSize;$i++)
			{
				if (trim($services[$i]['service_value_eng'])=='')
				{
					$translateValue = $translator->translate($services[$i]['service_value']);
					$services[$i]['service_value'] = $translateValue;
				}
				else
				{
					$services[$i]['service_value'] = $services[$i]['service_value_eng'];
				}
			}
		}
		return $services;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->serviceNameField = 'service_name_eng';
		}
		else
		{
			$this->serviceNameField = 'service_name';
		}
	}
	
}