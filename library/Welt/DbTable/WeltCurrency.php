<?php
class Welt_DbTable_WeltCurrency extends Welt_DbTableBase
{
	protected $_name = 'currency';
	
	/**
	 * Выбор БД welt
	 * @return 
	 */
	protected function _setupDatabaseAdapter()
    {
         $this->_db = Zend_Registry::get('welt');
		 parent::_setupDatabaseAdapter();
    }
	
	/**
	 * Получение курса для заданной валюты
	 * @param string $currAlias
	 * @return 
	 */
	public function getRate($currAlias)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('rate'))
			   ->where('cur_alias=?',$currAlias);
		return $this->fetchRow($select)->rate;
	}
	
}