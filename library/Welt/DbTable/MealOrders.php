<?php
class Welt_DbTable_MealOrders extends Welt_DbTable_BaseOrders
{
	protected $_name = 'meal_orders';
	
	private $mealNameField;
	private $mealTypeNameField;
	
	
	/**
	 * Получение информации о заказах на банкеты в рамках заданной заявки
	 * @param int $appId
	 * @param mixed $serviceStatuses Масив статусов услуг для отбора
	 * @return 
	 */
	public function getUserMealOrders($appId,$serviceStatuses)
	{
		$this->setQueryFields();
		$yFields = array(
			'mealName' 			=> 	'y.'.$this->mealNameField,
			'date' 				=> 	'DATE_FORMAT(date,"%d.%m.%Y")',
			'incl_reg_fee',
			'with_meal_types'
		);
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name))
			   ->joinLeft(array('y'=>'meals'),'x.meal_id=y.id', $yFields)
			   ->joinLeft(array('z'=>'meal_types'),'x.meal_type_id=z.id',array('mealTypeName'=>'z.'.$this->mealTypeNameField))
			   ->where('application_id=?',$appId);
		//Если идёт выборка заказов по заданным статусам
		if (is_array($serviceStatuses) && sizeof($serviceStatuses)>0)
		{
			$select->where('status IN('.Welt_Functions::arrayToSqlWhere($serviceStatuses).')');
		}
		$rows=$this->fetchAll($select)->toArray();	
		return $rows;
	}
	
	/**
	 * Получение всех заказов на банкеты для заданной персоны в рамках заданной заявки со статусами 0,1 и 2
	 * @param int $appId
	 * @param int $personId
	 * @return 
	 */
	public function getPersonMealOrders($appId,$personId)
	{
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name))
			   ->joinLeft(array('y'=>'services_and_persons'),'x.id=y.service_id',array(''))
			   ->where('x.application_id=?',$appId)
			   ->where('x.status IN(0,1,2)')
			   ->where('y.person_id=?',$personId);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение детальной информации по заказанной услуге
	 * @param int $serviceId Id заказанной услуги
	 * @return 
	 */
	public function getOrderDetails($serviceId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'summ',
			'meal_id'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'meals'),'x.meal_id=y.id',array(
			   		'mealName'=>$this->mealNameField,
					'incl_reg_fee',
					'date' => 'DATE_FORMAT(date,"%d.%m.%Y")'
				))
			   ->joinLeft(array('z'=>'meal_types'),'x.meal_type_id=z.id',array('mealTypeName'=>$this->mealTypeNameField))
			   ->where('x.id=?',$serviceId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->mealNameField = 'name_eng';
			$this->mealTypeNameField = 'name_eng';
		}
		else
		{
			$this->mealNameField='name';
			$this->mealTypeNameField = 'name';
		}
	}
}