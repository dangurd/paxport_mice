<?php
class Welt_DbTable_Confirmations extends Welt_DbTableBase
{
	
	protected $_name = 'confirmations';
	
	/**
	 * Получение последнего подтверждения для заданной услуги
	 * @param int $serviceId
	 * @param string $serviceSysName
	 * @return 
	 */
	public function getLastConf($serviceId,$serviceSysName)
	{
		//Формируем запрос
		$select = $this->select();
		$select->from(array($this->_name),array('id','link','status','created'))
			   ->where('service_id=?',$serviceId)
			   ->where('service_sys_name=?',$serviceSysName)
			   ->where('status=?',2)
			   ->order('created DESC')
			   ->limit(1,0);
		$row = $this->fetchRow($select);
		if ($row)
		{
			return $row->toArray();
		}
		else
		{
			return $row;
		}
	}
	
}