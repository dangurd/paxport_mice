<?php
class Welt_DbTable_ThesisAuthors extends Welt_DbTableBase
{
    protected $_name = 'thesis_authors';
    
    
    /**
     * Получение данных тезиса
     * @param string $thesisId
     */
    public function getThesisAuthorsByThesisId($thesisId)
    {
        $fields = array(
            'id',
            'thesis_id',
            'person_id',
            'fname',
            'lname',
            'workspace',
            'position',
            'country',
            'city',
        );
        $select = $this->select();
        $select->from($this->_name, $fields)
            ->order('person_id DESC')
        ->where('thesis_authors.thesis_id = ?', $thesisId);
        
        $thesis = $this->fetchAll($select);
        return $thesis->toArray();
    }
    
    public function getAuthorsIds($thesisId)
    {
        $field = array(
          'id',  
        );
        $select = $this->select();
        $select->from($this->_name, $field)
        ->where('thesis_id = ?', $thesisId)
        ->where('person_id = 0');
        $authorsIds = $this->fetchAll($select);
        return $authorsIds->toArray();
    }
}