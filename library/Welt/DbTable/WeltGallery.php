<?php
class Welt_DbTable_WeltGallery extends Welt_DbTableBase
{
	protected $_name = 'gallery';
	
	/**
	 * Выбор БД welt
	 * @return 
	 */
	protected function _setupDatabaseAdapter()
    {
         $this->_db = Zend_Registry::get('welt');
		 parent::_setupDatabaseAdapter();
    }
	
	/**
	 * Получение фоток данного номера
	 * @param string $currAlias
	 * @return 
	 */
	public function getRoomPhotos($room_id)
	{
		$select = $this->select();
		$select->from(array($this->_name))
			   ->where('room_id=?',$room_id);
		return $this->fetchAll($select)->toArray();
	}
	
}