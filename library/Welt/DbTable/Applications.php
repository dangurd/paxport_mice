<?php
class Welt_DbTable_Applications extends Welt_DbTableBase
{
	protected $_name = 'applications';
	
	/**
	 * Переопределяем функцию insert (добавляем дату создания заявки)
	 * @param array $data
	 * @return 
	*/
	public function insert(array $data)
    {
        if (empty($data['created']))
		{
            $data['created'] = Welt_Functions::getDateTimeMysql();
        }
		//Сборка мусора
		$this->deleteOld();
		return parent::insert($data);
    }
	
	
	/**
	 * Получение информациии о заявке участника
	 * @param int $userId
	 * @return 
	 */
	public function getUserApp($userId)
	{
		$select = $this->select();
		$select->from(array($this->_name))
			   ->where('user_id=?',$userId);
		$row = $this->fetchRow($select);
		if ($row)
		{
			$appData = $row->toArray();
		}
		else
		{
			$appData = false;
		}
		return $appData;
	}
	
	/**
	 * Получение заявок, для которых ещё не отправлены данные организатору
	 * @return 
	 */
	public function getAppsForSend()
	{
		$select = $this->select();
		$select->from(array($this->_name),array('id','user_id'))
			   ->where('data_sended=?',0)
			   ->where('complete=?',1)
			   ->where('status=?',2);
		$row = $this->fetchAll($select)->toArray();
		return $row;
	}
	
	/**
	 * Получение краткой информации по заявкам
	 * @return
	 */
	public function getAppsShortInfo($appsIds)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('id','user_id'))
			   ->where('id IN('.Welt_Functions::arrayToSqlWhere($appsIds).')');
		$row = $this->fetchAll($select)->toArray();
		return $row;
	}
	
	/**
	 * Получение всех завершённых заявок
	 * @return 
	 */
	public function getCompleteApps()
	{
		$select = $this->select();
		$select->from(array($this->_name),array('id','user_id'))
			   ->where('complete=?',1);
		$row = $this->fetchAll($select)->toArray();
		return $row;
	}
	
	/**
	 * Получение списка изменённых заявок за указанный период
	 * @param string $sdate Дата начала периода в формате mysql date
	 * @param string $edate Дата окончания периода в формате mysql date
	 * @return 
	 */
	public function geUpdatedApps($sdate, $edate)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('DISTINCT(id) as app'))
			   ->where('(updated BETWEEN ' . $this->getAdapter()->quote($sdate) . ' AND ' . $this->getAdapter()->quote($edate) . ')')
			   ->where('status>?', 1)
			   //Чтобы в синхронизацию не попадали только что созданные заявки
			   ->where('ROUND(TIME_TO_SEC(TIMEDIFF(updated, finished))) > ?', Welt_Applications::appProcessTime)
			   ->where('data_sended = ?', 1);
		$rows = $this->fetchAll($select);
		if (is_null($rows))
		{
			return false;
		}
		$apps = array();
		foreach ($rows as $row)
		{
			$apps[] = $row['app'];
		}
		return $apps;
	}
	
	
	/**
	 * Сборка мусора
	 * @return 
	 */
	public function deleteOld()
	{
		//Время жизни незавершённой заявки в часах
		$lifetime = Zend_Registry::get('settings')->reg->appLifetime;
		//Текущая дата
		$curDate = new Zend_Date();
		$curDate->sub($lifetime,Zend_Date::HOUR);
		$settings = Zend_Registry::get('settings');
		//Если включен режим предварительной регистрации
		if ($settings->reg->type == 1)
		{
			/*
			 * Определяем устаревшие и незавершённые заявки, удаляем их
			 */
			$where[] = $this->getAdapter()->quoteInto('created<?',$curDate->toString('yyyy-MM-dd HH:mm:ss'));
			$where[] = $this->getAdapter()->quoteInto('complete=?',0);
			$this->delete($where);
		}
		//Если регистрация открытая
		else
		{
			/*
			 * Определяем устаревшие записи, удаляем их, удаляем пользователя, для которого создана заявка
			 */
			$usersTable = new Welt_DbTable_Users();
			$select = $this->select();
			$select->from(array($this->_name),array('id','user_id'))
				   ->where('created<?',$curDate->toString('yyyy-MM-dd HH:mm:ss'))
				   ->where('complete=?',0);
			$rowset = $this->fetchAll($select);
			foreach ($rowset as $row)
			{
				$usersWhere = $usersTable->getAdapter()->quoteInto('id=?',$row->user_id);
				$usersTable->delete($usersWhere);
				$appWhere = $usersTable->getAdapter()->quoteInto('id=?',$row->id);
				$this->delete($appWhere);
			}
		}
	}
}