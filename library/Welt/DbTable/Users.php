<?php
class Welt_DbTable_Users extends Welt_DbTableBase
{
	protected $_name = 'users';
	
	/**
	 * Проверка существования пользователя по email
	 * @param object $email
	 * @return 
	 */
	public function checkByEmail($email)
	{
		$select=$this->select();
		$select->from(array($this->_name),array('COUNT(*) AS count'))
			   ->where('email = ?',$email)
			   ->limit(1,0);
		return $this->fetchRow($select)->count;
	}
	
	
	/**
	 * Выборка данных по пользователю с поиском по email
	 * @param string $email
	 * @return 
	 */
	public function getDataByEmail($email)
	{
		$select=$this->select();
		$select->from(array($this->_name),array('fio'=>'CONCAT_WS(" ",title,fname,lname)','password'))
			   ->where('email = ?',$email);
		$rows = $this->fetchRow($select)->toArray(); 
		return $rows;
	}
	
	
	/**
	 * Получение информации по пользователю
	 * @param int $userId
	 * @param int $type [optional]
	 * @return 
	 */
	public function getUserInfo($userId,$type=0)
	{
		$select=$this->select();
		if ($type==1)
		{
			$fields = array('keystring','email');
			$select->from(array($this->_name),$fields);
		}
		elseif ($type==2)
		{
			$fields = array('lang');
			$select->from(array($this->_name),$fields);
		}
		else
		{
			$select->from(array($this->_name));
		}
		$select->where('id = ?',$userId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	/**
	 * Получение списка заявок, в которых есть изменёния по данным пользователя, за указанный период
	 * @param string $sdate Дата начала периода в формате mysql date
	 * @param string $edate Дата окончания периода в формате mysql date
	 * @return 
	 */
	public function geUpdatedApps($sdate, $edate)
	{
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x' => $this->_name), array())
			   ->joinLeft(array('y' => 'applications'), 'x.id=y.user_id', array('DISTINCT(y.id) as app'))
			   ->where('(x.updated BETWEEN ' . $this->getAdapter()->quote($sdate) . 'AND ' . $this->getAdapter()->quote($edate) . ')')
			   ->where('x.email IS NOT NULL')
			   ->where('x.created < ?', $sdate);
		$rows = $this->fetchAll($select);
		if (is_null($rows))
		{
			return false;
		}
		$apps = array();
		foreach ($rows as $row)
		{
			$apps[] = $row['app'];
		}
		return $apps;
	}
	
	
	
	/**
	 * Переопределяем функцию insert (добавляем дату создания)
	 * @param array $data
	 * @return 
	*/
	public function insert(array $data)
    {
        if (empty($data['created']))
		{
            $data['created'] = Welt_Functions::getDateTimeMysql();
        }
		return parent::insert($data);
    }
	
	/**
	 * Добавление пустого пользователя - требуется на 1 шаге регистрации
	 * @return 
	 */
	public function insertEmptyUser()
	{
		$data = array(
			'changed'		=>	0,
			'browser'		=>	Welt_Functions::userBrowser($_SERVER['HTTP_USER_AGENT']),
			'ip'			=>	$_SERVER['REMOTE_ADDR']
		);
		return $this->insert($data);
	}
	
	/**
	 * Переопределяем функцию update (добавляем по умолчанию флаг changed=1 в строки)
	 * @param array $data
	 * @param string $where
	 * @return 
	*/
	public function update(array $data, $where)
    {
        if (!isset($data['changed']))
		{
            $data['changed'] = 1;
        }
        return parent::update($data, $where);
    }
	
	
}