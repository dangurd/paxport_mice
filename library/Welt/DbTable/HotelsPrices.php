<?php
class Welt_DbTable_HotelsPrices extends Welt_DbTable_BaseOrders
{
	protected $_name = 'hotels_prices';
	
	
	/**
	 * Получение списка прайсов для номера
	 * @param string $room - id номера
	 * @param string $startDate - дата начала периода
	 * @param string $stopDate - дата окончания периода
	 * @return 
	 */
	public function getRoomPrices($roomId,$startDate,$stopDate)
	{
		$fields = array(
			'date'				=>	'DATE_FORMAT(date,"%d.%m.%Y")',
			'price1'			=>	'ROUND(price1)',
			'price2'			=>	'ROUND(price2)',
			'price3'			=>	'ROUND(price3)',
			'breakfast_incl',
			'breakfast_price'	=>	'ROUND(breakfast_price)'
		);
		$select = $this->select();
		$select->from(array($this->_name),$fields)
			   ->where('room_id = ?',$roomId)
			   ->where('date <= ?',Welt_Functions::dateToMysql($stopDate))
			   ->where('date >= ?',Welt_Functions::dateToMysql($startDate))
			   ->order($this->_name.'.date ASC');
		$prices = $this->fetchAll($select)->toArray();
		return $prices;
	}
	
}