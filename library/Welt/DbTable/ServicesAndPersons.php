<?php
class Welt_DbTable_ServicesAndPersons extends Welt_DbTableBase
{
	protected $_name = 'services_and_persons';
	
	/**
	 * Получение персон, для которых заказана услуга с указанного типа с указанным Id
	 * @param int $serviceId
	 * @param int $serviceType
	 * @return 
	 */
	public function getServicePersons($serviceId,$serviceType)
	{
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name),array(''))
			   ->joinLeft(array('y'=>'persons'),'x.person_id=y.id',array('id','name'=>'CONCAT(fname," ",lname)'))
			   ->where('x.service_id=?',$serviceId)
			   ->where('service_sys_name=?', $serviceType);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение массива из id персон, для которых заказаны услуги указанного типа в рамках указанной заявки
	 * @param int $appId Id заявки
	 * @param string $serviceType Тип услуги
	 * @return 
	 */
	public function getServicesPersonsByType($appId,$serviceType)
	{
		$select = $this->select();
		$select->from(array('x'=>$this->_name),array('person_id'))
			   ->where('application_id=?',$appId)
			   ->where('service_sys_name=?', $serviceType);
		$rows = $this->fetchAll($select)->toArray();
		$persons = array();
		foreach($rows as $row)
		{
			$persons[] = $row['person_id'];
		}
		return $persons;
	}
	
	/**
	 * Подсчёт количества заказанных услуг и персон в рамках заявки
	 * @param int $appId
	 * @return 
	 */
	public function getAppServAndPersCount($appId)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('servicesCount'=>'COUNT(*)','personsCount'=>'COUNT(DISTINCT(person_id))'))
			   ->where('application_id=?',$appId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	/**
	 * Получение списка из id услуг, которые заказаны для указанной персоны, возможно указание типа услуги
	 * @param int $personId
	 * @param string $serviceType [optional]
	 * @return 
	 */
	public function getPersonServices($personId,$serviceType='')
	{
		$select = $this->select();
		$select->from(array($this->_name),array('service_id'))
			   ->where('person_id=?',$personId);
		if (isset($serviceType) && $serviceType!='')
		{
			$select->where('service_sys_name=?',$serviceType);
		}
		$rows = $this->fetchAll($select)->toArray();
		$services = array();
		foreach($rows as $row)
		{
			$services[] = $row['service_id'];
		}
		return $services;
	}
	
	/**
	 * Получение всех связей услуг с персонами для указанной заявки
	 * @param int $appId
	 * @return 
	 */
	public function getAppServicesAndPersons($appId)
	{
		$select = $this->select();
		$select->from(array($this->_name))
			   ->where('application_id=?',$appId);
		$row = $this->fetchAll($select)->toArray();
		return $row;
	}
	
	/**
	 * Проверка, есть ли заказы услуг (всех  типов или заданного типа) на указанную персону
	 * @param int $personId
	 * @param string $serviceSysName
	 * @return 
	 */
	public function checkPersonServicesOrders($personId,$serviceSysName='all')
	{
		$select = $this->select();
		$select->from(array($this->_name),array('count'=>'COUNT(*)'))
			   ->where('person_id=?',$personId);
		//Добавляем ограничение по типу услуги, если нужно
		if ($serviceSysName!='all')
		{
			$select->where('service_sys_name=?',$serviceSysName);
		}
		return $this->fetchRow($select)->count;
	}
	
}