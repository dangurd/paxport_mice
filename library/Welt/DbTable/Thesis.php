<?php
class Welt_DbTable_Thesis extends Welt_DbTableBase
{
	protected $_name = 'thesis';
	
	
	
	/*
	$select=$this->select();
		$select->from(array($this->_name),array('COUNT(*) AS count'))
			   ->where('email = ?',$email)
			   ->limit(1,0);
		return $this->fetchRow($select)->count;
	*/
	public function checkThesis($userId) {
		if(is_null ($userId)) {
			$result['result'] = false;
			$result['error'] = 'No user ID';
			return $result;
		}
		$fields = array('id', 'user_id');
		$select = $this->select();
		$select->from($this->_name, $fields)
			   ->where('user_id = ?', $userId);
		return $this->fetchRow($select);
		
	}
	
	public function getThesisListByUserId($userId)
	{
	    
	    
	    $fields = array(
	        'id',
	        'user_id',
	        'person_id',
	        'created',
	        'is_ready',
	        'is_applied',
	        'recensor_id',
	        'applied_date',
	        'comment',
	        'is_verbal',
	        'verbal_date',
	        'is_publishied',
	        'is_paid',
	    );
	    $select = $this->select();
	    $select->from($this->_name)
	           ->where('user_id = ?', $userId);
	    $thesisList = $this->fetchAll($select);
	    return $thesisList->toArray();
	}
	
	public function getThesisById($thesisId) {
	    
	    $fields = array(
	        'id',
	        'user_id',
	        'person_id',
	        'created',
	        'is_ready',
	        'is_applied',
	        'recensor_id',
	        'applied_date',
	        'comment',
	        'is_verbal',
	        'verbal_date',
	        'is_publishied',
	        'is_paid',
	    );
	    $select = $this->select();
		$select->from(array($this->_name))
			   ->where('id = ?', $thesisId);
	   $thesis = $this->fetchRow($select);
		return $thesis->toArray();
	}
	
	
	public function addThesis ($userId, $status, $isReady) {
		//$date = date('Y-m-d h:i:s');
		$date = Welt_Functions::getDateTimeMysql();
		$data = array (
		'user_id' => $userId,
		'created' => $date,
		'is_applied' => $status,
		'is_ready' => $isReady,
		);
		$this->insert($data);
		$select->from(array($this->_name))
			   ->where('user_id = ?', $userId);
		return $this->fetchRow($select);
		
	}
	
	public function getUserSymposiums ($userId)
	{
	    $select = $this->select();
	    $select->from(array('x' => $this->_name),array('id'))
	           //->joinLeft(array('y' => 'thesis_text'), 'x.id = y.thesis_id', array('name' => 'y.name'))
	           ->where('x.type = ?', 3)
	           ->where('x.user_id = ?', $userId);
	    $symposiums = $this->fetchAll($select);
	    return $symposiums->toArray();
	}
	
	public function updateThesis ($thesisId, $isReady, $isApplied) {
		/* $q = 'UPDATE thesis SET is_ready = "'.$isReady.'", is_applied = "'.$isApplied.'" WHERE id = "'.$thesisId.'"';
		
		$update = $this->conn($q); */
		
	}
}