<?php
class Welt_DbTable_PersonsDocuments extends Welt_DbTableBase
{
	protected $_name = 'persons_documents';
	
	/**
	 * Получение информации по загруженным для персоны документам
	 * @param int $personId
	 * @return 
	 */
	public function getPersonDocsList($personId)
	{
		$fields = array('id','person_id','file_length','extension','upload_date');
		$select = $this->select();
		$select->from(array($this->_name),$fields)
			   ->where('person_id=?',$personId);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Подсчёт количества файлов для персоны
	 * @param int $personId
	 * @return 
	 */
	public function getPersonFilesCount($personId)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('count'=>'COUNT(*)'))
			   ->where('person_id=?',$personId);
		$rows = $this->fetchRow($select)->count;
		return $rows;
	}
	
	/**
	 * Переопределяем функцию insert (добавляем дату загрузки файла и флаг для ТМ)
	 * @param array $data
	 * @return 
	*/
	public function insert(array $data)
    {
        if (empty($data['upload_date']))
		{
            $data['upload_date'] = Welt_Functions::getDateTimeMysql();
        }
		$data['status'] = 0;
		return parent::insert($data);
    }
}