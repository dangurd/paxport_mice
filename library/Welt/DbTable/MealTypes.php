<?php
class Welt_DbTable_MealTypes extends Welt_DbTableBase
{
	protected $_name = 'meal_types';
	
	private $nameField;
	
	/**
	 * Получение типов блюд для выбранного типа банкета
	 * @return mixed
	 */
	public function getMealTypes($mealId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField))
			   ->where('meal_id=?',$mealId);
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
	
}