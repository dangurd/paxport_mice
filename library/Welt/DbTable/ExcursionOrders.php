<?php
class Welt_DbTable_ExcursionOrders extends Welt_DbTable_BaseOrders
{
	protected $_name = 'excursion_orders';
	
	private $nameField;
	
	
	/**
	 * Получение информации о заказах на экскурсии в рамках заданной заявки
	 * @param int $appId
	 * @param mixed $serviceStatuses Масив статусов услуг для отбора
	 * @return 
	 */
	public function getUserExcursionOrders($appId,$serviceStatuses)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'id',
			'id_1c',
			'application_id',
			'status',
			'order_date',
			'excursion_id',
			'start_date'		=>	'DATE_FORMAT(start_date,"%d.%m.%Y")',
			'end_date'			=>	'DATE_FORMAT(end_date,"%d.%m.%Y")',
			'start_time'		=>	'TIME_FORMAT(start_time,"%H:%i")',
			'end_time'			=>	'TIME_FORMAT(end_time,"%H:%i")',
			'summ',
			'comment',
			'updated'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'excursions'),'x.excursion_id=y.id',array('name'=>'y.'.$this->nameField))
			   ->where('x.application_id=?',$appId);
		//Если идёт выборка заказов по заданным статусам
		if (is_array($serviceStatuses) && sizeof($serviceStatuses)>0)
		{
			$select->where('x.status IN('.Welt_Functions::arrayToSqlWhere($serviceStatuses).')');
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	
	/**
	 * Получение детальной информации по заказанной услуге
	 * @param int $serviceId Id заказанной услуги
	 * @return 
	 */
	public function getOrderDetails($serviceId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'excursion_id',
			'start_date'		=>	'DATE_FORMAT(start_date,"%d.%m.%Y")',
			'end_date'			=>	'DATE_FORMAT(end_date,"%d.%m.%Y")',
			'start_time'		=>	'TIME_FORMAT(start_time,"%H:%i")',
			'end_time'			=>	'TIME_FORMAT(end_time,"%H:%i")',
			'summ',
			'comment'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'excursions'),'x.excursion_id=y.id',array('name'=>$this->nameField))
			   ->where('x.id=?',$serviceId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField= 'name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
}