<?php
class Welt_DbTable_PaymentAndServices extends Welt_DbTableBase
{
	protected $_name = 'payment_and_services';
	
	/**
	 * Проверка, определены ли методы оплаты для услуг в рамках данной заявки
	 * @param int $appId
	 * @return 
	 */
	public function checkServicesPayment($appId)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('count'=>'COUNT(*)'))
			   ->where('application_id=?',$appId);
		$count = $this->fetchRow($select)->count;
		if($count>0) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	/**
	 * Получение выбранного для услуги метода оплаты
	 * @param int $serviceId
	 * @param string $serviceSysName
	 * @return 
	 */
	public function getServicePaymentMethod($serviceId,$serviceSysName)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('method_sys_name'))
			   ->where('service_id=?',$serviceId)
			   ->where('service_sys_name=?',$serviceSysName);
		$row = $this->fetchRow($select);
		if ($row)
		{
			return $row->method_sys_name;
		}
		else
		{
			return '';
		}
	}
	
	
	/**
	 * Определение методов оплаты для услуги
	 * @param int $appId
	 * @param array $paymentServiceData
	 * @return 
	 */
	public function setServicePayment($appId,$paymentService)
	{
		$paymentAndServicesTable = new Welt_DbTable_PaymentAndServices();
		$paymentServiceData = array(
			'application_id'		=>	$appId,
			'method_sys_name'		=>	$paymentService['method_sys_name'],
			'service_id'			=>	$paymentService['service_id'],
			'service_sys_name'		=>	$paymentService['service_sys_name'],
			'created'				=>	Welt_Functions::getDateTimeMysql()
		);
		return $this->insert($paymentServiceData);
	}
	
}