<?php
class Welt_DbTable_VisaTypes extends Welt_DbTable_BaseOrders
{
	/**
	 * День, в который меняется стоимость визы
	 */
	const visaChangeDate = '00.00.0000';
	
	protected $_name = 'visa_types';
	
	private $nameField,$priceField;
	
	/**
	 * Получение информации о доступных типах визовой поддержки
	 * @return 
	 */
	public function getVisaTypes()
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array('x'=>$this->_name),array('id','name'=>$this->nameField,'price'=>$this->priceField,'is_free'));
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение информации о доступных типах визовой поддержки
	 * @return 
	 */
	public function getVisaTypeInfo($visaTypeId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array('x'=>$this->_name),array('id','name'=>$this->nameField,'price'=>$this->priceField,'is_free'))
			   ->where('id=?',$visaTypeId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	/**
	 * Определение стоимости визовой поддержки по id указанного типа
	 * @param int $visaTypeId
	 * @return 
	 */
	public function getVisaTypePrice($visaTypeId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array('x'=>$this->_name),array('price'=>$this->priceField))
			   ->where('id=?',$visaTypeId);
		$row = $this->fetchRow($select)->toArray();
		return $row['price'];
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка, стоимость в зависимости от текущего дня
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
		if (self::visaChangeDate != '00.00.0000')
		{
			//Определяем поле со стоимостью визы в зависимости от текущего дня
			$curDate = new Zend_Date();
			$visaChangeDate = new Zend_Date(self::visaChangeDate,'dd.MM.yyyy');
			//Добавляем к дате маскимально возможные час и минуту (для корректного сравнения дат)
			$visaChangeDate->add('23:59:59', Zend_Date::TIMES);
			$curDateUnix = (int) $curDate->toString('U');
			$visaChangeDateUnix = (int) $visaChangeDate->toString('U');
			if ($curDateUnix >= $visaChangeDateUnix)
			{
				$this->priceField = 'ROUND(price2)';
			}
			else
			{
				$this->priceField = 'ROUND(price1)';
			}
		}
		else
		{
			$this->priceField = 'ROUND(price1)';
		}
	}
	
}