<?php
class Welt_DbTable_BaseOrders extends Welt_DbTableBase
{
	/**
	 * Выборка id всех заказанных услуг в рамках заданной заявки
	 * @param int $appId
	 * @return 
	 */
	public function getUserOrdersIds($appId)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('id'))
			   ->where('application_id=?',$appId);
		return $this->fetchAll($select)->toArray();
	}
	
	
	/**
	 * Поиск всех услуг с заданным статусом и изменение его на указанный
	 * @param int $appId
	 * @param int $findStatus
	 * @param int $newStatus
	 * @return 
	 */
	public function setAppServicesOrdersStatus($appId,$findStatus,$newStatus)
	{
		$where[] = $this->getAdapter()->quoteInto('application_id = ?', $appId);
		$where[] = $this->getAdapter()->quoteInto('status = ?', $findStatus);
		$data = array('status'=>$newStatus);
		return $this->update($data,$where);
	}
	
	/**
	 * Получение списка заявок, в которых есть изменённые услуги, за указанный период
	 * @param string $sdate Дата начала периода в формате mysql date
	 * @param string $edate Дата окончания периода в формате mysql date
	 * @return 
	 */
	public function geUpdatedApps($sdate, $edate)
	{
		$select = $this->select();
		$select->from(array('x' => $this->_name),array('DISTINCT(application_id) as app'))
			   ->joinLeft(array('y' => 'applications'), 'x.application_id=y.id', array())
			   ->where('(x.updated BETWEEN ' . $this->getAdapter()->quote($sdate) . 'AND ' . $this->getAdapter()->quote($edate) . ')')
			   ->where('x.status>?', 1)
			   //Чтобы в синхронизацию не попадали только что созданные заявки
			   ->where('ROUND(TIME_TO_SEC(TIMEDIFF(x.updated, y.finished))) > ?', Welt_Applications::appProcessTime)
			   ->where('y.data_sended = ?', 1);
		$rows = $this->fetchAll($select);
		if (is_null($rows))
		{
			return false;
		}
		$apps = array();
		foreach ($rows as $row)
		{
			$apps[] = $row['app'];
		}
		return $apps;
	}
	
	
	/**
	 * Переопределяем функцию insert (добавляем дату заказа)
	 * @param array $data
	 * @return 
	*/
	public function insert(array $data)
    {
        if (empty($data['order_date']))
		{
            $data['order_date'] = Welt_Functions::getDateTimeMysql();
        }
		return parent::insert($data);
    }
}