<?php
class Welt_DbTable_WeltCountries extends Welt_DbTableBase
{
	const russiaId = 'd2b8e812-b5b4';
	
	protected $_name = 'countries';
	
	private $nameField;
	
	/**
	 * Выбор БД welt
	 * @return 
	 */
	protected function _setupDatabaseAdapter()
    {
         $this->_db = Zend_Registry::get('welt');
		 parent::_setupDatabaseAdapter();
    }
	
	/**
	 * Получение списка стран
	 * @return 
	 */
	public function getCountriesList()
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField))
			   //Попросили убрать Afganistan :) 
			   ->where('id NOT IN("fb349b46-a245")')
			   ->order($this->nameField.' ASC');
		$rows = $this->fetchAll($select)->toArray();
		return $this->resultToUtf($rows);
	}
	
	/**
	 * Получение данных по указанным странам
	 */
	public function getCountries($countriesIds, $idsAsKeys = true)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField))
			   ->where('id IN(' . Welt_Functions::arrayToSqlWhere($countriesIds) . ')');
		$rows = $this->fetchAll($select)->toArray();
		$rows = $this->resultToUtf($rows);
		if ($idsAsKeys === true)
		{
			$persons = array();
			foreach($rows as $row)
			{
				$persons[$row['id']] = $row['name'];
			}
			$rows = $persons;
		}
		return $rows;
	}
	
	/**
	 * Получение названия страны по id
	 * @return 
	 */
	public function getCountryById($countryId)
	{
		if ($countryId == '00000000-0000')
		{
			return '';
		}
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('name'=>$this->nameField))
			   ->where('id = ?', $countryId);
		$row = $this->fetchRow($select)->toArray();
		$row = $this->resultToUtf($row);
		return $row['name'];
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField = 'country_name_eng';
		}
		else
		{
			$this->nameField='country_name';
		}
	}
}
