<?php
class Welt_DbTable_PersonsTypes extends Welt_DbTableBase
{
	protected $_name = 'persons_types';
	private $nameField;
	
	/**
	 * Получение возможных форм участия (участник, сопровождающее лицо и т.д.)
	 * @return mixed
	 */
	public function getPersonTypes()
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField))
			   ->where('visible=?', 1)
			   ->order(array('id ASC'));
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
	
}