<?php
class Welt_DbTable_RegFeeOrders extends Welt_DbTable_BaseOrders
{
	protected $_name = 'reg_fee_orders';
	
	private $regFeeTypeNameField;
	
	/**
	 * Получение информации о заказах на регистрационный сбор в рамках заданной заявки
	 * @param int $appId
	 * @param mixed $serviceStatuses Масив статусов услуг для отбора
	 * @return 
	 */
	public function getUserRegFeeOrders($appId,$serviceStatuses)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name))
			   ->joinLeft(array('y'=>'reg_fee_types'),'x.reg_fee_type_id=y.id',array('regFeeTypeName'=>'y.'.$this->regFeeTypeNameField))
			   ->where('application_id=?',$appId);
		//Если идёт выборка заказов по заданным статусам
		if (is_array($serviceStatuses) && sizeof($serviceStatuses)>0)
		{
			$select->where('status IN('.Welt_Functions::arrayToSqlWhere($serviceStatuses).')');
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение детальной информации по заказанной услуге
	 * @param int $serviceId Id заказанной услуги
	 * @return 
	 */
	public function getOrderDetails($serviceId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'summ'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'reg_fee_types'),'x.reg_fee_type_id=y.id',array('regFeeTypeName'=>'y.'.$this->regFeeTypeNameField))
			   ->where('x.id=?',$serviceId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->regFeeTypeNameField = 'name_eng';
		}
		else
		{
			$this->regFeeTypeNameField = 'name';
		}
	}
}