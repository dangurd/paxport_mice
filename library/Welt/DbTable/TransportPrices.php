<?php
class Welt_DbTable_TransportPrices extends Welt_DbTableBase
{
	protected $_name = 'transport_prices';
	
	/**
	 * Параметры ночного коэффициента на транспортные услуги для СПб
	 * @var array
	 */
	public static $spbPricesRatio = array(
		'sTime' => '06:00:00',
		'eTime' => '23:00:00',
		'ratio' => 1.5
	);
	
	/**
	 * Выборка цен для всех типов транспорта или для заданного типа транспорта.
	 * Необходимо для формирования таблицы цен.
	 * @param int $transportServiceId [optional]
	 * @return mixed
	 */
	public function getTransportPrices($cityId = NULL, $transportServiceId=0)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name),array('price'=>'ROUND(price)'))
			   ->joinLeft(array('y'=>'transport_services'),'x.transport_service_id=y.id',array(''))
			   ->where('y.is_free!=?',1)
			   ->order(array('y.'.$this->transpServiceNameField.' ASC'))
			   ->order(array('x.transport_type_id ASC'));
		//Если необходима строка для конкретного типа транспорта, добавляем условие
		if ($transportServiceId!=0)
		{
			 $select->where('transport_service_id=?',$transportServiceId);
		}
		if (!is_null($cityId))
		{
			$select->where('y.city_id=?',$cityId);
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Определение цены для заданных типа транспорта и услуги
	 * @param int $transportServiceId
	 * @param int $transportTypeId
	 * @return int
	 */
	public function getCurPrice($transportServiceId,$transportTypeId)
	{
		$select = $this->select();
		$select->from(array($this->_name),array('price'=>'ROUND(price)'))
			   ->where('transport_service_id=?',$transportServiceId)
			   ->where('transport_type_id=?',$transportTypeId);
		$row = $this->fetchRow($select)->toArray();
		return (int)$row['price'];
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->transpServiceNameField = 'name_eng';
		}
		else
		{
			$this->transpServiceNameField = 'name';
		}
	}
}
?>