<?php
class Welt_DbTable_Hotels extends Welt_DbTableBase
{
	protected $_name = 'hotels';
	
	private $nameField;
	private $noteField;
	
	/**
	 * Получение информации о гостиницах
	 * @return 
	 */
	public function getHotels($cityId = NULL)
	{
		$this->setQueryFields();
        $fields = array(
            'id' => 'id_1c',
            'name' => $this->nameField,
            'stars',
            'city_id',       
        );
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x' => $this->_name), $fields)
               ->joinLeft(array('y'=>'hotels_details'),'x.id_1c = y.hotel_id',array('priceNote' => $this->priceNoteField))
			   ->order('order ASC')
			   ->order($this->nameField.' ASC');
		if ($_SERVER['REMOTE_ADDR'] != Zend_Registry::get('settings')->system->admin->ip)
		{
			$select->where('disabled!=?', 1);
		}
		if (!is_null($cityId))
		{
			$select->where('city_id=?', $cityId);
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}
	
	/**
	 * Получение названия гостиницы по id_1c
	 * @param int $hotelId
	 * @return 
	 */
	public function getNameById($hotelId)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('name'=>$this->nameField))
			   ->where('id_1c=?',$hotelId);
		$row = $this->fetchRow($select)->toArray();
		return $row['name'];
	}
	
	/**
	 * Получение информации о гостинице по id_1c
	 * @param int $hotelId
	 * @return 
	 */
	public function getHotelDetails($hotelId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x' => $this->_name), array('name'=>$this->nameField, 'stars'))
               ->joinLeft(
                    array('y'=>'hotels_details'),
                    'x.id_1c = y.hotel_id',
                    array('note' => $this->noteField, 'priceNote' => $this->priceNoteField)
               )
			   ->where('id_1c=?',$hotelId);
		$row = $this->fetchRow($select)->toArray();
		return $row;
	}
	
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
			$this->noteField='note_eng';
            $this->priceNoteField='price_note_eng';
		}
		else
		{
			$this->nameField='name';
			$this->noteField='note';
            $this->priceNoteField='price_note';
		}
	}
	
}