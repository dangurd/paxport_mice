<?php
class Welt_DbTable_Abstracts extends Welt_DbTableBase
{
	
	const DEADLINE_DATE = '10.12.2015';
	
	protected $_name = 'abstracts';
	
	/**
	 * Получение списка тезисов для персон
	 * @param array $personsIds
	 * @param bool $withContributors
	 * @return 
	 */
	public function getPersonsAbstracts($personsIds, $withContributors = FALSE)
	{
        $rows=array();
		$select = $this->select();
		$select->from(array('x' => $this->_name))
				->where('x.person_id IN(' . Welt_Functions::arrayToSqlWhere($personsIds) . ')')
				->order('x.person_id ASC');
		$rows = $this->fetchAll($select);
		$rows = $rows->toArray();
		if ($withContributors && !empty($rows))
		{
			$contribsTable = new Welt_DbTable_AbstractsContributors();
			foreach ($rows as $key => $row)
			{
				$rows[$key]['contribs'] = $contribsTable->getsAbstractContributors($row['id']);
			}
		}


		return $rows;
	}
	
	public function updateContribs($abstractId, $contribs)
	{
		$contribsTable  = new Welt_DbTable_AbstractsContributors();
		$contribsMapper = new Welt_AbstractsContribsMapper();
		$where = $contribsTable->getAdapter()->quoteInto('abstract_id = ?', $abstractId);
		$contribsTable->delete($where);
		foreach ($contribs as $contrib)
		{
			$empyContrib = TRUE;
			foreach ($contrib as $val)
			{
				if (!empty($val))
				{
					$empyContrib = FALSE;
					break;
				}
			}
			if (!$empyContrib)
			{
				$contrib['abstractId'] = $abstractId;
				$contribData = $contribsMapper->map($contrib, Welt_FieldsMapper::DIRECTION_TO_DB);
				$contribsTable->insert($contribData);
			}
		}
	}

    public function getAbstractsToRemote($user_id)
    {

        $select = $this->select()->setIntegrityCheck(false);
        $select->from(array('a'=>$this->_name),
                     array('id','person_id','title','form','theme','section','intro_text','goal_text','mm_text','result_text','findings_text'))
                ->join(array('p'=>'persons'),'p.id = a.person_id',array('fname','lname'))
                ->join(array('u'=>'users'),'u.id = p.user_id',array())
                ->join(array('pd'=>'persons_details'),'pd.person_id = p.id')
                ->where('u.id=?',$user_id)
                ->where('p.status IN(0,1,2)');

        $rows = $this->fetchAll($select)->toArray();

        return $rows;


    }
	
}