<?php
class Welt_DbTable_Invoices extends Welt_DbTableBase
{
	
	protected $_name = 'invoices';
	
	/**
	 * Получение списка счетов по заявке
	 * @param int $appId
	 * @return 
	 */
	public function getAppInvoices($appId)
	{
		//Формируем запрос
		$select = $this->select();
		$select->from(array($this->_name))
			   ->where('application_id=?',$appId)
			   ->where('status=?',2)
			   ->order('created DESC');
		$rows = $this->fetchAll($select);
		return $rows->toArray();
	}
}