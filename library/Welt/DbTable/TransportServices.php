<?php
class Welt_DbTable_TransportServices extends Welt_DbTableBase
{
	protected $_name = 'transport_services';
	private $nameField;
	
	/**
	 * Получение всех возможных транспортных услуг
	 * @return mixed
	 */
	public function getTransportServices($cityId = NULL)
	{
		$this->setQueryFields();
		$select = $this->select();
		$select->from(array($this->_name),array('id','name'=>$this->nameField,'order_available','station_select','transport_category','is_free','fix_date'=>'DATE_FORMAT(fix_date,"%d.%m.%Y")'))
			   ->order('is_free DESC')
			   ->order($this->nameField.' ASC');
		if (!is_null($cityId))
		{
			$select->where('city_id=?',$cityId);
		}
		$rows = $this->fetchAll($select)->toArray();
		return $rows;
	}

	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->nameField='name_eng';
		}
		else
		{
			$this->nameField='name';
		}
	}
	
}