<?php
class Welt_DbTable_HotelOrders extends Welt_DbTable_BaseOrders
{
	protected $_name = 'hotel_orders';
	
	private $hotelNameField;
	private $roomNameField;
	
	/**
	 * Получение информации о заказах на проживание в рамках заданной заявки
	 * @param mixed $appId Id заявки
	 * @param mixed $serviceStatuses Масив статусов услуг для отбора
	 * @return 
	 */
	public function getUserHotelOrders($appId,$serviceStatuses)
	{
		$fields = array(
			'id',
			'id_1c',
			'application_id',
			'status',
			'order_date',
			'hotel_id',
			'room_id',
			'accommodation',
			'breakfast',
			'extraBed'				=>		'extra_bed',
			'is_extra_bed',
			'bedType'				=>		'bed_type',
			'start_date',
			'end_date',
			'summ'					=>		'summ',
			'comment',
			'updated'
		);
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'hotels'),'y.id_1c=x.hotel_id',array('hotelName'=>$this->hotelNameField))
			   ->joinLeft(array('z'=>'hotels_rooms'),'z.id_1c=x.room_id',array('roomName'=>$this->roomNameField))
			   ->where('x.application_id=?',$appId);
		//Если идёт выборка заказов по заданным статусам
		if (is_array($serviceStatuses) && sizeof($serviceStatuses)>0)
		{
			$select->where('x.status IN('.Welt_Functions::arrayToSqlWhere($serviceStatuses).')');
		}
		$rows = $this->fetchAll($select)->toArray();
		/*
		 * Обрабатываем ситуацию, когда добавлена из ТМ гостиница или номер, которых нет в проекте.
		 * Получаем название гостиницы и номера из базы Welt. 
		 * Важно: обращаемся к базе welt, если не стоит флаг, что заказ - доп. кровать
		 */
		$weltHotelsTable = new Welt_DbTable_WeltHotels();
		$weltRoomsTable = new Welt_DbTable_WeltRooms();
		$rowsSize = sizeof($rows);
		for ($i=0; $i<$rowsSize; $i++)
		{
			if ($rows[$i]['is_extra_bed']!=1)
			{
				if ($rows[$i]['hotelName'] == '')
				{
					$rows[$i]['hotelName'] = $weltHotelsTable->getNameById($rows[$i]['hotel_id']);
				}
				if ($rows[$i]['roomName'] == '')
				{
					$rows[$i]['roomName'] = $weltRoomsTable->getNameById($rows[$i]['room_id']);
				}
			}
		}
		return $rows;
	}
	
	
	/**
	 * Получение детальной информации по заказанной услуге
	 * @param int $serviceId Id заказанной услуги
	 * @return 
	 */
	public function getOrderDetails($serviceId)
	{
		$this->setQueryFields();
		$select = $this->select()->setIntegrityCheck(false);
		$fields = array(
			'hotel_id',
			'room_id',
			'accommodation',
			'breakfast',
			'extra_bed',
			'bed_type',
			'start_date',
			'end_date',
			'summ',
			'comment'
		);
		$select->from(array('x'=>$this->_name),$fields)
			   ->joinLeft(array('y'=>'hotels'),'y.id_1c=x.hotel_id',array('hotelName'=>$this->hotelNameField))
			   ->joinLeft(array('z'=>'hotels_rooms'),'z.id_1c=x.room_id',array('hotelRoomName'=>$this->roomNameField))
			   ->where('x.id=?',$serviceId);
		$row = $this->fetchRow($select)->toArray();
		/*
		 * Обрабатываем ситуацию, когда добавлена из ТМ гостиница или номер, которых нет в проекте.
		 * Получаем название гостиницы и номера из базы Welt.
		 */
		if ($row['hotelName'] == '')
		{
			$weltHotelsTable = new Welt_DbTable_WeltHotels();
			$row['hotelName'] = $weltHotelsTable->getNameById($row['hotel_id']);
		}
		if ($row['hotelRoomName'] == '')
		{
			$weltRoomsTable = new Welt_DbTable_WeltRooms();
			$row['hotelRoomName'] = $weltRoomsTable->getNameById($row['room_id']);
		}
		return $row;
	}
	
	/**
	 * Установка полей для выборки в зависимости от языка
	 */
	private function setQueryFields()
	{
		//Определяем поля для выборки из таблиц в зависимости от языка
		if($this->lang=='en') 
		{
			$this->hotelNameField = 'name_eng';
			$this->roomNameField = 'name_eng';
		}
		else
		{
			$this->hotelNameField = 'name';
			$this->roomNameField = 'name';
		}
	}
	
	
}