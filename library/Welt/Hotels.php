<?php
class Welt_Hotels {
	/**
	 * @var Объект класса для работы с таблицей прайсов
	 */
	protected $pricesTable;
	
	public function __construct()
	{
		$this->pricesTable = new Welt_DbTable_HotelsPrices();
	}
	
	/**
	 * Получение цены проживания для номера
	 * @param string $roomId
	 * @param string $startDate
	 * @param string $stopDate
	 * @param int $accommodation
	 * @param int $mode [optional] Тип выборки (1 - без раскадровки по сезонам, 2 - с раскадровкой)
	 * @param $breakfast Определяет, требуется ли завтрак
	 * @param $personsCount Количество человек, на котрое идёт расчёт - влияет на стоимость завтрака
	 * @return 
	 */
	public function getRoomPeriodPrice($roomId,$startDate,$stopDate,$accommodation,$mode=1,$breakfast=0,$personsCount=1)
	{
		//Проверяем, допустимо ли запрошенное размещение для номера, если нет, то ставим одноместное
		$roomsTable = new Welt_DbTable_HotelsRooms();
		$roomAcc = $roomsTable->getRoomAcc($roomId);
		if ((int) $accommodation > 1 && $roomAcc == 1 && $roomAcc !== false)
		{
			$accommodation = 1;
		}
		//Разница между датами начала и конца сезона в секундах и днях
		$secondsDiff = Welt_Functions::convertDateUnix($stopDate) - Welt_Functions::convertDateUnix($startDate);
		$nightsDiff = round($secondsDiff / 86400);
		//Дата окончания в unix-формате
		$dateUnix = Welt_Functions::convertDateUnix($startDate);
		//Флаг определяет наличие периодов по запросу для номера
		$byRequestRoom = false;
		//Массив сезонов для гостиницы
		$prices = array();
		//Получаем цены по сезонам для номера
		$prices = $this->pricesTable->getRoomPrices($roomId,$startDate,$stopDate);
		/*
		 * Проверяем, по всем ли дням есть цены в прайсах, если не все, то очищаем массив прайсов - все цены по запросу
		 */
		$allPricesExist = true;
		//Проходимся по дням в выбранном периоде
		for ($j=0;$j<$nightsDiff;$j++)
		{
			$priceDate = date('d.m.Y', $dateUnix);
			//Ищем прайс на конкретный день
			$priceExist = false;
			foreach ($prices as $price)
			{
				//Если прайс существует
				if ($price['date'] == $priceDate)
				{
					$priceExist = true;
				}
			}
			//Если нет прайса конкретный день, ставим флаг
			if ($priceExist === false)
			{
				$allPricesExist = false;
			}
			$dateUnix += 86400;
		}
		//Если не на все дни существуют прайсы, очищаем массив прайсов
		if ($allPricesExist === false)
		{
			$prices = array();
		}
		$pricesCount = sizeof($prices);
		//Суммы размещения за период для номера
		$curPrice1Summ = 0;
		$curPrice2Summ = 0;
		$curPrice3Summ = 0;
		//Минимальная стоимость проживания за период
		$minPrice1 = 0;
		$minPrice2 = 0;
		$minPrice3 = 0;
		//Максимальная стоимость проживания за период
		$maxPrices = array(0, 0, 0);
		//Флаги, определяющие, постоянна ли стоимость проживания за указанный период
		$price1Const = 1;
		$price2Const = 1;
		$price3Const = 1;
		//Флаг, определяющий, доступна ли для заказа доп. кровать
		$extraBedAv = 1;
		//Сумма завтрака
		$breakfastSumm = 0;
		//Стоимость завтрака в день
		$breakfastPrice = 0;
		//Флаг определяет наличие периодов по запросу для завтрака
		$byRequestBreakfast = false;
		//Флаг определяющий, что для каждого из периодов завтрак включён/не включён
		$breakfastAllIncl = 1;
		//Флаг определяющий, что завтрак включён/не включён для всего периода проживания
		$breakfastConst = 1;
		//Флаг определяющий, что завтрак включён/не включён для конкретного дня
		$breakfastIncl = 2;
		//Флаг, что завтрак доступен/не доступен для заказа
		$breakfastAv = 1;
		//Проходимся по полученным прайсам
		foreach ($prices as $price)
		{
			$price1 = round($price['price1']);
			$price2 = round($price['price2']);
			$price3 = round($price['price3']);
			$curPrices = array($price1, $price2, $price3);
			//Если хотя бы для 1 прайса не доступна доп. кровать, то ставим флаг
			if ($price3 == -1)
			{
				$extraBedAv = 0;
			}
			//Увеличиваем общую стоимость завтрака, если он не включён в стоимость
			$curBreakfastPrice = 0;
			if ($price['breakfast_incl']!=1 && (int)$price['breakfast_price']!=0)
			{
				$curBreakfastPrice = round($price['breakfast_price']);
			}
			$breakfastPrice = $curBreakfastPrice;
			//Если хотя бы для 1 периода цена завтрака по запросу, то ставим флаг
			if ($price['breakfast_incl']!=1 && (int)$price['breakfast_price']==0)
			{
				$byRequestBreakfast = true;
			}
			//Если флаг, определяющий,что завтрак включён/не включён не равен параметру по умолчанию
			if ($breakfastIncl!=2)
			{
				/*
				 * Если завтрак для текущего дня отличается от предыдущего (включён/не включён), то 
				 * ставим флаг
				 */
				if ($breakfastIncl != $price['breakfast_incl'])
				{
					$breakfastConst = 0;
				}
			}
			$breakfastIncl = $price['breakfast_incl'];
			//Если хотя бы для одного периода завтрак не включён, то ставим флаг
			if ($price['breakfast_incl']!=1)
			{
				$breakfastAllIncl = 0;
			}
			//Если хотя бы для 1 периода завтрак не доступен для заказа, ставим флаг
			if ((int)$price['breakfast_price'] == -1)
			{
				$breakfastAv = 0;
			}
			//Если цены по запросу для периода, то ставим флаг по запросу для номера
			if ($price1==0 && $price2==0)
			{
				$byRequestRoom = true;
			}
			//Увеличиваем общие суммы проживания, завтрака
			$curPrice1Summ += $price1;
			$curPrice2Summ += $price2;
			$curPrice3Summ += $price3;
			$breakfastSumm += $curBreakfastPrice;
			//Меняем по необходимости минимальную стоимость и флаг, определяющий постоянство стоимости
			if ($minPrice1 == 0) 
			{
				$minPrice1 = $price1;
			}
			elseif ($price1 < $minPrice1 && $price1 != 0) 
			{
				$minPrice1 = $price1;
				$price1Const = 0;
			}
			elseif($price1 > $minPrice1)
			{
				$price1Const = 0;
			}
			if ($minPrice2 == 0) 
			{
				$minPrice2 = $price2;
			}
			elseif ($price2 < $minPrice2 && $price2 != 0) 
			{
				$minPrice2 = $price2;
				$price2Const = 0;
			}
			elseif($price2 > $minPrice2)
			{
				$price2Const = 0;
			}
			if ($minPrice3 == 0) 
			{
				$minPrice3 = $price3;
			}
			elseif ($price3 < $minPrice3 && $price3 != 0) 
			{
				$minPrice3 = $price3;
				$price3Const = 0;
			}
			elseif($price3 > $minPrice3)
			{
				$price3Const = 0;
			}
			// Меняем по необходимости максимальную стоимость
			for ($i = 0; $i < 3; $i++ )
			{
				if ($maxPrices[$i] == 0) 
				{
					$maxPrices[$i] = $curPrices[$i];
				}
				elseif ($curPrices[$i] > $maxPrices[$i] && $curPrices[$i] != 0) 
				{
					$maxPrices[$i] = $curPrices[$i];
				}
			}
		}
		//Стоимость проживания в зависимости от размещения
		$priceSumm = 0;
		if ($accommodation==1)
		{
			$priceSumm = $curPrice1Summ;
			$minPrice = $minPrice1;
			$priceConst = $price1Const;
		}
		elseif ($accommodation==2)
		{
			$priceSumm = $curPrice2Summ;
			$minPrice = $minPrice2;
			$priceConst = $price2Const;
		}
		elseif ($accommodation==3)
		{
			$priceSumm = $curPrice3Summ;
			$minPrice = $minPrice3;
			$priceConst = $price3Const;
		}
		$maxPrice = $maxPrices[$accommodation - 1];
		//Если требуется завтрак, добавляем его, умножив на количество человек, увеличиваем сумму заказа
		if ($breakfast==1)
		{
			$breakfastSumm = $breakfastSumm*$personsCount;
			$priceSumm = $priceSumm + $breakfastSumm;
		}
		//Результирующий массив
		$roomPrice = array(
				'summ'				=>	$priceSumm,
				'minPrice'			=>	$minPrice,
				'maxPrice'			=>	$maxPrice,
				'priceConst'		=>	$priceConst,
				'extraBedAv'		=>	$extraBedAv,
				'breakfastConst'	=>	$breakfastConst,
				'breakfastIncl'		=>	$breakfastAllIncl,
				'breakfastAv'		=>	$breakfastAv,
				'breakfastPrice'	=>	$breakfastPrice,
				'breakfastSumm'		=>	$breakfastSumm,
				'nights'			=>	$nightsDiff,
				'priceAcc'			=>	$accommodation
		);
		//Если есть хотя бы один день по запросу или нет прайсов, цены по запросу
		if ($byRequestRoom || $pricesCount==0)
		{
			$roomPrice['summ'] = 0;
			$roomPrice['minPrice'] = 0;
			$roomPrice['priceConst'] = 1;
		}
		//Если нет прайсов, то завтрак не включён
		if ($pricesCount==0)
		{
			$roomPrice['breakfastIncl'] = 0;
			$roomPrice['breakfastPrice'] = 0;
			$roomPrice['breakfastSumm'] = 0;
		}
		/*
		 * Если хотя бы для 1 периода завтрак по запросу или включён/не включён не на всём периоде проживания, то 
		 * сумма стоимости завтрака по запросу
		 */
		if ($byRequestBreakfast || $breakfastConst==0)
		{
			$roomPrice['breakfastPrice'] = 0;
			$roomPrice['breakfastSumm'] = 0;
		}
		//Если выбран режим с раскадровкой по сезонам, то получаем сезоны
		if ($mode==2)
		{
			$seasons = $this->getPeriodSeasons($prices,$startDate,$stopDate,$breakfast,$personsCount);
			$roomPrice['seasons'] = $seasons;
			$roomPrice['seasonsCount'] = sizeof($seasons);
		}
		return $roomPrice;
	}
	
	/**
	 * Получение списка сезонов на основе списка прайсов для периода
	 * @param mixed $periodPrices
	 * @param string $startDate
	 * @param string $stopDate
	 * @param int $breakfast [optional]
	 * @param int $personsCount [optional]
	 * @return 
	 */
	public function getPeriodSeasons($periodPrices,$startDate,$stopDate,$breakfast=1,$personsCount=1)
	{
		$pricePeriods = array();
		//Если есть прайсы для данного номера
		if (sizeof($periodPrices))
		{	
			$period = 0;
			$nights = 1;
			$startDate = $periodPrices[0]['date'];
			$price1 = $periodPrices[0]['price1'];
			$price2 = $periodPrices[0]['price2'];
			$price3 = $periodPrices[0]['price3'];
			$summ1 = $price1;
			$summ2 = $price2;
			$summ3 = $price3;
			//Раскладываем на сезоны
			for ($i=1, $iCount = count($periodPrices); $i<=$iCount; $i++)
			{
				//Если встретилась цена отличная от предыдущей или уже прошёл последний день прайса 
				if (!isset($periodPrices[$i]) || $periodPrices[$i]['price1'] !== $price1)
				{
					//Если цены по запросу
					if ($price1==0 && $price2==0)
					{
						$pricePeriods[$period]['price1'] = 0;
						$pricePeriods[$period]['price2'] = 0;
						$pricePeriods[$period]['price3'] = 0;
						$pricePeriods[$period]['summ1'] = 0;
						$pricePeriods[$period]['summ2'] = 0;
						$pricePeriods[$period]['summ3'] = 0;
					}
					//Если цены не по запросу
					else
					{
						$pricePeriods[$period]['price1'] = $price1;
						$pricePeriods[$period]['price2'] = $price2;
						$pricePeriods[$period]['price3'] = $price3;
						$pricePeriods[$period]['summ1'] = $summ1;
						$pricePeriods[$period]['summ2'] = $summ2;
						$pricePeriods[$period]['summ3'] = $summ3;
					}
					$pricePeriods[$period]['dateStart'] = $startDate;
					$pricePeriods[$period]['nights'] = $nights;
					$pricePeriods[$period]['breakfastIncl'] = $periodPrices[$i-1]['breakfast_incl'];
					if ($periodPrices[$i-1]['breakfast_price'] == -1)
					{
						$pricePeriods[$period]['breakfastAv'] = 0;
					}
					else
					{
						$pricePeriods[$period]['breakfastAv'] = 1;
					}
					$pricePeriods[$period]['breakfastPrice'] = $periodPrices[$i-1]['breakfast_price'];
					//Если данный день прайса существует
					if (isset($periodPrices[$i]))
					{
						$pricePeriods[$period]['dateStop'] = $periodPrices[$i]['date'];
						$startDate = $periodPrices[$i]['date'];
					}
					$nights = 0;
					$summ1 = 0;
					$summ2 = 0;
					$summ2 = 3;
					$period++;
				}
				//Если данный день прайса существует
				if (isset($periodPrices[$i]))
				{
					$price1 = $periodPrices[$i]['price1'];
					$price2 = $periodPrices[$i]['price2'];
					$price3 = $periodPrices[$i]['price3'];
					$endDate = $periodPrices[$i]['date'];
				}
				$nights++;
				$summ1 += $price1;
				$summ2 += $price2;
				$summ3 += $price3;
			}
			//Меняем дату окончания для последнего сезона
			list($day,$month,$year) = explode('.', $periodPrices[$i-2]['date']);
			$pricePeriods[$period-1]['dateStop'] = date("d.m.Y", mktime(0, 0, 0, $month, $day+1, $year));
			//Если необходим завтрак, проходимся по сезонам и добавляем к сумме стоимость завтрака
			if ($breakfast == 1)
			{
				$pricePeriodsSize = sizeof($pricePeriods);
				for($k=0;$k<$pricePeriodsSize;$k++)
				{
					if ($pricePeriods[$k]['breakfastIncl']!=1)
					{
						$curBreakfastSumm = $pricePeriods[$k]['breakfastPrice'] * $pricePeriods[$k]['nights'] * $personsCount;
						$pricePeriods[$k]['summ1'] += $curBreakfastSumm;
						$pricePeriods[$k]['summ2'] += $curBreakfastSumm;
						$pricePeriods[$k]['summ3'] += $curBreakfastSumm;
					}
				}
			}
		}
		//Если нет прайсов, то в результате 1 сезон длинной во весь период
		else
		{
			//Разница между датами начала и конца сезона в днях
			$secondsDiff = Welt_Functions::convertDateUnix($stopDate) - Welt_Functions::convertDateUnix($startDate);
			$nightsDiff = round($secondsDiff / 86400);
			$pricePeriods[0] = array(
				'dateStart'			=>		$startDate,
				'dateStop'			=>		$stopDate,
				'price1'			=>		0,
				'price2'			=>		0,
				'price3'			=>		0,
				'nights'			=>		$nightsDiff,
				'summ1'				=>		0,
				'summ2'				=>		0,
				'summ3'				=>		0,
				'breakfastIncl'		=>		0,
				'breakfastPrice'	=>		0
			);
		}
		return $pricePeriods;
	}
	
	/**
	 * Получение стоимости проживания по всем номерам гостиницы с сортировкой по цене
	 * @param mixed $hotelRoomsIds Список номеров в гостинице
	 * @param string $startDate
	 * @param string $stopDate
	 * @param int $accommodation
	 * @return Список номеров с ценами на проживание
	 */
	public function getHotelRoomsPrice($hotelRooms,$startDate,$stopDate,$accommodation)
	{
		$hotelRoomsSize = sizeof($hotelRooms);
		$rooms = array();
		for($i=0;$i<$hotelRoomsSize;$i++)
		{
			$rooms[$i] = $this->getRoomPeriodPrice($hotelRooms[$i]['id'],$startDate,$stopDate,$accommodation);
			$rooms[$i] = $rooms[$i] + $hotelRooms[$i];
		}
		$sortedRooms = self::roomsSortByPrice($rooms,'summ');
		return $sortedRooms;
	}
	
	
	
	/**
	 * Получение списка из всех номеров в гостинице со стоимостями проживания
	 * @param string $hotelId
	 * @param string $sDate
	 * @param string $eDate
	 * @param int $accommodation
	 * @param int $mode [optional] Параметр, определяющий формат выходного массива с номерами
	 * @return 
	 */
	public function getHotelRoomsAndPrice($hotelId,$sDate,$eDate,$accommodation,$mode=1)
	{
		//Получаем список номеров для гостиницы
		$hotelsRoomsTable = new Welt_DbTable_HotelsRooms();
		$roomsList = $hotelsRoomsTable->getHotelsRooms($hotelId,$accommodation);
		//На основе списка номеров в гостинице, формируем список номеров с прайсами
		$roomsList = $this->getHotelRoomsPrice($roomsList,$sDate,$eDate,$accommodation);
		/*
		 * Добавляем строки со стоимостью проживания за сутки и стомостью проживания за период
		 */
		$roomsListSize = sizeof($roomsList);
		$translator = Zend_Registry::get('Zend_Translate');

		for($i=0;$i<$roomsListSize;$i++)
		{
			//Сумма за день
			if ($roomsList[$i]['summ']!=0)
			{
				if ($roomsList[$i]['priceConst']!=1)
				{
					$roomsList[$i]['dayPriceText'] = $translator->translate('from').' '.$roomsList[$i]['minPrice'].' '.$translator->translate('RUR');
					//$roomsList[$i]['dayPriceText'] .= $translator->translate('to').' '.$roomsList[$i]['maxPrice'].' '.$translator->translate('RUR');
				}
				else
				{
					$roomsList[$i]['dayPriceText'] = $roomsList[$i]['minPrice'].' '.$translator->translate('RUR');
				}
			}
			else
			{
				$roomsList[$i]['dayPriceText'] = $translator->translate('by request');
			}
			
		}
		//Приводим список в нужный вид (room[id]=>array()...), если необходимо
		if (isset($mode) && $mode==2)
		{
			$roomsListConverted = array();
			foreach ($roomsList as $room)
			{
				$roomsListConverted[$room['id']] = $room;
			}
			$roomsList = $roomsListConverted;
		}
		return $roomsList;
	}
	
	/**
	 * Сортировка массива номеров с периодами по стоимости проживания
	 * @param array $rooms
	 * @return array $rooms - отсортированный массив
	*/
	public function roomsSortByPrice($rooms,$sortByElem)
	{
		$roomsSize = sizeof($rooms);
		$byRequest = array();
		$k = 0;
		//Отбираем элементы с ценами по запросу и помещаем их в отдельный массив
		for($i=0; $i<$roomsSize; $i++)
		{
			$j = $i;
			$tmpRoom = $rooms[$i];
			$tmpMin = $rooms[$i]['minPrice'];
			//Если цена по запросу то помещаем элементы в отдельный массив
			if ($tmpMin==0)
			{
				$byRequest[$k] = $tmpRoom;
				$k++;
				unset($rooms[$i]);
			}
		}
		//Сортируем массив по стоимости проживания
		usort($rooms,array('Welt_Hotels','cmpByPrice'));
		//Если были номера по запросу, то добавляем их в конец массива
		if (sizeof($byRequest)) 
		{
			$rooms = array_merge($rooms,$byRequest);
		}
		return $rooms;
	}
	
	/***
	 * Функция для пользовательской сортировки массива с номерами по стоимости проживания
	 * @param mixed $a
	 * @param mixed $b
	 * @return int
	 */
	public function cmpByPrice($a,$b)
	{
        if($a['minPrice'] == $b['minPrice'])
		{
            return 0;
        }
        return ($a['minPrice'] < $b['minPrice']) ? -1 : 1;
    }
	
	/**
	 * Получение 1d гостиницы/номера по 1d_1c
	 * @param int $hotelId
	 * @return 
	 */
	public static function getIdByFullId($fullId)
	{
		$id = substr($fullId,0,13);
		return $id;
	}
	
}
