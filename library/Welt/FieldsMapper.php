<?php
class Welt_FieldsMapper
{
	const DIRECTION_FROM_DB = 1;
	
	const DIRECTION_TO_DB = 2;
	
	const SERIALIZE_SEP = '|';
	
	protected $_map;
	
	protected $_direction = 1;
	
	protected $_dates = array();
	
	protected $_serializable = array();
	
	public function map(array $data, $direction)
	{
		$this->_direction = $direction;
		$this->_setMap();
		$mapped = array();
		foreach ($data as $key => $val)
		{
			$fieldMap = $this->_mapField($key);
			if (is_null($fieldMap))
			{
				continue;
			}
			$mapped[$fieldMap] = $this->_processField($key, $val);
		}
		return $mapped;
	}
	
	protected function _setMap()
	{
		$map = $this->_fieldsMap;
		if ($this->_direction == self::DIRECTION_TO_DB)
		{
			$map = array_flip($map);
		}
		$this->_map = $map;
	}
	
	protected function _mapField($name)
	{
		if (isset($this->_map[$name]))
		{
			return $this->_map[$name];
		}
		return NULL;
	}
	
	protected function _processField($name, $value)
	{
		if (is_string($value) && trim($value) == '')
		{
			return $value;
		}
		if (in_array($name, $this->_dates) || in_array($this->_mapField($name), $this->_dates))
		{
			if ($this->_direction == self::DIRECTION_TO_DB)
			{
				$value = Welt_Functions::dateToMysql($value);
			}
			else
			{
				$value = Welt_Functions::dateFromMysql($value);
			}
		}
		elseif (in_array($name, $this->_serializable) || in_array($this->_mapField($name), $this->_serializable))
		{
			if ($this->_direction == self::DIRECTION_TO_DB)
			{
				$value = $this->_arrSerialize($value);
			}
			else
			{
				$value = $this->_arrDeserialize($value);
			}
		}
		return $value;
	}
	
	protected function _arrSerialize($value)
	{
		if (!is_array($value) || empty($value))
		{
			return '';
		}
		return implode(self::SERIALIZE_SEP, $value);
	}
	
	protected function _arrDeserialize($value)
	{
		if (trim($value) == '')
		{
			return array();
		}
		return explode(self::SERIALIZE_SEP, $value);
	}
}
