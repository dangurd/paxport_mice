<?php
/**
 * Класс, предназначенный для реализации функционала. связанного и импортом данных из системы EDUS
 */
class Welt_Edus {
	/**
	 * Названия файлов с логами
	 */
	const errorLogName = 'edus_error.log';
	const attendeesLogName = 'edus_attendees.log';
	const paymentsLogName = 'edus_payments.log';
	const invitesLogName = 'edus_invites.log';
	
	/**
	 * @var Лог, в который пишутся ошибки
	 */
	protected $errorLog;
	/**
	 * @var Лог, в который пишутся результаты по добавленным участникам
	 */
	protected $attendeesLog;
	/**
	 * @var Лог, в который пишутся результаты по добавленным оплатам
	 */
	protected $paymentsLog;
	/**
	 * @var Лог, в который пишутся результаты по отправке соообщений с приглашением к заказу услуг
	 */
	protected $invitesLog;
	/**
	 * @var Допустимые виды данных для экспорта из EDUS
	 */
	protected static $edusDataTypes = array('attendees','payments');
	/**
	 * @var Параметры для соединения с системой EDUS
	 */
	protected static $edusParams = array(
		'url'			=>	'http://edas.info/exportPeople.php?_qf__exportPeople=&c=9185&conferencegroup=0&whom=#data_type#&action=Export+people',
		'username'		=>	'ssi@welt.ru',
		'password'		=>	'become8careful9develop7',
		'uAgent'		=>	'User-Agent: Mozilla/4.0 (compatible; MSIE 5.01; Widows NT)',
		'ref'			=>	'http://edas.info/index.php'
	);
	
	
	public function __construct()
	{
		//Инициализация логов
		$this->initLogs();
	}
	
	
	/**
	 * Импорт данных по участникам из системы EDUS
	 * @return 
	 */
	public function processAttendees()
	{
		//Загружаем данные по оплатившим участникам
		$this->loadEdusData('attendees');
		//Обрабатываем полученный файл
		if (!file_exists(APPLICATION_PATH.'/../data/tmp/attendees.csv'))
		{
			throw new Welt_Exception_Edus('Error loading file attendees.csv');
		}
		$file = fopen(APPLICATION_PATH.'/../data/tmp/attendees.csv', 'a+');
		$usersTable = new Welt_DbTable_Users();
		//Устанавливаем указатель на 1 строку, минуя шапку
		fseek($file,154);
		//Проходимся по строкам файла
		while (($userData = fgetcsv($file, 1000, ",")) !== false) 
		{
			//Если пользователь не существует в БД
			if($usersTable->checkByIdEdus($userData[1]) === false)
			{
				//Формируем массив с данными пользователя для записи в БД
				$data = array(
					'id_edus'			=>	$userData[1],
					'email'				=>	$userData[13],
					'password'			=>	Welt_Functions::getUniqueCode(8),
					'fname'				=>	$userData[3],
					'lname'				=>	$userData[4],
					'title'				=>	$userData[2],
					'company'			=>	$userData[5],
					'address'			=>	$this->generateAdressStr($userData),
					'fax'				=>	$userData[14]
				);
				//Записываем пользователя в БД и пишем результат в лог
				if ($usersTable->insert($data))
				{
					$this->attendeesLog->info('User '.$userData[1].' added ');
				}
				else
				{
					$this->attendeesLog->emerg('Eror inserting user '.$userData[1]);
				}
			}
		}
	}
	
	/**
	 * Импорт данных по оплатам из системы EDUS
	 * @return 
	 */
	public function processPayments()
	{
		//Загружаем данные по оплатившим участникам
		$this->loadEdusData('payments');
		//Обрабатываем полученный файл
		if (!file_exists(APPLICATION_PATH.'/../data/tmp/payments.csv'))
		{
			throw new Welt_Exception_Edus('Error loading file payments.csv');
		}
		$file = fopen(APPLICATION_PATH.'/../data/tmp/payments.csv', 'a+');
		$edusPaymentsTable = new Welt_DbTable_EdusPayments();
		//Устанавливаем указатель на 1 строку, минуя шапку
		fseek($file,59);
		//Проходимся по строкам файла
		while (($paymentData = fgetcsv($file, 1000, ",")) !== false) 
		{
			//Если платёжа для пользователя ещё нет в БД
			if($edusPaymentsTable->checkUserPaymentExist($paymentData[0]) === false)
			{
				//Формируем массив с данными по платежу для записи в БД
				$data = array(
					'user_id_edus'		=>	$paymentData[0],
					'fname'				=>	$paymentData[1],
					'lname'				=>	$paymentData[2],
					'company'			=>	$paymentData[3],
					'country'			=>	$paymentData[4],
					'amount'			=>	$paymentData[5],
					'method'			=>	$paymentData[6]
				);
				//Записываем платёж в БД и пишем результат в лог
				if ($edusPaymentsTable->insert($data))
				{
					$this->paymentsLog->info('Payment for user '.$paymentData[0].' added ');
				}
				else
				{
					$this->paymentsLog->emerg('Eror inserting payment for user '.$paymentData[0]);
				}
			}
		}
	}
	
	/**
	 * Отправка сообщений с приглашением к заказу услуг пользователям
	 * @return 
	 */
	public function sendUsersInvite()
	{
		//Получаем список пользователей, которым не отправлено приглашение
		$usersTable = new Welt_DbTable_Users();
		$users = $usersTable->getNotInvitedUsers();
		//Если пользователей, удволетворяющих условию нет
		if (!is_array($users) || sizeof($users) == 0)
		{
			return false;
		}
		$mailerObj = new Welt_Mailer();
		//Проходимся по списку пользователей
		foreach ($users as $user)
		{
			//Отправляем сообщение
			$mailerObj->sendInvite($user);
			//Ставим для пользователя флаг, что сообщение отправлено
			$where = $usersTable->getAdapter()->quoteInto('id = ?',$user['id']);
			$usersTable->update(array('invite_sended'=>1),$where);
			//Делаем отметку в логе
			$this->invitesLog->info('Invite message for user '.$user['id'].' (email: '.$user['email'].') sended');
		} 
	}

	
	/**
	 * Инициализация логов
	 * @return 
	 */
	protected function initLogs()
	{
		//Задаём формат логов
		$format = '%timestamp% | %priorityName% (%priority%): %message%'.PHP_EOL;
		$formatter = new Zend_Log_Formatter_Simple($format);
		//Инициализия логов
		$errorWriter = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/'.self::errorLogName);
		$errorWriter->setFormatter($formatter);
		$this->errorLog = new Zend_Log($errorWriter);
		
		$attendeesWriter = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/'.self::attendeesLogName);
		$attendeesWriter->setFormatter($formatter);
		$this->attendeesLog = new Zend_Log($attendeesWriter);
		
		$paymentsWriter = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/'.self::paymentsLogName);
		$paymentsWriter->setFormatter($formatter);
		$this->paymentsLog = new Zend_Log($paymentsWriter);
		
		$invitesWriter = new Zend_Log_Writer_Stream(APPLICATION_PATH.'/../data/logs/'.self::invitesLogName);
		$invitesWriter ->setFormatter($formatter);
		$this->invitesLog = new Zend_Log($invitesWriter);
	}
	
	/**
	 * Загрузка данных заданного типа из системы EDUS
	 * @param string $dataType Тип данных
	 * @return 
	 */
	protected function loadEdusData($dataType)
	{
		//Проверка, корректый ли тип данных запрошен
		if (!in_array($dataType,self::$edusDataTypes))
		{
			throw new Welt_Exception_Edus('Incorrect EDUS data type');
		}
		//Меняем url в зависимости от типа запрашиваемых данных
		$url = str_replace('#data_type#',$dataType,self::$edusParams['url']);
		//Устанавливаем параметры соединения
		$client = new Zend_Http_Client($url, array(
			'adapter' 	   => 	'Zend_Http_Client_Adapter_Curl',
        	'maxredirects' => 	10,
        	'timeout'      => 	60,
			'useragent'	   =>	self::$edusParams['uAgent'],
			'curloptions' => array(
				CURLOPT_REFERER => self::$edusParams['ref']
			)
		));
		$client->setParameterPost(array(
			'username'		=>	self::$edusParams['username'],
			'password'		=>	self::$edusParams['password']
		));
		//Получаем ответ
		$response = $client->request('POST');
		//Если сервер ответил ошибкой, то пишем её в лог
		if ($response->isError())
		{
			$this->errorLog->emerg('EDUS server error. Server reply was: '.$response->getStatus().' '.$response->getMessage());
			throw new Welt_Exception_Edus('EDUS server error');
		}
		//Cохраняем ответ в файл
		$content = $response->getRawBody();
		$file = fopen(APPLICATION_PATH.'/../data/tmp/'.$dataType.'.csv', 'w+');
		fwrite($file,$content);
	}
	
	/**
	 * Формирование строки с адресом на основе данных о пользователе
	 * @param array $userData
	 * @return 
	 */
	protected function generateAdressStr($userData)
	{
		$address = '';
		if (trim($userData[10])!='')
		{
			$address .= $userData[10]; //Индекс
		}
		if (trim($userData[7])!='')
		{
			$address .= ', '.$userData[7]; //Улица
		}
		if (trim($userData[8])!='')
		{
			$address .= ', '.$userData[8]; //Город
		}
		if (trim($userData[9])!='')
		{
			$address .= ', '.$userData[9]; //Штат
		}
		if (trim($userData[11])!='')
		{
			$address .= ', '.$userData[11]; //Область
		}
		//Страна
		if (trim($userData[12])!='')
		{
			if (trim($address!=''))
			{
				$address .= ', '.$userData[12];
			}
			else
			{
				$address = $userData[12];
			}
		}
		return $address;
	}
	
}
