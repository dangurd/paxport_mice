<?php
/**
 * Класс для работы с заявками (турами)
 */
class Welt_Applications {
	/**
	 * Максимальное количество секунд, которое может проходить между моментом завершения
	 * регистрации пользователем и обработкой заявки ТМ.
	 */
	const appProcessTime = 120;
	/**
	 * @var $userId Id участника
	 */
	protected $userId;
	/**
	 * @var $appId Id заявки (тура)
	 */
	protected $appId;
	
	/**
	 * @var $servicesObj Объект класса для работы с услугами
	 */
	protected $servicesObj;
	/**
	 * @var $appTable Объект класса для работы с таблицей заявок
	 */
	protected $appTable;
	/**
	 * @var Системные настройки из конфига
	 */
	protected $settings;
	
	public function __construct($appId=0,$userId=0)
	{
		//Если конструктору переданы параметры
		if (isset($appId) && isset($userId))
		{
			$this->appId = $appId;
			$this->userId  = $userId;
			$this->servicesObj = new Welt_Services($this->appId,$this->userId);
		}
		$this->appTable = new Welt_DbTable_Applications();
		$this->settings = Zend_Registry::get('settings');
	}
	
	/**
	 * Сеттеры для свойств
	 */
	public function setUser($userId)
	{
		$this->userId  = $userId;
	}
	
	public function setAppId($appId)
	{
		$this->appId = $appId;
	}
	
	/**
	 * Добавление новой заявки
	 * @return bool
	 */
	public function addNewApplication($appData)
	{
		//Если включён автоматич. режим подтверждения заявок
		if ($this->settings->reg->autoConfirm == 1)
		{
			$appData['status'] = 1;
		}
		else
		{
			$appData['status'] = 0;
		}
		return $this->appTable->insert($appData);
	}
	
	/**
	 * Добавление пустой заявки с привязкой к пользователю - требуется на 1 шаге регистрации
	 * @return 
	 */
	public static function addEmptyApp($userId)
	{
		$appTable = new Welt_DbTable_Applications();
		$appData = array(
			'user_id'		=>	$userId
		);
		//Если включён автоматич. режим подтверждения заявок
		if (Zend_Registry::get('settings')->reg->autoConfirm == 1)
		{
			$appData['status'] = 1;
		}
		else
		{
			$appData['status'] = 0;
		}
		return $appTable->insert($appData);
	}
	
	/**
	 * Обновление информации по заявке
	 * @return 
	 */
	public function updateApp($data)
	{
		$where = $this->appTable->getAdapter()->quoteInto('id = ?', $this->appId);
		return $this->appTable->update($data,$where);
	}
	
	/**
	 * Отправка данных по всем заявкам (которые ещё не отправлены) на удалённый скрипт организатора
	 * @return 
	 */
	public function sendCompleteApps()
	{
		//Получаем список неотправленных заявок
		$appsForSend = $this->appTable->getAppsForSend();
		//Отправляем данные по заявкам
		$this->sendAppsData($appsForSend);
	}
	
	/**
	 * Отправка данных по указанным заявкам на удалённый скрипт организатора
	 * @return 
	 */
	public function sendAppsData($apps, $mode = 'export')
	{
		if (sizeof($apps) == 0)
		{
			return false;
		}
		//Проходимся по заявкам, получаем все данные по ним
		$appsData = array();
		$appsIds = array();
		foreach($apps as $appForSend)
		{
			$appsIds[] = $appForSend['id'];
			//Получаем данные о языке на котором была сделана заявка, устанавливаем данный язык как системный
			$usersTable = new Welt_DbTable_Users();
			$userData = $usersTable->getUserInfo($appForSend['user_id']);
			Zend_Registry::set('lang', $userData['lang']);
			//Устанавливаем параметры, получаем данные
			$this->setAppId($appForSend['id']);
			$this->setUser($appForSend['user_id']);
			$this->servicesObj->setAppId($appForSend['id']);
			$this->servicesObj->setUser($appForSend['user_id']);
			$appsData[] = $this->getApplicationData();
		}
		//Создаём новое представление на основе шаблона, передаём переменные и рендерим результат в переменную
		$xmlView = new Zend_View();
		$xmlTemplatesPath = Zend_Registry::get('config')->paths->templates->xml;
		$xmlView->setScriptPath($xmlTemplatesPath);
		$xmlView->apps = $appsData;
		$xmlData = $xmlView->render('applications-xml.phtml');
		//Отправляем полученный xml
		$this->sendAppsDataXml($appsIds, $xmlData, $mode);
	}
	
	/**
	 * Отправка данных по заявкам организатору (на удалённый скрипт)
	 * @return 
	 */
	private function sendAppsDataXml($appsIds, $xmlData, $mode = 'export')
	{
		$fp1 = fopen(APPLICATION_PATH.'/../data/logs/apps.xml','w');
		fwrite($fp1,$xmlData);
		//Устанавливаем параметры соединения
		$organizerScriptUrl = $this->settings->event->organizer->script_url;
		$client = new Zend_Http_Client($organizerScriptUrl, array(
			'adapter' 	   => 'Zend_Http_Client_Adapter_Socket',
        	'maxredirects' => 3,
        	'timeout'      => 60
		));
		$response = $client->setRawData($xmlData)->setEncType('text/xml')->request('POST');
		//Лог-файл для записи результата отправки данных
		$logPath = $this->settings->reg->transferLogPath;
		$format = '%timestamp% | ' . $mode . ' | ' . implode(',', $appsIds) . ' |%priorityName% (%priority%): %message%'.PHP_EOL;
		$formatter = new Zend_Log_Formatter_Simple($format); 
		$writer = new Zend_Log_Writer_Stream($logPath);
		$writer->setFormatter($formatter);
		$logger = new Zend_Log($writer);
		//Если сервер ответил ошибкой, то пишем её в лог
		if ($response->isError())
		{
			$logger->emerg('Data transfer error. Server reply was: '.$response->getStatus().' '.$response->getMessage());
		}
		//Если ответ от сервера успешный, проверяем ответ
		else
		{
			$responseXml = @simplexml_load_string($response->getBody());
			//Если ответ в корректном формате
			if($responseXml)
			{
				//Пишем в лог результат из ответа от сервера организатора
				if ($responseXml->status==1)
				{
					$logger->info('Data transfer complete');
					//При соответствующем режиме устанавливаем флаг, что данные отправлены
					if ($mode == 'export')
					{
						$this->setDataSended($appsIds);
					}
				}
				else
				{
					$logger->emerg('Status is response xml is 0. Message: '.$responseXml->message);
				}
			}
			else
			{
				$logger->emerg('Response is not valid xml file');
			}
		}
	}
	
	/**
	 * Установка флага "Данные по заявке отправлены организатору" для переданных заявок
	 * @return 
	 */
	public function setDataSended($appsIds)
	{
		$where = 'id IN(' . Welt_Functions::arrayToSqlWhere($appsIds) . ')';
		$data['data_sended'] = 1;
		$data['updated'] = new Zend_Db_Expr('updated');
		$res = $this->appTable->update($data, $where);
		return $res;
	}
	
	
	
	/**
	 * Получение всех данных по заявке участника в указанном формате
	 * @param string $format [optional]
	 * @return 
	 */
	public function getApplicationData($format='')
	{
		$appData = $this->appTable->find($this->appId)->current()->toArray();
		//Получаем данные по участнику
		$usersTable = new Welt_DbTable_Users();
		$appData['user'] = $usersTable->getUserInfo($this->userId);
		//Данные по персонам
		$personsTable = new Welt_DbTable_Persons();
		$appData['persons'] = $personsTable->getUserPersonsDetail($this->userId, 1);
		//Получаем данные по заказанным услугам
		$appData['services'] = $this->servicesObj->getAllUserServicesOrders();
		if ($format=='xml')
		{
			//Создаём новое представление на основе шаблона, передаём переменные и рендерим результат в переменную
			$xmlView = new Zend_View();
			$xmlTemplatesPath = Zend_Registry::get('config')->paths->templates->xml;
			$xmlView->setScriptPath($xmlTemplatesPath);
			$xmlView->data = $appData;
			$appDataXml = $xmlView->render('application-xml.phtml');
			return $appDataXml;
		}
		else
		{
			return $appData;
		}	
	}
	
	/**
	 * Отправка данных по изменённым заявкам за прошедший день
	 * @return 
	 */
	public function sendModifedApps()
	{
		/*
		 * Устанавливаем даты для поиска изменений - прошедшие сутки. Таким образом, синхронизация должна
		 * выполняться ежедневно в одно и тоже время.
		 */
		$curDate = new Zend_Date();
		$eDate = $curDate->toString('yyyy-MM-dd HH:mm:ss');
		$curDate->sub(1, Zend_Date::DAY);
		$curDate->sub(5, Zend_Date::MINUTE); // Добавляем 5 минут для того, чтобы при случайном отклонении времени все данные попадали в синхронизацию
		$sDate = $curDate->toString('yyyy-MM-dd HH:mm:ss');
		//Получаем список заявок, которые были изменены
		$modApps = $this->appTable->geUpdatedApps($sDate, $eDate);
		//Получаем список заявок, в которых были изменены данные по пользователю
		$usersTable = new Welt_DbTable_Users();
		$modApps = array_merge($modApps, $usersTable->geUpdatedApps($sDate, $eDate));
		//Получаем список заявок, в которых были изменены данные по персонам
		$personsTable = new Welt_DbTable_Persons();
		$modApps = array_merge($modApps, $personsTable->geUpdatedApps($sDate, $eDate));
		//Получаем список заявок, в которых были изменены данные по услугам
		$modApps = array_merge($modApps, $this->servicesObj->geUpdatedApps($sDate, $eDate));
		$modApps = array_unique($modApps);
		//Получаем данные для полученных заявок
		$modAppsInfo = $this->appTable->getAppsShortInfo($modApps);
		//Отправляем данные по заявкам
		$this->sendAppsData($modAppsInfo, 'synch');
	}
	
	
	/**
	 * Получение общей информации по заявке без детальной информации по услугам
	 * @return mixed
	 */
	public function getApplicationInfo()
	{
		$appInfo = $this->appTable->find($this->appId)->current()->toArray();
		$appInfo['counts'] = array();
		$personsTable = new Welt_DbTable_Persons();
		$userPersons = $personsTable->getUserPersons($this->userId,1,array(1,2));
		$appInfo['counts']['personsCount'] = sizeof($userPersons);
		return $appInfo;
	}
	
	
	/**
	 * Получение информациии о заявке участника
	 * @return 
	 */
	public function getUserAppData()
	{
		$appTable = new Welt_DbTable_Applications();
		return $appTable->getUserApp($this->userId);
	}

	/**
	 * Установка флага "Формирование заявки закончено"
	 * @return 
	 */
	public function setComplete()
	{
		$where = $this->appTable->getAdapter()->quoteInto('id = ?', $this->appId);
		$data['complete'] = 1;
		//Добавляем дату завершения заявки
		$data['finished'] = Welt_Functions::getDateTimeMysql();
		return $this->appTable->update($data,$where);
	}
	
	
	/**
	 * Проверка на существование заявки
	 * @return bool
	 */
	public function checkAppExist()
	{
		if(count($this->appTable->find($this->appId))>0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Удаление незавершённых заявок для пользователя
	 * @param int $userId
	 * @return 
	 */
	public function deleteUserNotCompletedApps($userId)
	{
		$where[] = $this->appTable->getAdapter()->quoteInto('user_id = ?', $userId);
		$where[] = $this->appTable->getAdapter()->quoteInto('complete = ?', 0);
		return $this->appTable->delete($where);
	}
	
	/**
	 * Получение номера тура
	 * @return 
	 */
	public function getTourNumber()
	{
		return $this->appTable->find($this->appId)->current()->tour_number;
	}
}
