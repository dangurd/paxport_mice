<?php
/**
 * Класс для работы с оплатами
 */
class Welt_Payment {
	/**
	 * @var Id заявки (тура)
	 */
	protected $appId;
	/**
	 * @var $userId Id участника
	 */
	protected $userId;
	/**
	/**
	 * @var Системные настройки из конфига
	 */
	protected $settings;
	/**
	 * @var Объект класса для работы с услугами
	 */
	protected $servicesObj;
	/**
	 * @var Объект класса для работы с таблицей связей услуг и методов оплаты
	 */
	protected $paymentAndServicesTable;
	
	public function __construct($appId,$userId)
	{
		$this->appId = $appId;
		$this->userId = $userId;
		$this->servicesObj = new Welt_Services($this->appId,$this->userId);
		$this->paymentAndServicesTable = new Welt_DbTable_PaymentAndServices();
		$this->settings = Zend_Registry::get('settings');
	}
	
	/**
	 * Получение списка всех услуг, без бесплатной визовой поддержки и включённых в регистрационный сбор банкетов.
	 * @return 
	 */
	public function getServicesForPayment()
	{
		//Получаем весь список заказанных услуг со статусом 2
		$orderedServices = $this->servicesObj->getAllUserServicesOrders(1,array(2,10));
		$servicesForPayment = array();
		//Отбираем все услуги, кроме визовой поддержки и включённых в рег. сбор банкетов
		foreach ($orderedServices as $order)
		{
			if (!(($order['service_sys_name']=='meal' && $order['incl_reg_fee']==1) || $order['summ']==0))
			{
				$servicesForPayment[] = $order;
			}
		}
		return $servicesForPayment;
	}
	
	/**
	 * Получение списка всех услуг для оплаты с выбранными способами оплаты
	 * @return 
	 */
	public function getServicesAndPayment()
	{
		//Получаем список услуг для оплаты
		$servicesForPayment = $this->getServicesForPayment();
		//Получаем выбранный для услуги метод оплаты
		$servicesForPaymentSize = sizeof($servicesForPayment);
		$paymentTable = new Welt_DbTable_PaymentMethods();
		for ($i=0;$i<$servicesForPaymentSize;$i++)
		{
			$servicesForPayment[$i]['method'] = $this->paymentAndServicesTable->getServicePaymentMethod($servicesForPayment[$i]['id'],$servicesForPayment[$i]['service_sys_name']); 
		}
		return $servicesForPayment;
	}
	
	
	/**
	 * Получение списка методов оплаты
	 * @return 
	 */
	public function getPaymentMethods()
	{
		$paymentTable = new Welt_DbTable_PaymentMethods();
		$methods = $paymentTable->getPaymentMethods();
		return $methods;
	}
	
	/**
	 * Обработка массива со свзями методов оплаты и услуг и занесение этой информации в БД
	 * @param array $paymentServices Массив со свзями методов оплаты и услуг
	 * @return 
	 */
	public function setServicesPayment($servicesPayment)
	{
		//Проходимя по массиву и добавляем методы оплаты для услуг
		foreach($servicesPayment as $servicePayment)
		{
			if(!$this->paymentAndServicesTable->setServicePayment($this->appId,$servicePayment))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Выбор метода оплаты для услуги
	 * @param int $serviceId
	 * @param int $serviceType
	 * @param int $paymentMethod
	 * @return 
	 */
	public function setServicePayment($serviceId,$serviceSysName,$paymentMethod)
	{
		$paymentAndService = array (
			'service_id'		=>	$serviceId,
			'service_sys_name'	=>	$serviceSysName,
			'method_sys_name'	=>	$paymentMethod
		);
		return $this->paymentAndServicesTable->setServicePayment($this->appId,$paymentAndService);
	}
	
	/**
	 * Проверка, определены ли методы оплаты для услуг в рамках данной заявки
	 * @return 
	 */
	public function checkServicesPayment()
	{
		return $this->paymentAndServicesTable->checkServicesPayment($this->appId);
	}
	
	/**
	 * Сброс выбранных способов оплаты для услуг
	 * @return 
	 */
	public function resetServicesPayment()
	{
		$where = $this->paymentAndServicesTable->getAdapter()->quoteInto('application_id = ?', $this->appId);
		return $this->paymentAndServicesTable->delete($where);
	}
	
	/**
	 * Обработка запроса на создание счёта/авторизационного письма для указанных услуг
	 * @param array $requestData Массив с данными по запросу
	 * @return 
	 */
	public function proccessDocRequest($requestData)
	{
		//Определяем тип документа для данного способа оплаты
		$paymentTable = new Welt_DbTable_PaymentMethods();
		$requestData['doc_type'] = $paymentTable->find($requestData['method_sys_name'])->current()->doc_type;
		//Добавляем заказ на документ
		$docsRequestsTable = new Welt_DbTable_DocsRequests();
		$docsRequestsTable->addDocRequest($this->appId,$requestData);
		//Получаем id созданного заказа
		$requestId = $docsRequestsTable->getAdapter()->lastInsertId();
		//Добавляем связь данного заказа с услугами
		$docsRequestsAndServicesTable = new Welt_DbTable_DocsRequestsAndServices();
		foreach ($requestData['services'] as $serviceData)
		{
			$docsRequestsAndServicesTable->saveRequestService($this->appId,$requestId,$serviceData);
		}
	}
}
