<?php
class Welt_Model_EventParams extends Welt_Model
{
	protected $_langDep = array(
		'eventName' => array('event_name', 'event_name_eng'),
		'coordName' => array('coord_name', 'coord_name_eng'),
	);
	
	
	public function getEventNameLng()
	{
		return $this->_getLangDep('eventName');
	}
	
	public function getCoordNameLng()
	{
		return $this->_getLangDep('coordName');
	}
}
