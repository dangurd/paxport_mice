<?php
class Welt_AbstractsContribsMapper extends Welt_FieldsMapper
{
	protected $_fieldsMap = array(
		'abstract_id' => 'abstractId',
		'fname' => 'fname',
		'lname' => 'lname',
		'patronymic' => 'patronymic',
		'company' => 'company',
	);
	
}