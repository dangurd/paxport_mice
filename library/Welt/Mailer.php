<?php
class Welt_Mailer
{
	/**
	 * Smtp сервер
	 */
	const smtpServer = '192.168.2.7';
	//Каталог с шаблонами для писем
	protected $emailTemplatesPath;
	//Системные настройки
	protected $systemSettings;
	//Объект вида
	protected $emailTemplate;
	
	public function __construct()
	{
		$this->systemSettings = Zend_Registry::get('settings')->system;
		$lang = Zend_Registry::get('lang');
		$this->emailTemplatesPath = Zend_Registry::get('config')->paths->templates->email.'/'.$lang.'/';
		//Создаём новый объект вида и устанавливаем каталог для шаблонов
		$this->emailTemplate = new Zend_View();
		$this->emailTemplate->setScriptPath($this->emailTemplatesPath);
		//Конфигурируем smtp
		$tr = new Zend_Mail_Transport_Smtp(self::smtpServer);
		Zend_Mail::setDefaultTransport($tr);
	}
	
	/**
	 * Отправка письма c восстановленным паролем
	 * @param string $email
	 * @return 
	 */
	public function sendPassRemind($userData)
	{
		$emailTemplate = $this->emailTemplate;
		//Устанавливаем значения для переменных вида
		$emailTemplate->fio = $userData['fio'];
		$emailTemplate->email = $userData['email'];
		$emailTemplate->password = $userData['password'];
		//Рендерим представление в переменную
		$remindBody = $emailTemplate->render('remind.phtml');
		//Отправляем сообщение
		$mail = new Zend_Mail('UTF-8');
		$mail->setBodyHtml($remindBody);
		$mail->setFrom(
			$this->systemSettings->admin->email1,
			$this->systemSettings->admin->name
		);
		$mail->addTo($userData['email'], $userData['fio']);
		$translator = Zend_Registry::get('Zend_Translate');
		$subject = $translator->translate('Password recovery for Personal Participant Cabinet');
		$mail->setSubject($subject);
		$mail->send();
	}
	
}
