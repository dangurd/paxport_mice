<?php
class Application_Form_AbstractContribs extends Application_Form_Base_RegForm
{
	/**
     * Whether or not form elements are members of an array
     * @var bool
     */
    protected $_isArray = true;
    
    public function init()
    {
        $this->addElements(array(
            new Zend_Form_Element_Text('fname', array(
                'label' => 'First name',
                'class' => 'main-form-input-text',
                'id' => 'fname',
            )),
			new Zend_Form_Element_Text('lname', array(
                'label' => 'Last name',
                'class' => 'main-form-input-text',
                'id' => 'lname',
            )),
			new Zend_Form_Element_Text('patronymic', array(
                'label' => 'Отчество',
                'class' => 'main-form-input-text',
                'id' => 'patronymic',
            )),
			new Zend_Form_Element_Text('company', array(
                'label' => 'Организация',
                'class' => 'main-form-input-text',
                'id' => 'company',
            )),
		));
	}
	
	public function buildLabel($element)
    {
		return $element->getLabel();
    }
}