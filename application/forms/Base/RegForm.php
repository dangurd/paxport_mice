<?php
class Application_Form_Base_RegForm extends Application_Form_ProjectForm
{

	/**
     * Форматирование label
     *
     * @see Zend_Form::addElements()
     */
	public function addElements(array $elements)
	{
		parent::addElements($elements);
		foreach($elements as $element)
		{
			$element->setLabel($this->buildLabel($element));
		}
	}
	
	/**
	 * Добавление * для обязательных полей
	 * @param object $element
	 * @return 
	 */
	public function buildLabel($element)
    {
        $label = $element->getLabel();
        if ($translator = $element->getTranslator()) 
		{
            $label = $translator->translate($label);
        }
        if ($element->isRequired()) {
            $label .= '<span class="required">*</span>';
        }
        $label .= ' :';
		return $label;
    }
    
    public function renderElem($name, $class = '')
    {
        $elem = $this->getElement($name);
        if (!$elem)
        {
            return;
        }
        if ($translator = $elem->getTranslator())
        {
            $elem->placeholder = $translator->translate($elem->placeholder);
        }
        $elemId = $elem->getId();
        if ($class != '')
        {
            $class = ' ' . $class;
        }
        return '<div class="main-form-block-line' . $class .'"><label for="' . $elemId . '" style="display:none;">' . $elem->getLabel() . '</label>' . $elem . '</div>';
    }
}