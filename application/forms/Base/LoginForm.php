<?php
class Application_Form_Base_LoginForm extends Application_Form_ProjectForm
{
	
	/**
     * Добавление нескольких элементов в форму без декораторов
     *
     * @see Zend_Form::addElements()
     */
	public function addElements($elements)
	{
		parent::addElements($elements);
		foreach($elements as $element)
		{
			$element->removeDecorator('Errors');
		}
	}
}