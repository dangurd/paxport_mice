<?php
//Форма для авторизации
class Application_Form_Login extends Application_Form_Base_LoginForm
{
	public function init()
	{
		$this->setAction('');
		$this->setMethod('post');
		$this->addElements(array(
	        new Zend_Form_Element_Text('email', array(
	        	'required' => true,
	            'label' => 'Email',
				'class' => 'auth-form-text-input',
	            'validators' => array(
	                array('NotEmpty', true, array('messages' => array('isEmpty' => 'Empty login'))),
	            ),
				'filters' => array('StringTrim')
	        )),
	        new Zend_Form_Element_Password('password',array(
	            'required' => true,
	            'label' => 'Password',
				'class' => 'auth-form-text-input',
	            'validators' => array(
	                array('NotEmpty', true, array('messages' => array('isEmpty' => 'Empty password'))),
	            )
	        )),
			new Zend_Form_Element_Submit('smbt', array(
				'label' => 'Login',
				'class' =>	'base-submit'
			))
		));
		//Подключаем шаблон формы
		$this->setTemplate('login');
	}
}