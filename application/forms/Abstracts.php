<?php
class Application_Form_Abstracts extends Application_Form_MultiInst
{
	const SEL_LABEL = 'Выберите из списка';
	
	const CONTRIBS_COUNT = 1;
	
	const MAX_TEXTS_SYMBOLS = 5000;
	
	/* Статус тезиса: 
	 * 0 - не добавлен,
	 * 1 - запрос на добавление,
	 * 2 - запрос на удаление,
	 * 3 - был добавлен ранее, возможно изменён
	 */
    const STATUS_NOT_ADDED = 0;
	const STATUS_TO_ADD = 1;
	const STATUS_TO_DEL = 2;
	const STATUS_EXIST = 3;
	
	public static $textFields = array(
		'introText',
		'goalText',
		'mmText',
		'resultText',
		'findingsText',
	);



	protected $_elemsArrName = 'abstracts';
    
    protected $_lang;
    
    public function init()
    {
    	$this->_lang = Zend_Registry::get('lang');
        $this->setAction('');
        $this->setMethod('post');

        $this->setAttrib('id', 'abstracts-form');
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        $this->addElements(array(
        	new Zend_Form_Element_Hidden('status', array(
                'id' => 'status',
                'class' => 'status-el',
            )),
        	new Zend_Form_Element_Hidden('personId', array(
        		'required' => true,
                'id' => 'person-id',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),


			new Zend_Form_Element_Select('theme', array(
				//'required' => true,
                'label' => 'Тема',
                'id' => 'theme',
               'multiOptions'=> array(
                    '0' => self::SEL_LABEL,
                    '1' => 'Современные методы диагностики нарушений слуха',
                    '2' => 'Электрофизиология слуха',
                    '3' => 'Психоакустика',
                    '4' => 'Детская аудиология',
                    '5' => 'Дифференциальная и топическая диагностика нарушений слуха',
                    '6' => 'Имплантационные технологии',
                    '7' => 'Генетика нарушений слуха',
                    '8' => 'Слухоулучшающие и реконструктивные операции на ухе',
                    '9' => 'Электроакустическая коррекция нарушений слуха',
                ),
			//	'validators' => array(
             //       array(
             //           new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::INTEGER + Zend_Validate_NotEmpty::ZERO),
             //           true
             //       )
            //    ),
            )),
			new Zend_Form_Element_Select('section', array(
			//	'required' => true,
                'label' => 'Вторая тема',
                'id' => 'section',
                 'multiOptions'=> array(
                    '0' => self::SEL_LABEL,
                    '1' => 'Современные методы диагностики нарушений слуха',
                    '2' => 'Электрофизиология слуха',
                    '3' => 'Психоакустика',
                    '4' => 'Детская аудиология',
                    '5' => 'Дифференциальная и топическая диагностика нарушений слуха',
                    '6' => 'Имплантационные технологии',
                    '7' => 'Генетика нарушений слуха',
                    '8' => 'Слухоулучшающие и реконструктивные операции на ухе',
                    '9' => 'Электроакустическая коррекция нарушений слуха',
                ),

			//	'validators' => array(
            //        array(
             //           new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::INTEGER + Zend_Validate_NotEmpty::ZERO),
             //           true
             //       )
             //   ),
            )),

            new Zend_Form_Element_Select('form', array(
                'required' => true,
                'label' => 'Форма тезиса',
                'id' => 'form',
                'class'=>'thes-form',
                'multiOptions'=> array(
                    '1' => 'Публикация тезиса',
                    '2' => 'Устный доклад и публикация тезиса',
                    '3' => 'Только устный доклад',
                ),
            )),

            new Zend_Form_Element_Text('title', array(
                'required' => true,
                'label' => 'Название (первый язык)',
                'class' => 'main-form-input-text',
                'id' => 'title',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),

            new Zend_Form_Element_Text('title2', array(

                'label' => 'Название (второй язык)',
                'class' => 'main-form-input-text',
                'id' => 'title2',
            )),

            new Zend_Form_Element_Text('authors', array(
                'required' => true,
                'label' => 'Авторы (первый язык)',
                'class' => 'main-form-input-text',
                'id' => 'authors',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),


            new Zend_Form_Element_Text('authors2', array(
                'label' => 'Авторы (второй язык)',
                'class' => 'main-form-input-text',
                'id' => 'authors2',

            )),



            new Zend_Form_Element_Text('company', array(
                'required' => true,
                'label' => 'Место работы',
                'class' => 'main-form-input-text',
                'id' => 'company',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),

            new Zend_Form_Element_Text('company2', array(
                'label' => 'Место работы (второй язык)',
                'class' => 'main-form-input-text',
                'id' => 'company2',
            )),


            new Zend_Form_Element_Text('city', array(
                'required' => true,
                'label' => 'Город(а) места работы(первый язык)',
                'class' => 'main-form-input-text',
                'id' => 'city',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),


            new Zend_Form_Element_Text('city2', array(
                'label' => 'Город(a) места работы(второй язык)',
                'class' => 'main-form-input-text',
                'id' => 'city2',
            )),


            new Zend_Form_Element_Text('notes', array(
                'label' => 'Заметки',
                'class' => 'main-form-input-text',
                'id' => 'notes',
            )),


            new Zend_Form_Element_Text('lector', array(
                'required' => true,
                'label' => 'Докладчик',
                'class' => 'main-form-input-text lector-ab',
                'id' => 'lector',
                'value'=>'--',
               'validators' => array(
                    array('NotEmpty', true)
                ),
            )),



            new Zend_Form_Element_Textarea('goalText', array(
                'required' => true,
                'label' => 'Текст тезиса без заголовков (до 250 слов)',
                'class' => 'main-form-input-text',
                'id' => 'goal-text',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),

            /*
            new Zend_Form_Element_File('passport', array(
                'required' => false,
                'maxFileSize'=> 5 * 102400,
                'label' => 'Выберите файл для загрузки',
                'validators' => array(
                    array('Size', true, array('max' => 5 * 102400)),
                    array('Extension', true, 'pdf,doc,docx,txt')
                ),
                'id' => 'passport',
            )),
            */

        ));

       // if($_SERVER['REMOTE_ADDR']=='192.168.2.98' || $_SERVER['REMOTE_ADDR']=='192.168.2.186'){

        $this->addElement('file', 'passport'.$this->num, array(
            'validators' => array(
                array('Size', true, array('max' => 5 * 102400)),
                array('Extension', true, 'pdf,doc,docx,txt')
            ),
            'label'=>'Загрузите файл ',
            'required' => false,
            'id' => 'passport'.$this->num,
            'maxFileSize'=> 5 * 102400,
        ));


      //  }

		$contribsForm = new Application_Form_AbstractContribs();
		$contribsForm->setIsArray(TRUE);
		$contribsForm->removeDecorator('form');
		for ($i = 0; $i < self::CONTRIBS_COUNT; $i++)
		{
			$curForm = clone $contribsForm;
			$curForm->setElementsBelongTo($this->_elemsArrName . '[' . $this->num . ']' . '[contribs][' . $i . ']');
			$this->addSubForm($curForm, 'contribs'. $i);
		}
		$this->status->setValue(self::STATUS_NOT_ADDED);
		$this->setTemplate('abstracts');
	}

	/**
	 * Переопределяем правила валидации
	 */
	public function isValid($data)
	{
		$lang = Zend_Registry::get('lang');
		$valid = parent::isValid($data);
		// Если нет изменений по тезису или отправлен запрос на удаление, не выполняем никакие проверки
		if (isset($data['status']) && in_array($data['status'], array(self::STATUS_NOT_ADDED, self::STATUS_TO_DEL)))
		{
			$valid = TRUE;
		}
		// Проверяем общще количество символов дл/ всех полей
		else
		{
			$textsLen = 0;
			foreach (self::$textFields as $field)
			{
				if (!empty($data[$field]))
				{
					$textsLen += mb_strlen($data[$field], 'UTF-8');
				}
			}
			if ($textsLen > self::MAX_TEXTS_SYMBOLS)
			{
				$this->getElement('findingsText')->addError(
					'Максимальное количество сиволов для всех разделов тезисов' . ': ' .  self::MAX_TEXTS_SYMBOLS
				);
				$valid = FALSE;
			}
		}
		return $valid;
	}

	public static function getStatuses()
	{
		return array(
			self::STATUS_NOT_ADDED,
			self::STATUS_TO_ADD,
			self::STATUS_TO_DEL,
			self::STATUS_EXIST,
		);
	}
}
    	