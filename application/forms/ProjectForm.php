<?php
class Application_Form_ProjectForm extends Zend_Form
{
    /**
     * Установить шаблон для формы
     *
     * @param string $template Имя файла с шаблоном без расширения
     */
    public function setTemplate($template)
    {
    	$formsPath=Zend_Registry::get('config')->paths->view->forms;
        $this->setDecorators(array(
            array('viewScript', array(
               'viewScript' => $formsPath . $template . '.phtml'
            )))
        );
    }

	/**
     * Добавление нескольких элементов в форму без декораторов, перевод текста в title
     *
     * @see Zend_Form::addElements()
     */
	public function addElements($elements)
	{
		parent::addElements($elements);
		foreach($elements as $element)
		{
			$element->removeDecorator('Label');
			$element->removeDecorator('HtmlTag');
			$element->removeDecorator('DtDdWrapper');
			$element->removeDecorator('DtDdWrapper');
			$element->title = $this->getTranslator()->translate($element->title);
		}
	}
	
    /**
     * Добавление элемента в форму без декораторов, перевод текста в title
     *
     * @see Zend_Form::addElement()
     */
    public function addElement($element, $name = null, $options = null)
    {
        parent::addElement($element, $name, $options);

        if (isset($this->_elements[$name])) {
            $this->_elements[$name]->removeDecorator('Label');
            $this->_elements[$name]->removeDecorator('HtmlTag');
            $this->_elements[$name]->removeDecorator('DtDdWrapper');
            $this->_elements[$name]->removeDecorator('DtDdWrapper');
			$this->_elements[$name]->title = $this->getTranslator()->translate($this->_elements[$name]->title);
        }
    }

    /**
     * Создание элемента формы
     *
     * @see Zend_Form::createElement()
     */
    public function createElement($type, $name, $options = null)
    {
        $element = parent::createElement($type, $name, $options);
        $element->removeDecorator('Label');
        $element->removeDecorator('HtmlTag');
        $element->removeDecorator('DtDdWrapper');
        $element->removeDecorator('Description');
        return $element;
    }
}