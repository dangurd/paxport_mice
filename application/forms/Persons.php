<?php
class Application_Form_Persons extends Application_Form_MultiInst
{
    
    protected $_elemsArrName = 'persons';
    
    protected $_lang;
    
    public function init()
    {
        $this->_lang = Zend_Registry::get('lang');
        $this->setAction('');
        $this->setMethod('post');
        $this->setAttrib('id', 'persons-form');
        $this->addElements(array(
            new Zend_Form_Element_Text('fname', array(
                'required' => true,
                'label' => 'First name',
                'class' => 'input-field',
                'id' => 'fname',
				'placeholder' => 'Имя',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),
            new Zend_Form_Element_Text('lname', array(
                'required' => true,
                'label' => 'Last name',
                'class' => 'input-field',
                'id' => 'lname',
				'placeholder' => 'Фамилия',
                'validators' => array(
                    array('NotEmpty', true)
                ),
            )),
            new Zend_Form_Element_Select('sex', array(
                'required' => true,
                'label' => 'Sex',
				'class' => 'select-field',
                'id' => 'sex',
                'multiOptions'=> array(
                    0 => 'Male',
                    1 => 'Female',
                ),
            )),
            new Zend_Form_Element_Select('citizenship', array(
                'required' => true,
				'class' => 'select-field',
                'label' => 'Citizenship',
                'id' => 'citizenship',
                'validators' => array(
                    array(
                        new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::INTEGER + Zend_Validate_NotEmpty::ZERO),
                        true
                    )
                ),
                'multiOptions'=> array(
                    '0' => 'Select citizenship',
                ),
            )),
         ));
         // Подключаем шаблон формы
        $this->setTemplate('persons');
    }

    /**
     * Заполнение формы исходными данным
     */
    public function populateSourceData($data)
    {
        if (isset($data['pts']) && is_array($data['pts']) && !empty($data['pts']))
        {
            $this->addElements(array(
                new Zend_Form_Element_Select('pt', array(
                    'required' => true,
					'class' => 'select-field',
                    'label' => 'Form of Attendance',
                    'id' => 'pt',
                    'class' => 'pers-pt',
                    'multiOptions'=> array(),
                )),
            ));
            foreach ($data['pts'] as $pt)
            {
                $this->pt->addMultiOption($pt['id'], $pt['name']);
            }
            $this->pt->setValue($data['pts'][0]['id']);
        }
        if (isset($data['countries']) && is_array($data['countries']) && !empty($data['countries']))
        {
            foreach ($data['countries'] as $country)
            {
                $this->citizenship->addMultiOption($country['id'], $country['name']);
            }
        }
        if (isset($data['visaTypes']) && is_array($data['visaTypes']) && !empty($data['visaTypes']))
        {
            $this->addElements(array(
                 new Zend_Form_Element_Select('visa', array(
                    'label' => 'Visa support <br><span style="z-index: 100000; font-size:  11px;">In case you need Business Visa Support, <br/> please contact coordinator by e-mail: <a href="mailto:okaev@paxport.ru">okaev@paxport.ru</a></span>',
					'class' => 'select-field',
                    'id' => 'visa',
                    'multiOptions'=> array(
                        0 => 'Not required',
                    ),
                )),
            ));
            foreach ($data['visaTypes'] as $type)
            {
                $this->visa->addMultiOption($type['id'], $type['name']);
            }
        }
        if ($this->_lang == 'ru')
        {
            $this->setDefault('citizenship', Welt_DbTable_WeltCountries::russiaId);
        }
    }
  
}
