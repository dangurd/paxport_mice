<?php
abstract class Application_Form_MultiInst extends Application_Form_Base_RegForm
{
    public $num = null;
    
    protected $_elemsArrName;
    
    public function __construct($num, $options = null)
    {
        $this->num = $num;
        parent::__construct($options);
    }
    
    /**
     * При добавлении элементов, меняем их name и id
     * @see Zend_Form::addElements()
     */
    public function addElements(array $elements)
    {
        foreach($elements as $key => $element)
        {
            if ($element instanceof Zend_Form_Element_Submit)
            {
                continue;
            }
            $elements[$key]->id = $element->id . $this->num;
            $elements[$key]->setBelongsTo($this->_elemsArrName . '[' . $this->num . ']');
        }
        parent::addElements($elements);
    }
}
