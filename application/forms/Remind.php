<?php
//Форма восставновления пароля
class Application_Form_Remind extends Application_Form_Base_LoginForm
{
	public function init()
	{
		$this->setAction('');
		$this->setMethod('post');
		$this->addElements(array(
	        new Zend_Form_Element_Text('email', array(
	        	'required' => true,
	            'label' => 'Email',
				'class' => 'auth-form-text-input',
	            'validators' => array(
	                array('NotEmpty', true, array('messages' => array('isEmpty' => 'Empty email'))),
					array(new Application_Validator_NotExistingEmail(), true)
	            ),
				'filters' => array('StringTrim'),
	        )),
	        new Zend_Form_Element_Text('keystring', array(
				'required' => true,
	            'label' => 'Number from picture',
				'class' => 'auth-form-text-input',
	            'validators' => array(
	                array('NotEmpty', true, array('messages' => array('isEmpty' => 'Empty number from picture'))),
					array(new Application_Validator_Kcaptcha(), true)
	            ),
				'filters' => array('StringTrim')
				
			)),
			new Zend_Form_Element_Submit('smbt', array(
				'label' => 'Recovery',
				'class' =>	'base-submit'
			))
		));
		//Подключаем шаблон формы
		$this->setTemplate('remind');
	}
}