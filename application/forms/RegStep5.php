<?php
class Application_Form_RegStep5 extends Application_Form_Base_RegForm
{
    protected $_userId;
    public $userAppNamespace;

    public function init()
    {
        $this->userAppNamespace = new Zend_Session_Namespace('user');

        $this->_userId = $this->userAppNamespace->userId;

        $person_data = new Welt_DbTable_PersonsDetails();
        $person_info = $person_data->getFirstPersonByUser( $this->_userId );


        $this->setAction('');
        $this->setMethod('post');
        $this->setAttrib('class', 'form-style-2');
        $this->addElements(array(
            new Zend_Form_Element_Text('fname', array(
                'required' => true,
                'label' => 'Contact person first name',
           //     'class' => 'main-form-input-text',
                'class' => 'input-field-2',
                'id' => 'contacts-fname',
                'value'=>$person_info['fname'],
                'validators' => array(
                    array('NotEmpty', true, array('messages' => array('isEmpty' => 'First name field is required')))
                )
            )),
            new Zend_Form_Element_Text('lname', array(
                'required' => true,
                'label' => 'Contact person last name',
            //    'class' => 'main-form-input-text',
                'class' => 'input-field-2',
                'id' => 'contacts-lname',
                'value'=>$person_info['lname'],
                'validators' => array(
                    array('NotEmpty', true, array('messages' => array('isEmpty' => 'Last name field is required'))),
                )
            )),
            new Zend_Form_Element_Radio('title', array(
                'label' => 'Title',
                'id' => 'contacts-title',
                'multiOptions'=> array('Mr.' => 'Mr.', 'Ms.' => 'Ms.','Mrs.' => 'Mrs.'),
            )),
            new Zend_Form_Element_Text('company', array(
                'label' => 'Affiliation',
             //   'class' => 'main-form-input-text',
                'class' => 'input-field-2',
                'value'=>isset($person_info['company'])?$person_info['company']:"",
                'id' => 'contacts-organization'
            )),
            new Zend_Form_Element_Text('address', array(
                'label' => 'Company address',
           //     'class' => 'main-form-input-text',
                'id' => 'contacts-adress'
            )),

            new Zend_Form_Element_Text('phone', array(
                'required' => true,
                'label' => 'Phone',
           //     'class' => 'main-form-input-text',
                'class' => 'input-field-2',
                'value'=>isset($person_info['phone'])?$person_info['phone']:"",
                'id' => 'contacts-phone',
                'validators' => array(
                    array('NotEmpty', true, array('messages' => array('isEmpty' => 'Phone field is required')))
                )
            )),
            new Zend_Form_Element_Text('fax', array(
                'label' => 'Fax',
            //    'class' => 'main-form-input-text',
                'class' => 'input-field-2',
                'id' => 'contacts-fax'
            )),
            new Zend_Form_Element_Select('payment', array(
                'label' => 'Way of payment',
                'id' => 'payment-type',
                'class' => 'select-field',
                'required' => true,
                'multiOptions'=> array(
                    '0' => 'Bank wire transfer',
                    '4' => 'Оплата кредитной картой',
                )
            )),
            new Zend_Form_Element_Checkbox('pdnAgr', array(
                'label'	=> 'I have read and accepted "Consent to Personal Data Processing"',
             //   'class'	=>	'main-form-input-checkbox',
                'class'	=>	'main-form-input-checkbox',
                'required' => true,
                'id' => 'pdn-agr'
            )),
            new Zend_Form_Element_Checkbox('pdnDelete', array(
                'label'	=> 'Delete my Personal Data after the Event',
                'class'	=>	'main-form-input-checkbox',
                'id' => 'pdn-delete'
            )),
            new Zend_Form_Element_Image('finish', array(
                'src' => '/img/finish.gif',
                'id' => 'nex-step-btn'
            )),
        ));
        //Подключаем шаблон формы
        $this->setTemplate('reg-step5');
        // Способы оплаты в зависимости от языка
        $lang = Zend_Registry::get('lang');
        if ($lang == 'ru')
        {
            $this->payment->addMultiOptions(array(
                '3' => 'По квитанции сбербанка',
            ));
        }
        /*
         * Если установлен тип регистрации - открытая, то добавляем поля с email, паролем, устанавливаем валидатор для пароля
         */
        $settings = Zend_Registry::get('settings');
        if ($settings->reg->type == 0)
        {
            $this->addElements(array(
                new Zend_Form_Element_Text('email', array(
                    'required' => true,
                    'label' => 'Email',
            //        'class' => 'main-form-input-text',
                    'class' => 'input-field-2',
                    'value'=> isset($person_info['email'])?$person_info['email']:"",
                    'id' => 'contacts-email',
                    'validators' => array(
                        array('NotEmpty', true, array('messages' => array('isEmpty' => 'Email field is required'))),
                        array(new Zend_Validate_EmailAddress(), true, array('messages' => array('EmailAddress' => 'Invalid email address'))),
                        array(new Application_Validator_ExistingEmail(), true)
                    ),
                    'filters'  => array('StringTrim')
                )),
                new Zend_Form_Element_Password('password', array(
                    'required' => true,
                    'label' => 'Password for personal account',
             //       'class' => 'main-form-input-text',
                    'class' => 'input-field-2',
                    'id' => 'contacts-password',
                    'title'	=> 'Password is to be 5-15 symbols long and to include numbers and Latin symbols only. It will be used for an enter to your Personal cabinet.',
                    'validators' => array(
                        array('NotEmpty', true, array('messages' => array('isEmpty' => 'Password field is required')))
                    )
                )),
                new Zend_Form_Element_Password('passwordConf', array(
                    'required' => true,
                    'label' => 'Password confirm',
              //      'class' => 'main-form-input-text',
                    'class' => 'input-field-2',
                    'id' => 'contacts-cpassword',
                    'validators' => array(
                        array('NotEmpty', true, array('messages' => array('isEmpty' => 'Password confirm field is required'))),
                        array(new Application_Validator_Confirmation(), true)
                    )
                ))
            ));
            $validator = new Zend_Validate_Regex(array('pattern' => '/^[A-Za-z0-9]{5,15}$/'));
            $validator->setMessage('Invalid password format');
            $this->password->addValidator($validator);
        }
    }

    /**
     * Перопределяем функцию для установки значений по умолчанию
     * @param array $data
     * @return
     */
    public function setDefaults(array $data)
    {
        $titleElements = $this->title->getMultiOptions();
        //Установка значения по умолчанию для radio с выбором title
        foreach ($titleElements as $titleElement)
        {
            if ($titleElement == $data['title'])
            {
                $this->title->curValue = $data['title'];
            }
        }
        return parent::setDefaults($data);
    }


}
