<?php
/*
 * Перевод на английский язык модуля "Регистрационная форма"
 */
return array (
	/*
	 * Общее
	 */
	/*
	 * Вступительный и завершающий шаг
	 */
	'#pcc_intro' => 'Personal Account',
	/*
	 * 1 шаг
	 */
	'#reg_fee_incl' => 'Registration fee includes participation, conference proceedings, lunches, coffee breaks. Registration fee does not include transportation, accommodation, visa, insurance.',
	'#persons_attention' => '<p class="bolder">Some personal data were filled in incorrectly.</p><p>Please check the data’s correctness for all the participants.</p>',
	/*
	 * 2 шаг
	 */
	'#hotel-help-tooltip'	=>	'<p>In this section you can book hotel rooms (single/ double accommodation). For booking a room, please, specify period of accommodation, accommodation type (single/ double) and tick persons on the list. If needed, add your comments and press "Book room" button.</p><p>If you need to Book more rooms repeat the procedure and press "Book more room" button. If you need to delete a booking, please, press red cross. After completing the list of rooms, please, proceed to the next section pressing "Next step". </p>',
	'Да' => 'Yes',
	'Нет' => 'No',
	'Да, 24 часа' => 'Yes, 24 h.',
	'Да (бесплатно)' => 'Yes, free of charge',
	'Да (07:00 - 22:00)' => 'Yes, 07:00 - 22:00',
	'Да (08:00 - 22:00)' =>	'Yes, 08:00 - 22:00',
	/*
	 * 3 шаг
	 */
	'#transport-help-tooltip' => '<p>In this section you can order transportation services for persons. For ordering transportation services, please, tick names of persons on the list, choose transportation/transfer type, specify date and time of services and fill in all the necessary fields. </p><p>If needed, add your comments and press "Book transfer" button. If you need to order other transportation services repeat the procedure and press "Book more transfer" button. If you need to delete a service, please, press red cross.</p>',
	'#meal-help-tooltip' => '<p>In this section you can order meal services for persons. For ordering meal services, please, tick names of persons on the list, choose meal and meal type. If needed, add your comments and press "Book meal" button. </p><p>If you need to order other meal services repeat the procedure and press "Book more meal" button. If you need to delete a service, please, press red cross.</p>',
	'#excursion-help-tooltip' => '<p>In this section you can order excursion services for persons. For ordering excursion services, please, tick names of persons on the list, choose excursion type and preferred date. If needed, add your comments and press "Book event" button. </p><p>If you need to order other excursion services repeat the procedure and press "Book more event" button. If you need to delete a service, please, press red cross.</p>',
	'#excursions_attention' => '',
	'#transport_notes' => 'All rates are quoted in rubles per vehicle, inclusive VAT. All rates are subject to change. Price also includes 1 waiting hour for meeting at airports, 30 minutes at railway station and 15 waiting minutes for seeing off. Extra time will be charged at the rate of 1 working hour of chosen vehicle. In this case the Client also pays the parking fee.',
	/*
	 * 4 шаг
	 */
	'#summary-note' => 'Please check with your order once again and make sure that you have not forgotten anything.<br />If you want to add more services, please, return to previous steps by pressing «Previous step» button.',
	'#summary_visa_attention' => 'Please, pay attention!!! In case you didn’t book a room through our agency we can’t provide you with visa support. If you have any questions please write to us',
	'Оплата кредитной картой' => 'Credit Card payment',
);