<?php
/*
 * Перевод на английский язык модуля "ЛК участника"
 */
return array (
	'#login_form_header' => 'Login',
	'#payment-online-warning' => '<p class="first">Dear Clients!</p><p>We kindly pay your attention that grounded on reasons of your safety the bank fixes the limit for one-time online payments by bank card - 100 000.00 RUR. If the total amount of your order exceeds the limit, please, split your payment into separate parts no more than 100 000.00 RUR each or contact us.</p><p>Thank you for your understanding.</p>',
	'#payment-attention' => '<p class="bolder">Please pay attention!!!</p><p>If you paid online for the ordered services, information about this payment will be specified in your Personal cabinet only on the next working day.</p>'
);