<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
/**
* Класс начальной загрузки приложения
*/	

    /**
     * Кешируем пути к удачно подключенным файлам для увеличения производитель-
     * ности
     *
     * Пути удачно подключенных файлов в процессе работы приложения кешируются
     * в файл pluginLoaderCache.php и в последующим загрузчику плагинов не
     * нужно лишний раз сканировать файловую систему на проверку наличия 
     * необходимых для работы подключаемых файлов, что многократно увеличивает
     * производительность (как минимум в 3-4 раза).
     */
    protected function _initPluginLoaderCache()
    {
        if ( (APPLICATION_ENV == 'production') || (APPLICATION_ENV == 'development')) 
		{
            $classFileIncCache = APPLICATION_PATH.'/../data/cache/pluginLoaderCache.php';
            if (file_exists($classFileIncCache) ) {
                include_once $classFileIncCache;
            }
            Zend_Loader_PluginLoader::setIncludeFileCache($classFileIncCache);
        }
    }
	
	/**
    * Задаем параметры кеширования
    */
    protected function _initCache()
    {
        // Получаем доступ к ресурсу Zend_Cache_Manager
        $this->bootstrap('CacheManager');
        $cacheManager = $this->getResource('CacheManager');
        $this->_cacheManager = $cacheManager;
        // заносим объект Zend_Cache_Manager в реестр приложения
        Zend_Registry::set('cacheManager', $cacheManager);
        // Включаем кеширование метаданных БД
        // @since ZF 1.10.0
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cacheManager->getCache('metadataCache'));
		//Включаем кеширование для Zend_Translate и Zend_Locale
		$langCache = $cacheManager->getCache('lang');
		Zend_Translate::setCache($langCache);
		Zend_Locale::setCache($langCache);
        // для использования в ZFDebug
        // @since ZF 1.10.0
        return $cacheManager->getCache('metadataCache');
    }


	/**
	* Установка конфигурации приложения, а также загрузка пользовательских настроек
	*/	
	 protected function _initConfig()
     {
     	/**
     	 * Если конфиги закешированы, грузим из кэша
     	 */
     	$configsCache = $this->_cacheManager->getCache('configsCache');
     	if(!$config = $configsCache->load('application_ini'))
		{
			$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini',APPLICATION_ENV);
			$configsCache->save($config,'application_ini',array('configs'));
		}
		if(!$user_config = $configsCache->load('settings_ini'))
		{
			$user_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini');
			$configsCache->save($user_config,'settings_ini',array('configs'));
		}
		if(!$navConfig = $configsCache->load('navigation_xml'))
		{
			$navConfig = new Zend_Config_Xml(APPLICATION_PATH . '/configs/navigation.xml');
			$configsCache->save($navConfig,'navigation_xml',array('configs'));
		}
		$this->_config = $config;
		/**
		 * Добавляем объекты конфигов в реестр
		 */
		Zend_Registry::set('config', $config);
		Zend_Registry::set('settings', $user_config->production);
		Zend_Registry::set('navigation', $navConfig);
     } 
	 
	 /**
	 * Запись в реестр настроек Db
	 */	
	 protected function _initDbs()
     {
     	 $this->bootstrap('multidb');
         $resource = $this->getPluginResource('multidb');
		 $db1 = $resource->getDb('mainDb');
		 $db2 = $resource->getDb('welt');
		 $db3 = $resource->getDb('expoMice');
		 Zend_Registry::set('mainDb',$db1);
		 Zend_Registry::set('welt',$db2);
		 Zend_Registry::set('expoMice',$db3);
	 }

	/*
	protected function _initZFDebug()
    {
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('ZFDebug');

        $options = array(
            'plugins' => array(
				'Database' => array('adapter' => $db),
                'Variables',
				'File' => array('base_path' => APPLICATION_PATH),
                //'Memory',
                //'Time',
                //'Registry',
                'Exception',
                'Html',
            )
        );

        // Настройка плагина для адаптера базы данных
        if ($this->hasPluginResource('db')) {
            $this->bootstrap('db');
            $db = $this->getPluginResource('db')->getDbAdapter();
            $options['plugins']['Database']['adapter'] = $db;
        }

        // Настройка плагина для кеша
        if ($this->hasPluginResource('cache')) {
            $this->bootstrap('cache');
            $cache = $this-getPluginResource('cache')->getDbAdapter();
            $options['plugins']['Cache']['backend'] = $cache->getBackend();
        }

        $debug = new ZFDebug_Controller_Plugin_Debug($options);

        $this->bootstrap('frontController');
        $frontController = $this->getResource('frontController');
        $frontController->registerPlugin($debug);
    }
	*/

	 
	/**
	* Настройка автозагрузки ресурсов
	*/
	protected function _initAutoload()
	{
		require_once 'Zend/Loader/Autoloader.php';
		$autoloader=Zend_Loader_Autoloader::getInstance();
		$autoloader->setFallbackAutoloader(true);
		$resourceLoader = new Zend_Loader_Autoloader_Resource(array(
		          'basePath'  => APPLICATION_PATH,
		          'namespace' => $this->_config->appnamespace,
		));
		//Регистрируем новые типы ресурсов - плагины для Zend_Controller, роли, валидаторы
		$resourceLoader->addResourceType('acl', 'acls/', 'Acl');
		$resourceLoader->addResourceType('validators', 'validators/', 'Validator');
		$front = Zend_Controller_Front::getInstance();
         //Плагин, загружающий настройки мероприятия
        $front->registerPlugin(new Welt_Controller_Plugin_EventParams());
		//Плагин, контролирующий срок проведения мероприятия
		$front->registerPlugin(new Welt_Controller_Plugin_Event());
		//Плагин, контролирующий корректность заявки в сессии у пользователя
		$front->registerPlugin(new Welt_Controller_Plugin_CheckAppExist());
		//Плагин для проверки шага регистрации, на котором находится пользователь на основании данных сессии
		$front->registerPlugin(new Welt_Controller_Plugin_RegStepCheck());
		//Плагин для вывода сообщений об ошибках
		$front->registerPlugin(new Welt_Controller_Plugin_FlashMessenger());
		//Определяем правила для подключения моделей при использовании модульной структуры (с помощью плагина фронт-контроллера)
		$front->registerPlugin(new Welt_Controller_Plugin_Models());
		//Плагин для загрузки конфигов модулей
		$front->registerPlugin(new Welt_Controller_Plugin_ModuleConfigLV());
		//Плагин для обеспечения мультиязычности
		$front->registerPlugin(new Welt_Controller_Plugin_Lang());
	}

	
	/**
	* Инициализация ролей через плагин
	*/
	protected function _initAcl() 
    {
		$acl  = new Application_Acl_Roles();
		Zend_Registry::set('acl', $acl);
		$front = Zend_Controller_Front::getInstance();
		$front->registerPlugin(new Welt_Controller_Plugin_Auth($acl));
	}
	
	/**
	 * Firebug logger
	 * @return 
	 */
	protected function _initFirebugLogger()
	{
		$logger = new Zend_Log();
		$writer = new Zend_Log_Writer_Firebug();
		$logger->addWriter($writer);
		Zend_Registry::set('logger',$logger);
	}
	


}
