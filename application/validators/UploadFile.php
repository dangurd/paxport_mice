<?php
//Валидатор,проверяющий соответствие введённых паролей
class Application_Validator_UploadFile extends Zend_Validate_Abstract
{
	const MAX_SIZE = 'maxSize';
	const MAX_FILES = 'maxFiles';
	
	public $maxFileSize;
	
	protected $_messageVariables = array(
        'maxFileSize' 	  => 'maxFileSize'
    );
	
	protected $_messageTemplates = array(
			self::MAX_SIZE 			=> 'File size greater than %maxFileSize% Kb'
	);
	
	
	public function isValid($fileKey)
	{
		$this->_setValue($fileKey);
		$this->maxFileSize = (int) $this->maxFileSize;
		//Загруженный файл
		$file = $_FILES[$fileKey];
		$fileSize = (int) round($file['size']/1024);
		//Проверка на максимальный объём
		if ($fileSize > $this->maxFileSize)
		{
			$this->_error(self::MAX_SIZE);
			return false;
		}
		return true;
	}

}