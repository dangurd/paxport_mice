<?php
/**
 * Валидатор для проверки корректности заказа на проживание в номере
 */
class Application_Validator_RoomOrder extends Zend_Validate_Abstract
{
	const IS_EMPTY = 'isEmpty';
	const INCORRECT_PERDIOD = 'incorrectPeriod';
	const ACCOMODATION_ERROR = 'accError';
	
	protected $_messageTemplates = array(
			self::IS_EMPTY 				=> 	'Empty order',
			self::INCORRECT_PERDIOD 	=> 	'Period incorrect',
			self::ACCOMODATION_ERROR 	=>	'Selected accommodation does not match the number of persons'
	);
	
	public function isValid($order)
	{
		$this->_setValue((array) $order);
		//Проверка на наличие элементов
		if (is_array($order) && sizeof($order)>0)
		{
			//Проверка корректности периода
			if(!Welt_Functions::checkPeriod($order['startDate'],$order['endDate']))
			{
				$this->_error(self::INCORRECT_PERDIOD);
				return false;
			}
			//Проверка на соответствие выбранного типа проживания и количества добавленных персон
			$selectedAcc = $order['accommodation'];
			if ($selectedAcc==2 && $order['extraBed']==1)
			{
				$selectedAcc++;
			}
			if (sizeof($order['persons'])!=$selectedAcc)
			{
				$this->_error(self::ACCOMODATION_ERROR);
				return false;
			}
			return true;
		}
		else
		{
			$this->_error(self::IS_EMPTY);
			return false;
		}
	}

}