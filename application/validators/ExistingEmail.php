<?php
//Валидатор проверяющий, не зарегистрирован ли уже пользователь в системе
class Application_Validator_ExistingEmail extends Zend_Validate_Abstract
{
	const IS_EXIST = 'isExist';
	
	protected $_messageTemplates = array(
			self::IS_EXIST => "User with email '%value%' already exists"
	);
	
	public function isValid($value)
	{
		//Получаем введённый пользователем email
		$userEmail = (string) $value;
		$this->_setValue($userEmail);
		//Проверяем существование email
		$usersTable=new Welt_DbTable_Users();
		$emailExist=$usersTable->checkByEmail($userEmail);
		//Если пользователь существует
		if ($emailExist==0) 
		{
			return true;
		}
		else 
		{
			$this->_error(self::IS_EXIST);
			return false;
		}
		
	}

}