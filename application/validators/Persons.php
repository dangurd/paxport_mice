<?php
/**
 * Валидатор для проверки корректности введённых пользователем данных по персонам на 2 шаге регистрации
 */
class Application_Validator_Persons extends Zend_Validate_Abstract
{
	const IS_EMPTY = 'isEmpty';
	const ELEMENTS_EMPTY = 'elementsEmpty';
	const ELEMENT_EMPTY = 'elementEmpty';
	const INCORRECT_CITIZENSHIP = 'incorrectCitizenship';
	
	protected $_messageTemplates = array(
			self::IS_EMPTY => 'Please add persons',
			self::ELEMENTS_EMPTY => 'Please fill all data for added persons',
			self::ELEMENT_EMPTY => 'Please fill required fields for person',
			self::INCORRECT_CITIZENSHIP => 'Please select citizenship for added persons',
	);
	
	/**
	 * @var Поля, обязательные для заполнения
	 */
	protected $requiredFields = array(
		'fname',
		'lname'
	);
	
	public function isValid($persons)
	{
		$this->_setValue((string) $persons);
		$personsSize = sizeof($persons);
		if (is_array($persons) && $personsSize>0)
		{
			foreach ($persons as $person)
			{
				foreach ($person as $param=>$value)
				{
					//Проверка элементов на пустоту
					if (in_array($param, $this->requiredFields) && trim($value)=='')
					{
						if ($personsSize==1)
						{
							$this->_error(self::ELEMENT_EMPTY);
						}
						else
						{
							$this->_error(self::ELEMENTS_EMPTY);
						}
						return false;
					}
					//Проверка корректности ввода даты рождения
					if ($param == 'citizenship_id' && in_array($param, $this->requiredFields))
					{
						if ($value == '0')
						{
							$this->_error(self::INCORRECT_CITIZENSHIP);
							return false;
						}
					}
				}
			}
			return true;
		}
		else
		{
			$this->_error(self::IS_EMPTY);
			return false;
		}
	}
	
	/**
	 * Добавление обязательных для заполнения полей
	 * @param array $fields
	 */
	public function addRequiredFields($fields)
	{
		if (is_array($fields) && sizeof($fields) > 0)
		{
			$this->requiredFields  = array_merge($this->requiredFields, $fields);
		}
	}

}