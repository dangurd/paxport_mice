<?php
//Валидатор проверяющий email на существование в системе
class Application_Validator_NotExistingEmail extends Zend_Validate_Abstract
{
	const NOT_EXIST = 'notExist';
	
	protected $_messageTemplates = array(
			self::NOT_EXIST => "Email '%value%' not found"
	);
	

	public function isValid($value)
	{
		$this->_setValue($value);
		$emailValidator = new Zend_Validate_Db_RecordExists(
    		array(
        		'table' => Auth::usersTable,
       			'field' => 'email'
			)
		);
		if ($emailValidator->isValid($value))
		{
			return true;
		}
		else
		{
			$this->_error(self::NOT_EXIST);
			return false;
		}	
	}
}