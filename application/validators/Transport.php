<?php
/**
 * Валидатор для проверки корректности заказа на транспорт
 */
class Application_Validator_Transport extends Zend_Validate_Abstract
{
	const IS_EMPTY = 'isEmpty';
	const EMPTY_FIELDS = 'missingDate';
	const DATE_INCORRECT = 'dateIncorrect';
	const TIME_INCORRECT = 'timeIncorrect';
	
	protected $_messageTemplates = array(
			self::IS_EMPTY 				=> 	'Empty order',
			self::EMPTY_FIELDS 			=>	'Please type all required fields',
			self::DATE_INCORRECT		=>	'Incorrect date format',
			self::TIME_INCORRECT		=>	'Incorrect time format'
	);
	
	/**
	 * @var Поля, не обязательные для заполнения
	 */
	protected $notRequiredFields = array(
		'persons',
		'comment',
		'train_car',
		'station',
		'stationId',
		'number'
	);
	
	public function isValid($order)
	{
		$this->_setValue((array) $order);
		//Проверка на наличие элементов
		if (is_array($order) && sizeof($order)>0)
		{
			foreach ($order as $name=>$value)
			{
				//Проверка на пустые поля
				if (!in_array($name,$this->notRequiredFields) && trim($value)=='')
				{
					$this->_error(self::EMPTY_FIELDS);
					return false;
				}
				//Проверка корректности введённой даты
				if ($name == 'date')
				{
					if (!Welt_Functions::checkDate($value))
					{
						$this->_error(self::DATE_INCORRECT);
						return false;
					}
				}
				//Проверка корректности введённого времени
				if ($name == 'time')
				{
					if(!preg_match('/^([0-1][0-9]|[2][0-3]){1,2}\:([0-5][0-9])$/',$value))
					{
						$this->_error(self::TIME_INCORRECT);
						return false;
					}
				}
			}
			return true;
		}
		else
		{
			$this->_error(self::IS_EMPTY);
			return false;
		}
	}

}