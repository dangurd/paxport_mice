<?php
/**
 * Валидатор для проверки корректности заказа на проживание на 3 шаге регистрации
 */
class Application_Validator_Excursion extends Zend_Validate_Abstract
{
	const IS_EMPTY = 'isEmpty';
	
	protected $_messageTemplates = array(
			self::IS_EMPTY 				=> 	'Empty order'
	);
	
	public function isValid($order)
	{
		$this->_setValue((array) $order);
		//Проверка на наличие элементов
		if (is_array($order) && sizeof($order)>0)
		{
			return true;
		}
		else
		{
			$this->_error(self::IS_EMPTY);
			return false;
		}
	}

}