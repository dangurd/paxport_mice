<?php
class Application_Validator_Kcaptcha extends Zend_Validate_Abstract
{
	const IS_VALID = 'isValid';
	
	protected $_messageTemplates = array(
			self::IS_VALID => "Wrong number from picture"
	);
	

	public function isValid($value)
	{
		$this->_setValue($value);
		if (Zend_Session::namespaceIsset('Captcha'))
		{
			$captchaNamespace = new Zend_Session_Namespace('Captcha');
			if($captchaNamespace->keystring == $value)
			{
				return true;
			}
			else
			{
				$this->_error(self::IS_VALID);
				return false;
			}
		}
		else
		{
			$this->_error(self::IS_VALID);
			return false;
		}	
	}
}