<?php
class Application_View_Helper_DisplayOrderDates extends Zend_View_Helper_Abstract
{
	/**
	 * Отображение даты/периода, на который заказана услуга
	 * @param array $order 
	 */
	public function displayOrderDates($order, $diplayTime = true)
	{
		$periodStr = '';
		// Дата/период
		if (isset($order['start_date']) && isset($order['end_date']))
		{
			if ($order['start_date'] == $order['end_date'])
			{
				$periodStr = $order['start_date'];
			}
			else
			{
				$periodStr = $order['start_date'] . ' - ' . $order['end_date'];
			}
		}
		elseif (isset($order['date']))
		{
			$periodStr = $order['date'];
		}
		else
		{
			$periodStr = '';
		}
		// Время
		if ($diplayTime && isset($order['start_time']) && isset($order['end_time']) && ($order['start_time'] != '00:00' || $order['end_time'] != '00:00'))
		{
			if ($order['start_time'] == $order['end_time'])
			{
				$periodStr .= ' (' . $order['start_time'] . ')';
			}
			else
			{
				$periodStr .= ' (' . $order['start_time'] . ' - ' . $order['end_time'] . ')';
			}
		}
		return $periodStr;
	}
}
