<?php
class Application_View_Helper_ErrorMessages extends Zend_View_Helper_Abstract
{
	public function errorMessages()
	{
		// Инициализируем помощник FlashMessenger и получаем сообщения
        $actionHelperFlashMessenger = new Zend_Controller_Action_Helper_FlashMessenger();
        $messages = $actionHelperFlashMessenger->setNamespace('ErrorMessages')->getMessages();
		//Если сообщений нет
        if (empty($messages)) 
		{
        	return;
        }
        // Создаём объект  вида
       $view = new Zend_View();
	   $view->setScriptPath(APPLICATION_PATH . '/templates/');
       // Добавляем переменную для вида
	   $view->messages = $messages;        
	   // Устанавливаем объект вида с новыми переменным и производим рендеринг скрипта вида в сегмент messages
       $messagesContent = $view->render('messages.phtml');
	   return $messagesContent;
	}
}