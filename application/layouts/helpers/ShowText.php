<?php
/**
 * Помощник для вывода текста с выполнением замены в нём некоторых значений/символов 
 */
class Application_View_Helper_ShowText extends Zend_View_Helper_Abstract
{
	public function showText($text)
	{
		$phoneHelper = new Application_View_Helper_DisplayCoordPhone();
		$eventParams = Zend_Registry::get('eventParams'); 
		$translator = Zend_Registry::get('Zend_Translate');
		$text = strtr($text, array(
			'#name' => $eventParams->coordNameLng,
			'#email' => '<a href="mailto:' . $eventParams['coord_email'] . '">' . $eventParams['coord_email']  .'</a>',
			'#phone' => $phoneHelper->displayCoordPhone(
				$eventParams['coord_phone'], $eventParams['coord_phone_ext']
			),
			'#lk' => '<a target="_blank" href="/ppc">' . $translator->translate('#pcc_intro') . '</a>',
		));
		return $text;
	}
}
