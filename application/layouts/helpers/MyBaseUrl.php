<?php
/**
 * Помощник для вывода текущего url
 */
class Application_View_Helper_MyBaseUrl extends Zend_View_Helper_Abstract
{
	public function myBaseUrl()
	{
		$front = Zend_Controller_Front::getInstance();
		$request = $front->getRequest();
		$layout = Zend_Layout::getMvcInstance();
		$view = $layout->getView();
		$url = $view->url(array(
			'module'		=>	$request->module,
			'controller'	=>	$request->controller,
			'action'		=>	$request->action
		));
		//Если на конце полученного url нет "/", то добавляем его, чтобы получился корректный адрес
		$curUrlLastSymb = $url[strlen($url)-1];
		if ($curUrlLastSymb != '/')
		{
			$url.='/';
		}
		return $url;
	}
}