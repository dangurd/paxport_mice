<?php
/**
 * Получение авторизационных данных пользователя
 */
class Application_View_Helper_GetAuthData extends Zend_View_Helper_Abstract
{
	public function getAuthData($var='')
	{
		//Получаем экземпляр Zend_Auth, устанавливаем пространство имён
		$auth = Zend_Auth::getInstance();
		$auth->setStorage(new Zend_Auth_Storage_Session('ppc'));
		//Если пользователь не залогинен
		if (!$auth->hasIdentity())
		{
			return true;
		}
		//Получаем данные пользователя
		$authData = $auth->getIdentity();
		//Если запрошена конкретная переменная
		if (isset($var) && $var!='')
		{
			if (array_key_exists($var,$authData)) return $authData[$var];
		}
		//Если запрошена, не переменная, а все данные
		else
		{
			return $authData;
		}
	}
}