<?php
/**
 * Помощник для вывода телефона координатора 
 */
class Application_View_Helper_DisplayCoordPhone extends Zend_View_Helper_Abstract
{
	public function displayCoordPhone($phone, $phoneExt)
	{
		$translator = Zend_Registry::get('Zend_Translate');
		$phoneStr = $phone;
		if (!is_null($phoneExt) && $phoneExt != '')
		{
			$phoneStr .= ' ' . $translator->translate('ext.') . ' ' . $phoneExt;
		}
		return $phoneStr;
	}
}
