<?php
class Registration {
	
	private $userAppNamespace;
	private $dbAdapter;
	private $appId;
	private $userId;
	private $settings;
	
	private $appObj;
	private $servicesObj;
	protected $servicesTypes;
	protected $lang;
	
	public function __construct()
	{
		$this->userAppNamespace = new Zend_Session_Namespace('user');
		$this->dbAdapter = Zend_Registry::get('mainDb');
		$this->settings = Zend_Registry::get('settings');
		$this->eventParams = Zend_Registry::get('eventParams');
		//Если сессия существует, получаем из неё id заявки и юзера, создаём объекты для работы с заявками и услугами
		if ($this->checkSessionExist())
		{
			$this->appId = $this->userAppNamespace->appId;
			$this->userId = $this->userAppNamespace->userId;
			$this->appObj = new Welt_Applications($this->appId,$this->userId);
			$this->servicesObj = new Welt_Services($this->appId,$this->userId);
			//Устанавливаем доступные для заказа типы услуг
			$this->servicesTypes = $this->servicesObj->getServicesTypes();
		}
		//Если сессия не создана, получаем доступные для заказа типы услуг из БД
		else
		{
			$servicesTypesTable = new Welt_DbTable_ServicesTypes();
			$this->servicesTypes = $servicesTypesTable->getSysNamesArr();
		}
		$this->lang = Zend_Registry::get('lang');
	}
	
	/**
	 * Получение списка доступных для заказа типов услуг
	 */
	public function getServicesTypes()
	{
		return $this->servicesTypes;
	}
	
	/**
	 * Подготовка данных для регистрации - создание пустой заявки и юзера с пустыми данными.
	 * @return 
	 */
	public function prepareReg()
	{
		//Записываем пустого пользователя в БД
		$usersTable = new Welt_DbTable_Users();
		//Если произошла ошибка записи в таблицу
		if(!$usersTable->insertEmptyUser())
		{
			return false;
		}
		//Получаем id созданного пользователя
		$this->userId = $usersTable->getAdapter()->lastInsertId();
		//Записываем в сессию userId
		$this->userAppNamespace->userId = $this->userId;
		//Добавлем новую пустую заявку
		if(!Welt_Applications::addEmptyApp($this->userId))
		{
			return false;
		}
		//Добавляем в сессию номер заявки и текущий шаг
		$this->appId = $this->dbAdapter->lastInsertId();
		$this->userAppNamespace->appId = $this->appId;
		$this->userAppNamespace->step = 1;
		//Создаём объекты классов для работы с заявками и услугами
		$this->appObj = new Welt_Applications($this->appId,$this->userId);
		$this->servicesObj = new Welt_Services($this->appId,$this->userId);
		return true;
	}
	
	
	/**
	 * Проверка на существование сессии и необходимых параметров в ней
	 * @return 
	 */
	public function checkSessionExist()
	{
		if (isset($this->userAppNamespace->appId) && isset($this->userAppNamespace->userId))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Подготовка данных по персонам (первый шаг регистрации)
	 * @return
	 */
	public function preparePersonsList()
	{
		//Получаем данные по добавленным персонам и передаём их во view
		$personsTable = new Welt_DbTable_Persons();
		$persons = $personsTable->getUserPersons($this->userId,0,array(0,1,2));
		//Если возможен заказ визы, то получаем для каждой персоны заказы на визовую поддержку
		if (in_array('visa',$this->servicesTypes) && ($this->lang == 'en' || $this->settings->show_visa == 1))
		{
			$personsSize = sizeof($persons);
			$servicesAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
			$visaOrdersTable = new Welt_DbTable_VisaOrders();
			for($i=0;$i<$personsSize;$i++)
			{
				$personVisaOrdersIds = $servicesAndPersonsTable->getPersonServices($persons[$i]['id'],'visa');
				//Если есть заказы на визу для персоны
				if (is_array($personVisaOrdersIds) && sizeof($personVisaOrdersIds)>0)
				{
					$personVisaOrderId = $personVisaOrdersIds[0]; //Т.к. для 1 персоны м.б. заказана только 1 визовая поддержка
					$persons[$i]['visa']['status'] = 1;
					//Получаем выбранный тип визы и стоимость
					$persons[$i]['visa']['details'] = $visaOrdersTable->getOrderDetails($personVisaOrderId);
				}
				//Если заказов нет
				else
				{
					$persons[$i]['visa']['status'] = 0;
				}
			}
		}
		return $persons;
	}
	
	/**
	 * Подготовка данных для 1 шага
	 * @return 
	 */
	public function step1Prepare()
	{
		//Получаем возможные типы участников
        $personTypesTable = new Welt_DbTable_PersonsTypes();
        $personsTypes = $personTypesTable->getPersonTypes();
        //Если существует тип услуги рег. сбор, то получаем типы и стоимости рег. сборов
        if (in_array('reg_fee',$this->servicesTypes))
        {
            $stepData['regFeeFlag'] = 1;
            //Получаем возможные типы регистрационного сбора
            $regFeeTypesTable = new Welt_DbTable_RegFeeTypes();
            $regFeeTypes = $regFeeTypesTable->getRegFeeTypes();
            $stepData['regFeeTypes'] = $regFeeTypes;
            $stepData['regFeeTypesSize'] = sizeof($regFeeTypes);
            //Получаем стоимость рег. сбора для разных типов участников
            $regFeePricesTable = new Welt_DbTable_RegFeePrices();
            $personsTypesSize = sizeof($personsTypes);
            for ($i=0; $i<$personsTypesSize; $i++)
            {
                $personsTypes[$i]['prices'] = $regFeePricesTable->getRegFeePricesRow($personsTypes[$i]['id']);
            }
        }
        else
        {
            $stepData['regFeeFlag'] = 0;
        }
        $stepData['personsTypes'] = $personsTypes;
        //Если возможен заказ визы, то получаем все возможные варианты визовой поддержки
        if (in_array('visa',$this->servicesTypes) && ($this->lang == 'en' || $this->settings->show_visa == 1))
        {
            $stepData['visaFlag'] = 1;
            $visaTypesTable = new Welt_DbTable_VisaTypes();
            $stepData['visaTypes'] = $visaTypesTable->getVisaTypes();
        }
        else
        {
            $stepData['visaFlag'] = 0;
        }
        //Получаем список стран для выбора гражданства
        $countriesTable = new Welt_DbTable_WeltCountries();
        $stepData['countries'] = $countriesTable->getCountriesList();
        //Id России для выбора по умолчанию в русской версии формы
        $stepData['russiaId'] = Welt_DbTable_WeltCountries::russiaId;
        return $stepData;
	}
	
	/**
	 * Обработка данных по персонам (первый шаг регистрации)
	 * @param mixed $persons
	 * @return 
	 */
	public function personsProcess($persons)
	{
	    
		//Если регистрация открытая и сесия не существует подготовка к процессу регистрации
        if($this->settings->reg->type!=1 && !$this->checkSessionExist())
        {
            $this->prepareReg();
        }
        //Массив из id добавленных персон
        $personIds = array();
        //Обрабатываем персоны и их услуги
        $i = 0;
        $personsTable = new Welt_DbTable_Persons();
        $personsDetsTable = new Welt_DbTable_PersonsDetails();
        foreach ($persons as $person) 
        {
            // Скидка на участие
            $discount = 0;
            //Сохраняем информацию о персоне
            $personData = array();
            $personData['user_id'] = $this->userId;
            $personData['application_id'] = $this->appId;
            $personData['sex'] = $person['sex'];
            $personData['fname'] = $person['fname'];
            $personData['lname'] = $person['lname'];
            $personData['citizenship_id'] = $person['citizenship'];
           // $personData['person_type_id'] = $person['pt'];
            //print_r($personData);die;
            if (is_null($personData['person_type_id']) || $personData['person_type_id'] == '')
            {
                $personData['person_type_id'] = 1;
            }
            $personsTable->insert($personData);
            $personId = $personsTable->getAdapter()->lastInsertId();
            
            //Сохраняем детали по персоне

            $personDetails = array(
                'person_id' => $personId,
            ); 
            $personsDetsTable->insert($personDetails);

            //Если доступен заказ визы и необходима визовая поддержка, добавляем к заявке
            if(in_array('visa',$this->servicesTypes) && isset($person['visa']) && $person['visa'] != 0)
            {
                //Устанавливаем персоны для заказа, в данном случае одна персона
                $this->servicesObj->setPersons(array(0=>array('id'=>$personId)));
                $this->servicesObj->setServiceType('visa');
                $serviceData['visaTypeId'] = $person['visa'];
                $this->servicesObj->addVisaOrder($serviceData);
            }
            //Если существует тип услуги рег. сбор, то добавляем заказ рег. сбора
            if (in_array('reg_fee',$this->servicesTypes))
            {          
                $this->servicesObj->setPersons(array(0=>array('id'=>$personId)));
                $this->servicesObj->setServiceType('reg_fee');
                $regFeeData = array(
                    'person_type_id' => $person['pt'],
                );
                $this->servicesObj->addRegFeeOrder($regFeeData);
            }
            //Список id добавленных персон
            $personIds[$i]['id'] = $personId;
            $i++;
        }
        //Изменяем номер шага в сессии
        if (!isset($this->userAppNamespace->step) || $this->userAppNamespace->step == 1)
        {
            $this->userAppNamespace->step = 2;
        }
        return true;
	}
	
	/**
	 * Удаление персоны + удаление персоны из всех услуг + удаление услуг, которые заказаны только для этой персоны
	 * @return 
	 */
	public function personDelete($personId)
	{
		//Получаем все заказы на проживание, в которых участвует персона
		$serviceAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
		//Удаляем полученные заказы на проживание
		if (in_array('hotel',$this->servicesTypes))
		{
			$personHotelOrdersIds = $serviceAndPersonsTable->getPersonServices($personId,'hotel');
			$this->servicesObj->setServiceType('hotel');
			foreach ($personHotelOrdersIds as $orderId)
			{
				$this->servicesObj->deleteServiceOrder($orderId);
			}	
		}
		//Получаем услуги, заказанные только для заданной персоны
		$onlyPersonOrders = $this->servicesObj->getOnlyPersonServicesOrders($personId);
		//Удаляем персону
		$personsTable = new Welt_DbTable_Persons();
		$where = $personsTable->getAdapter()->quoteInto('id = ?', $personId);
		if (!$personsTable->delete($where))
		{
			return false;
		}
		//Удаляем услуги, заказанные только для заданной персоны
		foreach($onlyPersonOrders as $order)
		{
			$this->servicesObj->setServiceType($order['service_type']);
			$this->servicesObj->deleteServiceOrder($order['service_id'],1);
		}
		return true;
	}
	
	/**
	 * Заказ визовой поддержки для указанной персоны
	 * @param int $personId
	 * @return 
	 */
	public function addPersonVisaOrder($personId,$visaTypeId)
	{
		//Устанавливаем персоны для заказа, в данном случае 1 персона
		$this->servicesObj->setPersons(array(0=>array('id'=>$personId)));
		//Добавляем услугу визовая поддержка
		$this->servicesObj->setServiceType('visa');
		$this->servicesObj->addVisaOrder(array('visaTypeId'=>$visaTypeId));
		//Получаем данные по заказываемому типу визовой поддержки, формируем ответ
		$visaTypesTable = new Welt_DbTable_VisaTypes();
		$visaData = $visaTypesTable->getVisaTypeInfo($visaTypeId);
		$layout = Zend_Layout::getMvcInstance();
		$view = $layout->getView();
		$displayVisaNameHelper = new Zend_View_Helper_DisplayVisaName();
		$displayVisaNameHelper->setView($view);
		$visaStr = $displayVisaNameHelper->displayVisaName($visaData, 2);
		return $visaStr;
	}
	
	/**
	 * Удаление заказа на визовую поддержку для указанной персоны
	 * @param int $personId
	 * @return 
	 */
	public function deletePersonVisaOrder($personId)
	{
		//Получаем заказы на визовую поддержку для персоны
		$servicesAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
		$personVisaOrders = $servicesAndPersonsTable->getPersonServices($personId,'visa');
		//Т.к. для одной персоны только может быть только 1 услуга визовая поддержка, берём первый элемент массива
		$personVisaOrder = $personVisaOrders[0];
		//Удаляем услугу
		$this->servicesObj->setServiceType('visa');
		return $this->servicesObj->deleteServiceOrder($personVisaOrder);
	}
	
	/**
	 * Подготовка данных для 2 шага регистрации (гостиницы)
	 * @return 
	 */
	public function step2Prepare()
	{
		// Получаем список городов
		$citiesTable = new Welt_DbTable_Cities();
		$cities = $citiesTable->getCities();
		$curCity = $cities[0]['id'];
		foreach ($cities as $city)
		{
			if ($city['id'] == $this->eventParams['main_city'])
			{
				$curCity  = $city['id'];
			}
		}
		//Получаем список гостиниц
		$hotelsTable = new Welt_DbTable_Hotels();
		$hotels = $hotelsTable->getHotels($curCity);
		$hotelsSize = sizeof($hotels);
        $priceNotes = array();
		//Добавляем звёздность к названию, формиуем массив примечаний гостиниц
		for($i=0; $i<$hotelsSize; $i++)
		{
			$hotels[$i]['name'] = str_repeat('*',$hotels[$i]['stars']).' '.$hotels[$i]['name'];
            if (isset($hotels[$i]['priceNote']) && $hotels[$i]['priceNote'] != '')
            {
                $priceNotes[$hotels[$i]['id']] = $hotels[$i]['priceNote'];
            }
		}
		//Берём даты по умолчанию для формы их конфига или равные периоду проведения мероприятия
		$sDate = $this->settings->form->acc_def_dates->start;
		if ($sDate == '')
		{
			$sDate = $this->eventParams['event_sdate'];
			$sDateZend = new Zend_Date($sDate, 'dd.mm.yyyy');
		}
		else
		{
			$sDateZend = new Zend_Date($sDate,'dd.mm.yyyy');
		}
		$sDate = $sDateZend->toString('dd.mm.yyyy');
		$eDate = $this->settings->form->acc_def_dates->end;
		if ($eDate == '')
		{
			$eDate = $this->eventParams['event_edate'];
			$eDateZend = new Zend_Date($eDate, 'dd.mm.yyyy');
		}
		else
		{
			$eDateZend = new Zend_Date($eDate, 'dd.mm.yyyy');
		}
		$eDate = $eDateZend->toString('dd.mm.yyyy');
		if ($sDate == $eDate)
		{
			$eDate = $sDateZend->toString('dd.mm.yyyy');
			$sDateZend->sub(1, Zend_Date::DAY);
			$sDate = $sDateZend->toString('dd.mm.yyyy');
		}
		/*
		 * Получаем список номеров и стоимость размещения для гостиницы Парк Инн Прибалтийская. 
		 * Даты равные периоду проведения мероприятия
		*/
		$hotelsObj = new Welt_Hotels();
		$curHotelId = $hotels[0]['id'];
		if (isset($this->settings->event->main_hotel) && $this->settings->event->main_hotel != '')
		{
			$curHotelId = $this->settings->event->main_hotel;
		}
		$roomsList  = $hotelsObj->getHotelRoomsAndPrice($curHotelId,$sDate,$eDate,1);
		//Первый номер по умолчанию
		$curRoom = $roomsList[0];
		//Получаем данные о персонах, прикреплённых к данному пользователю
		$personsTable = new Welt_DbTable_Persons();
		$persons = $personsTable->getUserPersons($this->userId,0,array(0,1,2));
		//Получаем данные об уже имеющихся заказах, передаём их в шаблон
		$this->servicesObj->setServiceType('hotel');
		$hotelOrders = $this->servicesObj->getUserServiceOrders(array(0,1,2));
	 	/*
		 * Определяем, нужно ли скрывать форму заказа проживания на основе настроек и заказов на визу
		 */
		$displayOrderForm = $this->settings->form->order_blocks_display->hotel;
		if(in_array('visa', $this->servicesTypes))
		{
			$this->servicesObj->setServiceType('visa');
			$visaOrders = $this->servicesObj->getUserServiceOrders(array(0,1,2));
			//Если заказана виза, форму отображаем в любом случае
			if (is_array($visaOrders) && sizeof($visaOrders) > 0)
			{
				$displayOrderForm = 1;
			}
		}
		//Получаем информационный текст для проживания
		$evParamsGw = new Welt_DbTable_ExpoMice_EventParams();
		$textsArr = $evParamsGw->getTexts($this->eventParams['organizer_id_1c'], array('hotel'));
		//Возвращаем массив с результатом для контроллера
		$stepData = array(
			'cities'			=>		$cities,
			'curCity'			=>		$curCity,
			'startDate'			=>		$sDate,
			'endDate'			=>		$eDate,
			'persons'			=>		$persons,
			'hotels'			=>		$hotels,
			'curHotelId'		=>		$curHotelId,
			'curHotelRooms'		=>		$roomsList,
			'curRoom'			=>		$curRoom,
			'hotelOrders'		=>		$hotelOrders,
			'displayOrderForm'	=>		$displayOrderForm,
			'priceNotes'        =>      $priceNotes,
			'infoText'			=>		$textsArr['hotel'],
		);
		return $stepData;
	}
	
	/**
	 * Обработка заказа на проживание
	 * @return 
	 */
	public function processRoomOrder($order)
	{
		//Считаем общую сумму заказа и получаем раскадровку по сезонам для заказа на проживание
		$personsSize = sizeof($order['persons']);
		$hotelsObj = new Welt_Hotels();
		$accommodation = $order['accommodation'];
		if ($accommodation==2 && $order['extraBed']==1)
		{
			$accommodation = 3;
		}
		$roomOrderPrices = $hotelsObj->getRoomPeriodPrice(
			$order['roomId'],
			$order['startDate'],
			$order['endDate'],
			$accommodation,
			2,
			$order['breakfast'],
			$personsSize
		);
		$hotelOrder = $order;
		$order['prices'] = $roomOrderPrices;
		//Добавляем к параметрам заказа сумму и данные по завтраку
		$hotelOrder['summ'] = $roomOrderPrices['summ'];
		$hotelOrder['breakfastIncl'] = $roomOrderPrices['breakfastIncl'];
		$hotelOrder['breakfastPrice'] = $roomOrderPrices['breakfastPrice'];
		//Устанавливаем персоны для заказа
		$this->servicesObj->setPersons($order['persons']);
		//Устанавливаем тип услуги
		$this->servicesObj->setServiceType('hotel');
		//Добавляем заказ на проживание получаем id добавленной услуги
		$orderResult = $this->servicesObj->addHotelOrder($hotelOrder);
		$order['id'] = $orderResult['id'];
		//Получаем название номера и гостиницы и возвращаем массив с данными по заказу
		$hotelsTable = new Welt_DbTable_Hotels();
		$roomsTable = new Welt_DbTable_HotelsRooms();
		$hotelDetails = $hotelsTable->getHotelDetails($order['hotelId']);
		$order['price_note'] = $hotelDetails['priceNote'];

		$order['hotelName'] = $hotelsTable->getNameById($order['hotelId']);
		$order['roomName'] = $roomsTable->getNameById($order['roomId']);
		return $order;
	}
	
	
	/**
	 * Обработка данных по 2 шагу
	 * @return 
	 */
	public function step2Process()
	{
		if ($this->userAppNamespace->step == 2)
		{
			$this->userAppNamespace->step = 3;
		}
	}
	
	/**
	 * Подготовка данных для 3 шага регистрации (транспорт, питание, экскурсии)
	 */
	public function step3Prepare($cityId)
	{
		/**
		 * Даннные по персонам
		 */
		$personsTable = new Welt_DbTable_Persons();
		$persons = $personsTable->getUserPersons($this->userId,0,array(0,1,2));
		// Получаем список городов
		$citiesTable = new Welt_DbTable_Cities();
		$cities = $citiesTable->getCities();
		//Получаем примечания для транспорта, экскурсий
		$evParamsGw = new Welt_DbTable_ExpoMice_EventParams();
		$textsArr = $evParamsGw->getTexts($this->eventParams['organizer_id_1c'], array('transport', 'excursion'));
		/**
		 * Получаем данные по транпорту, если доступен заказ транпорта
		 */
		if(in_array('transport',$this->servicesTypes))
		{
			//Получаем данные по вокзалам
			$trainStationsTable = new Welt_DbTable_TrainStations();
			$stations = $trainStationsTable->getStations($cityId);
			//Получаем возможные типы транспорта
			$transportTypesTable = new Welt_DbTable_TransportTypes();
			$transportTypes = $transportTypesTable->getTransportTypes($cityId);
			$transportTypesSize = sizeof($transportTypes);
			//Получаем возможные типы транспортных услуг
			$transportServicesTable = new Welt_DbTable_TransportServices();
			$transportServices = $transportServicesTable->getTransportServices($cityId);
			//Получаем цены на транспорт для услуг
			$transportPricesTable = new Welt_DbTable_TransportPrices();
			$transportServicesSize = sizeof($transportServices);
			$transportPrices = $transportPricesTable->getTransportPrices($cityId);
			//Выбираем цены для каждой услуги из общего массива цен
			$j = 0;
			for ($i=0;$i<$transportServicesSize;$i++)
			{
				//Цены получаем только для платных услуг
				if ($transportServices[$i]['is_free']!=1)
				{
					$offset = $transportTypesSize*$j;
					$transportServices[$i]['prices'] = array_slice($transportPrices,$offset,$transportTypesSize);
					$j++;
				}
			}
//			echo '<pre>';
//			print_r($transportServices);
//			echo '</pre>';
//			die();
			//Получем данные об уже заказанных транспортных услугах
			$this->servicesObj->setServiceType('transport');
			$transportOrders = $this->servicesObj->getUserServiceOrders(array(0,1,2));
			$transportData = array(
				'transportTypes' 		=>	$transportTypes,
				'transportServices'		=>	$transportServices,
				'transportOrders'		=>	$transportOrders,
				'trainStations'			=>	$stations,
				'note'					=>	$textsArr['transport'],
			);
		}
		else
		{
			$transportData = array();
		}
		/**
		 * Получаем данные по экскурсиям, если доступен заказ экскурсий
		 */
		if(in_array('excursion',$this->servicesTypes))
		{
			$excursionsTable = new Welt_DbTable_Excursions();
			$excursionsPeriodsTable = new Welt_DbTable_ExcursionPeriods();
			$excursions = array();
			$excursions = $excursionsTable->getExcursions($cityId);
			$excursionsSize = sizeof($excursions);
			//Получаем возможные даты (периоды) для экскурсий
			for ($i=0;$i<$excursionsSize;$i++)
			{
				$excursions[$i]['periods'] = $excursionsPeriodsTable->getExcursionPeriods($excursions[$i]['id']);
			}
			//Получем данные об уже заказанных экскурсиях
			$this->servicesObj->setServiceType('excursion');
			$excursionOrders = $this->servicesObj->getUserServiceOrders(array(0,1,2)); 
			$excursionData = array(
				'excursions' 			=>	$excursions,
				'excursionOrders'		=>	$excursionOrders,
				'note'					=>	$textsArr['excursion'],
			);
		}
		else
		{
			$excursionData = array();
		}
		/**
		 * Получаем данные по банкетам, если доступен заказ банкетов
		 */
		if(in_array('meal',$this->servicesTypes))
		{
			//Получаем список банкетов
			$mealsTable = new Welt_DbTable_Meals();
			$meals = $mealsTable->getMeals();
			//Получаем список типов питания для 1 банкета
			$firstMealTypes = array();
			if ($meals[0]['with_meal_types'] == 1)
			{
				$mealTypesTable = new Welt_DbTable_MealTypes();
				$firstMealTypes = $mealTypesTable->getMealTypes($meals[0]['id']);
			}
			//Получем данные об уже заказанных банкетах
			$this->servicesObj->setServiceType('meal');
			$mealOrders = $this->servicesObj->getUserServiceOrders(array(0,1,2)); 
			$mealData = array(
				'meals' 			=>	$meals,
				'firstMealTypes'	=>	$firstMealTypes,
				'mealOrders'		=>	$mealOrders
			);
		}
		else
		{
			$mealData = array();
		}
        /**
         * Получаем данные по другим услугам, если доступен их заказ
         */
        if(in_array('other',$this->servicesTypes))
        {
            //Получаем список банкетов
            $servsTable = new Welt_DbTable_OtherServices();
            $servs = $servsTable->getServices();
            //Получем данные об уже заказанных услугах
            $this->servicesObj->setServiceType('other');
            $otherOrders = $this->servicesObj->getUserServiceOrders(array(0,1,2)); 
            $otherData = array(
                'services'          =>  $servs,
                'otherOrders'       =>  $otherOrders
            );
        }
        else
        {
            $otherData = array();
        }
		/**
		 * Передаём подготовленные данные в контроллер
		 */
		$result = array(
			'transport' 	=>	$transportData,
			'excursion'		=>	$excursionData,
			'meal'			=>	$mealData,
			'other'         =>  $otherData,
			'persons'		=>	$persons,
			'cities'		=>	$cities,
		);
		return $result;
	}
	
	/**
	 * Обработка данных по 3 шагу
	 * @return 
	 */
	public function step3Process()
	{
		if ($this->userAppNamespace->step == 3)
		{
			$this->userAppNamespace->step = 4;
		}
	}
	
	/**
	 * Подготовка данных для 4 шага (итоговая информация по заказу)
	 * @return 
	 */
	public function step4Prepare()
	{
		//Если доступен заказ визы
		if(in_array('visa',$this->servicesTypes))
		{
			//Удаляем заказы на визу без проживания и получаем список персон, для которых данная услуга была удалена
			$personsVisaDeleted = $this->checkHotelOrdersForVisa();
		}
		//Получаем заказы по всем услугам в рамках заявки
		$servicesOrders = $this->servicesObj->getAllUserServicesOrders();
		//Если доступен заказ визы
		if(in_array('visa',$this->servicesTypes) && ($this->lang == 'en' || $this->settings->show_visa == 1))
		{
			$servicesOrders['visaFlag'] = 1;
			$servicesOrders['personsVisaDeleted'] = $personsVisaDeleted;
			if (is_array($personsVisaDeleted) && sizeof($personsVisaDeleted)>0)
			{
				$servicesOrders['personsVisaDeletedSize'] = 1; 
			}
			else
			{
				$servicesOrders['personsVisaDeletedSize'] = 0;
			}
		}
		else
		{
			$servicesOrders['visaFlag'] = 0;
		}
		//Получаем сумму заказа и раскадровку для заказов на проживание
		$hotelsObj = new Welt_Hotels();
		$hotelOrdersSize = sizeof($servicesOrders['hotel']);
		for ($i=0;$i<$hotelOrdersSize;$i++)
		{
			$accommodation = $servicesOrders['hotel'][$i]['accommodation'];
			if ($accommodation == 2 && $servicesOrders['hotel'][$i]['extraBed']==1)
			{
				$accommodation = 3;
			}
			$personsSize = sizeof($servicesOrders['hotel'][$i]['persons']);
			$servicesOrders['hotel'][$i]['prices'] = $hotelsObj->getRoomPeriodPrice(
				$servicesOrders['hotel'][$i]['room_id'],
				Welt_Functions::dateFromMysql($servicesOrders['hotel'][$i]['start_date']),
				Welt_Functions::dateFromMysql($servicesOrders['hotel'][$i]['end_date']),
				$accommodation,
				2,
				$servicesOrders['hotel'][$i]['breakfast'],
				$personsSize
			);
		}
		return $servicesOrders;
	}
	
	/**
	 * Обработка данных по 4 шагу
	 * @return 
	 */
	public function step4Process()
	{
		if ($this->userAppNamespace->step == 4)
		{
			$this->userAppNamespace->step = 5;
		}
	}
	
	/**
	 * Подготовка данных для 5 шага (контактная информация)
	 * @return 
	 */
	public function step5Prepare()
	{
		//Получаем информацию по текущему пользователю
		$usersTable = new Welt_DbTable_Users();
		$userData = $usersTable->getUserInfo($this->userId);
		return $userData;
	}
	
	
	/**
	 * Обработка данных по 5 шагу регистрации (контакные данные)
	 * @param array $formData
	 * @return bool
	 */
	public function step5Process($formData)
	{
		//Удаляем не нужные элементы и обновляем информацию по пользователю в БД
		$usersTable = new Welt_DbTable_Users();
		$userData = $formData;
		unset($userData['passwordConf'],$userData['finish'],$userData['pdnAgr'],$userData['pdnDelete']);
		//Добавляем секретную строку
		$userData['keystring'] = md5(md5($userData['email'].$this->settings->system->salt));
		//Добавляем выбранный пользователем язык
		$userData['lang'] = $this->userAppNamespace->lang;
		/*
		 * Добавляем флаг сообшающий о том, нужно ли удалять данные по ПДН после завершения мероприятия
		 */
		$userData['pdn_delete_request'] = $formData['pdnDelete'];
		//Чтобы пользователь не помечался как изменённый
		$userData['changed'] = 0;
		//Если регистрация предварительная, то добавляем данные о браузере и ip пользователя
		if ($this->settings->reg->type == 1)
		{
			$userData['browser'] = Welt_Functions::userBrowser($_SERVER['HTTP_USER_AGENT']);
			$userData['ip'] = $_SERVER['REMOTE_ADDR'];
		}
		$where = $usersTable->getAdapter()->quoteInto('id=?',$this->userId);
		//Обновляем данные по пользователю
		if(!$usersTable->update($userData,$where))
		{
			return false;
		}
		return true;
	}
	
	
	/**
	 * Завершение процесса регистрации
	 * @return 
	 */
	public function completeReg()
	{
		//Отмечаем заявку как выполненную
		$this->appObj->setComplete();
		//Удаляем данные из сесиии
		Zend_Session::namespaceUnset('user');
		Zend_Session::forgetMe();
		return true;
	}
	
	
	/**
	 * Обработка заказа на услугу
	 * @param string $serviceType
	 * @param mixed $serviceData
	 * @return mixed
	 */
	public function serviceOrderProcess($serviceType,$serviceData)
	{
		//Устанавливаем персоны для заказа
		$this->servicesObj->setPersons($serviceData['persons']);
		//Устанавливаем тип услуги
		$this->servicesObj->setServiceType($serviceType);
		//Добавляем услугу заданного типа
		switch($serviceType)
		{
			case 'transport' : $result = $this->servicesObj->addTransportOrder($serviceData); break;
			case 'excursion' : $result = $this->servicesObj->addExcursionOrder($serviceData); break;
			case 'meal' : $result = $this->servicesObj->addMealOrder($serviceData); break;
            case 'other' : $result = $this->servicesObj->addOtherOrder($serviceData); break;
		}
		return $result;
	}
	
	
	/**
	 * Удаление заказанной услуги
	 * @param int $serviceId
	 * @param string $serviceType
	 * @return 
	 */
	public function deleteServiceOrder($serviceId,$serviceType)
	{
		$this->servicesObj->setServiceType($serviceType);
		return $this->servicesObj->deleteServiceOrder($serviceId);
	}
	
	/**
	 *  Проверка, заказана ли на кого-либо из персон визовая поддержка, если заказана, то проверяем, заказано ли 
	 * проживание в гостинице на эту персону, если не заказано, то удаляем визовую поддержку 
	 * @param int $servicesStatus Статус услуг, которые учитываются при обработке
	 * @return 
	 */
	public function checkHotelOrdersForVisa($serviceStatus=0)
	{
		//Массив персон, для которых будет удалена визовая поддержка
		$personsVisaDeleted = array();
		//Получаем список заказов на визовую поддержку в рамках заявки
		$servicesAndPersonsTable = new Welt_DbTable_ServicesAndPersons();
		$this->servicesObj->setServiceType('visa');
		$visaOrders = $this->servicesObj->getUserServiceOrders(array(0=>$serviceStatus), array('is_free'=>1));
		//Если есть заказы на визовую поддержку
		if (is_array($visaOrders) && sizeof($visaOrders)>0)
		{
			//Получаем список персон, для которых заказано проживание
			$hotelOrderedPersons = $servicesAndPersonsTable->getServicesPersonsByType($this->appId,'hotel');
			$hotelOrderedPersonsCount = sizeof($hotelOrderedPersons);
			//Проходимся по всем персонам, заказавшим визовую поддержку и проверяем, есть ли для них заказы на проживание
			foreach($visaOrders as $order)
			{
				/*
				 * Поскольку услуга визовая поддержка заказывается только на 1 человека, то в массиве персон для
				 * конкретного заказа будет только 1 персона => берём 1 элемент массива
				*/
				$orderPerson = $order['persons'][0]['id'];
				//Если есть заказы на проживание
				if ($hotelOrderedPersons>0)
				{
					//Если на персону не заказано проживание
					if (!in_array($orderPerson,$hotelOrderedPersons))
					{
						$this->servicesObj->deleteServiceOrder($order['id']);
						$personsVisaDeleted[] = $order['persons'][0];
					}
				}
				//Если нет заказов на проживание, удаляем заказ на визовую поддержку
				else
				{
					$this->servicesObj->deleteServiceOrder($order['id']);
					$personsVisaDeleted[] = $order['persons'][0];
				}
			}
		}
		return $personsVisaDeleted;
	}
	
	/**
	 * Завершение процесса добавления услуг из ЛК
	 * @return 
	 */
	public function orderFromPpcComplete()
	{
		//Завершаем процесс добавления персон
		$personsTable = new Welt_DbTable_Persons();
		$personsTable->changeAppPersonsStatus($this->userId,0,1);
		//Завершаем процесс добавления услуг
		$this->servicesObj->servicesOrderFromPpcComplete();
		//Если возможен заказ визы, то проверяем, нет ли заказанной без проживания визовой поддержки
		if (in_array('visa',$this->servicesTypes))
		{
			$this->checkHotelOrdersForVisa(1);
		}
		//Находим все добавленные услуги (со статусом 1)
		$newServicesSumm = 0;
		$byRequestFlag = 0;
		$newServices = $this->servicesObj->getAllUserServicesOrders(1,array(1));
		//Находим все добавленные персоны (со статусом 1)
		$personsTable = new Welt_DbTable_Persons();
		$newPersons = $personsTable->getUserPersons($this->userId, 1, array('1'));
		//Если есть добавленные услуги
		if((is_array($newServices) && sizeof($newServices)>0) || (is_array($newPersons) && sizeof($newPersons) > 0))
		{
			//Получаем общую сумму по заявке 
			$appTable = new Welt_DbTable_Applications();
			$appSumm = $appTable->find($this->appId)->current()->summ;
			//Если стоимость заявки по запросу, то оставляем её 0
			if((int)$appSumm == 0)
			{
				$sumVal = 0;
			}
			//Если стоимость не по запросу, то добавляем к ней сумму по добавленных услугам
			else
			{
				//Получаем сумму стоимостей по добавленным услугам
				foreach($newServices as $newService)
				{
					//Если заказан постконгресс и цена по запросу
					if ($newService['service_sys_name']=='excursion' && $newService['summ']==0)
					{
						$byRequestFlag = 1;
					}
					$newServicesSumm += $newService['summ'];
				}
				//Если есть цены по запросу, то общая стоимость - по запросу
				if ($byRequestFlag==1)
				{
					$newServicesSumm = 0;
					$sumVal = 0;
				}
				else
				{
					$sumVal = new Zend_Db_Expr('summ+'.$newServicesSumm);
				}
			}
			//Меняем сумму и cтавим специальный статус для завки 
			$data = array(
				'summ'		=>	$sumVal,
				'status'	=>	3
			);
			$this->appObj->updateApp($data);
		}
	}
	
}
