<?php
/**
 * Вывод названия типа визовой поддержки в зависимости от её типа
 */
class Zend_View_Helper_DisplayVisaName extends Zend_View_Helper_Abstract {
	
	public function displayVisaName($visaType, $displayType = 1)
	{
		$visaStr = 1;
		$visaName = $visaType['name'];
		$visaPrice = '';
		if ($visaType['price'] != 0)
		{
			$visaPrice = $visaType['price'] . $this->view->translate('RUR');
		}
		else
		{
			if ($visaType['is_free'] != 1)
			{
				$visaPrice = $this->view->translate('by request');
			}
			else
			{
				$visaPrice .= $this->view->translate('free');
			}
		}
		if ($displayType == 1)
		{
			$visaStr = $visaName . ' (' . $visaPrice . ')';
		}
		elseif ($displayType == 2)
		{
			$visaStr = '(' . strtolower($visaName) . ', ' . $visaPrice . ')';
		}
		return $visaStr;
	}
}
?>