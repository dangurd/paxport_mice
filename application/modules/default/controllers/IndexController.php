<?php
class IndexController extends Zend_Controller_Action
{
	protected $regObj;
	protected $settings;
	protected $eventParams;
	protected $userAppNamespace;
	protected $showVisa;
	
	
	/**
	 * Доступные для заказа типы услуг
	 */
	protected $servicesTypes;
	
	/**
	 * @var Параметр, определяющий, вызвана ли форма как отдельный модуль или из ЛКУ 
	 */
	protected $fromPpc;
	
    public function init()
    {
		//$remote = new Welt_AbstractRemote('292');
        //$remote->setToRemote();

    	$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('helps', 'json')
                    ->initContext();
    	$this->regObj = new Registration();
		$this->servicesTypes = $this->regObj->getServicesTypes();
		$this->settings = Zend_Registry::get('settings');
		$this->eventParams = Zend_Registry::get('eventParams');
		$this->userAppNamespace = new Zend_Session_Namespace('user');
		//Параметр, определяющий, вызвана ли форма как отдельный модуль или из ЛКУ 
		if (isset($this->userAppNamespace->fromPpc) && $this->userAppNamespace->fromPpc==1)
		{
			$this->fromPpc = 1;
		}
		else
		{
			$this->fromPpc = 0;
		}
		//Параметры для представления
		$this->view->fromPpc = $this->fromPpc;
		$this->view->settings = $this->settings;
		$this->view->eventParams = $this->eventParams;
		$this->view->lang = Zend_Registry::get('lang');
		//Флаг отображения визы
		if (in_array('visa',$this->servicesTypes) && ($this->view->lang == 'en' || $this->settings->show_visa == 1))
		{
			$this->view->visaDisabled = FALSE;
		}
		else
		{
			$this->view->visaDisabled = TRUE;
		}
    }
	
	public function indexAction()
	{
		//echo 'GET^ '.$this->_request;

		//Если форма вызвана из ЛК или включён режим предварительной регистрации, редирект на 1 шаг
        if ($this->fromPpc == 1 || $this->settings->reg->type == 1)
		{
			$this->_helper->_redirector->gotoRoute(array(''),'step1');
		}
		//Получаем вступительный текст
		$evParamsGw = new Welt_DbTable_ExpoMice_EventParams();
		$textsArr = $evParamsGw->getTexts($this->eventParams['organizer_id_1c'], array('intro'));
		$this->view->introText = $textsArr['intro'];
	}
	
    public function step1Action()
    {


		
		$formErrors = false;
        $personsExist = false;
        $personsForms = array();
        // Подготавливаем данные для 1 шага и передаём их во view
        $stepData = $this->regObj->step1Prepare();
        $stepData['ptCh'] = (bool) $this->eventParams['person_types'];
        $formSource = array(
            'countries' => $stepData['countries'],
        );
        if ($stepData['visaFlag'] == 1)
        {
            $formSource['visaTypes'] = $stepData['visaTypes'];
        }
        if ($stepData['ptCh'])
        {
           $formSource['pts'] = $stepData['personsTypes'];
        }
        //Если форма отправлена
        if ($this->_request->isPost())
        {
            $stepData['showPartInfo'] = FALSE;
            // Если переданы данные по персонам
            if (isset($_POST['persons']) && is_array($_POST['persons']) && !empty($_POST['persons']))
            {
                // Проводим валидацию персон
                $persons = array();
                $personsData = $this->_request->getParam('persons');
                $i = 0;
                foreach($personsData as $person)
                {
                    $personsForms[$i] = new Application_Form_Persons($i);
                    $personsForms[$i]->populateSourceData($formSource);
                    if ($personsForms[$i]->isValid($person))
                    {
                        $formValues = $personsForms[$i]->getValues();
                        $persons[] = $formValues['persons'][$i];
                    }
                    else
                    {
                        $formErrors = true;
                    }
                    $i++;
                }
                // Если все персоны прошли валидацию
                if ($formErrors === false)
                {
                    //print_r($persons);die;
                    // Обрабатываем данные по ним
                    $this->regObj->personsProcess($persons);
                    // Делаем редирект на следующий шаг

                        $this->_helper->_redirector->gotoRoute(array(''), 'step2');

                }
            }
            else
            {

                    $this->_helper->_redirector->gotoRoute(array(''), 'step2');

            }
        }
        else
        {
            $curForm = new Application_Form_Persons(0);
            $curForm->populateSourceData($formSource);
            $personsForms[] = $curForm;
            $stepData['showPartInfo'] = TRUE;
        }
        // Если существует, то получаем данные по уже добавленным персонам и передаём их во view
        if ($this->regObj->checkSessionExist())
        {
            $persons = $this->regObj->preparePersonsList();
            $this->view->persons = $persons;
            if (is_array($persons) && !empty($persons))
            {
                $personsExist = true;
            }
        }   
        // Передаём во view флаги, установленные на основе настроек
        $stepData['formErrors'] = $formErrors;
        $stepData['personsExist'] = $personsExist;
        $this->view->stepData = $stepData;
        $this->view->forms = $personsForms;
    }

    public function step11Action()
    {
        $formErrors = false;
        $personsExist = false;
        $personsForms = array();
        // Подготавливаем данные для 1 шага и передаём их во view
        $stepData = $this->regObj->step1Prepare();
        $stepData['ptCh'] = (bool) $this->eventParams['person_types'];
        $formSource = array(
            'countries' => $stepData['countries'],
        );
        if ($stepData['visaFlag'] == 1)
        {
            $formSource['visaTypes'] = $stepData['visaTypes'];
        }
        if ($stepData['ptCh'])
        {
            $formSource['pts'] = $stepData['personsTypes'];
        }
        //Если форма отправлена
        if ($this->_request->isPost())
        {
            $stepData['showPartInfo'] = FALSE;
            // Если переданы данные по персонам
            if (isset($_POST['persons']) && is_array($_POST['persons']) && !empty($_POST['persons']))
            {
                // Проводим валидацию персон
                $persons = array();
                $personsData = $this->_request->getParam('persons');
                $i = 0;
                foreach($personsData as $person)
                {
                    $personsForms[$i] = new Application_Form_Persons($i);
                    $personsForms[$i]->populateSourceData($formSource);
                    if ($personsForms[$i]->isValid($person))
                    {
                        $formValues = $personsForms[$i]->getValues();
                        $persons[] = $formValues['persons'][$i];
                    }
                    else
                    {
                        $formErrors = true;
                    }
                    $i++;
                }
                // Если все персоны прошли валидацию
                if ($formErrors === false)
                {
                    // Обрабатываем данные по ним
                    $this->regObj->personsProcess($persons);
                    // Делаем редирект на следующий шаг

                        $this->_helper->_redirector->gotoRoute(array(''), 'step2');

                }
            }
            else
            {

                    $this->_helper->_redirector->gotoRoute(array(''), 'step2');

            }
        }
        else
        {
            $curForm = new Application_Form_Persons11(0);
            $curForm->populateSourceData($formSource);
            $personsForms[] = $curForm;
            $stepData['showPartInfo'] = TRUE;
        }
        // Если существует, то получаем данные по уже добавленным персонам и передаём их во view
        if ($this->regObj->checkSessionExist())
        {
            $persons = $this->regObj->preparePersonsList();
            $this->view->persons = $persons;
            if (is_array($persons) && !empty($persons))
            {
                $personsExist = true;
            }
        }
        // Передаём во view флаги, установленные на основе настроек
        $stepData['formErrors'] = $formErrors;
        $stepData['personsExist'] = $personsExist;
        $this->view->stepData = $stepData;
        $this->view->forms = $personsForms;
    }

    public function step22Action()
    {
       // $remote = new Welt_AbstractRemote($this->userAppNamespace->userId);
        $stepData = $this->regObj->step22Prepare();
        // Если уже наступил дейлан по тезисам, ничего не делаем
        if (is_null($stepData))
        {
            //$this->view->deadline = TRUE;
            if ($this->_request->isPost())
            {
                $this->_helper->_redirector->gotoRoute(array(''), 'step2');
            }
            return;
        }
        $persons = $stepData['persons'];
        $personsIds = array_keys($persons);
        $abstractsForms = array();
        $docs = $stepData['docs'];
        $docsCount = $stepData['docsCount'];
        // Создаём объекты форм тезиса для каждой из персон
        $i = 0;
        foreach($persons as $key => $person)
        {
            // Пропускаем сопровождающих
           // if ($person['person_type_id'] == Welt_DbTable_PersonsTypes::ACCOMP_PERSON_TYPE)
           // {
             //   continue;
           // }
            $curForm = new Application_Form_Abstracts($i);
            $curForm->personId->setValue($person['id']);
            $persons[$key]['abstractForm'] = $curForm;
            $abstractsForms[] = $curForm;
            $i++;
        }
        //Если форма отправлена
        if ($this->_request->isPost())
        {
            $redir = FALSE;
            // Если есть данные по тезисам, обрабатываем их
            if (isset($_POST['abstracts']) && is_array($_POST['abstracts']) && !empty($_POST['abstracts']))
            {
                $i = 0;
                $abstractsData = array();
                $valid = TRUE;
                foreach($_POST['abstracts'] as $formData)
                {
                    // Проверяем корректность personId, а также статус формы (если 0 - не обрабатываем данные)
                    if (isset($formData['personId']) && in_array($formData['personId'], $personsIds)
                        && $abstractsForms[$i]->isValid($formData))
                    {
                        $formValues = $abstractsForms[$i]->getValues();

                       // if($_SERVER['REMOTE_ADDR']=='192.168.2.98'  || $_SERVER['REMOTE_ADDR']=='192.168.2.186' ){

                        $paspElem = 'passport' . $i;
                        if (isset($abstractsForms[$i]->$paspElem))
                        {
                            $fileInfo = $abstractsForms[$i]->$paspElem->getFileInfo();
                            $file = $fileInfo['passport' .$i];
                            if ($file['received'])
                            {
                                $formValues['abstracts'][$i]['paspFile'] = $file;
                            }
                        }

                       // }

                        $abstractsData[] = $formValues['abstracts'][$i];






                    }
                    else
                    {
                        $valid = FALSE;
                    }
                    $i++;
                }
                if ($valid && !empty($abstractsData))
                {
                    $redir = TRUE;
                    $this->regObj->step22Process(array(
                        'persons' => $persons,
                        'existAbstracts' => $stepData['abstracts'],
                        'abstractsChanges' => $abstractsData,
                    ));



                }
            }
            else
            {
                $redir = TRUE;
            }
            if ($redir)
            {
                $this->_helper->_redirector->gotoRoute(array(''), 'step2');
            }
        }
        // Если форма не отправлена, заполняем формы данными по имеющимся тезисам
        elseif (is_array($stepData['abstracts']) && !empty($stepData['abstracts']))
        {
            $abstractsMapper = new Welt_AbstractsMapper();
            $contribsMapper = new Welt_AbstractsContribsMapper();
            foreach ($abstractsForms as $curForm)
            {
                $curPersId = $curForm->personId->getValue();
                // Если тезис для данной персоны уже существует, ставим соответствующий статус форме
                if (isset($stepData['abstracts'][$curPersId]))
                {
                    $curAbst = $stepData['abstracts'][$curPersId];
                    $curFormData = array_merge(
                        $abstractsMapper->map($curAbst, Welt_FieldsMapper::DIRECTION_FROM_DB),
                        array('status' => 3, 'contribs' => array())
                    );
                    if (!empty($curAbst['contribs']))
                    {
                        foreach ($curAbst['contribs'] as $contrib)
                        {
                            $curFormData['contribs'][] = $contribsMapper->map(
                                $contrib, Welt_FieldsMapper::DIRECTION_FROM_DB
                            );
                        }
                    }
                    $curForm->populate($curFormData);
                }
            }
        }

        if(sizeof($docs))
             {
                 $docs = Welt_Functions::listToKeysArray($docs, 'person_id',true,1);
             }

        $helpLinkParams =array();
        $helpLinkParams['name'] ="tezis";
        $helpLinkParams['title']="Информация об экскурсиях";
        $this->view->persons = $persons;
        $this->view->docs = $docs;
        $this->view->absFormsSize = 10;
        $this->view->helpLinkParams = $helpLinkParams;
        $this->view->deadline = FALSE;
    }

	
	public function step2Action()
	{
       // echo ($this->userAppNamespace->step).'<hr>';
        //exit;
		//Флаг, определяющий наличие/отсутствие 3 шага в зависимости от доступных для заказа типов услуг
		if (!in_array('excursion',$this->servicesTypes) && !in_array('transport',$this->servicesTypes))
		{
			$step3Exist = false;
		}
		else
		{
			$step3Exist = true;
		}
		//die('hell');
		//Если форма отправлена (закончено добавление услуг), перенаправляем на следующий шаг
		if ($this->_request->isPost())
		{
			//Следующий шаг в зависимости от того, существует ли 3 шаг (если нет заказа экскурсий и транспорта, то он не нужен)
			if (!$step3Exist)
			{
				//Устанавливаем в сессии 4 шаг, если ранее не было установлено
				if ($this->userAppNamespace->step == 2)
				{
					$this->userAppNamespace->step = 4;
				}
				$nextStepRoute = 'step4'; 
			}
			//Если есть 3 шаг
			else
			{
				$this->regObj->step2Process();
				$nextStepRoute = 'step3'; 
			}
			$this->_helper->_redirector->gotoRoute(array(''),$nextStepRoute);
		}
		//Если нет, то отображаем форму
		else
		{
			/*
			 * Получаем данные по доступным для заказа гостиницам и номерам, а также их характеристики 
			 * и передаём их во view
			 */
			$stepData = $this->regObj->step2Prepare();
			//print_r($stepData);die;
			$this->view->stepData = $stepData;
			//Если есть данные об уже имеющихся заказах на проживание
			if (is_array($stepData['hotelOrders']))
			{
				//Создаём новое представление, передаём переменные и рендерим результат в переменную
				$ordersView = new Zend_View();
				$curViewPaths = $this->view->getScriptPaths();
				$ordersView->setScriptPath($curViewPaths[0]);
				$ordersContent = '';
				//Проходимся по всем заказам и формируем html-код на основе шаблона
				$hotelsObj = new Welt_Hotels();
				foreach ($stepData['hotelOrders'] as $order)
				{
					//Если это не доп. кровать, то получаем раскадровку по сезонам для заказа и передаём заказ во view
					if (!isset($order['is_extra_bed']) || $order['is_extra_bed'] !=1)
					{
						$personsSize = sizeof($order['persons']);
						$accommodation = $order['accommodation'];
						if ($order['accommodation']==2 && $order['extraBed']==1) 
						{
							$accommodation = 3;
						}
						$order['prices'] = $hotelsObj->getRoomPeriodPrice(
							$order['room_id'],
							Welt_Functions::dateFromMysql($order['start_date']),
							Welt_Functions::dateFromMysql($order['end_date']),
							$accommodation,
							2,
							$order['breakfast'],
							$personsSize
						);
					}
					//Если номер - доп. кровать, то форматируем даты и считаем количество дней для периода проживания
					else
					{
						$order['start_date'] = Welt_Functions::dateFromMysql($order['start_date']);
						$order['end_date'] = Welt_Functions::dateFromMysql($order['end_date']);
						$order['nights'] = round((Welt_Functions::convertDateUnix($order['end_date']) - Welt_Functions::convertDateUnix($order['start_date'])) / 86400);
					}
					$ordersView->orderData = $order;
					//Рендерим результат в переменную
					$ordersContent = $ordersContent.$ordersView->render('/process/hotel-order-line.phtml');
				}
				$stepData['ordersContent'] = $ordersContent;
			}
		}
		$stepData['step3Exist'] = $step3Exist;
		$this->view->stepData = $stepData;
	}
	
	public function step3Action()
	{
		/*
		 * Если не доступен заказ как транспорта, так и экскурсий (данный шаг не нужен) 
		 * или была отправлена форма(закончено добавление услуг), перенаправляем на следующий шаг
		 */
		if ((!in_array('excursion',$this->servicesTypes) && !in_array('transport',$this->servicesTypes)) || $this->_request->isPost())
		{
			$this->regObj->step3Process();
			$this->_helper->_redirector->gotoRoute(array(''),'step4');
		}
		//Если нет, то отображаем форму
		else
		{
			$cityId = $this->_request->getParam('city');
			if (is_null($cityId))
			{
				$cityId = $this->eventParams['main_city'];
			}
			//Получаем данные по услугам
			$stepData = $this->regObj->step3Prepare($cityId);
			$stepData['cityId'] = $cityId;
			//Дата для календаря
			$zendDate = new Zend_Date($this->eventParams['event_sdate'],'dd.MM.yyyy');
			$zendDate->sub(1,Zend_Date::DAY);
			$this->view->startDate = $zendDate->toString('dd.MM.yyyy');
			//Создаём новое представление
			$ordersView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$ordersView->setScriptPath($curViewPaths[0]);
			//Если есть данные об уже имеющихся заказах на транспорт
			if (isset($stepData['transport']['transportOrders']))
			{
				$transportOrders = $stepData['transport']['transportOrders'];
				if (is_array($transportOrders) && sizeof($transportOrders)>0)
				{
					//Передаём переменные в созданное представление и рендерим результат в переменную
					$ordersContent = '';
					//Проходимся по всем заказам
					foreach ($transportOrders as $order)
					{
						$ordersView->orderData = $order;
						$ordersContent = $ordersContent.$ordersView->render('/process/transport-order-line.phtml');
					}
					$stepData['transport']['ordersContent'] = $ordersContent;
				}
			}
			//Если есть данные об уже имеющихся заказах на экскурсии
			if (isset($stepData['excursion']['excursionOrders']))
			{
				$excursionOrders = $stepData['excursion']['excursionOrders'];
				if (is_array($excursionOrders) && sizeof($excursionOrders)>0)
				{
					//Передаём переменные в созданное представление и рендерим результат в переменную
					$ordersContent = '';
					//Проходимся по всем заказам
					foreach ($excursionOrders as $order)
					{
						$ordersView->orderData = $order;
						$ordersContent = $ordersContent.$ordersView->render('/process/excursion-order-line.phtml');
					}
					$stepData['excursion']['ordersContent'] = $ordersContent;
				}
			}
			//Если есть данные об уже имеющихся заказах на банкеты
			if (isset($stepData['meal']['mealOrders']))
			{
				$mealOrders = $stepData['meal']['mealOrders'];
				if (is_array($mealOrders) && sizeof($mealOrders)>0)
				{
					//Передаём переменные в созданное представление и рендерим результат в переменную
					$ordersContent = '';
					//Проходимся по всем заказам
					foreach ($mealOrders as $order)
					{
						$ordersView->orderData = $order;
						$ordersContent = $ordersContent.$ordersView->render('/process/meal-order-line.phtml');
					}
					$stepData['meal']['ordersContent'] = $ordersContent;
				}
			}
            //Если есть данные об уже имеющихся заказах на услуги типа "Другое"
            if (isset($stepData['other']['otherOrders']))
            {
                $otherOrders = $stepData['other']['otherOrders'];
                if (is_array($otherOrders) && sizeof($otherOrders)>0)
                {
                    //Передаём переменные в созданное представление и рендерим результат в переменную
                    $ordersContent = '';
                    //Проходимся по всем заказам
                    foreach ($otherOrders as $order)
                    {
                        $ordersView->orderData = $order;
                        $ordersContent = $ordersContent.$ordersView->render('/process/other-order-line.phtml');
                    }
                    $stepData['other']['ordersContent'] = $ordersContent;
                }
            }
			//Передаём данные во view
			$this->view->stepData = $stepData;
		}
	}
	
	public function step4Action()
	{
		//Если форма отправлена, перенаправляем на следующий шаг
		if ($this->_request->isPost())
		{
			$this->regObj->step4Process();
			$this->_helper->_redirector->gotoRoute(array(''),'step5');
		}
		//Если нет, то отображаем форму
		else
		{
			//Получаем данные по всем услугам в рамках заявки и передаём во view
			$stepData = $this->regObj->step4Prepare();
			//Массив для передачи во view
			$viewData = array();
			/*
			 * Проходимся по всем заказам для каждого из типов услуг. Если есть заказы, то формируем контент на основе соответствующего 
			 * шаблона
			*/
			$ordersView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$ordersView->setScriptPath($curViewPaths[0]);
			//Передаём в шаблон параметр, определяющий, что идёт вывод на шаге Информация по заказу
			$ordersView->summary = 1;
			if (isset($stepData['hotel']) && sizeof($stepData['hotel'])>0)
			{
				$ordersContent = '';
				foreach ($stepData['hotel'] as $order)
				{
					$ordersView->orderData = $order;
					//Рендерим результат в переменную
					$ordersContent = $ordersContent.$ordersView->render('/process/hotel-order-line.phtml');
				}
				$viewData['hotel']['ordersContent'] = $ordersContent;
			}
			if (isset($stepData['transport']))
			{
				$viewData['transport']['available'] = 1;
				if (sizeof($stepData['transport']) > 0)
				{
					$ordersContent = '';
					foreach ($stepData['transport'] as $order)
					{
						$ordersView->orderData = $order;
						//Рендерим результат в переменную
						$ordersContent = $ordersContent.$ordersView->render('/process/transport-order-line.phtml');
					}
					$viewData['transport']['ordersContent'] = $ordersContent;
				}
			}
			else
			{
				$viewData['transport']['available'] = 0;
			}
			if (isset($stepData['excursion']))
			{
				$viewData['excursion']['available'] = 1;
				if (sizeof($stepData['excursion']) > 0)
				{
					$ordersContent = '';
					foreach ($stepData['excursion'] as $order)
					{
						$ordersView->orderData = $order;
						//Рендерим результат в переменную
						$ordersContent = $ordersContent.$ordersView->render('/process/excursion-order-line.phtml');
					}
					$viewData['excursion']['ordersContent'] = $ordersContent;
				}
			}
			else
			{
				$viewData['excursion']['available'] = 0;
			}
			//Данные по банкетам
			if (isset($stepData['meal']))
			{
				$viewData['meal']['available'] = 1;
				if (sizeof($stepData['meal']) > 0)
				{
					$ordersContent = '';
					foreach ($stepData['meal'] as $order)
					{
						$ordersView->orderData = $order;
						//Рендерим результат в переменную
						$ordersContent = $ordersContent.$ordersView->render('/process/meal-order-line.phtml');
					}
					$viewData['meal']['ordersContent'] = $ordersContent;
				}
			}
			else
			{
				$viewData['meal']['available'] = 0;
			}
            //Данные по услугам типа "Другое"
            if (isset($stepData['other']))
            {
                $viewData['other']['available'] = 1;
                if (sizeof($stepData['other']) > 0)
                {
                    $ordersContent = '';
                    foreach ($stepData['other'] as $order)
                    {
                        $ordersView->orderData = $order;
                        //Рендерим результат в переменную
                        $ordersContent = $ordersContent.$ordersView->render('/process/other-order-line.phtml');
                    }
                    $viewData['other']['ordersContent'] = $ordersContent;
                }
            }
            else
            {
                $viewData['other']['available'] = 0;
            }
			//Данные по рег. сбору
			if (isset($stepData['reg_fee']))
			{
				$viewData['reg_fee']['available'] = 1;
				if (sizeof($stepData['reg_fee']) > 0)
				{
					$viewData['reg_fee']['orders'] = $stepData['reg_fee'];
				}
			}
			else
			{
				$viewData['reg_fee']['available'] = 0;
			}
			//Передаём необходимые данные во view
			if ($stepData['visaFlag'] == 1)
			{
				$viewData['visa'] = $stepData['visa'];
				$viewData['personsVisaDeleted'] = $stepData['personsVisaDeleted'];
				$viewData['personsVisaDeletedSize'] = $stepData['personsVisaDeletedSize'];
			}
			$this->view->stepData = $viewData;
			//Предыдущий шаг в зависимости от того, существует ли 3 шаг (если не возможен заказ экскурсий и транспорта, то он не нужен)
			if ($viewData['excursion']['available'] != 1 && $viewData['transport']['available']!=1)
			{
				$this->view->prevStep = 2;
			}
			else
			{
				$this->view->prevStep = 3;
			}
		}
	}

    public function step5Action()
    {

       //Создаём объект формы регистрации
        $regStep5Form = new Application_Form_RegStep5();
        $f = $regStep5Form->getValues();
        $pers = new Welt_DbTable_Persons();
        $pers_arr = $pers->getUserPersons($_SESSION['user']['userId']);
        $pers_d = new Welt_DbTable_PersonsDetails();


/*
        print_r($_SESSION);
        echo '<hr>';
        print_r($pers_arr);
        echo '<hr>';
        print_r($pers__dets_arr);
*/
        //Если форма отправлена, проверяем данные
        if ($this->_request->isPost())
        {
            //Если данные корректные
            if ($regStep5Form->isValid($_POST))
            {
                $fileName='';
				//print_r ($_SESSION);
				/*
                if (isset($_SESSION['form']) and $_SESSION['form']=='simple') {


                    foreach ($pers_arr as $person_for_invite) {
                        $fileName = '../public/invite_open.pdf';
                        $pdf = Zend_Pdf::load($fileName);
                        $today = date("d.m.Y");
                        $font = Zend_Pdf_Font::fontWithPath('../public/fonts/ARIALN.TTF');
                        $pdf->pages[0]->setFont($font, 14);
                        $pdf->pages[0]->drawText($today, 480, 670);

                        $fio = $person_for_invite['name'];
                        if ($person_for_invite['sex']==0) {
                            $sex = 'Глубокоуважаемый ';
                        } else {
                            $sex = 'Глубокоуважаемая ';
                        }
                        $pdf->pages[0]->drawText($sex.$fio.'!', 115, 415, 'UTF-8');
                        $newFileName = '../public/files/invite_hidden_edited.pdf';
                        $pdf->save($newFileName);

                        $pers__dets_arr = $pers_d->getPersonDetails($person_for_invite['id']);
                        $mail_to = $pers__dets_arr['email'];
                        if ((strpos($mail_to, '@mail')) or (strpos($mail_to, '@rambler'))) $mail_to = 'ssi@msk.welt.ru';
                        
						$to = array($fio,$mail_to); // кому отправляем
						//$to = array($fio,'mdv@msk.welt.ru'); // кому отправляем

                        $subject = "Приглашение на VI Национальный Конгресс Аудиологов и X Международный Симпозиум «Современные проблемы физиологии и патологии слуха»"; // Тема письма
                        $from = array("Стегалин Станислав", "ssi@msk.welt.ru"); // От кого отправляем

                        // Тут текст письма. Может содержать html теги
                        $message = "Добрый день!<br/><br/>В приложении Вы найдете приглашение на VI Национальный Конгресс Аудиологов и X Международный Симпозиум «Современные проблемы физиологии и патологии слуха»<br/><br/><br/>С уважением, <br/>Оргкомитет конгресса";

                        $f         = fopen($newFileName,"rb");
                        $un        = strtoupper(uniqid(time()));
                        $head      = "From: $from[0] <$from[1]> \n";
                        $head     .= "To: $to[0] <$to[1]> \n";
                        $head     .= "Subject: $subject\n";
                        $head     .= "X-Mailer: PHPMail Tool\n";
                        $head     .= "Reply-To: $from\n";
                        $head     .= "Mime-Version: 1.0\n";
                        $head     .= "Content-Type:multipart/mixed;";
                        $head     .= "boundary=\"----------".$un."\"\n\n";
                        $zag       = "------------".$un."\nContent-Type:text/html;\n";
                        $zag      .= "Content-Transfer-Encoding: 8bit\n\n$message\n\n";
                        $zag      .= "------------".$un."\n";
                        $zag      .= "Content-Type: application/octet-stream;";
                        $zag      .= "name=\"".basename($newFileName)."\"\n";
                        $zag      .= "Content-Transfer-Encoding:base64\n";
                        $zag      .= "Content-Disposition:attachment;";
                        $zag      .= "filename=\"Приглашение.pdf\"\n\n";
                        $zag      .= chunk_split(base64_encode(fread($f,filesize($newFileName))))."\n";

                        // отправляем письмо
                        mail("$to[1]", "$subject", $zag, $head);

                    }

                } else {
                    $fileName = '../public/invite_hidden.pdf';
                    $pdf = Zend_Pdf::load($fileName);
                    $today = date("d.m.Y");
                    $font = Zend_Pdf_Font::fontWithPath('../public/fonts/ARIALN.TTF');
                    $pdf->pages[0]->setFont($font, 14);
                    $pdf->pages[0]->drawText($today, 480, 670);
                    $fields = $regStep5Form->getValues();
                    $company_name = $fields['company'];
                    if (!$company_name) $company_name ='#организация не указана#';
                    $pdf->pages[0]->drawText($company_name, 255, 387, 'UTF-8');
                    $newFileName = '../public/files/invite_hidden_edited.pdf';
                    $pdf->save($newFileName);

                    $mail_to = $fields['email'];
                    $fio = $fields['fname'].' '.$fields['lname'];
                    if ((strpos($mail_to, '@mail')) or (strpos($mail_to, '@rambler'))) $mail_to = 'ssi@msk.welt.ru';
                    $to = array($fio,$mail_to); // кому отправляем
                    //$to = array($fio,'mdv@msk.welt.ru'); // кому отправляем
                    $subject = "Приглашение на VI Национальный Конгресс Аудиологов и X Международный Симпозиум «Современные проблемы физиологии и патологии слуха»"; // Тема письма
                    $from = array("Стегалин Станислав", "ssi@msk.welt.ru"); // От кого отправляем

                    // Тут текст письма. Может содержать html теги
                    $message = "Добрый день!<br/><br/>В приложении Вы найдете приглашение на VI Национальный Конгресс Аудиологов и X Международный Симпозиум «Современные проблемы физиологии и патологии слуха»<br/><br/><br/>С уважением, <br/>Оргкомитет конгресса";

                    $f         = fopen($newFileName,"rb");
                    $un        = strtoupper(uniqid(time()));
                    $head      = "From: $from[0] <$from[1]> \n";
                    $head     .= "To: $to[0] <$to[1]> \n";
                    $head     .= "Subject: $subject\n";
                    $head     .= "X-Mailer: PHPMail Tool\n";
                    $head     .= "Reply-To: $from\n";
                    $head     .= "Mime-Version: 1.0\n";
                    $head     .= "Content-Type:multipart/mixed;";
                    $head     .= "boundary=\"----------".$un."\"\n\n";
                    $zag       = "------------".$un."\nContent-Type:text/html;\n";
                    $zag      .= "Content-Transfer-Encoding: 8bit\n\n$message\n\n";
                    $zag      .= "------------".$un."\n";
                    $zag      .= "Content-Type: application/octet-stream;";
                    $zag      .= "name=\"".basename($newFileName)."\"\n";
                    $zag      .= "Content-Transfer-Encoding:base64\n";
                    $zag      .= "Content-Disposition:attachment;";
                    $zag      .= "filename=\"Приглашение.pdf\"\n\n";
                    $zag      .= chunk_split(base64_encode(fread($f,filesize($newFileName))))."\n";

                    // отправляем письмо
                   mail("$to[1]", "$subject", $zag, $head);
                }
*/



                //Получаем данные формы и отправляем их в модель на обработку
                $formValues = $regStep5Form->getValues();
                //Обработка данных, редирект на шаг завершающий регистрацию
                	$this->regObj->step5Process($formValues);
                	$this->_helper->_redirector->gotoRoute(array(''),'complete');
            }
        }
        else
        {
            /*
             * Если включен режим предварительной регистрации, то получаем данные пользователя и подставляем
             * их в форму  в качестве значений по умолчанию
             */
            if ($this->settings->reg->type == 1)
            {
                $userData = $this->regObj->step5Prepare();
                $regStep5Form->setDefaults($userData);
            }
        }
        $this->view->regStep5Form = $regStep5Form;
    }
	
	public function completeAction()
	{
		$this->_helper->viewRenderer->setNoRender();
       //Если пользователь дошёл до 5 шага
		if ($this->userAppNamespace->step==5)
		{
			$this->regObj->completeReg();
			$this->_helper->redirector->gotoRoute(array(''),'result');
		}
		else
		{
			$this->_helper->redirector->gotoSimple('index','index','default',array());
		}
	}
	
	public function resultAction()
	{
		$this->view->onlinePay = (bool) $this->_request->getParam('op');
	}
	
	/**
	 * Страница, которая выдаётся посетителям после завершения мероприятия
	 * @return 
	 */
	public function deadlineAction()
	{
		
	}
	
	/**
	 * Загрузка справочной информации для указанного разделе
	 * @return 
	 */
	public function helpsAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('step')!='')
		{
			$step = $this->_request->getParam('step');
			$cityId = $this->_request->getParam('cityId');
			$status = 1;
			$content = '';
			//Проверка на корректность шага
			if ($step != '' && in_array($step, array(1,3,5)))
			{
				$contentView = new Zend_View();
				$curViewPaths = $this->view->getScriptPaths();
				$contentView->setScriptPath($curViewPaths[0]);
				$contentView->cityId = $cityId;
				//Проверка на существование файла шаблона
				$stepHelpsPath = 'index/helps/' . Zend_Registry::get('lang') . '/' . 'step' .  $step . '-helps.phtml';
				if (file_exists($curViewPaths[0] . $stepHelpsPath))
				{
					// Для 3 шага передаём во view доступные в проекте экскурсии
					$excursionsTable = new Welt_DbTable_Excursions();
					$excursions = $excursionsTable->getExcursions($cityId);
					$excsIds = array();
					if (is_array($excursions) && !empty($excursions))
					{
						foreach($excursions as $excursion)
						{
							$excsIds[] = $excursion['id_1c'];
						}
					}
					$contentView->excsIds = $excsIds;
					$content = $contentView->render($stepHelpsPath);
				}
				else
				{
					$status = 0;
				}
			}
			else
			{
				$status = 0;
			}
			$result = array('status' => $status, 'content' => $content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	
}
