<?php
/**
 * Контроллер для обработки результатов по всем шагам регистрации
 */
class ProcessController extends Zend_Controller_Action
{
	private $regObj;
	
    public function init()
    {
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('loadPersonLine', 'html')
					->addActionContext('personsSave', 'json')
					->addActionContext('deletePerson', 'json')
					->addActionContext('roomsPrices', 'json')
					->addActionContext('hotelDetails', 'json')
					->addActionContext('addHotelRoom', 'json')
					->addActionContext('addTransport', 'json')
					->addActionContext('addExcursion', 'json')
					->addActionContext('excursionPeriods', 'json')
					->addActionContext('processMeals', 'json')
					->addActionContext('deleteService', 'json')
					->addActionContext('addVisa', 'json')
					->addActionContext('deleteVisa', 'json')
                    ->initContext();
		$this->regObj = new Registration();
		$this->settings = Zend_Registry::get('settings');
		$this->eventParams = Zend_Registry::get('eventParams');
    }
	public function preDispatch()
	{
		$this->_helper->layout()->disableLayout();
	}
    public function indexAction()
    {
    	
    }
    
    public function newPersonAction()
    {
        if(!is_null($this->_request->getParam('num')))
        {
            $lang = Zend_Registry::get('lang');
            $personNum = $this->_request->getParam('num');
            $this->view->num = $personNum;
            
            $personsForm = new Application_Form_Persons($personNum);
            
			
            $cache = Zend_Registry::get('cacheManager')->getCache('databaseExtraLong');
            $cacheId = 'personsFormData_' . $lang;
            if ($cache->test($cacheId))
            {
                $formSource = $cache->load($cacheId);
            }
            else
            {
                $visaTypesTable = new Welt_DbTable_VisaTypes();
                $personTypesTable = new Welt_DbTable_PersonsTypes();
                $countriesTable = new Welt_DbTable_WeltCountries();
                $formSource = array(
                    'countries' => $countriesTable->getCountriesList(),
                );
                if ($this->eventParams['person_types'] == 1)
                {
                    $formSource['pts'] = $personTypesTable->getPersonTypes();  
                }
                $servsTypes = $this->regObj->getServicesTypes();
                if (in_array('visa',$servsTypes) && ($lang == 'en' || $this->settings->show_visa == 1))
                {
                    $formSource['visaTypes'] = $visaTypesTable->getVisaTypes();  
                }
                $cache->save($formSource, $cacheId);
            }
            $personsForm->populateSourceData($formSource);
            $this->view->form = $personsForm;
        }           
    }
	
	/**
	 * Вывод строки для добавления новой персоны на 2 шаге
	 */
	public function loadPersonLineAction()
    {
    	//Получаем возможные типы туристов
		$personTypesTable = new Welt_DbTable_PersonsTypes();
		$personsTypes = $personTypesTable->getPersonTypes();
		$this->view->personsTypes = $personsTypes;
		//Номер добавляемой строки в таблице туристов
    	$lineNumber = $this->_request->getParam('number');
		$this->view->lineNumber = $lineNumber;
    }
	
	/**
	 * Обработка введённых данных по персонам
	 */
	public function personsSaveAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('persons')!='')
		{
			$persons = $this->_request->getParam('persons');
			//Проверяем данные на корректность
			$personsValid = new Application_Validator_Persons();
			//Добавляем проверку гражданства
			$personsValid->addRequiredFields(array('citizenship_id'));
			//Статус валидации
			$status = 1;
			$messages = array();
			//Если данные не верные, добавляем сообщения об ошибке и ставим статус 0
			if(!$personsValid->isValid($persons))
			{
				$status = 0;
				$validMessages = $personsValid->getMessages();
				foreach ($validMessages as $message)
				{
					$messages[] = $message;
				}
			}
			/* Если данные корректные, cохраняем данные по персонам, добавляем визовую поддержку по необходимости.
			*/
			else
			{
				$this->regObj->personsProcess($persons);
			}
			//Формируем массив с результатом и отправляем его в json
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Удаление персоны
	 * @return 
	 */
	public function deletePersonAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('personId')!='')
		{
			$personId = $this->_request->getParam('personId');
			$status = 1;
			$messages = array();
			//Удаляем персону и её услуги
			if (!$this->regObj->personDelete($personId))
			{
				$status = 0;
				$messages[] = 'Person delete error';
			}
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}

    /**
     * Редактирование персоны
     * @return
     */
    public function editPersonAction()
    {
        //Если были переданы корректные данные
        if($this->_request->isPost() && $this->_request->getParam('personId')!='')
        {
            $personId = $this->_request->getParam('personId');
            $status = 1;
            $messages = array();

            $result = array('status'=>$status,'messages'=>$messages);
            $this->_helper->json($result, array('enableJsonExprFinder' => true));
        }
    }
	
	/**
	 * Получение списка номеров для гостиницы с ценами на размещения дял указанного периода 
	 * @return 
	 */
	public function roomsPricesAction()
	{
		if($this->_request->isPost() && $this->_request->getParam('request')!='')
		{
			$request = $this->_request->getParam('request');
			$status = 1;
			$messages = array();
			$rooms = array();
			//Проверяем переданные данные на корректность (период проживания)
			if(!Welt_Functions::checkPeriod($request['sDate'],$request['eDate']))
			{
				$status = 0;
				$translator = Zend_Registry::get('Zend_Translate');
				$messages[] = $translator->translate('Period incorrect');
			}
			//Период не больше указанного в настройках значения
			else
			{
				$settings = Zend_Registry::get('settings');
				$maxHotelOrderPeriod = (int) $settings->reg->maxHotelOrderPeriod;
				$daysDiff = (int) round((Welt_Functions::convertDateUnix($request['eDate']) - Welt_Functions::convertDateUnix($request['sDate']))/86400);
				if ($daysDiff > $maxHotelOrderPeriod)
				{
					$status = 0;
					$translator = Zend_Registry::get('Zend_Translate');
					$messages[] = $translator->translate('Задан слишком большой период проживания. Максимальное количество дней').': '.$maxHotelOrderPeriod;
				}
			}
			//Если не было ошибок
			if ($status == 1)
			{
				$accommodation = $request['accommodation'];
				if ($accommodation==2 && $request['extraBed']==1)
				{
					$accommodation = 3;
				}
				//Получаем список номеров с ценами и выдаём ответ
				$hotelsObj = new Welt_Hotels();
				$rooms = $hotelsObj->getHotelRoomsAndPrice(
					$request['hotelId'],$request['sDate'],$request['eDate'],$accommodation,2
				);
			}
			$result = array('status'=>$status,'messages'=>$messages,'rooms'=>$rooms);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Получение детальной информации  по указанной гостинице
	 * @return 
	 */
	public function hotelDetailsAction()
	{
		if($this->_request->isPost() && $this->_request->getParam('hotelId')!='')
		{
			$hotelId = $this->_request->getParam('hotelId');
			//Получаем детальную информацию по гостинице
			$weltHotelTable = new Welt_DbTable_WeltHotels();
			$hotelDetails = $weltHotelTable->getHotelDetails($hotelId);
			$hotelsTable = new Welt_DbTable_Hotels();
			/*
			 * Если нет деталей по гостинице в базе welt - не выгружена по какой-то причине, например гостиница - хостел 
			 * (не показывается на сайте), то получаем информацию из БД проекта, ставим флаг
			 */
			if ($hotelDetails === false)
			{
				$hotelDetails = $hotelsTable->getHotelDetails($hotelId);
				$hotelDetails['fromWelt'] = 0;
			}
			//Если информация по гостинице получена из базы welt, то получаем доп. описание из БД проекта
			else
			{
				//Получаем примечание по гостинице
				//$hotelDetails['note'] = $hotelsTable->getNoteById($hotelId);
				$hotelDetails['fromWelt'] = 1;
				//Если необходимо выводить доп. услуги по гостинице, выводим
				if ($this->settings->services->hotels->loadAdditionals == 1)
				{
					$WeltAdditionalsTable = new Welt_DbTable_WeltHotelsAdditionalServices();
					$hotelDetails['additionals'] = $WeltAdditionalsTable->getHotelsAdditionals($hotelId);
				}
				else
				{
					$hotelDetails['additionals'] = '';
				}
				//Ссылка на подробную информацию по гостинице
				if ($hotelDetails['city_link'] == '')
				{
					$hotelDetails['city_link'] = str_replace(' ', '-', $hotelDetails['city_name_eng']);
				}
				$hotelLink = $hotelDetails['city_link'] . '/' . $hotelDetails['link_name'] .'.html';
				$lang = Zend_Registry::get('lang');
				if ($lang == 'en')
				{
					$hotelLink = 'http://www.reisebuero-welt.com/en/hotels/' . $hotelLink;
				}
				else
				{
					$hotelLink = 'http://www.welt.ru/ru/hotels/' . $hotelLink;
				}
				$hotelDetails['cityLinkUrl'] = $hotelLink;
			}
			//Создаём объект вида, передаём в него полученную информацию и рендерим результат в шаблон
			$detailsView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$detailsView->setScriptPath($curViewPaths[0]);
			$detailsView->hotelInfo = $hotelDetails;
			$hotelInfoShort = array(
				'stars'		=>	$hotelDetails['stars'],
				'name'		=>	$hotelDetails['name'],
			);
			if ($hotelDetails['fromWelt'] == 1)
			{
				$hotelInfoShort['coords'] = explode(',', $hotelDetails['google_coordinates']);
				$hotelInfoShort['address'] = $hotelDetails['address'];
			}
			$detailsView->hotelInfoShort = $hotelInfoShort;
			$content = $detailsView->render('/process/hotel-details.phtml');
			$result = array('content'=>$content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Обработка заказа на проживание
	 * @return 
	 */
	public function addHotelRoomAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('order')!='')
		{
			$roomOrder = $this->_request->getParam('order');
			//Проверяем данные на корректность
			$roomOrderValid = new Application_Validator_RoomOrder();
			//Статус валидации
			$status = 1;
			$messages = array();
			$content  = '';
			//Если данные не верные, добавляем сообщения об ошибке и ставим статус 0
			if(!$roomOrderValid->isValid($roomOrder))
			{
				$status = 0;
				$validMessages = $roomOrderValid->getMessages();
				foreach ($validMessages as $message)
				{
					$messages[] = $message;
				}
			}
			//Если данные корректные
			else
			{
				//Обрабатываем заказ на проживание и получаем параметры добавленной услуги
				$orderRes = $this->regObj->processRoomOrder($roomOrder);
				/* Создаём новое представление, передаём необходимые переменные, рендерим шаблон.
				 * (генерится строка с информацией по заказанной услуге)
				 */
				//Создаём новое представление, передаём переменные и рендерим результат в переменную
				$ordersView = new Zend_View();
				$curViewPaths = $this->view->getScriptPaths();
				$ordersView->setScriptPath($curViewPaths[0]);
				//Получаем раскадровку по сезонам для заказа и передаём заказ во view
				$hotelsObj = new Welt_Hotels();
				$personsSize = sizeof($orderRes['persons']);
				$accommodation = $orderRes['accommodation'];
				if ($orderRes['accommodation']==2 && $orderRes['extraBed']==1) 
				{
					$accommodation = 3;
				}
				$orderRes['prices'] = $hotelsObj->getRoomPeriodPrice(
					$orderRes['roomId'],
					$orderRes['startDate'],
					$orderRes['endDate'],
					$accommodation,
					2,
					$orderRes['breakfast'],
					$personsSize
				);
				$ordersView->orderData = $orderRes;
				//Рендерим результат в переменную
				$content = $ordersView->render('/process/hotel-order-line.phtml');
			}
			//Формируем массив с результатом и отправляем его в json
			$result = array('status'=>$status,'messages'=>$messages,'content'=>$content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Список гостиниц города
	 */
	public function cityHotelsAction()
	{
		$status = 1;
		$messages = array();
		$cityId = $this->_request->getParam('city');
		if (is_null($cityId))
		{
			throw new Zend_Controller_Dispatcher_Exception();
		}
		$hotelsTable = new Welt_DbTable_Hotels();
		$hotelsData = $hotelsTable->getHotels($cityId);
		$hotels = array();
		if (is_array($hotelsData) && !empty($hotelsData))
		{
			foreach ($hotelsData as $hotel)
			{
				$hotels[] = array(
					'id' => $hotel['id'],
					'name' => str_repeat('*', $hotel['stars']) . ' ' . $hotel['name'],
				);
			}
		}
		$result = array('status'=>$status,'messages'=>$messages,'hotels'=>$hotels);
		$this->_helper->json($result, array('enableJsonExprFinder' => true));
	}
	
	/**
	 * Обработка заказа на транспорт
	 * @return 
	 */
	public function addTransportAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('order')!='')
		{
			$transportOrder = $this->_request->getParam('order');
			//Проверяем данные на корректность
			$transportOrderValid = new Application_Validator_Transport();
			//Статус валидации
			$status = 1;
			$messages = array();
			$content  = '';
			//Если данные не верные, добавляем сообщения об ошибке и ставим статус 0
			if(!$transportOrderValid->isValid($transportOrder))
			{
				$status = 0;
				$validMessages = $transportOrderValid->getMessages();
				foreach ($validMessages as $message)
				{
					$messages[] = $message;
				}
			}
			//Если данные корректные
			else
			{
				//Обрабатываем заказ на транспорт и получаем параметры добавленной услуги
				$orderRes = $this->regObj->serviceOrderProcess('transport',$transportOrder);
				$transportOrder['summ'] = $orderRes['summ'];
				$transportOrder['id'] = $orderRes['id'];
				/* Создаём новое представление, передаём необходимые переменные, рендерим шаблон.
				 * (генерится строка с информацией по заказанной услуге)
				 */
				$contentView = new Zend_View();
				$curViewPaths = $this->view->getScriptPaths();
				$contentView->setScriptPath($curViewPaths[0]);
				$contentView->orderData = $transportOrder;
				$content = $contentView->render('/process/transport-order-line.phtml');
			}
			//Формируем массив с результатом и отправляем его в json
			$result = array('status'=>$status,'messages'=>$messages,'content'=>$content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Добавление заказа на экскурсию
	 * @return 
	 */
	public function addExcursionAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('order')!='')
		{
			$excursionOrder = $this->_request->getParam('order');
			$status = 1;
			$messages = array();
			$content  = '';
			//Проверяем данные на корректность
			$excursionOrderValid = new Application_Validator_Excursion();
			//Если данные не верные, добавляем сообщения об ошибке и ставим статус 0
			if(!$excursionOrderValid->isValid($excursionOrder))
			{
				$status = 0;
				$validMessages = $excursionOrderValid->getMessages();
				foreach ($validMessages as $message)
				{
					$messages[] = $message;
				}
			}
			else
			{
				//Обрабатываем заказ на транспорт и получаем параметры добавленной услуги
				$orderRes = $this->regObj->serviceOrderProcess('excursion',$excursionOrder);
				$excursionOrder += $orderRes;
				/* Создаём новое представление, передаём необходимые переменные, рендерим шаблон.
				 * (генерится строка с информацией по заказанной услуге)
				 */
				$contentView = new Zend_View();
				$curViewPaths = $this->view->getScriptPaths();
				$contentView->setScriptPath($curViewPaths[0]);
				$contentView->orderData = $excursionOrder;
				$content = $contentView->render('/process/excursion-order-line.phtml');
			}
			//Формируем массив с результатом и отправляем его в json
			$result = array('status'=>$status,'messages'=>$messages,'content'=>$content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Получение информации по возможным периодам для экскурсии 
	 * @return 
	 */
	public function excursionPeriodsAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('excursionId')!='')
		{
			//Получаем возможные периоды для экскурсии
			$excursionId = $this->_request->getParam('excursionId');
			$excursionPeriodsTable = new Welt_DbTable_ExcursionPeriods();
			$periods = $excursionPeriodsTable->getExcursionPeriods($excursionId);
			//Формируем массив с результатом и отправляем его в json
			$result = array('periods'=>$periods);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Обработка заказов на банкеты
	 * @return 
	 */
	public function addMealAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('order')!='')
		{
			$mealOrder = $this->_request->getParam('order');
			$status = 1;
			$messages = array();
			$content  = '';
			//Обрабатываем заказ на банкет и получаем параметры добавленной услуги
			$orderRes = $this->regObj->serviceOrderProcess('meal', $mealOrder);
			$mealOrder += $orderRes;
			/* Создаём новое представление, передаём необходимые переменные, рендерим шаблон.
			 * (генерится строка с информацией по заказанной услуге)
			 */
			$contentView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$contentView->setScriptPath($curViewPaths[0]);
			$contentView->orderData = $mealOrder;
			$content = $contentView->render('/process/meal-order-line.phtml');
			//Формируем массив с результатом и отправляем его в json
			$result = array('status'=>$status,'messages'=>$messages,'content'=>$content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Получение информации по возможным типам питания для банкета 
	 * @return 
	 */
	public function mealTypesAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('mealId')!='')
		{
			//Получаем возможные типы питания
			$mealId = $this->_request->getParam('mealId');
			$mealTypesTable = new Welt_DbTable_MealTypes();
			$mealTypes = $mealTypesTable->getMealTypes($mealId);
			//Формируем массив с результатом и отправляем его в json
			$result = array('count'=>sizeof($mealTypes),'types'=>$mealTypes);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
    
    /**
     * Обработка заказов на услуги типа "Другое"
     * @return 
     */
    public function addOtherAction()
    {
        //Если были переданы корректные данные
        if($this->_request->isPost() && $this->_request->getParam('order')!='')
        {
            $otherOrder = $this->_request->getParam('order');
            $status = 1;
            $messages = array();
            $content  = '';
            //Обрабатываем заказ и получаем параметры добавленной услуги
            $orderRes = $this->regObj->serviceOrderProcess('other', $otherOrder);
            $otherOrder += $orderRes;
            /* Создаём новое представление, передаём необходимые переменные, рендерим шаблон.
             * (генерится строка с информацией по заказанной услуге)
             */
            $contentView = new Zend_View();
            $curViewPaths = $this->view->getScriptPaths();
            $contentView->setScriptPath($curViewPaths[0]);
            $contentView->orderData = $otherOrder;
            $content = $contentView->render('/process/other-order-line.phtml');
            //Формируем массив с результатом и отправляем его в json
            $result = array('status'=>$status,'messages'=>$messages,'content'=>$content);
            $this->_helper->json($result, array('enableJsonExprFinder' => true));
        }
    }
	
	/**
	 * Удаление заказанной усуги
	 * @return 
	 */
	public function deleteServiceAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('type')!='' && $this->_request->getParam('id')!='')
		{
			$status = 1;
			$serviceId = $this->_request->getParam('id');
			$serviceType = $this->_request->getParam('type');
			//Удаляем заказ услуги
			if (!$this->regObj->deleteServiceOrder($serviceId,$serviceType))
			{
				$status = 0;
			}
			//Формируем массив с результатом и отправляем его в json
			$result = array('status'=>$status);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Заказ услуги визовая поддержка
	 * @return 
	 */
	public function addVisaAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('personId')!='')
		{
			$personId = $this->_request->getParam('personId');
			$visaTypeId = $this->_request->getParam('visaTypeId');
			$status = 1;
			$messages = array();
			$orderRes = $this->regObj->addPersonVisaOrder($personId,$visaTypeId);
			if(!$orderRes)
			{
				$status = 0;
				$messages[] = 'Error adding visa support';
			}
			$result = array('status'=>$status,'messages'=>$messages,'visaStr'=>$orderRes);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Удаление услуги визовая поддержка
	 * @return 
	 */
	public function deleteVisaAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('personId')!='')
		{
			$personId = $this->_request->getParam('personId');
			$status = 1;
			$messages = array();
			if(!$this->regObj->deletePersonVisaOrder($personId))
			{
				$status = 0;
				$messages[] = 'Error refuse visa support';
			}
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Завершение процесса добавления услуг из ЛК
	 * @return 
	 */
	public function orderFromPpcCompleteAction()
	{
		//Проверяем, действительно ли форма открыта из ЛК
		$userAppNamespace = new Zend_Session_Namespace('user');
		if (isset($userAppNamespace->fromPpc) && $userAppNamespace->fromPpc==1)
		{
			//Завершаем процесс заказа услуг
			$this->regObj->orderFromPpcComplete();
			$status = 1;
			$result = array('status'=>$status);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
}