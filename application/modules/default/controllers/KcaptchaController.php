<?php
class KcaptchaController extends Zend_Controller_Action
{
	/**
	 * Генерация captcha с помощью модуля kcaptcha
	 * @return 
	 */
	public function captchaAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout->disableLayout();
   	 	header('Content-type: image/jpeg');
		$capctha = new Kcaptcha_Kcaptcha();
		$capctha->setKeyString();
	}
}