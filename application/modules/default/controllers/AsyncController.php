<?php
/**
 * Description of AsyncController
 *
 * @author jon
 */
class AsyncController extends Zend_Controller_Action
{
	public function init()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->getHelper('layout')->disableLayout();
	}


	public function deleteAction()
	{
        $personsDocsTable = new Welt_DbTable_PersonsDocuments();
        $doc = $this->getRequest()->getParam('doc');

        $where = $personsDocsTable->getAdapter()->quoteInto('id = ?', $doc);

        $docDescription = $personsDocsTable->getDoc($doc);

        $file = $docDescription['id'].".".$docDescription['extension'];
        $file2 = $docDescription['file_name'].".".$docDescription['extension'];

        $file = realpath(APPLICATION_PATH . '/../data/uploads/persons/'.$file);
        $file2 = realpath(APPLICATION_PATH . '/../public/uploads/').DIRECTORY_SEPARATOR.$file2;
        unlink($file2);

        if(unlink($file)){
            $personsDocsTable->delete($where);
            echo "ok";
        }

        else

            echo "not ".$file;

		//echo Zend_Json_Encoder::encode($glData->getAll());

	}





}