<?php
class Ppc_IndexController extends Welt_Controller_PpcBaseController
{

    public function init()
    {
    	parent::init();	
    }
	
	/**
	 * Информация по заказу
	 * @return 
	 */
	public function indexAction()
    {
    	//Получаем информацию по заявке и передаём её во view
		$appData = $this->appObj->getApplicationInfo();
		//Считаем сумму к оплате
		if ($appData['summ'] != 0)
		{
			$appData['summ_to_pay'] = $appData['summ'] - $appData['paid_summ'];
			if ($appData['summ_to_pay'] != 0 && $appData['summ']!=0)
			{
				$appData['summ_to_pay'] = sprintf('%01.2f',$appData['summ_to_pay']);
			}	
		}
		else
		{
			$appData['summ_to_pay'] = '-';
		}
		$this->view->appData = $appData;
		//Получаем информацию по заказанным услугам со статусами 1,2,8,10
    	$servicesData = $this->servicesObj->getAllUserServicesOrders(1,array(1,2,8,10));
		/*
		 * Получаем подтверждения для каждой услуги
		 */
		$servicesSize = sizeof($servicesData);
		$confTable = new Welt_DbTable_Confirmations();
		for($i=0; $i<$servicesSize; $i++)
		{
			$servicesData[$i]['confirmation'] = $confTable->getLastConf($servicesData[$i]['id'],$servicesData[$i]['service_sys_name']);
		}
		$this->view->servicesData = $servicesData;
		//Получаем все возможные типы услуг и передаём её во view
		$servicesTypesTable = new Welt_DbTable_ServicesTypes();
		$servicesTypes = $servicesTypesTable->getAllServiceTypes();
		$this->view->servicesTypes = $servicesTypes;
		//Передаём во view флаг, определяющий, закончилась ли уже регистрация
		$this->view->regFinished = $this->eventParams['regFinished'];
    }
	
	
	/**
	 * Контакты
	 * @return 
	 */
	public function contactsAction()
	{
		
	}
	
	/**
	 * Переход к управлению материлами на сайт организатора
	 * @return 
	 */
	public function materialsManAction()
	{
		//Отключаем шаблон и рендеринг вида
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Получение данных, необходимых для редиректа к модулю управления материлами на сервер организатора
		$userTable = new Welt_DbTable_Users();
		$dataToRedir = $userTable->getUserInfo($this->userId,1);
		//Получаем ссылку из настроек и подставляем в неё keystring и email
		$orgMaterialsUrl = $this->settings->event->organizer->materials_url;
		$redirUrl = str_replace('#keystring#',$dataToRedir['keystring'],$orgMaterialsUrl);
		$redirUrl = str_replace('#email#',$dataToRedir['email'],$redirUrl);
		//Редирет на систему организатора
		$this->_helper->redirector->gotoUrl($redirUrl);
	}
	
	
	
	
}