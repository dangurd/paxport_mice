<?php
class Ppc_UserController extends Zend_Controller_Action
{
	/**
	 * @var Объект класса авторизации
	 */
	private $authObj;
	
	public function init()
	{
		$this->authObj = new Auth();
		$this->messenger = $this->_helper->FlashMessenger;
		$this->messenger->setNamespace('ErrorMessages');
		$this->view->lang = Zend_Registry::get('lang');
		//Получаем системные настройки
		$this->settings = Zend_Registry::get('settings');
		$this->view->settings = $this->settings;
		$this->eventParams = Zend_Registry::get('eventParams');
		$this->view->eventParams = $this->eventParams;
		$this->view->lang = Zend_Registry::get('lang');
        if ($this->settings->system->lang->switch->ppc_login == 1 && $this->eventParams['multilang'] == 1)
        {
            $this->view->langSwLogin = TRUE;
        }
        else
        {
            $this->view->langSwLogin = FALSE;    
        }
	}
	
	/**
	 * Вход
	 * @return 
	 */
	public function loginAction()
	{
		//Если данные отправлены через GET запрос (доступ для менеджеров из ТМ)
		if ($this->_request->isGet() && $this->_request->getParam('email')!='' && $this->_request->getParam('password')!='')
		{
			//Отключаем шаблон и рендериг view
			$this->_helper->viewRenderer->setNoRender();
			Zend_Layout::getMvcInstance()->disableLayout();
			//Получаем параметры запроса и проводим авторизацию
			$userData = array(
				'email'		=> $this->_request->getParam('email'),
				'password'	=> $this->_request->getParam('password')
			);
			$authRes = $this->authObj->userAuth($userData);
			//Успешная авторизация
			if($authRes === true)
			{
				$this->_helper->redirector->gotoSimple('index','index','ppc');
			}
			//Ошибка авторизации
			else
			{
				$this->_helper->redirector->gotoRoute(array(''),'ppc-login');
			}
		}
		$loginForm = new Application_Form_Login();
		//Если форма авторизации отправлена
		if ($this->_request->isPost())
		{
			//Если данные корректные
			if ($loginForm->isValid($_POST)) 
			{
				$userData = $this->_request->getPost();
				//Авторизация
				$authRes = $this->authObj->userAuth($userData);
				//Успешная авторизация
				if($authRes === true)
				{
					$this->_helper->redirector->gotoSimple('index','index','ppc');
				}
				elseif ($authRes == 'newApp')
				{
					$this->_helper->redirector->gotoSimple('index','step1','default');
				}
				//Ошибка авторизации
				else
				{
					$this->_helper->redirector->gotoRoute(array(''),'ppc-login');
				}
			}
			//Если данные не корректные
			else
			{
				$this->_helper->redirector->gotoRoute(array(''),'ppc-login');
			}
		}
		$this->view->loginForm = $loginForm;
		//Передаём системные настройки во view
		$this->view->settings = Zend_Registry::get('settings');
	}
	
	/**
	 * Выход
	 * @return 
	 */
	public function logoutAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->authObj->logout();
		$this->_helper->redirector->gotoSimple(array('index','index','ppc'));
	}
	
	/**
	 * Восстановление пароля
	 * @return 
	 */
	public function remindAction()
	{
		$remindForm = new Application_Form_Remind();
		//Проверяем,если были отправлены POST данные
		if( $this->_request->isPost())
		{
			//Проверяем валидность данных формы
			if ($remindForm->isValid($_POST) )
			{
				//Получаем из формы данные, фио пользователя из БД и отправляем письмо с паролем
				$userData = $remindForm->getValues();
				$this->authObj->passwordRemind($userData);
				$this->_helper->redirector->gotoSimple('remind.success','user','ppc');
			}
			else
			{
				//Получаем сообщения об ошибках и передаём их во view
				$formErrorMessages = $remindForm->getMessages();
				foreach ($formErrorMessages as $fieldMessage)
				{
					foreach ($fieldMessage as $message)
					{
						$this->messenger->addMessage($message);
					}
				}
				$this->_helper->redirector->gotoRoute(array(''),'ppc-remind');
			}
		}
		$this->view->remindForm = $remindForm;
		//Передаём системные настройки во view
		$this->view->settings = Zend_Registry::get('settings');
	}
	/**
	 * Успешное сообщение о том, что пароль восстановлен
	 * @return 
	 */
	public function remindSuccessAction()
	{
		
	}
	
	/**
	 * Просмотр профиля
	 * @return 
	 */
	public function profileAction()
	{
		//Получаем id юзера из данных сессии
		$auth = Zend_Auth::getInstance();
		$authData = $auth->getIdentity();
		//Получаем информацию по юзеру
		$usersTable = new Welt_DbTable_Users();
		$this->view->userDetails = $usersTable->getUserInfo($authData['userId']);
	}
	
	/**
	 * Редактирование профиля
	 * @return 
	 */
	public function profileEditAction()
	{
		if($this->_request->isPost() && $this->_request->getParam('userId')!='' && $this->_request->getParam('userData')!='')
		{
			$status = 1;
			$messages = array();
			$userId = $this->_request->getParam('userId');
			$userData = $this->_request->getParam('userData');
			//Если не заполнены необходимые поля
			if (!isset($userData['fname']) || $userData['fname']=='' || !isset($userData['lname']) || $userData['lname']=='' || 
			    !isset($userData['phone']) || $userData['phone']=='')
			{
				$status = 0;
				$messages[] = 'Please type required fields';
			}
			//Если все данные переданы, обновляем информацию в БД
			else
			{
				$usersTable = new Welt_DbTable_Users();
				$where = $usersTable->getAdapter()->quoteInto('id = ?',$userId);
				$usersTable->update($userData,$where);
			}
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
}