<?php
class Ppc_DocsController extends Welt_Controller_PpcBaseController
{
    public function init()
    {
    	parent::init();
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
    }
	
	/**
	 * Загрузка файла с подтверждением, авторизационным письмом или счётом
	 * @return 
	 */
	public function downloadAction()
	{
		//$this->_helper->redirector->gotoSimple('index','index','ppc');
		if($this->_request->isGet())
		{
			//Паараметры для реждиректа в случае ошибки или отсутствия доступа к файлу
			$redirParams = array(
				'action'		=>	'error',
				'controller'	=>	'error',
				'module'		=>	'ppc'
			);
			//Получаем параметры документа, проверяем на пустоту
			$docType = $this->_request->getParam('type');
			$docId = $this->_request->getParam('id');
			if ($docType=='' || $docId=='')
			{
				$this->_helper->redirector->gotoSimple($redirParams['action'],$redirParams['controller'],$redirParams['module']);
			}
			//Создаём объект класса для работы с документами
			try
			{
				$appDoc = new Welt_AppDocs($docType);
				//Получаем путь к файлу
				$docLoc = $appDoc->getDocLocation($docId,$this->appId);
				if(!$docLoc || trim($docLoc['link'])=='' || !file_exists($docLoc['filePath']))
				{
					$this->_helper->redirector->gotoSimple($redirParams['action'],$redirParams['controller'],$redirParams['module']);
				}
				//Устанавливаем заголовки и выдаём файл
				$this->getResponse()
           			 ->setHttpResponseCode(200)
            		 ->setHeader('Pragma', 'public', true)
					 ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
					 ->setHeader('Content-type', 'application/octet-stream', true)
					 ->setHeader('Content-Length', filesize($docLoc['filePath']))
					 ->setHeader('Content-Disposition', 'attachment; filename='.$docLoc['link'])
					 ->clearBody();
        		$this->getResponse()->sendHeaders();
				echo readfile($docLoc['filePath']);
				exit();
			}
			//В случае ошибки редирект на главную
			catch(Exception $e)
			{
				$this->_helper->redirector->gotoSimple($redirParams['action'],$redirParams['controller'],$redirParams['module']);
			}
		}
	}
}