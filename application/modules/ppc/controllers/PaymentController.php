<?php
class Ppc_PaymentController extends Welt_Controller_PpcBaseController
{
	protected $paymentObj;

    public function init()
    {
    	parent::init();	
		$this->paymentObj = new Welt_Payment($this->appId,$this->userId);
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('docRequest', 'json')
					->addActionContext('setPayment', 'json')
					->addActionContext('docsList', 'json')
					->initContext();
    }
	
	/**
	 * Оплата услуг
	 * @return 
	 */
	public function indexAction()
	{
		//Если способы оплаты ещё не выбраны, то редирект на шаг выбора
		if (!$this->paymentObj->checkServicesPayment())
		{
			$this->_helper->redirector->gotoSimple('select','payment','ppc');
		}
		//Получаем список заказанных счетов и авторизационных писем для заявки
		$appDocsObj = new Welt_AppDocs('invoice');
		$this->view->invoices = $appDocsObj->getAppDocs($this->appId);
		$appDocsObj->setDocType('auth_letter');
		$this->view->authLetters = $appDocsObj->getAppDocs($this->appId);
		//Получаем список услуг с выбранными способами оплаты и передаём во view
		$this->view->services = $this->paymentObj->getServicesAndPayment();
		//Получаем список методов оплаты и передаём во view
		$paymentMethods = $this->paymentObj->getPaymentMethods();
		$this->view->methods = $paymentMethods;
		$this->view->methodsSize = sizeof($paymentMethods);
	}
	
	/**
	 * Выбор способов оплаты для услуг
	 * @return 
	 */
	public function selectAction()
	{
		//Если данные по выбранным методам оплаты отправлены
		if ($this->_request->isPost())
		{
			//Отключаем шаблон и представление
			Zend_Layout::getMvcInstance()->disableLayout();
			$this->_helper->viewRenderer->setNoRender();
			//Получаем данные формы
			$servicesPaymentData = $this->_request->getPost();
			$servicesPayment = array();
			$i = 0;
			//Формируем на основе данных формы массив выбранных методов оплаты по услугам
			foreach($servicesPaymentData as $service=>$payment)
			{
				list($servicesPayment[$i]['service_sys_name'],$servicesPayment[$i]['service_id']) = explode('-',$service);
				$servicesPayment[$i]['method_sys_name'] = $payment;
				$i++;
			}
			//Сохраняем выбранные для услуг методы оплаты
			$this->paymentObj->setServicesPayment($servicesPayment);
			//Редирект на шаг оплаты
			$this->_helper->redirector->gotoSimple('index','payment','ppc');
		}
		//Если данные не отправлены, выводим таблицу для выбора
		else
		{
			//Если способы оплаты выбраны, то редирект на шаг оплаты
			if ($this->paymentObj->checkServicesPayment())
			{
				$this->_helper->redirector->gotoSimple('index','payment','ppc');
			}
			//Получаем список услуг для оплаты и передаём во view
			$this->view->services = $this->paymentObj->getServicesForPayment();
			//Получаем список методов оплаты и передаём во view
			$this->view->methods = $this->paymentObj->getPaymentMethods();
			//Получаем список заказанных счетов и авторизационных писем для заявки
			$appDocsObj = new Welt_AppDocs('invoice');
			$this->view->invoices = $appDocsObj->getAppDocs($this->appId);
			$appDocsObj->setDocType('auth_letter');
			$this->view->authLetters = $appDocsObj->getAppDocs($this->appId);
		}
	}
	
	/**
	 * Выбор указанного способа оплаты для услуги
	 * @return 
	 */
	public function setPaymentAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost())
		{
			$status = 1;
			$messages = array();
			//Получаем тип услуги и id, метод оплаты проверяем на пустоту
			$serviceId = $this->_request->getParam('serviceId');
			$serviceSysName = $this->_request->getParam('serviceSysName');
			$paymentMethod = $this->_request->getParam('paymentMethod');
			if (trim($serviceId)=='' || trim($serviceSysName)=='' || trim($paymentMethod)=='')
			{
				$status = 0;
				$messages[] = 'Missing params';
			}
			//Сохраняем данные по запросу и выдаём ответ
			if(!$this->paymentObj->setServicePayment($serviceId,$serviceSysName,$paymentMethod))
			{
				$status = 0;
				$messages[] = 'Service set payment error';
			}
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Сброс выбранных способов оплаты для услуг
	 * @return 
	 */
	public function resetAction()
	{
		Zend_Layout::getMvcInstance()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Удаляем выбранные для услуг методы оплаты
		$this->paymentObj->resetServicesPayment();
		//Редирект на шаг выбора методов оплаты
		$this->_helper->redirector->gotoSimple('select','payment','ppc');
	}
	
	/**
	 * Редирект на страницу онлайн-оплаты
	 * @return 
	 */
	public function onlinePayAction()
	{
		//Отключаем шаблон и рендеринг вида
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		//Получаем параметры для редиректа на страницу оплаты
		$lang = $this->_request->getParam('lang');
		$paymentMethod = $this->_request->getParam('method');
		$summ = $this->_request->getParam('summ');
		$tourNumber = $this->appObj->getTourNumber();
		//Редирект на страницу оплаты
		//$redirUrl = 'http://hotels-booking.ru/'.$lang.'/merchant.html?op=ok&act=service&payment_method='.$paymentMethod.'&service_0='.$summ.'&tour_number='.$tourNumber;
		$redirUrl = 'https://bs.welt.ru/?r=payment/#method/index&amount=#amount&tourNum=#tour&lang=#lang';
		$redirUrl = strtr($redirUrl, array(
			'#method' => $paymentMethod,
			'#amount' => $summ,
			'#tour' => $tourNumber,
			'#lang' => $lang,
		));
		$this->_helper->redirector->gotoUrl($redirUrl);
	}
	
	/**
	 * Создание запроса на платёжный документ
	 * @return 
	 */
	public function docRequestAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('request')!='')
		{
			$status = 1;
			$messages = array();
			$docRequest = $this->_request->getParam('request');
			//Сохраняем данные по запросу и выдаём ответ
			$this->paymentObj->proccessDocRequest($docRequest);
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Получения списка документов заданного типа
	 * @return 
	 */
	public function docsListAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('type')!='')
		{
			$status = 1;
			$messages = array();
			$content = '';
			$docType = $this->_request->getParam('type');
			//Получаем документы заданного типа
			$appDocsObj = new Welt_AppDocs($docType);
			$appDocs = $appDocsObj->getAppDocs($this->appId);
			//Создаём новое представление, передаём необходимые переменные, рендерим шаблон.
			$contentView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$contentView->setScriptPath($curViewPaths[0]);
			$contentView->docs = $appDocs;
			$content = $contentView->render('/payment/docs-list/'.$docType.'.phtml');
			$result = array('status'=>$status,'messages'=>$messages,'content'=>$content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	
}