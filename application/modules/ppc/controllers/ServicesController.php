<?php
class Ppc_ServicesController extends Welt_Controller_PpcBaseController
{

    public function init()
    {
		parent::init();
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('editParams', 'json')
					->initContext();
    }
	
	
	
	/**
	 * Получение деталей по услуге
	 * @return 
	 */
	public function detailsAction()
	{
		//Отключаем шаблон и рендеринг вида
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->_request->isPost())
		{
			//Получаем тип услуги и id, проверяем на пустоту
			$serviceId = $this->_request->getParam('serviceId');
			$serviceType = $this->_request->getParam('serviceType');
			if (trim($serviceId)=='' || trim($serviceType)=='')
			{
				return false;
			}
			//Получаем информацию по услуге и передаём во view
			$this->servicesObj->setServiceType($serviceType);
			//Создаём объект вида и передаём ему информацию по услуге
			$serviceView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$serviceView->setScriptPath($curViewPaths[0]);
			$serviceView->serviceDetails = $this->servicesObj->getServiceDetails($serviceId);
			//В соответствии с типом услуги, определяем шаблон
			$template = 'services/details/'.$serviceType.'.phtml';
			//Рендерим шаблон, устанавливаем ответ
			$content = $serviceView->render($template);
			$response = $this->getResponse();
        	$response->setBody($content)->setHeader('Content-type','text/html');
		}
	}
	
	/**
	 * Подача запроса на аннуляцию услуги
	 * @return 
	 */
	public function cancelAction()
	{
		//Отключаем шаблон и рендеринг вида
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->_request->isPost())
		{
			//Получаем тип услуги и id, проверяем на пустоту
			$serviceId = $this->_request->getParam('serviceId');
			$serviceType = $this->_request->getParam('serviceType');
			if (trim($serviceId)=='' || trim($serviceType)=='')
			{
				return false;
			}
			//Устанавливаем тип услуги
			$this->servicesObj->setServiceType($serviceType);
			//Запрос на аннуляцию услуги
			$this->servicesObj->serviceCancelRequest($serviceId);
		}
	}
	
	/**
	 * Получение параметров для редактирования по услуге
	 * @return 
	 */
	public function editParamsAction()
	{
		//Отключаем шаблон и рендеринг вида
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
		if($this->_request->isPost())
		{
			//Получаем тип услуги и id, проверяем на пустоту
			$serviceId = $this->_request->getParam('serviceId');
			$serviceType = $this->_request->getParam('serviceType');
			if (trim($serviceId)=='' || trim($serviceType)=='')
			{
				return false;
			}
			//Получаем информацию по услуге и передаём во view
			$this->servicesObj->setServiceType($serviceType);
			//Создаём объект вида и передаём ему информацию по услуге
			$serviceView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$serviceView->setScriptPath($curViewPaths[0]);
			$serviceView->serviceDetails = $this->servicesObj->getServiceDetailsForEdit($serviceId);
			//Получаем список персон для услуги, формируем массив из id данных персон и передаём во view
			$servicePersons = $this->servicesObj->getServicePersons($serviceId);
			foreach($servicePersons as $person)
			{
				$servicePersonsIds[] = $person['id'];
			}
			$serviceView->servicePersons = $servicePersons;
			$serviceView->servicePersonsIds = $servicePersonsIds;
			//Получаем всех персон для пользователя и передаём его во view
			$personsTable = new Welt_DbTable_Persons();
			$auth = Zend_Auth::getInstance();
			$authData = $auth->getIdentity();
			$serviceView->appPersons = $personsTable->getUserPersons($authData['userId'],0,array(0,1,2));
			//В соответствии с типом услуги, определяем шаблон
			$template = 'services/edit-params/'.$serviceType.'.phtml';
			//Рендерим шаблон, устанавливаем ответ
			$content = $serviceView->render($template);
			$response = $this->getResponse();
        	$response->setBody($content)->setHeader('Content-type','text/html');
		}
	}
	
	/**
	 * Создание запроса на редактирование услуги
	 * @return 
	 */
	public function editRequestAction()
	{
		if($this->_request->isPost())
		{
			$status = 1;
			$messages = array();
			$editRequestData = $this->_request->getParam('data');
			/*
			 * Создаём объект вида и передаём в него параметры для запроса на редактирование услуги, рендерим
			 * результат в переменную
			 */
			$requestView = new Zend_View();
			$curViewPaths = $this->view->getScriptPaths();
			$requestView->setScriptPath($curViewPaths[0]);
			//Передаём во view параметры запроса, а также № тура и дату, название мероприятия
			$appTable = new Welt_DbTable_Applications();
			$editRequestData['tourNumber'] = $appTable->find($this->appId)->current()->tour_number;
			$curDate = new Zend_Date();
			$editRequestData['requestDate'] = $curDate->toString('dd.MM.yyyy HH:mm:ss');
			//Обрабатываем комментарий
			$editRequestData['comment'] = Welt_Functions::filterComment($editRequestData['comment']);
			$requestView->requestData = $editRequestData;
			//В соответствии с типом услуги, определяем шаблон и рендерим его во view
			$template = 'services/edit-request/request.phtml';
			$requestContent = $requestView->render($template);
			//Добавляем новый запрос на изменение услуги
			$servicesEditRequestsTable = new Welt_DbTable_ServicesEditRequests();
			$data = array(
				'service_id'			=>	$editRequestData['serviceId'],
				'service_sys_name'		=>	$editRequestData['serviceType'],
				'application_id'		=>	$this->appId,
				'request'				=>	$requestContent,
				'status'				=>	0,
				'created'				=>	$curDate->toString('yyyy-MM-dd HH:mm:ss')
			);
			if(!$servicesEditRequestsTable->insert($data))
			{
				$status = 0;
				$messages[] = 'Service edit request error';
			}
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
}