<?php
class Ppc_ThesisController extends Welt_Controller_PpcBaseController
{
	
	public function indexAction ()
	{
	    
	    // смотрим аутентификаци пользователя
	    $auth = Zend_Auth::getInstance();
	    $authData = $auth->getIdentity();
	    // подключаем нужные таблицы
	    $usersTable = new Welt_DbTable_Users();
	    $personsTable = new Welt_DbTable_Persons();
	    $thesisTable = new Welt_DbTable_Thesis();
	    $thesisTextTable = new Welt_DbTable_ThesisText();
	    $thesisAuthorsTable = new Welt_DbTable_ThesisAuthors();
	    $countryTable = new Welt_DbTable_WeltCountries();
	    $servicePersonsTable = new Welt_DbTable_ServicesAndPersons();
	    $regFeeTable = new Welt_DbTable_RegFeeOrders();
	    
	    //mailer
	    $mail = new Welt_PHPMailer();
	    
	    //если тезис был отправлен
	    if($this->_request->isPost() != '') {
	        if($this->_request->getParam('newThesis') != ''){
	            
	            
	                $type = $this->_request->getParam('type');
	            
	            
	            $mainAuthorInfo = explode(',', $this->_request->getParam('main_author'));
	            $personId = $mainAuthorInfo[0];
	            if($this->_request->getParam('sym') != '')
	            {
	                $sym = $this->_request->getParam('sym');
	            } else {
	                $sym = 0;
	            }
	            
	            $newThesisData = array(
	                'user_id'    => $authData['userId'],
	                'person_id'  => $personId,
	                'created'    => date('Y-m-d'),
	                'is_ready'   => 1,
	                'is_applied' => 0,
	                'type'       => $type,
	                'sym'        => $sym,

	            );
	            $thesisTable->insert($newThesisData);
	            $newThesisId = $thesisTable->getAdapter()->lastInsertId();
	            
	            $newThesisTextData = array(
	                'thesis_id' => $newThesisId,
	                'name' => $this->_request->getParam('thesis_name'),
	                'text' => $this->_request->getParam('thesis_text'),
	                'press' => $this->_request->getParam('pressParagraph'),
	                'general' => $this->_request->getParam('generalSummary'),
	                'requirements' => $this->_request->getParam('requirements'),
	            );
	            $thesisTextTable->insert($newThesisTextData);
	            
	            // don't forget to add thesis authors
	            
	            $mainAuthorData = array(
	                'thesis_id' => $newThesisId,
	                'person_id' => $personId,
	                'fname' => $mainAuthorInfo['2'],
	                'lname' => $mainAuthorInfo['1'],
	                'position' => $this->_request->getParam('position0'),
	                'workspace' => $this->_request->getParam('company0'),
	                'country' => $mainAuthorInfo['3'],
	                'city' => $this->_request->getParam('city0'),
	            );
	            if($_FILES['cvs0'] != '') {
	                $uploaddir = '/home/mices/ecp2019/data/uploads/';
	                move_uploaded_file($_FILES['cvs0']['tmp_name'], $uploaddir.'CVSPanelDiscussion_'.$newThesisId.'_'.$mainAuthorData['fname'].'_'.$mainAuthorData['lname']);
	            }
	            $thesisAuthorsTable->insert($mainAuthorData);
	            
	           
	            
	            for($i=1;$i<=4;$i++)
	            {
	                $fname = 'fname'.$i;
	                $lname = 'sname'.$i;
	                $country = 'country'.$i;
	                $city = 'city'.$i;
	                $company = 'company'.$i;
	                $position = 'position'.$i;
	                $author = '';
                    if ($this->_request->getParam($fname) != null) {
                        $author['fname'] = $this->_request->getParam($fname);
                        $author['lname'] = $this->_request->getParam($lname);
                        $author['country'] = $this->_request->getParam($country);
                        $author['city'] = $this->_request->getParam($city);
                        $author['workspace'] = $this->_request->getParam($company);
                        $author['position'] = $this->_request->getParam($position);
                        
                    }
                    if($_FILES['cvs'.$i] != '') {
                        $uploaddir = '/home/mices/ecp2019/data/uploads/';
                        move_uploaded_file($_FILES['cvs'.$i]['tmp_name'], $uploaddir.'CVSPanelDiscussion_'.$newThesisId.'_'.$author['fname'].'_'.$author['lname']);
                    }
                    
	                $authors[] = $author;
	                
	            }
	            //print_r($authors);die;
	            
	            foreach($authors as $author){
	                if($author['fname'] != ''){
	                    $newAuthorData = array(
	                        'thesis_id' => $newThesisId,
	                        'person_id' => '',
	                        'fname' => $author['fname'],
	                        'lname' => $author['lname'],
	                        'position' => $author['position'],
	                        'workspace' => $author['workspace'],
	                        'country' => $author['country'],
	                        'city' => $author['city'],
	                    );
	                    $thesisAuthorsTable->insert($newAuthorData);
	                }
	                
	            }      
	            $subjectOrg = 'New Abstracts Submitted # '.$newThesisId;
	            $messageOrg = '<p>Attn. Scientific Committee!</p>
                               <p>Abstracts # '.$newThesisId.' have been uploaded to <a href="http://ecplko.forms.paxport.tech/">Personal Organizer’s Cabinet</a>';
	            /*try {
	                $mail->SMTPDebug = 0;
	                $mail->isSMTP();
	                $mail->CharSet = 'UTF-8';
	                $mail->Host = 'smtp.yandex.ru';
	                $mail->SMTPAuth = true;
	                $mail->Username = 'robot@paxport.ru';
	                $mail->Password = 'liz567890';
	                $mail->SMTPSecure = 'ssl';
	                $mail->Port = 465;
	                $mail->setFrom('robot@paxport.ru', 'ECP2019');
	                $mail->addAddress('abstract@ecp2019.ru', '');
	                $mail->addCC('solodilov@paxport.ru');
	                $mail->isHTML(true);
	                $mail->Subject = $subjectOrg;
	                $mail->Body    = $messageOrg;
	                $mail->send();
	                //echo 'Message has been sent';
	            } catch (Exception $e) {
	                //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
	            }*/
	            
	            $subject = 'Abstracts # '.$newThesisId.' Sent to Scientific Committee';
	            $message = '<p>Dear participant!</p>
                            <p>Abstracts # '.$newThesisId.' you had uploaded in your Personal Cabinet have been sent to Scientific Committee.</p>
                            <p>Title: "'.$this->_request->getParam('thesis_name').'"</p>
                            <p>You will be able to make changes to your abstract (authorship, title and contents) on the Personal Cabinet before the deadline for abstract submission on 3 December 2018.
                            </p><p>Notification of acceptance by the Scientific Committee will be emailed to the corresponding author. Please note that the Scientific Committee reserves the right to change submissions for oral presentations to posters, rather than refusing abstracts, depending on the number, theme, and quality of submissions
                           </p> <p>Best regards,</p>
                            <p>Scientific Committee</p>
                            <p>16th European Congress of Psychology</p>';
	            
	            $mailer = new Mail();
	            $mailer->mailToUser($authData['email'], $subject, $message);
	            /* echo '<pre>';
	            print_r($authors);
	            echo'</pre>';
	            die; */
	            
	        } elseif ($this->_request->getParam('editThesis') != '') {
	            //print_r($_POST);die;
	            $thesisId = $this->_request->getParam('editThesis');
	            
	                $type = $this->_request->getParam('type');
	            
	                if($this->_request->getParam('sym') != '')
	                {
	                    $sym = $this->_request->getParam('sym');
	                } else {
	                    $sym = 0;
	                }

	            $editThesisData = array(
	                'user_id'    => $authData['userId'],
	                'person_id'  => $this->_request->getParam('person_id'),
	                'created'    => date('Y-m-d'),
	                'is_ready'   => 1,
	                'is_applied' => 0,
	                'type'       => $type,
	                'sym'        => $sym,
	            );
	            $where = $thesisTable->getAdapter()->quoteInto('id=?',$thesisId);
	            $thesisTable->update($editThesisData, $where);
	            
	            $editThesisTextData = array(
	                'name' => $this->_request->getParam('thesis_name'),
	                'text' => $this->_request->getParam('thesis_text'),
	                'press' => $this->_request->getParam('pressParagraph'),
	                'general' => $this->_request->getParam('generalSummary'),
	                'requirements' => $this->_request->getParam('requirements'),
	            );
	            $where = $thesisTextTable->getAdapter()->quoteInto('thesis_id=?',$thesisId);
	            
	            $thesisTextTable->update($editThesisTextData, $where);
	            $mainAuthorId = $this->_request->getParam('mainAuthorId');
	            $thesisAuthorsIds = $thesisAuthorsTable->getAuthorsIds($thesisId);
	            if($this->_request->getParam('mainAuthor') != ''){
	               $mainAuthorNames = explode(',',$this->_request->getParam('mainAuthor'));
	               $newAuthor = '1';
	               if($_FILES['cvs'.$mainAuthorId] != '') {
	                   $uploaddir = '/home/mices/ecp2019/data/uploads/';
	                   move_uploaded_file($_FILES['cvs'.$mainAuthorId]['tmp_name'], $uploaddir.'CVSPanelDiscussion_'.$thesisId.'_'.$mainAuthorNames[1].'_'.$mainAuthorNames[2]);
	               }
	            }
	            if(!isset($newAuthor)){
	            $mainAuthorData = array (
	                'city' => $this->_request->getParam('city0'),
	                'workspace' => $this->_request->getParam('company0'),
	                'position' => $this->_request->getParam('position0'),
	            );
	            $mainAuthorOldNames = explode(';',$this->_request->getParam('oldMainAuthor'));
	            if($_FILES['cvs'.$mainAuthorId] != '') {
	                $uploaddir = '/home/mices/ecp2019/data/uploads/';
	                move_uploaded_file($_FILES['cvs'.$mainAuthorId]['tmp_name'], $uploaddir.'CVSPanelDiscussion_'.$thesisId.'_'.$mainAuthorOldNames[0].'_'.$mainAuthorOldNames[1]);
	            }
	            
	            $mainAuthorWhere = $thesisAuthorsTable->getAdapter()->quoteInto('id=?',$this->_request->getParam('mainAuthorId'));
	            $thesisAuthorsTable->update($mainAuthorData,$mainAuthorWhere);
	            } else {
	                $mainAuthorWhere = $thesisAuthorsTable->getAdapter()->quoteInto('id=?',$this->_request->getParam('mainAuthorId'));
	                $thesisAuthorsTable->delete($mainAuthorWhere);
	                $mainAuthorData = array (
	                    'thesis_id' =>$thesisId,
	                    'person_id' => $mainAuthorNames[0],
	                    'fname' => $mainAuthorNames[1],
	                    'lname' => $mainAuthorNames[2],
	                    'country' => $mainAuthorNames[3],
	                    'city' => $this->_request->getParam('city0'),
	                    'workspace' => $this->_request->getParam('company0'),
	                    'position' => $this->_request->getParam('position0'),
	                );
	                $thesisAuthorsTable->insert($mainAuthorData);
                    $mainAuthorUpdateWhere = $thesisAuthorsTable->getAdapter()->quoteInto('id=?',$thesisId);
                    $updateMainAuthorId = array('person_id' => $mainAuthorNames[0]);
	                $thesisTable->update(
                        $updateMainAuthorId,
                        $mainAuthorUpdateWhere
                    );
	            //print_r($_POST);die;
	            }
	            foreach($thesisAuthorsIds as $thesisAuthorId)
	            {
	                $id = $thesisAuthorId['id'];
	                $fname = 'fname'.$id;
	                $lname = 'sname'.$id;
	                $country = 'country'.$id;
	                $city = 'city'.$id;
	                $company = 'company'.$id;
	                $position = 'position'.$id;
	                $thesisAuthor = '';
	                if ($this->_request->getParam($fname) != null) {
	                    $authorData = array(
	                        'fname' => $this->_request->getParam($fname),
	                        'lname' => $this->_request->getParam($lname),
	                        'position' => $this->_request->getParam($position),
	                        'workspace' => $this->_request->getParam($company),
	                        'country' => $this->_request->getParam($country),
	                        'city' => $this->_request->getParam($city),
	                    );
	                    if($_FILES['cvs'.$id] != '') {
	                        $uploaddir = '/home/mices/ecp2019/data/uploads/';
	                        move_uploaded_file($_FILES['cvs'.$id]['tmp_name'], $uploaddir.'CVSPanelDiscussion_'.$thesisId.'_'.$authorData['fname'].'_'.$authorData['lname']);
	                    }
	                    
	                 /*    $thesisAuthor['fname'] = $this->_request->getParam($fname);
	                    $thesisAuthor['lname'] = $this->_request->getParam($lname);
	                    $thesisAuthor['country'] = $this->_request->getParam($country);
	                    $thesisAuthor['city'] = $this->_request->getParam($city);
	                    $thesisAuthor['workspace'] = $this->_request->getParam($company);
	                    $thesisAuthor['position'] = $this->_request->getParam($position); */
	                }
	                $authorWhere = $thesisAuthorsTable->getAdapter()->quoteInto('id=?',$id);
	                $thesisAuthorsTable->update($authorData, $authorWhere);
	               
	            }
	            for($i=1;$i<=4;$i++)
	            {
	                $fname = 'fname'.$i;
	                $lname = 'sname'.$i;
	                $country = 'country'.$i;
	                $city = 'city'.$i;
	                $company = 'company'.$i;
	                $position = 'position'.$i;
	                $author = '';
	                if ($this->_request->getParam($fname) != null) {
	                    $author['fname'] = $this->_request->getParam($fname);
	                    $author['lname'] = $this->_request->getParam($lname);
	                    $author['country'] = $this->_request->getParam($country);
	                    $author['city'] = $this->_request->getParam($city);
	                    $author['workspace'] = $this->_request->getParam($company);
	                    $author['position'] = $this->_request->getParam($position);
	                }
	                
	                $authors[] = $author;
	                
	            }
	            //print_r($authors);die;
	            
	            foreach($authors as $author){
	                if($author['fname'] != ''){
	                    $newAuthorData = array(
	                        'thesis_id' => $thesisId,
	                        'person_id' => '',
	                        'fname' => $author['fname'],
	                        'lname' => $author['lname'],
	                        'position' => $author['position'],
	                        'workspace' => $author['workspace'],
	                        'country' => $author['country'],
	                        'city' => $author['city'],
	                    );
	                    $thesisAuthorsTable->insert($newAuthorData);
	                }
	                
	            } 
	            
	            $subject = 'Abstracts # '.$thesisId.' Sent to Scientific Committee';
	            $message = '<p>Dear participant!</p>
                            <p>Abstracts # '.$thesisId.' you had uploaded in your Personal Cabinet have been sent to Scientific Committee.</p>
                            <p>Title: "'.$this->_request->getParam('thesis_name').'"</p>
                            <p>You will be able to make changes to your abstract (authorship, title and contents) on the Personal Cabinet before the deadline for abstract submission on 3 December 2018.
                            </p><p>Notification of acceptance by the Scientific Committee will be emailed to the corresponding author. Please note that the Scientific Committee reserves the right to change submissions for oral presentations to posters, rather than refusing abstracts, depending on the number, theme, and quality of submissions
                           </p> <p>Best regards,</p>
                            <p>Scientific Committee</p>
                            <p>16th European Congress of Psychology</p>';
	            
	            
	               
	                /*$mail->isSMTP();
	                $mail->CharSet = 'UTF-8';
	                $mail->Host = 'smtp.yandex.ru';
	                $mail->SMTPAuth = true;
	                $mail->Username = 'robot@paxport.ru';
	                $mail->Password = 'liz567890';
	                $mail->SMTPSecure = 'ssl';
	                $mail->Port = 465;
	                $mail->setFrom('robot@paxport.ru', 'ECP2019');
	                $mail->addAddress('khimich@paxport.ru', '');
	                //$mail->addCC('khimich@paxport.ru');
	                $mail->isHTML(true);
	                $mail->Subject = $subject;
	                $mail->Body    = $message;
	                $mail->SMTPDebug = 0;
	                $mail->send();*/
	                //echo 'Message has been sent';
	           
	        }
	    }
	    
	    //создаем объект данных шага
	    $stepData = array();
	    
	    // заполняем объект данными
	    $stepData['userInfo'] = $usersTable->getUserInfo($authData['userId']);
	    // проверяем наличие тезисов
	    $checkThesis = $thesisTable->checkThesis($authData['userId']);
	    if (isset($checkThesis)) {
	        // если тезисы существуют - заполняем данные по этим тезисам
	        $thesisList = $thesisTable->getThesisListByUserId($authData['userId']);
	        foreach($thesisList as $thesis)
	        {
	            /*if($thesis['id'] == 234) {
                    echo '<pre>';
                    print_r($thesis);
                    echo '</pre>';
                    die();
                }*/
	            if($thesis['type'] == 1 && $thesis['sym'] != '0')
	            {
	                $symData = $thesisTextTable->getThesisTextByThesisId($thesis['sym']);
	                $thesis['symName'] = $symData[0]['name'];
	            }
	            $thesisTextData = $thesisTextTable->getThesisTextByThesisId($thesis['id']);
	            $thesis['name'] = $thesisTextData[0]['name'];
	            $thesis['text'] = $thesisTextData[0]['text'];
	            $thesis['press'] = $thesisTextData[0]['press'];
	            $thesis['general'] = $thesisTextData[0]['general'];
	            $thesis['requirements'] = $thesisTextData[0]['requirements'];
	            $thesisAuthorsData = $thesisAuthorsTable->getThesisAuthorsByThesisId($thesis['id']);
	            /*foreach($thesisAuthorsData as $author)
	            {
	                if(!empty($author['person_id']))
	                {
	                    $mainAuthorId = $author['person_id'];
	                    $personServices = $servicePersonsTable->getPersonServices($mainAuthorId, 'reg_fee');
	                    if(!empty($personServices))
	                    {
	                        $regFeeData = $regFeeTable->getOrderDetails($personServices[0]);
//	                        print_r($regFeeData);die;
	                        if($regFeeData['is_paid'] == '1')
	                        {
	                            $thesis['payment'] = '1';
	                        }
	                    }
	                    
	                }
	            }*/
	            if($thesis['is_paid'] == 1){
                    $thesis['payment'] = '1';
                }
	            $thesis['authors'][] = $thesisAuthorsData;
	            $stepData['thesisList'][] = $thesis;
	        }
	        
	        $symposiumsData = $thesisTable->getUserSymposiums($authData['userId']);
	        if($symposiumsData != ''){
	            foreach ($symposiumsData as $symposiumId)
	            {
	                $symposiumData = $thesisTextTable->getThesisTextByThesisId($symposiumId);
	                $symposiumName = $symposiumData[0]['name'];
	                $symposium['id'] = $symposiumId['id'];
	                $symposium['name'] = $symposiumName;
	                
	                $symposiums[] = $symposium;
	            }
	            
	        }
	        $stepData['symposiums'] = $symposiums;
/* 	        echo '<pre>';
	        print_r($thesis);
	        echo '<pre>';
	        die; */
	        
	        
	        
	    } else {
	        $stepData['thesisList'] = '';
	    }
	    
	        

	    $countryList = $countryTable->getCountriesListEn();
	    
	    // получаем список персон для этого пользователя
	    $personsList = $personsTable->getUserPersons($authData['userId']);
	    $personDetailsTable = new Welt_DbTable_PersonsDetails();
	    foreach ($personsList as $personInfo)
	    {
	        foreach($countryList as $country){
	            if($country['id'] == $personInfo['citizenship_id']){
	                $person['citizenship'] = $country['name'];
	            }
	        }
	        $person['id'] = $personInfo['id'];
	        $person['fname'] = $this->translitAction($personInfo['fname']);
	        $person['lname'] = $this->translitAction($personInfo['lname']);
	        $person['name'] = $this->translitAction($personInfo['name']);
	        $personDetailsData = $personDetailsTable->getPersonDetails($person['id']);
	        //print_r($personDetailsData);
	        $person['city'] = $this->translitAction($personDetailsData['city']);
	        $person['workspace'] = $this->translitAction($personDetailsData['workspace']);
	        $person['position'] = $this->translitAction($personDetailsData['position']);
	        $person['citizenship_id'] = $personInfo['citizenship_id'];
	        $stepData['personsList'][] = $person;
	        //print_r($stepData);die;
	        $personServices = $servicePersonsTable->getPersonServices($person['id'], 'reg_fee');
	        if(!empty($personServices))
	        {
	            $regFeeData = $regFeeTable->getOrderDetails($personServices[0]);
	            if($regFeeData['status'] == '9')
	            {
	                $thesis['payment'] = '1';
	            }
	        }
	        // тут наверно что-то должно быть
	    }
	    $stepData['countryList'] = $countryList;
	    $abstractTypes = array(
	        1 => 'Oral Presentation',
	        2 => 'Poster Presentation',
	        3 => 'Proposed Symposium',
	        4 => 'Panel Discussion'
	    );
	    $stepData['abstractTypes'] = $abstractTypes;
	    
	    $this->view->stepData = $stepData;

	}
	
	public function editAction ()
	{
	    
	    // смотрим аутентификаци пользователя
	    $auth = Zend_Auth::getInstance();
	    $authData = $auth->getIdentity();
	    
	    // подключаем нужные таблицы
	    $usersTable = new Welt_DbTable_Users();
	    $personsTable = new Welt_DbTable_Persons();
	    $thesisTable = new Welt_DbTable_Thesis();
	    $thesisTextTable = new Welt_DbTable_ThesisText();
	    $thesisAuthorsTable = new Welt_DbTable_ThesisAuthors();
	    $countryTable = new Welt_DbTable_WeltCountries();
	    
	    $stepData = array();
	    $countryList = $countryTable->getCountriesListEn();
	    
	    $stepData['countryList'] = $countryList;
	    if($this->_request->isPost() != '' && $this->_request->getParam('authorId') != '') {
	        $authorId = $this->_request->getParam('authorId');
	        $thesisAuthorsTable = new Welt_DbTable_ThesisAuthors();
	        $where = $thesisAuthorsTable->getAdapter()->quoteInto('id=?',$authorId);
	        $thesisAuthorsTable->delete($where);
	        
	    }
	    if($this->_request->getQuery(thesisId) != 0){
	        $thesisId = $this->_request->getQuery(thesisId);
	    }
	    if($this->_request->isPost() != '' && $this->_request->getParam('thesisId') != '' || $thesisId != '') {
	        if(!isset($thesisId)){
	            $thesisId = $this->_request->getParam('thesisId');
	        }
	        $thesisData = $thesisTable->getThesisById($thesisId);
	        
	        $thesis = $thesisData;
            if($thesis['is_paid'] == 1){
                $thesis['payment'] = '1';
            }
	        $thesisTextData = $thesisTextTable->getThesisTextByThesisId($thesisId);
	       // print_r($thesis);die;
	        $thesis['name'] = $thesisTextData[0]['name'];
	        $thesis['text'] = $thesisTextData[0]['text'];
	        $thesis['press'] = $thesisTextData[0]['press'];
	        $thesis['general'] = $thesisTextData[0]['general'];
	        $thesis['requirements'] = $thesisTextData[0]['requirements'];
	        //print_r($thesisTextData);die;
	        $thesisAuthorsData = $thesisAuthorsTable->getThesisAuthorsByThesisId($thesisId);
	        foreach($thesisAuthorsData as $author)
	        {
	            
	            if($author['person_id'] == $thesis['person_id'])
	            {
	                $author['is_main'] = '1';
	            } else {
	                $author['is_main'] = '0';
	            }
	            if(file_exists('/home/mices/ecp2019/data/uploads/CVSPanelDiscussion_'.$thesisId.'_'.$author['fname'].'_'.$author['lname']))
	            {
	                $author['cvs'] = '1';
	            } else {
	                $author['cvs'] = '0';
	            }
	            $thesis['authors'][] = $author;
	        }
	        $symposiumsData = $thesisTable->getUserSymposiums($authData['userId']);
	        if($symposiumsData != ''){
	            foreach ($symposiumsData as $symposiumId)
	            {
	                $symposiumData = $thesisTextTable->getThesisTextByThesisId($symposiumId);
	                $symposiumName = $symposiumData[0]['name'];
	                $symposium['id'] = $symposiumId['id'];
	                $symposium['name'] = $symposiumName;
	                
	                $symposiums[] = $symposium;
	            }
	            
	        }
	        $stepData['symposiums'] = $symposiums;
	        
	        $stepData['thesis'] = $thesis;

            $countryList = $countryTable->getCountriesListEn();

            // получаем список персон для этого пользователя
            $personsList = $personsTable->getUserPersons($authData['userId']);
            $personDetailsTable = new Welt_DbTable_PersonsDetails();
            foreach ($personsList as $personInfo)
            {
                foreach($countryList as $country){
                    if($country['id'] == $personInfo['citizenship_id']){
                        $person['citizenship'] = $country['name'];
                    }
                }
                $person['id'] = $personInfo['id'];
                $person['fname'] = $this->translitAction($personInfo['fname']);
                $person['lname'] = $this->translitAction($personInfo['lname']);
                $person['name'] = $this->translitAction($personInfo['name']);
                $personDetailsData = $personDetailsTable->getPersonDetails($person['id']);
                //print_r($personDetailsData);
                $person['city'] = $this->translitAction($personDetailsData['city']);
                $person['workspace'] = $this->translitAction($personDetailsData['workspace']);
                $person['position'] = $this->translitAction($personDetailsData['position']);
                $person['citizenship_id'] = $personInfo['citizenship_id'];
                $stepData['personsList'][] = $person;
                //print_r($stepData);die;
                /*$personServices = $servicePersonsTable->getPersonServices($person['id'], 'reg_fee');
                if(!empty($personServices))
                {
                    $regFeeData = $regFeeTable->getOrderDetails($personServices[0]);
                    if($regFeeData['status'] == '9')
                    {
                        $thesis['payment'] = '1';
                    }
                }*/
                // тут наверно что-то должно быть
                $persons[] = $person;
            }
	        $stepData['personsList'] = $persons;
//	        echo '<pre>';print_r($stepData['personsList']);echo '</pre>';die;

	       
	        
	    } else {
	        echo 'NO!';die;
	        $stepData = '';
	    }
	    $abstractTypes = array(
	        1 => 'Oral Presentation', 
	        2 => 'Poster Presentation', 
	        3 => 'Proposed Symposium', 	        
	        4 => 'Panel Discussion'
	    );
	    $stepData['abstractTypes'] = $abstractTypes;
	    $this->view->stepData = $stepData;
	}
	
	public function delabsAction ()
	{
	    // смотрим аутентификаци пользователя
	    $auth = Zend_Auth::getInstance();
	    $authData = $auth->getIdentity();
	    
	    // подключаем нужные таблицы
	    $usersTable = new Welt_DbTable_Users();
	    $personsTable = new Welt_DbTable_Persons();
	    $thesisTable = new Welt_DbTable_Thesis();
	    $thesisTextTable = new Welt_DbTable_ThesisText();
	    $thesisAuthorsTable = new Welt_DbTable_ThesisAuthors();
	    $countryTable = new Welt_DbTable_WeltCountries();
	    if($this->_request->isPost() != '' && $this->_request->getParam('thesisId') != '') {
	        $thesisId = $this->_request->getParam('thesisId');
	        $where = $thesisTextTable->getAdapter()->quoteInto('thesis_id=?',$thesisId);
	        $whereAtr = $thesisAuthorsTable->getAdapter()->quoteInto('thesis_id=?',$thesisId);
	        $whereMain = $thesisTextTable->getAdapter()->quoteInto('id=?',$thesisId);
	        $thesisAuthorsTable->delete($whereAtr);
	        $thesisTextTable->delete($where);
	        $thesisTable->delete($whereMain);
	        print_r('well');die;
	    }
	}
	
	
	public function delatrAction()
	{
	    if($this->_request->isPost() != '' && $this->_request->getParam('authorId') != '') {
	        $authorId = $this->_request->getParam('authorId');
	        $thesisAuthorsTable = new Welt_DbTable_ThesisAuthors();
	        $where = $thesisAuthorsTable->getAdapter()->quoteInto('id=?',$authorId);
	        $thesisAuthorsTable->delete($where);
	      /*   if($this->_request->getParam('thesisId') != ''){
	            $thesisId = $this->_request->getParam('thesisId');
	            $edit = $this->editAction($thesisId);
	            return $edit;
	        } */
	        print_r('deleted');die;
	    } else {
	        echo 'something went wrong';die;
	        
	    }
	    
	}
	public function translitAction ($string) {
	    $replace = array(
	        "а"=>"a","А"=>"A",
	        "б"=>"b","Б"=>"B",
	        "в"=>"v","В"=>"V",
	        "г"=>"g","Г"=>"G",
	        "д"=>"d","Д"=>"D",
	        "е"=>"e","Е"=>"E",
	        "ж"=>"zh","Ж"=>"Zh",
	        "з"=>"z","З"=>"Z",
	        "и"=>"i","И"=>"I",
	        "й"=>"y","Й"=>"Y",
	        "к"=>"k","К"=>"K",
	        "л"=>"l","Л"=>"L",
	        "м"=>"m","М"=>"M",
	        "н"=>"n","Н"=>"N",
	        "о"=>"o","О"=>"O",
	        "п"=>"p","П"=>"p",
	        "р"=>"r","Р"=>"R",
	        "с"=>"s","С"=>"S",
	        "т"=>"t","Т"=>"T",
	        "у"=>"u","У"=>"U",
	        "ф"=>"f","Ф"=>"F",
	        "х"=>"h","Х"=>"H",
	        "ц"=>"c","Ц"=>"C",
	        "ч"=>"ch","Ч"=>"Ch",
	        "ш"=>"sh","Ш"=>"Sh",
	        "щ"=>"sch","Щ"=>"Sch",
	        "ъ"=>"","Ъ"=>"",
	        "ы"=>"y","Ы"=>"Y",
	        "ь"=>"","Ь"=>"",
	        "э"=>"e","Э"=>"E",
	        "ю"=>"yu","Ю"=>"Yu",
	        "я"=>"ya","Я"=>"Ya",
	        "і"=>"i","І"=>"I",
	        "ї"=>"yi","Ї"=>"Yi",
	        "є"=>"e","Є"=>"E"
	    );
	    return iconv("UTF-8","UTF-8//IGNORE", strtr($string,$replace));
	}
	
	public function listAction ()
	{
		
	}
	public function delcvAction()
	{
	    if($_POST['filename'] != '')
	    {
	        $filename = $_POST['filename'];
	        $filedir = '/home/mices/ecp2019/data/uploads/';
	        if(file_exists($filedir.$filename)){
	            if(rename($filedir.$filename, $filedir . 'TO_DELETE_' . $filename)){
	                print_r('renamed');die;
	            }
	        }
	    }
	    print_r('wrong');die;
	}
}