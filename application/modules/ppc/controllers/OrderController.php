<?php
class Ppc_OrderController extends Welt_Controller_PpcBaseController
{

    public function init()
    {
    	parent::init();	
    }
	
	/**
	 * Добавления нового заказа с помощью подключения регистрационной формы
	 * @return 
	 */
	public function regFormAction()
	{
		/*
		 * Действие в модуле default, котроллере index, которое будет вызвано с помощью помощника вида action,
		 * в зависимости от переданного номера шага
		*/
		//Пронстранство имён сессий для рег. формы
		$userAppNamespace = new Zend_Session_Namespace('user');
		//Устанавливаем переменные в сессии для отображения информации о заявке в регистрационной форме
		$userAppNamespace->fromPpc = 1;
		$userAppNamespace->step = 4;
		$userAppNamespace->appId = $this->appId;
		$userAppNamespace->userId = $this->userId;
	}
	
	/**
	 * Завершение процесса добавления услуг посредством подключения регистрационной формы
	 * @return 
	 */
	public function finishAction()
	{
		//Удаляем данные из сесиии
		Zend_Session::namespaceUnset('user');
		//Редирект на главную
		$this->_helper->_redirector->gotoSimple('index','index','ppc');
	}
}