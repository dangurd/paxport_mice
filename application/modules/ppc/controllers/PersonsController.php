<?php
class Ppc_PersonsController extends  Welt_Controller_PpcBaseController
{
	protected $personsObj;
	
    public function init()
    {
    	parent::init();
		/*
		 * Получаем список услуг, доступных для заказа.
		 * Если визовая поддержка не доступна для заказа, то редирект на главную.
		 */
		$servicesTypesTable = new Welt_DbTable_ServicesTypes();
		$servicesTypes = $servicesTypesTable->getSysNamesArr();
		if (!in_array('visa',$servicesTypes))
		{
			$this->_helper->redirector->gotoSimple('index','index','ppc');
		}
		$this->personsObj = new Welt_Persons($this->userId);
		$ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->addActionContext('docDelete', 'json')
					->addActionContext('process', 'json')
					->addActionContext('deleteRequest', 'json')
					->initContext();
    }
	
	/**
	 * Вывод списка персон
	 * @return 
	 */
	public function indexAction()
    {
    	//Получаем список персон и передаём во view
    	$personsList = $this->personsObj->getPersonsList();
		$this->view->personsList = $personsList;
		//Получаем список стран для формы добавления/редактирования и передаём во view
		$countriesTable = new Welt_DbTable_WeltCountries();
		$countriesList = $countriesTable->getCountriesList();
		$this->view->countriesList = $countriesList;
		//Получаем список возможных типов персон и передаём во view
		$personsTypesTable = new Welt_DbTable_PersonsTypes();
		$personsTypes = $personsTypesTable->getPersonTypes();
		$this->view->personsTypes = $personsTypes;
		//Передаём во view параметр, отвечающий за то, нужно ли выводить список с выбором типа участия (получаем из настроек)
		$this->view->personTypeCh = $this->eventParams['person_types'];
    }
	
	/**
	 * Добавление или редактирование персоны 
	 * @return 
	 */
	public function processAction()
	{
		//Если были переданы корректные данные
		if($this->_request->isPost() && $this->_request->getParam('person')!='' && $this->_request->getParam('personAction')!='')
		{
			$person = $this->_request->getParam('person');
			$personAction = $this->_request->getParam('personAction');
			$personsValid = new Application_Validator_Persons();
			$status = 1;
			$messages = array();
			$content='';
			//Массив для валидации данных
			$persons = array(0=>$person);
			//Если данные не верные, добавляем сообщения об ошибке и ставим статус 0
			if(!$personsValid->isValid($persons))
			{
				$status = 0;
				$validMessages = $personsValid->getMessages();
				foreach ($validMessages as $message)
				{
					$messages[] = $message;
				}
			}
			else
			{
				//Устанавливаем персону
				$this->personsObj->setPerson($person['id']);
				//Подготавливаем данные для редактирования/добавления персоны
				$personData = $person;
				unset($personData['id']);
				unset($personData['personTypeName']);
				unset($personData['visa']);
				//Удаляем поле с текстовым представлением гражданства
				unset($personData['citizenship']);
				//Добавление персоны
				if($personAction=='add')
				{
					//Добавляем запись
					$personAddRes = $this->personsObj->addPerson($personData);
					//Добавляем в массив с данными id
					$person['id'] = $personAddRes;
				}
				elseif($personAction=='edit')
				{
					//Тип персоны редактировать нельзя
					unset($personData['person_type_id']);
					$this->personsObj->updatePerson($personData);
					$person['docs'] = $this->personsObj->getPersonDocs();
				}
				if ($status==1)
				{
					/*
					 * Создаём объект вида, передаём в него необходимые переменные, рендерим в него шаблон 
					 * со строкой таблицы персон для добавленной/отредактированной персоны
					 */
					$contentView = new Zend_View();
					$curViewPaths = $this->view->getScriptPaths();
					$contentView->setScriptPath($curViewPaths[0]);
					$contentView->person = $person;
					$contentView->personTypeCh = $this->eventParams['person_types'];
					$contentView->lang = $this->view->lang;
					$content = $contentView->render('persons/persons-line.phtml');
				}
			}
			$result = array('status'=>$status,'messages'=>$messages,'content'=>$content);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Загрузка файлов со сканами документов
	 * @return 
	 */
	public function docUploadAction()
	{
		/*
		 * Отключаем рендеригн view, а также шаблон. Результат данного действия отправляется фактически
		 * в виде json, но заголовок ставится, как для обычного текста. На клиенте ответ парсится как json.
		 * Сделано это из-за того, что используемый ajaxuploader не умеет корректно работать с ответом в формате
		 * json.
		*/
		$this->_helper->viewRenderer->setNoRender();
		Zend_Layout::getMvcInstance()->disableLayout();
		if($this->_request->isPost() && $this->_request->getParam('personId')!='')
		{
			$personId = $this->_request->getParam('personId');
			//Ключ в массиве $_FILES, в значении которого содержится информация о загруженном файле
			$fileKey = 'docFile';
			//Создаём валидатор для проверки соответствия загруженного файла
			$docFileValid = new Application_Validator_UploadFile();
			$status = 1;
			$messages = array();
			//Информация о загруженном файле для ответа
			$docInfo = array();
			//Устанавливаем максимально допустимый размер файла из настроек
			$docFileValid->maxFileSize = Zend_Registry::get('settings')->ppc->persons->docs_max_size;
			//Проверка на максимальное количество файлов
			$personDocumentsTable = new Welt_DbTable_PersonsDocuments();
			$maxFilesCount = Zend_Registry::get('settings')->ppc->persons->docs_max_files;
			$curPersonFilesCount = $personDocumentsTable->getPersonFilesCount($personId);
			$translator = Zend_Registry::get('Zend_Translate');
			if ($curPersonFilesCount >= $maxFilesCount)
			{
				$status = 0;
				$messages[] = $translator->translate('Maximum number of files for person is').' '.$maxFilesCount;
			}
			else
			{
				//Если загруженный файл корректный
				if (!$docFileValid->isValid($fileKey))
				{
					$status = 0;
					$validMessages = $docFileValid->getMessages();
					foreach ($validMessages as $message)
					{
						$messages[] = $message;
					}
				}
				//Если файл корректный, добавляем запись в БД и копируем файл в нужную папку
				else
				{
					$this->personsObj->setPerson($personId);
					$uploadRes = $this->personsObj->uploadPersonDoc($fileKey);
					//Если возникли ошибки при загрузке файла
					if($uploadRes === false)
					{
						$status = 0;
						$messages[] = $translator->translate('File upload error');
					}
					//Если файл успешно добавлен
					else
					{
						$docInfo = $uploadRes;
					}
				}
			}
			$result = Zend_Json::encode(array(
				'status'		=>	$status,
				'messages'		=>	$messages,
				'personId'		=>	$personId,
				'docInfo'		=>	$docInfo
			));
			echo $result;
		}
	}
	
	/**
	 * Удаление файлов со сканами документов
	 * @return 
	 */
	public function docDeleteAction()
	{
		if($this->_request->isPost() && $this->_request->getParam('docInfo')!='')
		{
			$docInfo = $this->_request->getParam('docInfo');
			$status = 1;
			$messages = array();
			//Если пользователь пытается удалить чужой файл
			$this->personsObj->setPerson($docInfo['personId']);
			$translator = Zend_Registry::get('Zend_Translate');
			if (!$this->personsObj->checkIsUserPerson())
			{
				$status = 0;
				$messages[] = $translator->translate('You do not have permission to delete this file');
			}
			else
			{
				$res = $this->personsObj->deletePersonDoc($docInfo['docId']);
				//Если возникли ошибки при удалении
				if ($res===false)
				{
					$status = 0;
					$messages[] = $translator->translate('File delete error');
				}
			}
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
	
	/**
	 * Запрос на удаление персоны
	 * (status=8 для персоны)
	 * @return 
	 */
	public function deleteRequestAction()
	{
		if($this->_request->isPost() && $this->_request->getParam('personId')!='')
		{
			$status = 1;
			$messages = array();
			$personId = $this->_request->getParam('personId');
			//Меняем статус для персоны
			$this->personsObj->changeStatusForPerson($personId,7);
			$result = array('status'=>$status,'messages'=>$messages);
			$this->_helper->json($result, array('enableJsonExprFinder' => true));
		}
	}
		
	
}