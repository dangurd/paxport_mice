<?php
class Auth {
	
	const userNamespace = 'ppc';
	const usersTable = 'users';
	
	/**
	 * @var $errMessages Сообщения об ошибках
	 */
	private static $errMessages = array (
		'invalidData'		=> 'Invalid password or email',
		'appNotComplete'	=> 'Your application is in process, not complete or canceled'
	);
	
	private $appObj;
	
	public function __construct()
	{
		$this->appObj = new Welt_Applications();
	}
	
	/**
	 * Авторизация
	 * @param array $userDetails Массив с пользовательскими данными для авторизации
	 */
	public function userAuth($userDetails)
	{
		//Плагин для перехвата с ошибок авторизации
		$messenger = new Zend_Controller_Action_Helper_FlashMessenger();
		$messenger->setNamespace('ErrorMessages');
		 // Получаем соединение с БД из registry
		$db = Zend_Registry::get('mainDb');
		//Создаем адаптер в виде базы данных
	    $authAdapter = new Zend_Auth_Adapter_DbTable($db);
	    $authAdapter->setTableName(self::usersTable)
	 				->setIdentityColumn('email')
	             	->setCredentialColumn('password');
	    //Передаем в адаптер данные пользователя
	    $authAdapter->setIdentity($userDetails['email']);
	    $authAdapter->setCredential($userDetails['password']);
		$select = $authAdapter->getDbSelect();
		$auth = Zend_Auth::getInstance();
		//Устанавливаем пространство имён
		$auth->setStorage(new Zend_Auth_Storage_Session(self::userNamespace));
		//Собственно, процесс аутентификация
	    $resultAuth = $auth->authenticate($authAdapter);
		//Проверяем валидность результата
		if($resultAuth->isValid())
	    {    	
			//В случае успешной авторизации, получаем данные пользователя
			$resultRow = (array) $authAdapter->getResultRowObject();
			//Получаем данные по заявке пользователя и проверяем, завершена ли она
			$this->appObj->setUser($resultRow['id']);
			$userAppData = $this->appObj->getUserAppData();
			//Если включен режим предварительной регистрации
			$settings = Zend_Registry::get('settings');
			if ($settings->reg->type == 1)
			{
				/*
			 	 * Перед тем, как найти заявку для пользователя, удаляем его предыдущую незавершённую заявку, если
				 * она есть, чтобы не нарушалась связь 1 пользователь - 1 заявка
			 	 */
				$this->appObj->deleteUserNotCompletedApps($resultRow['id']);
				/*
				 * Если заявка не найдена или найдена, но не завершена, то создаём новую сессию в пространстве имён рег. формы, 
				 * очищаем данные в сессии в пространсте имён ЛК, возвращаяем соответствующий результат в конроллер
				 */
				if ($userAppData === false || $userAppData['complete']!=1)
				{
					//Добавляем новую заявку в БД 
					$this->appObj->addEmptyApp($resultRow['id']);
					$appTable = new Welt_DbTable_Applications();
					$appId = $appTable->getAdapter()->lastInsertId();
					//Пишем данные по заявкеи юзеру в сесиию в пространстве имён рег. формы
					$userAppNamespace = new Zend_Session_Namespace('user');
					$userAppNamespace->userId = $resultRow['id'];
					$userAppNamespace->appId = $appId;
					//Очищаем данные в сессии в пространсте имён ЛК
					$auth->clearIdentity();
					return 'newApp';
				}
			}
			//Массив для записи в сессию
			$fio = $resultRow['title'].' '.$resultRow['fname'].' '.$resultRow['lname'];
			$sessionData = array (
				'userId'	=> 	$userAppData['user_id'],
				'appId'		=> 	$userAppData['id'],
				'fio'		=>	$fio,
				'email'		=>	$resultRow['email'],
				'company'	=>	$resultRow['company']
			);
			//Если заявка завершена и обработана ТМ, то пишем данные в сессию
			if ($userAppData['complete']==1 && ($userAppData['status']==2 || $userAppData['status']==3))
			{
				$auth->getStorage()->write($sessionData);
				return true;
			}
			else
			{
				//Очищаем данные в сессии
				$auth->clearIdentity();
				$messenger->addMessage(self::$errMessages['appNotComplete']);
			}
	    }
		//Если возникли ошибки при аутентификации
		else 
		{
			switch ($resultAuth->getCode()) 
			{
				case Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND :
					$messenger->addMessage(self::$errMessages['invalidData']);
				break;
							 
				case Zend_Auth_Result::FAILURE_CREDENTIAL_INVALID :
					$messenger->addMessage($errMsg = self::$errMessages['invalidData']);
				break;
			}
		}
		return false;
	}
	
	/**
	 * Выход из ЛК участника
	 * @return 
	 */
	public function logout()
	{
		$auth = Zend_Auth::getInstance();
		$auth->setStorage(new Zend_Auth_Storage_Session('ppc'));
        // Удалить из сессии информацию
        $auth->clearIdentity();
		Zend_Session::namespaceUnset('ppc');
		Zend_Session::namespaceUnset('user');
		Zend_Session::forgetMe();
	}
	
	
	/**
	 * Восстановление пароля (отправка письма)
	 * @param array $userData
	 * @return 
	 */
	public function passwordRemind($userData)
	{
		$usersTable = new Welt_DbTable_Users();
		$userDetails = $usersTable->getDataByEmail($userData['email']);
		$userData = array_merge($userData,$userDetails);
		$mailerObj = new Welt_Mailer();
		$mailerObj->sendPassRemind($userData);
		return true;
	}
}
