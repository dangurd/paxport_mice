<?php class Ppc_Bootstrap extends Zend_Application_Module_Bootstrap {
	
	/**
	 * Подключаем навигацию для модуля ЛК участника
	 * @return 
	 */
	public function _initNavigation()
	{
		//Получаем объект вида
		$appBootstrap = $this->getApplication();
      	$appBootstrap->bootstrap('view');
      	$view = $appBootstrap->getResource('view');
		//Получаем из Zend_Registry конфиг
		$navConfig = Zend_Registry::get('navigation')->ppc;
		//Если объекта навигации нет в кэше, создаём его
		$objectsCache = Zend_Registry::get('cacheManager')->getCache('objects');
		if ($objectsCache->test('ppc_navigation'))
		{
			$container = $objectsCache->load('ppc_navigation');
		}
		else
		{
			$container = new Zend_Navigation($navConfig);
			$objectsCache->save($container,'ppc_navigation');
		}
		/*
		 * Получаем список услуг, доступных для заказа.
		 * Если визовая поддержка не доступна для заказа, то скрываем раздел Persons.
		 */
		$servicesTypesTable = new Welt_DbTable_ServicesTypes();
		$servicesTypes = $servicesTypesTable->getSysNamesArr();
		if (!in_array('visa',$servicesTypes))
		{
			$personsPage = $container->findOneByController('persons');
			$personsPage->setVisible(false);
		}
        $view->navigation($container);
        return $container;
	}
}
