<?

class DB {
	
	public function conn($query) {
	$conn = new mysqli('144.76.201.184','webadmin','Hlb9f2k3Bh','mice_ecp2019');
	$response = $conn->query($query);
	return $response;
	}
	
	public function getUserIdByEmail ($userEmail) {
		if(is_null ($userEmail)) {
			$result['result'] = false;
			$result['error'] = 'No user Email';
			return $result;
		}
		
		$result = mysqli_fetch_assoc($this->conn('SELECT id FROM users WHERE email = "' . $userEmail . '"'));
		return $result;
		
	}
	
	public function checkThesis($userId) {
		if(is_null ($userId)) {
			$result['result'] = false;
			$result['error'] = 'No user ID';
			return $result;
		}
		
		$result = $this->conn('SELECT id FROM thesis WHERE user_id = "' . $userId . '"');
		return $result;
	}
	
	public function getThesisById($thesisId) {
		if(is_null ($thesisId)) {
			$result = 'No thesis ID';
			return $result;
		}
		
		$query = 'SELECT * FROM thesis WHERE id = "' . $thesisId . '"';
		$result = mysqli_fetch_assoc($this->conn($query));
		return $result;
	}
	
	public function getThesisTextById($thesisId) {
		if(is_null ($thesisId)) {
			$result = 'No thesis ID';
			return $result;
		}
		
		$query = 'SELECT * FROM thesis_text WHERE thesis_id = "' . $thesisId . '"';
		$result = mysqli_fetch_assoc($this->conn($query));
		return $result;
	}
	
	public function getThesisAuthorsById($thesisId) {
		if(is_null ($thesisId)) {
			$result = 'No thesis ID';
			return $result;
		}
		
		$query = 'SELECT * FROM thesis_authors WHERE thesis_id = "' . $thesisId . '"';
		$result = $this->conn($query);
		foreach ($result as $author) {
			$results[] = $author;
		}
		return $results;
	}
	
	public function addThesis ($userId, $status, $isReady) {
		$date = date('Y-m-d h:i:s');
		$q = 'INSERT INTO thesis (user_id, created, is_applied, is_ready) VALUES ("' . $userId . '", "' . $date . '", "' . $status . '", "' . $isReady . '")';
		$insert = $this->conn($q);
		$result = mysqli_fetch_assoc($this->conn('SELECT id FROM thesis WHERE user_id = "'.$userId.'"'));
		return $result;
	}
	
	public function addThesisInfo ($thesisId, $themeId, $name, $text) {
		//$text = $this->conn->real_escape_string($text);
		$q = 'INSERT INTO thesis_text (thesis_id, theme_id, name, text) VALUES ("'.$thesisId.'","'.$themeId.'","'.$name.'","'.$text.'")';
		$insert = $this->conn($q);
		//return $text;
	}
	
	public function addThesisAuthor ($thesisId, $fname, $sname, $lname, $workspace, $index, $country, $city,  $is_main) {
		
		$q = 'INSERT INTO thesis_authors (thesis_id, fname, sname, lname, workspace, w_index, country, city, is_main) VALUES ("'.$thesisId.'","'.$fname.'","'.$sname.'","'.$lname.'","'.$workspace.'","'.$index.'","'.$country.'","'.$city.'","'.$is_main.'")';
		$insert = $this->conn($q);
		return $q;
	}
	
	public function getThemes () {
		$q = 'SELECT * FROM thesis_theme WHERE is_active = 1';
		$result = $this->conn($q);
		foreach ($result as $theme) {
			$results[] = $theme;
		}
		return $result;
		
	}
	
	public function updateThesisText ($thesisId, $thesisThemeId, $thesisName, $thesisText) {
		$q = 'UPDATE thesis_text SET theme_id = "' . $thesisThemeId . '", name = "' . $thesisName . '", text = "' . $thesisText . '" WHERE thesis_id = "' . $thesisId . '"';
		$update = $this->conn($q);
		
		
	}
	
	public function updateThesisAuthor ($authorId, $fname, $sname, $lname, $workspace, $index, $country, $city, $is_main) {
		$q = 'UPDATE thesis_author SET lname = "' . $lname . '", sname = "' . $sname . '", fname = "' . $fname . '", workspace = "' . $workshop . '", w_index = "'.$index.'", country = "'.$country.'", city = "'.$city.'", is_main = "' . $is_main . '" WHERE id = "' . $authorId . '"';
		$update = $this->conn($q);
	}
	public function updateThesis ($thesisId, $isReady, $isApplied) {
		$q = 'UPDATE thesis SET is_ready = "'.$isReady.'", is_applied = "'.$isApplied.'" WHERE id = "'.$thesisId.'"';
		
		$update = $this->conn($q);
	}
	public function deleteAuthor ($authorId) {
		$q = 'DELETE * FROM thesis_authors WHERE id = "'.$authorId.'"';
		$delete = $this->conn($q);
	}
}

?>