<?

class DB {
	
	public function conn($query) {
	$conn = new mysqli('144.76.201.184','webadmin','Hlb9f2k3Bh','mice_ecp2019');
	
	$response = $conn->query($query);
	return $response;
	}
	
	public function getUsersIds () {
		
		$q = 'SELECT id, email, fname, lname, company, phone FROM users';
		$conn = new mysqli('144.76.201.184','webadmin','Hlb9f2k3Bh','mice_mobmed2018');
		$conn->set_charset('utf8');
		$result = $conn->query($q);
		foreach ($result as $author) {
			$results[] = $author;
		}
		return $results;
	}
	
	public function getThesisById ($thesisId) {
		$q = 'SELECT * FROM thesis WHERE id = "' . $thesisId . '"';
		$result = mysqli_fetch_assoc($this->conn($q));
		return $result;
	}
	
	public function getUserById ($userId) {
		$q = 'SELECT id, email, fname, lname, company, phone FROM users WHERE id = "' . $userId . '"';

		$result = mysqli_fetch_assoc($this->conn($q));
		return $result;
	}
	public function getThesisIdByUserId($userId) {
		if(is_null ($userId)) {
			$result = 'No user ID';
			return $result;
		}
		
		$query = 'SELECT * FROM thesis WHERE user_id = "' . $userId . '"';
		$result = mysqli_fetch_assoc($this->conn($query));
		return $result;
	}
	
	public function getThesisTextById($thesisId) {
		if(is_null ($thesisId)) {
			$result = 'No thesis ID on text search request';
			return $result;
		}
		
		$query = 'SELECT * FROM thesis_text WHERE thesis_id = "' . $thesisId . '"';
		$result = mysqli_fetch_assoc($this->conn($query));
		return $result;
	}
	
	public function getThesisAuthorsById($thesisId) {
		if(is_null ($thesisId)) {
			$result = 'No thesis ID';
			return $result;
		}
		
		$query = 'SELECT * FROM thesis_authors WHERE thesis_id = "' . $thesisId . '"';
		$result = $this->conn($query);
		foreach ($result as $author) {
			$results[] = $author;
		}
		return $results;
	}
	
	public function getThemeById ($themeId) {
		$q = 'SELECT * FROM thesis_theme WHERE id = "' . $themeId . '" AND is_active = 1';
		$result = mysqli_fetch_assoc($this->conn($q));
		return $result;
		
	}
	
	public function getThemes () {
		$q = 'SELECT * FROM thesis_theme WHERE is_active = 1';
		$result = $this->conn($q);
		foreach ($result as $theme) {
			$results[] = $theme;
		}
		return $result;
		
	}
	public function getRecensorList () {
		$q = 'SELECT * FROM recensors';
		$results = $this->conn($q);
		foreach ($results as $recenosor) {
			$result[] = $recenosor;
		}
		return $result;
	}
	public function getRecensorById ($recensorId) {
		
		$q = 'SELECT * FROM recensors WHERE id = "'.$recensorId.'"';
		$result = $this->conn($q);
		foreach ($result as $recenosor) {
			$results[] = $recenosor;
		}
		return $results;
	}
	
	public function updateThesisText ($thesisId, $thesisThemeId, $thesisName, $thesisText) {
		$q = 'UPDATE thesis_text SET theme_id = "' . $thesisThemeId . '", name = "' . $thesisName . '", text = "' . $thesisText . '" WHERE thesis_id = "' . $thesisId . '"';
		$update = $this->conn($q);
		
	}
	
	public function updateThesisAuthor ($authorId, $fname, $sname, $lname, $workspace) {
		$q = 'UPDATE thesis_authors SET fname = "' . $fname . '", lname = "' . $lname . '", sname = "' . $sname . '",  workspace = "' . $workspace . '" WHERE id = "' . $authorId . '"';
		$update = $this->conn($q);
	}
	
	public function updateThesisInfo ($thesisId, $status, $publish, $paid, $verbal, $verbalDateTime, $comment, $recensorId){
		$q = 'UPDATE thesis SET is_applied = "' . $status . '", is_publishied = "' . $publish . '", is_paid = "' . $paid . '", is_verbal = "' . $verbal . '", verbal_date = "' . $verbalDateTime . '", comment = "' . $comment . '", recensor_id = "'.$recensorId.'" WHERE id = "' . $thesisId .'"';
		$update = $this->conn($q);
		return $q;
	}
	
	public function newThesisTheme ($themeName) {
		$q = 'INSERT INTO thesis_theme (thesis_theme, is_active) VALUES ("'.$themeName.'","1")';
		$insert = $this->conn($q);
	}
	public function newHUI ($name, $email) {
		$q = 'INSERT INTO recensors (name, email) VALUES ("'.$name.'", "'.$email.'")';
		$insert = $this->conn($q);
		
	}
}