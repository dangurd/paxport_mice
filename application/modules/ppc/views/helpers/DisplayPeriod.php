<?php
/**
 * Отображение периода тестом
 */
class Zend_View_Helper_DisplayPeriod extends Zend_View_Helper_Abstract
{
	public function displayPeriod($sDateStr, $eDateStr, $format = 'dd.MM.yyyy')
	{
		$periodStr = '';
		$sDate = new Zend_Date($sDateStr, $format);
		$sDateMonth = $sDate->get(Zend_Date::MONTH_NAME);
		$eDate = new Zend_Date($eDateStr, $format);
		$eDateMonth = $eDate->get(Zend_Date::MONTH);
		// Для русского языка формируем родительский падеж для месяца
		$lang  = Zend_Registry::get('lang');
		if ($lang == 'ru')
		{
			$sDateMonth = Zend_Locale_Data::getContent('ru_RU', 'month', array('gregorian', 'format', 'wide', $sDate->get(Zend_Date::MONTH_SHORT)));
			$eDateMonth = Zend_Locale_Data::getContent('ru_RU', 'month', array('gregorian', 'format', 'wide', $eDate->get(Zend_Date::MONTH_SHORT)));
		}
		// Если даты совпадают
		if ($sDateStr == $eDateStr)
		{
			$periodStr .= $sDate->get(Zend_Date::DAY) . ' ' . $sDateMonth;
		}
		else
		{
			// Если месяца совпадают
			if ($sDate->get(Zend_Date::MONTH) == $eDate->get(Zend_Date::MONTH))
			{
				$periodStr .= $sDate->get(Zend_Date::DAY) . ' - ' . $eDate->get(Zend_Date::DAY) . ' ' . $sDateMonth;
			}
			// Если месяца не совпадают
			else
			{
				$periodStr .= $sDate->get(Zend_Date::DAY) . ' ' . $sDateMonth . ' - ' . $eDate->get(Zend_Date::DAY) . ' ' . $eDateMonth;
			}
		}
		$periodStr .= ' ' . $sDate->toString('yyyy');
		return $periodStr;
	}
}
