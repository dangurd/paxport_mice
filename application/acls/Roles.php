<?php
class Application_Acl_Roles extends Zend_Acl
/**
 * Определение прав доступа пользователей к ресурсам
*/
{
	public function __construct()
	{
		$roleGuest = new Zend_acl_Role('guest');
 
		/* Категории пользователей */
		$this->addRole(new Zend_Acl_Role('guest'))
		     ->addRole(new Zend_Acl_Role('participant'), 'guest')
			 ->addRole(new Zend_Acl_Role('organizer'), 'participant');
			 
		/* Модуль регистрация */
		$this->add(new Zend_Acl_Resource('default'))
			 ->add(new Zend_Acl_Resource('default:index'),'default')
			 ->add(new Zend_Acl_Resource('default:process'),'default')
			 ->add(new Zend_Acl_Resource('default:kcaptcha'),'default');
			 
		/* Модуль ЛК участника */
		$this->add(new Zend_Acl_Resource('ppc'))
			 ->add(new Zend_Acl_Resource('ppc:index'),'ppc')
			 ->add(new Zend_Acl_Resource('ppc:order'),'ppc')
			 ->add(new Zend_Acl_Resource('ppc:docs'),'ppc')
			 ->add(new Zend_Acl_Resource('ppc:persons'),'ppc')
			 ->add(new Zend_Acl_Resource('ppc:services'),'ppc')
			 ->add(new Zend_Acl_Resource('ppc:payment'),'ppc')
			 ->add(new Zend_Acl_Resource('ppc:user'),'ppc');
			   
		
		/* Определяем права доступа с учетом порядка наследования ролей */
		$this->deny('guest','default',null)
			 ->allow('guest','default:index',array('result','deadline'))
			 ->allow('guest','default:kcaptcha',null)
			 ->allow('participant','default',null);
		
		$this->deny('guest','ppc',null)
			 ->allow('guest','ppc:user',array('login','remind','remind.success'))
			 ->allow('participant','ppc',null)
			 ->deny('participant','ppc:user',array('login','remind'));
	}

}