<?php
/**
* Класс начальной загрузки приложения при запуска скрипта через cron
*/	
class BootstrapCron extends Zend_Application_Bootstrap_Bootstrap
{
	/**
	* Установка конфигурации приложения, а также загрузка пользовательских настроек
	*/	
	 protected function _initConfig()
     {
     	$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application_cron.ini',APPLICATION_ENV);
		$user_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/settings.ini');
		$this->_config = $config;
		/**
		 * Добавляем объекты конфигов в реестр
		 */
		Zend_Registry::set('config', $config);
		Zend_Registry::set('settings', $user_config->production);
     } 
	 
	 /**
	 * Запись в реестр настроек Db
	 */	
	 protected function _initDbs()
     {
     	 $this->bootstrap('multidb');
         $resource = $this->getPluginResource('multidb');
		 $db1 = $resource->getDb('mainDb');
		 $db2 = $resource->getDb('welt');
		 Zend_Registry::set('mainDb',$db1);
		 Zend_Registry::set('welt',$db2);
	 }
	
	/**
	* Настройка автозагрузки ресурсов
	*/
	protected function _initAutoload()
	{
		require_once 'Zend/Loader/Autoloader.php';
		$autoloader=Zend_Loader_Autoloader::getInstance();
		$autoloader->setFallbackAutoloader(true);
		$resourceLoader = new Zend_Loader_Autoloader_Resource(array(
		          'basePath'  => APPLICATION_PATH,
		          'namespace' => $this->_config->appnamespace,
		));
	}
	
	
}