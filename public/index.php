<?php
// Указание пути к директории приложения
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Определение текущего режима работы приложения
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

#
// Обычно требуется также добавить директорию library/
#
// в include_path, особенно если она содержит инсталляцию ZF
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

//Определяем нужный конфиг в зависимости от способа вызова скрипта
if (defined('_CRON_') && _CRON_ === true)
{
	$configPath = APPLICATION_PATH . '/configs/application_cron.ini';
}
else
{
	$configPath = APPLICATION_PATH . '/configs/application.ini';
}

// Создание объекта приложения, начальная загрузка, запуск
$application = new Zend_Application(
    APPLICATION_ENV,
    $configPath
);
$application->bootstrap();
//Если скрипт был вызван по cron, запуск приложения не нужен
if (!defined('_CRON_') || _CRON_ !== true)
{
	$application->run();
}
