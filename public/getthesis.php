<?
include_once 'dbTable.php';

$db = new DB();

$themes = $db->getThemes();
?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=7">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
<title>Личный кабинет</title><link href="/css/reset.css" media="screen" rel="stylesheet" type="text/css">
<link href="/css/jquery-ui.css" media="screen" rel="stylesheet" type="text/css">
<link href="/css/main.css" media="screen" rel="stylesheet" type="text/css">
<link href="/css/ppc.css" media="screen" rel="stylesheet" type="text/css">
<link href="/css/form_style.css" media="screen" rel="stylesheet" type="text/css">
<link href="/css/buttons_style.css" media="screen" rel="stylesheet" type="text/css">
<!--[if IE 6]> <link href="/css/ie6.css" media="screen" rel="stylesheet" type="text/css" /><![endif]--><script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.bgiframe-2.1.1.js"></script>
<script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="/js/downloaddoc/FileSaver.js"></script>
<script type="text/javascript" src="/js/downloaddoc/jquery.wordexport.js"></script>
<script type="text/javascript" src="/js/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript" src="/js/cufon-yui.js"></script><style type="text/css">cufon{text-indent:0!important;}@media screen,projection{cufon{display:inline!important;display:inline-block!important;position:relative!important;vertical-align:middle!important;font-size:1px!important;line-height:1px!important;}cufon cufontext{display:-moz-inline-box!important;display:inline-block!important;width:0!important;height:0!important;overflow:hidden!important;text-indent:-10000in!important;}cufon canvas{position:relative!important;}}@media print{cufon{padding:0!important;}cufon canvas{display:none!important;}}</style>
<script type="text/javascript" src="/js/Myriad_Pro_400.font.js"></script>
<script type="text/javascript" src="/js/ajaxupload.js"></script>
<script type="text/javascript" src="/js/functions.js"></script>
<script type="text/javascript" src="/js/lang/ru.js"></script>
<script type="text/javascript" src="/js/ppc.js"></script>
<!--[if IE 6]> <script type="text/javascript" src="/js/jquery.ifixpng.js"></script><![endif]-->
<!--[if IE 6]> <script type="text/javascript" src="/js/ie6.js"></script><![endif]--><link rel="stylesheet" type="text/css" id="u0" href="http://mobmed.forms.paxport.tech/js/tinymce/skins/lightgray/skin.min.css"></head>
<body>
<?
if ( !empty($_GET['thesisId']) ) {
	$thesisId = $db->getThesisById ($_GET['thesisId']);
	if(!empty($thesisId) && $thesisId['is_ready'] >= '1'){
		$user = $db->getUserById ($thesisId['user_id']);
		$thesis['id'] = $thesisId['id'];
		$thesis['user_id'] = $user['id'];
		$thesis['contact_fio'] = $user['fname']. ' ' .$user['lname'];
		$thesis['contact_email'] = $user['email'];
		$thesis['contact_phone'] = $user['phone'];
		$thesis['contact_company'] = $user['company'];
		$thesis['applied_status'] = $thesisId['is_applied'];
		$thesis['verbal_status'] = $thesisId['is_verbal'];
		$thesis['verbal_date_time'] = $thesisId['verbal_date'];
		if ( !empty($thesis['verbal_date_time']) ) {
			$thesisDateTime = explode(' ', $thesisId['verbal_date']);
			$thesis['verbal_date'] = $thesisDateTime[0];
			$thesis['verbal_time'] = $thesisDateTime[1];
		}
		$thesis['comment'] = $thesisId['comment'];
		$thesis['publishied_status'] = $thesisId['is_publishied'];
		$thesis['paid_status'] = $thesisId['is_paid'];
		$thesis['authors'] = $db->getThesisAuthorsById ($thesisId['id']);
		$thesis['authors'] = $thesis['authors'];
		$thesisText = $db->getThesisTextById ($thesisId['id']);
		$thesis['name'] = $thesisText['name'];
		$thesis['text'] = $thesisText['text'];
		$thesisTheme = $db->getThemeById($thesisText['theme_id']);
		$thesis['theme'] = $thesisTheme['thesis_theme'];
		$thesis['result'] = true;
		} else {
			$thesis['result'] = false;
		}
}?>
		
<span><a href="#" onclick="downloadDoc(<?=$thesis['id']?>)" style="line-height: 40px;background-color: limegreen;padding: 7px; font-size: 18px;text-decoration: none;color: #fff; border-radius: 5px;top: 10px;box-shadow: 2px 2px 0 0 rgba(0,0,0,0.3);">Скачать файл тезиса в формате *.doc </a></span>
<div id="thesisView<?=$thesis['id']?>" style="width:900px; margin: 0 auto; border:1px solid #000; padding:30px;">
<p align="center" style="font-size: 16px;font-weight: bold;">Тема: "<?=$thesis['theme']?>"</p>
												<p align="center" style="font-size: 18px;font-weight: bold;"><?=$thesis['name']?><br /></p>
												<p align="center">
												<?
												for ($k = 0; $k <= count($thesis['authors'])-1; $k++) {
													if (!empty($thesis['authors'][$k+1]) && ($thesis['authors'][$k]['workspace'] != $thesis['authors'][$k+1]['workspace'])) {
														$similiarWorkspace = '0';
														$k = 5;
													} else {
														$similiarWorkspace = '1';
													}
												}
												if ( $similiarWorkspace == '1') {
													if ( count($thesis['authors'] != 1) ) {
														foreach ($thesis['authors'] as $author) {
															
															echo $author['fname']. ' ' .substr($author['sname'], 0, 2). '. ' .substr($author['lname'], 0, 2).'., ';
														}
													} else {
														echo $thesis['authors']['fname']. ' ' .substr($thesis['authors']['sname'], 0, 2). '. ' .substr($thesis['authors']['lname'], 0, 2).'.';
													}
													echo '<br />'.$thesis['authors'][1]['workspace'].', '.$thesis['authors'][1]['w_index'];
												} else {
													foreach ($thesis['authors'] as $author) {
														echo $author['fname']. ' ' .substr($author['sname'], 0, 2). '. ' .substr($author['lname'], 0, 2).'., '.$author['workspace'].', '.$author['index'].'<br />';
													}
												}
												?>
												</p>
												<p><br/></p>
												<p><?=$thesis['text']?></p>
												<p><br/></p>
												<p>
													<?
														echo '<span>' . $thesis['contact_fio'] . ' </span><span> (' . $thesis['contact_phone'] . ', ' . $thesis['contact_email'] . ')</span>';
														
													?>
												</p>
												<p><br/></p>
												
												
												
											</div>
											<script>
		$(document).ready( 	
			function downloadDoc (id) {
				//$("#thesisView"+id).wordExport();
				var a = $('#thesisView'+id);
				a.hide();
				console.log(a);
		});
		
		function downloadDoc (id) {
				$("#thesisView"+id).wordExport();
		}
		</script>
<?

?>
</body>
</html>