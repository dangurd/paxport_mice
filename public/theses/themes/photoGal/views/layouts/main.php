<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<?php /**CB-5.1**/ ?>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->getBaseUrl(); ?>/css/bootstrap.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->getBaseUrl(); ?>/css/screen.css" media="screen, projection" />
	<?php /**CB-6.1**/ ?>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->getBaseUrl(); ?>/css/form.css" media="screen, projection" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::t('term',Yii::app()->name)); ?></div>
		<?php /**CB-6.2**/ ?>
		<div id="login">
		    <?php
            $lang = (Yii::app()->language=="en")?"ru":"en";
			if (Yii::app()->user->isGuest) {
				$userMenu=array(
                    array('label'=>$lang,'url'=>array("",'lang'=>$lang)),
					array('label'=>Yii::t('term','Login'), 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest)
					);
			} else {
			    $userMenu=array(
                array('label'=>$lang,'url'=>array("",'lang'=>$lang)),
				array('label'=>Yii::t('term','Welcome').' '.Yii::app()->user->name, 'url'=>array('/profile/index')),
				array('label'=>Yii::t('term','Logout'), 'url'=>array('/site/logout'))
				);
			}
			$this->widget('zii.widgets.CMenu',array(
			    'items'=>$userMenu,
			    'htmlOptions'=>array('class'=>'userMenu')
			));
			?>
		    
		</div>
		  <?php ?>
		 
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				//array('label'=>'Home', 'url'=>array( '/site/index')),
				array('label'=>Yii::t('term','Review'), 'url'=>array( '/review/index')),
				array('label'=>Yii::t('term','Profile'), 'url'=>array( '/profile/index')),
				//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('/site/contact')),
				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); 
		?>
		
	</div><!-- mainmenu -->
	
	<div class="search">
	    <?php
		echo CHtml::form('/album/search', 'GET');
		    echo CHtml::submitButton('go',array('class'=>'btn btn-small btn-success right small'));
		    echo CHtml::textField('tag','',array('class'=>'right small'));
		echo CHtml::endForm();
		?>
	</div>
	<div class="clear"></div>
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
        Copyright &copy; <?php echo date('Y'); ?> Reisebuero WELT <br/>
        All Rights Reserved.<br/>
	</div><!-- footer -->

</div><!-- page -->

<script src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/js/bootstrap.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->theme->getBaseUrl(); ?>/js/script.js" type="text/javascript"></script>
</body>
</html>
