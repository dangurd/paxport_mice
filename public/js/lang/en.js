/**
 * Язык системы
 */
var sysLang = 'en';
/**
 * Сообщения об ошибках
 */
var messages={};
//Регистрация
messages['reg'] = {};
messages['reg']['all'] = {
	'deleteError' 			:	'Service delete error',
	'hotelMissParams'		:	'Please specify accommodation parameters and tick names of persons on the list',
	'transportMissParams'	:	'Please specify transport parameters and tick names of persons on the list',
	'mealMissParams'		:	'Please specify transport parameters and tick names of persons on the list',
	'excursionMissParams'	:	'Please specify excursion parameters and tick names of persons on the list',
	'persNotSel' 			:	'Please choose persons',
	'persNotAdded'			:	'Please add persons',
	'personDelConf'			:	'Are you sure want to delete person? All ordered for this person services will be deleted!',
	'serviceDelConf'		:	'Are you sure want to delete service order?',
	'pdn'					:	'Please read and accept the "Consent to Personal Data Processing"'
};
//ЛК участника
messages['ppc'] = {};
messages['ppc']['all'] = {
	'upload_ext'		: 	'Error: invalid file extension. Allowed extensions: jpg,jpeg,gif,bmp,pdf,png,tiff.',
	'personDelConf'		:	'Are you sure want to delete person? All ordered for this person services will be deleted!'
};
messages['ppc']['persons'] = {
	'addPersonDialogTitle'	:	'Add person',
	'editPersonDialogTitle'	:	'Add/edit personal info'
};
messages['ppc']['services'] = {
	'serviceCancelConf'		:	'Are you sure want to cancel service?',
	'persNotSel'			:	'Please choose persons'
};
messages['ppc']['payment'] = {
	'servicesNotSel'	:	'Please select services'
};
