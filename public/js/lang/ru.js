/**
 * Язык системы
 */
var sysLang = 'ru';
/**
 * Язык календаря
 */
/* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Andrew Stromnov (stromnov@gmail.com). */
jQuery(function($){
	$.datepicker.regional['ru'] = {
		closeText: 'Закрыть',
		prevText: '&#x3c;Пред',
		nextText: 'След&#x3e;',
		currentText: 'Сегодня',
		monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
		'Июл','Авг','Сен','Окт','Ноя','Дек'],
		dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
		dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
		dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
		weekHeader: 'Не',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['ru']);
});
$.datepicker.setDefaults(
	$.extend($.datepicker.regional["ru"])
);
	

/**
 * Сообщения об ошибках
 */
var messages={};
//Регистрация
messages['reg'] = {};
messages['reg']['all'] = {
	'deleteError' 			:	'Ошибка удаления услуги',
	'hotelMissParams'		:	'Пожалуйста, введите данные второго участника',
	'transportMissParams'	:	'Пожалуйста, заполните необходимые поля и выберите гостей',
	'mealMissParams'		:	'Пожалуйста, заполните необходимые поля и выберите гостей',
	'excursionMissParams'	:	'Пожалуйста, заполните необходимые поля и выберите гостей',
	'persNotSel' 			:	'Пожалуйста, выберите гостей',
	'persNotAdded'			:	'Не добавлено ни одного гостя!',
	'personDelConf'			:	'Вы действительно хотите удалить данного гостя? Все заказанные на него услуги будут удалены!',
	'personEditConf'		:	'Вы действительно хотите изменить данные гостя? Все заказанные на него услуги будут останутся',
	'serviceDelConf'		:	'Вы действительно хотите удалить услугу?',
	'pdn'					:	'Вы должны принять "Соглашение на обработку персональных данных" для продолжения регистрации'
};
//ЛК участника
messages['ppc'] = {};
messages['ppc']['all'] = {
	'upload_ext'		: 	'Ошибка: недопустимое расширение файла. Разрешённые расширения: jpg,jpeg,gif,bmp,pdf,png,tiff.',
	'personDelConf'		:	'Вы действительно хотите удалить данного гостя? Все заказанные на него услуги будут удалены!'
};
messages['ppc']['persons'] = {
	'addPersonDialogTitle'	:	'Добавление гостя',
	'editPersonDialogTitle'	:	'Добавление/редактирование данных по гостю'
};
messages['ppc']['services'] = {
	'serviceCancelConf'		:	'Вы действительно хотите аннулировать услугу?',
	'persNotSel'			:	'Пожалуйста, выберите гостей'
};
messages['ppc']['payment'] = {
	'servicesNotSel'	:	'Пожалуйста, выберите услуги'
};
