/* ----------------------------------| Персоны |----------------------------------------------------*/
/**
 * Класс для работы с персонами
 */
with(Persons=function(){})
{
	/**
	 * Отображение формы для редактирования или добавления персоны
	 * @param string action Действие - add или edit
	 * @param int personId [optional] Id персоны для редактирования
	 */
	prototype.showActionForm = function(action,personId)
	{
		//Если передано неверное действие
		if (action!='edit' && action!='add')
		{
			return false;
		}
	    prototype.action = action;
		if (!personId)
		{
			prototype.personId = personId;
		}
		//Приводим форму к начальному состоянию
		this.resetActionForm();
		//Форма для добавления
		if (action=='add')
		{
			$('#persons-form h2').text(messages['ppc']['persons']['addPersonDialogTitle']);
			$('#persons-form-links .form-add-link').removeClass('hidden-imp');
			//Показываем поле для выбора типа участника
			$('#person-type-line').removeClass('hidden');
		}
		//Форма для редактирования
		if (action=='edit')
		{
			$('#persons-form h2').text(messages['ppc']['persons']['editPersonDialogTitle']);
			$('#persons-form-links .form-save-link').removeClass('hidden-imp');
			//Получаем данные для заполнения формы
			var personRow = $('#person-list-line'+personId);
			data = {
				'id'				:	personId,
				'fname' 			: 	$(personRow).find('td.persons-list-fname').text(),
				'lname' 			: 	$(personRow).find('td.persons-list-lname').text(),
				'birthday' 			: 	trim($(personRow).find('td.persons-list-birthday').text()),
				'citizenship' 		: 	$(personRow).find('td.persons-list-citizenship .citizenship-name').text(),
				'citizenship_id' 	: 	$(personRow).find('td.persons-list-citizenship .citizenship-id').text(),
				'passport' 			: 	$(personRow).find('td.persons-list-passport').text(),
				'passport_exp_date'	:	trim($(personRow).find('td.persons-list-pasp-exp-date').text())
			};
			//Заполняем форму
			this.fillForm(data);
		}
		//Показываем модальное окно
		var modalElem = $('#persons-form-container');
		loadPopup(modalElem);
	}
	
	/**
	 * Возращение формы добавления/редактирования персоны к исходному состоянию
 	*/
	prototype.resetActionForm = function()
	{
		clearForm('#persons-form');
		$('#persons-form-links .form-add-link').addClass('hidden-imp');
		$('#persons-form-links .form-save-link').addClass('hidden-imp');
		$('#person-type-line').addClass('hidden');
	}
	
	/**
	 * Заполнение добавления/редактирования персоны формы переданными данные
	 * @param {Object} data
	 */
	prototype.fillForm = function(data)
	{
		$('#person-fname').val(data.fname);
		$('#person-lname').val(data.lname);
		$('#person-birthday').val(data.birthday);
		$('#person-passport').val(data.passport);
		$('#person-id').val(data.id);
		if (data.citizenship_id!='' && data.citizenship_id!='null') 
		{
			$('#person-citizenship option[value="' + data.citizenship_id + '"]').attr('selected', 'selected');
		}
		else
		{
			$('#person-citizenship option:first').attr('selected', 'selected');
		}
		$('#person-passport-exp-date').val(data.passport_exp_date);
	}
	
	/**
	 * Обновление полей, связанных с выбором гражданства
	 */
	prototype.citizUpdate = function(data)
	{
		var selectedElem = $('#person-citizenship option:selected').val();
		if (selectedElem=='not')
		{
			$('#person-citizenship-spec-line').removeClass('hidden');
		}
		else
		{
			$('#person-citizenship-spec-line').addClass('hidden');
		}
	}
	
	/**
	 * Обработка данных по персоне
	 * @param {Object} action
	 */
	prototype.processPerson = function()
	{	
		prototype.person = {};
		//Формируем данные для передачи на сервер
		this.person = {
			'id'				:	$('#person-id').val(),
			'fname'				:	$('#person-fname').val(),
			'lname'				:	$('#person-lname').val(),
			'person_type_id'	:	$('#person-type option:selected').val(),
			'birthday'			:	$('#person-birthday').val(),
			'citizenship'		:	$('#person-citizenship option:selected').text(),
			'citizenship_id'	:	$('#person-citizenship option:selected').val(),
			'passport_number'	:	$('#person-passport').val(),
			'passport_exp_date'	:	$('#person-passport-exp-date').val()
		}
		//Если гражданство не выбрано, передаём выбранное ранее гражданство
		if (this.person['citizenship_id'] == '')
		{
			this.person['citizenship'] = $('#person-list-line'+this.person['id']).find('td.persons-list-citizenship .citizenship-name').text();
			this.person['citizenship_id'] = $('#person-list-line'+this.person['id']).find('td.persons-list-citizenship .citizenship-id').text();
		}
		/* 
		 * Определяем, откуда брать тип участника. Для режима редактирования - из таблицы, для режима добавления - 
		 * выбранный элемент списка в форме
		 */
		if (this.action=='add')
		{
			this.person['personTypeName'] = $('#person-type option:selected').text();
		}
		if (this.action=='edit')
		{
			this.person['personTypeName'] = trim($('#person-list-line'+this.person['id']).find('td.persons-list-pers-type').text());
			//Флаг, определяющий, заказана ли визовая поддержка
			this.person['visa'] = $('#person-list-line'+this.person['id']).find('span.person-visa-flag').text();
		}
		//Сохраняем данные по персоне
		this.personSave();
	}
	
	/**
	 * Передача данных по персоне на сервер для проверки и сохранения в БД
	 */
	prototype.personSave = function()
	{
		var curObject = this;
		//Данные для отправки на сервер
		var person = {};
		person['person'] = {};
		person['person'] = this.person;
		person['personAction'] = this.action;
		//Отправляем данные на сервер
		$.ajax({
			type: 'POST',
			url: '/ppc/persons/process/format/json/',
			dataType: 'json',
			data: person,
			beforeSend: function(){
				$('#persons-loader').show();
			},
			success: function(result) 
			{
				var status = parseInt(result.status);
				//Если в результате ошибка, то выводим её
				if (status == 0) 
				{
					alert(result.messages);
				}
				//Если ответ успешный
				else
				{
					//Если идёт добавление персоны, то добавляем строку в таблицу
					if (person['personAction']=='add')
					{
						$('#persons-list-table tbody').append(result.content);
						var curPersonRow = $('#persons-list-table tr:last');
					}
					//Если идёт редактирование персоны, то заменяем строку
					if (person['personAction']=='edit')
					{
						$('#person-list-line'+person['person']['id']).replaceWith(result.content);
						var curPersonRow = $('#person-list-line'+person['person']['id']);
					}
					//Назначаем события для добавленной строки
					curObject.setEvents(curPersonRow);
					//Скрываем модальное окно
					var modalElem = $('#persons-form-container');
					disablePopup(modalElem);
				}
			},
			complete: function(){
				$('#persons-loader').hide();
			}
	 	});
	}
	/**
	 * Создание поля для upload'а сканов документов
	 * @param {Object} elementId
	 */
	prototype.createDocsUploader = function(elementId,personId)
	{
		//Блоки для прелоадера
		var loaderBlock = '#persons-docs-loader'+personId;
		var loaderAreaBlock = '#uploader-link-block-inner'+personId;
		var curObject = this;
		//Создаём объект uploader'а
		var uploader = new AjaxUpload(elementId, {
			name: 'docFile',
	   		action: '/ppc/persons/doc.upload/',
			data: {
				'personId': personId
			},
			onComplete: function(file, response) 
			{
				ajaxLoaderContainer('hide',loaderBlock,loaderAreaBlock);
			  	responseObj = jQuery.parseJSON(response);
				//Если результат загрузки успешный
				if(responseObj.status == 1)
				{
					//Добавляем запись в список документов для данной персоны
					curObject.appendUploadedDoc(responseObj.docInfo,personId);
				}
				//Если возникла ошибка
				else
				{
					alert(responseObj.messages);
				}
			},
			onSubmit : function(file,ext)
			{
				//Контроль расширения
		        if (! (ext && /^(jpg|jpeg|gif|bmp|pdf|png|tiff)$/i.test(ext)))
				{
		            alert(messages['ppc']['all']['upload_ext']);
		            return false;
		        }
				ajaxLoaderContainer('show',loaderBlock,loaderAreaBlock);
		    }
		}); 
	}
	/**
	 * Добавление записи о загруженном документе в списое документов для конкретной персоны
	 * @param {Object} doc
	 * @param int personId
	 */
	prototype.appendUploadedDoc = function(doc,personId)
	{
		//Получаем щаблон добавляемого документа из ранее сгенирированного элемента
		var content = $('#person-doc-template').html();
		//Заменяем шаблонные значения на переданные
		content = content.replace('#docId',doc['docId']).replace('#docName',doc['docName']).replace('#docDate',doc['docDate']);
		//Добавляем элемент
		$('#person-list-line'+personId+' div.person-docs').append(content);
		//Устанавливаем обработчик для ссылки на удаление
		curObject = this;
		$('#person-doc'+doc['docId']).find('span.person-doc-delete-link').click(function() {
			//Создаём локальный объект для работы с персонами
			var docInfo={};
			docInfo = {
				'docId'	 	: doc['docId']
			};
			//Устанавливаем персону
			curObject.personId = personId;
			//Назначаем действие
			curObject.deleteUploadedDoc(docInfo);
		});
	}
	/**
	 * Удаление загруженного файла со сканом документа
	 * @param int docId
	 */
	prototype.deleteUploadedDoc = function(docInfo)
	{
		//Блоки для прелоадера
		var loaderBlock = '#persons-docs-loader'+this.personId;
		var loaderAreaBlock = '#person-doc'+docInfo['docId'];
		var docInfoArr= {};
		docInfoArr['docInfo'] = docInfo;
		docInfoArr['docInfo']['personId'] = this.personId;
		//Отправляем данные на сервер
		$.ajax({
			type: 'POST',
			url: '/ppc/persons/doc.delete/format/json/',
			dataType: 'json',
			data: docInfoArr,
			beforeSend: function(){
				ajaxLoaderContainer('show',loaderBlock,loaderAreaBlock);
			},
			success: function(result) 
			{
				var status = parseInt(result.status);
				//Если в результате ошибка, то выводим её
				if (status == 1) 
				{
					$('#person-doc'+docInfo['docId']).remove();
				}
				//Удаляем документ
				else
				{
					alert(result.messages);
				}
			},
			complete: function(){
				ajaxLoaderContainer('hide',loaderBlock,loaderAreaBlock);
			}
	 	});
	}
	/**
	 * Отправка запроса на удаление персоны
	 * @param int personId
	 */
	prototype.deleteRequest = function(personId)
	{
		if (confirm(messages['ppc']['all']['personDelConf'])) 
		{
			//Отправляем данные на сервер
			$.ajax({
				type: 'POST',
				url: '/ppc/persons/delete.request/format/json/',
				data: 'personId='+personId,
				success: function(result){
					//Удаляем строку с персоной
					$('#person-list-line'+personId).remove();
					//Отображаем диалог с сообщением об успешной отправке запроса на удаление
					$('#person-delete-dialog').dialog({
						width 		:	400,
						height		:	'auto',
						position 	:	'center',
						modal		:	true,
						resizable 	:	false,
						draggable 	:	false,
						buttons: {
							Ok	: function(){
								$(this).dialog('close');
							}
						}
					});
				}
			});
		}
	}
	/**
	 * Установка событий для элементов строки таблицы персон
	 * @param {Object} personLine Строка таблицы персон
	 */
	prototype.setEvents = function(personLine)
	{
		//Ссылка на объект
		var personsObj = this;
		//Ссылки в таблице персон
		$(personLine).find('span.person-edit-link').click(function(){
			var personId = $(this).attr('id').replace('person-edit-link','');
			personsObj.showActionForm('edit',personId);
		});
		$(personLine).find('span.person-delete-link').click(function(){
			var personId = $(this).attr('id').replace('person-delete-link','');
			personsObj.deleteRequest(personId);
		});
		//Ссылка на удаление загруженного файла со сканом документа
		$(personLine).find('span.person-doc-delete-link').click(function(){
			//Получаем необходимые параметры
			var parentElem = $(this).parent();
			var docInfo = {};
			docInfo = {
				'docId'	 	: parseInt($(parentElem).attr('id').replace('person-doc',''))
			};
			//Устанавливаем персону
			personsObj.personId = parseInt($(parentElem).parent().attr('id').replace('person-docs',''));
			//Назначаем действие
			personsObj.deleteUploadedDoc(docInfo);
		});
		//Upload файлов со сканами документов
		$(personLine).find('span.person-docs-uploader').each(function(){
			var elementId = $(this).attr('id');
			var personId =	parseInt(elementId.replace('docs-uploader',''));
			personsObj.createDocsUploader(elementId,personId);
		});
	}
}



/**
 * Класс для работы с персонами
 */
with (Services = function(){}) 
{
	
	/**
	 * Инициализация диалога для просмотра деталей по услуге
	 */
	prototype.detailsDialogInit = function()
	{
		this.detailsDialog = $('#services-details-dialog').dialog({
			width 		:	500,
			height		:	'auto',
			position 	:	'center',
			modal		:	true,
			resizable 	:	false,
			draggable 	:	true,
			autoOpen 	:	false,
			title		: 	$('#services-details-dialog span.dialog-title').text(),
			buttons: {
				Ok	: function(){
					$(this).dialog('close');
				}
			}
		});
	}
	/**
	 * Отображение детальной информации по услуге
	 * @param int serviceId
	 * @param string serviceType
	 */
	prototype.showDetails = function(serviceId,serviceType)
	{
		//Очищаем контейнер диалога
		$('#services-details-dialog-content').html('');
		this.serviceId = serviceId;
		this.serviceType = serviceType;
		var serviceLine = $('#services-list-line-'+this.serviceType+'-'+this.serviceId);
		var detailsContElem = $(serviceLine).find('div.service-details');
		var dialogElem = $('#services-details-dialog-content');
		//Если детали не загружены, то подгружаем
		if (!$(detailsContElem).find('#content-loaded').length) 
		{
			this.detailsLoad(detailsContElem);
		}
		//Если загружены, то извлекаем из контейнера
		else
		{
			$(dialogElem).html($(detailsContElem).html());
		}
		//Устанавливаем параметры и открываем диалог
		this.detailsDialog.dialog('open');
	}
	/**
	 * Получение детальной информации по услуге с сервера
	 * @param {Object} detailsContElem Элемент, в который сохраняются загруженные параметры
	 */
	prototype.detailsLoad = function(detailsContElem)
	{
		var serviceId = this.serviceId;
		var serviceType = this.serviceType;
		var loaderElem = $('#service-details-loader');
		//Отправляем данные на сервер
		$.ajax({
			type: 'POST',
			url: '/ppc/services/details/format/html/',
			data: 'serviceId='+serviceId+'&serviceType='+serviceType,
			beforeSend: function(){
				$(loaderElem).show();
			},
			success: function(result) 
			{
				$(loaderElem).hide();
				//Сохраняем в dom полученные данные
				$(detailsContElem).html(result);
				$('#services-details-dialog-content').html(result);
			},
			complete: function(){
				$(loaderElem).hide();
			}
	 	});
	},
	/**
	 * Отправка запроса на аннуляцию услуги
	 */
	prototype.serviceCancelRequest = function(serviceId,serviceType)
	{
		if (confirm(messages['ppc']['services']['serviceCancelConf'])) 
		{
			this.serviceId = serviceId;
			this.serviceType = serviceType;
			//Ссылка на текущий объект
			var servicesObj = this;
			//Отправляем данные на сервер
			$.ajax({
				type: 'POST',
				url: '/ppc/services/cancel/',
				data: 'serviceId=' + serviceId + '&serviceType=' + serviceType,
				success: function(result){
					//Отображаем сообщение об успешной отправке запроса
					servicesObj.serviceCancelSuccessDialog();
					//Отмечаем строку с услугой, как анулированную
					$('#services-list-line-' + serviceType + '-' + serviceId).addClass('canceled-service-line');
					//Убираем все действия для услуги
					$('#services-list-line-' + serviceType + '-' + serviceId).find('td.pcc-base-table-action span').remove();
					//Добавляем к названию услуги текст о том, что подан запрос на аннуляцию
					var cancelRequestText = $('.service-canceled-text').html();
					$('#services-list-line-' + serviceType + '-' + serviceId).find('td.pcc-base-table-name').append(cancelRequestText);
				},
				complete: function(){
				}
			});
		}
	},
	/**
	 * Показ диалога с сообщением об успешной отправке запроса на аннуляцию
	 */
	prototype.serviceCancelSuccessDialog = function()
	{
		$('#service-cancel-dialog').dialog({
			width 		:	400,
			height		:	140,
			position 	:	'center',
			modal		:	true,
			resizable 	:	false,
			draggable 	:	false,
			autoOpen 	:	true,
			buttons: {
				Ok	: function(){
					$(this).dialog('close');
				}
			}
		});
	},
	/**
	 * Отправка запроса на редактирование услуги
	 * @param int serviceId
	 */
	prototype.serviceEditDialogShow = function(serviceId,serviceType)
	{
		var dialogContainer = $('#services-edit-dialog-content');
		//Очищаем содержимое диалога
		$(dialogContainer).html('');
		this.serviceId = serviceId;
		this.serviceType = serviceType;
		//Ссылка на текущий объект
		var servicesObj = this;
		//Открываем диалог для редактирования услуги
		if (detectIe6())
		{
			height = 500;
		}
		else
		{
			height = 'auto';
		}
		this.serviceEditDialog = $('#services-edit-dialog').dialog({
			width 		:	580,
			height		:	height,
			position 	:	['center',50],
			modal		:	true,
			resizable 	:	false,
			draggable 	:	true,
			autoOpen 	:	true,
			title		: 	$('#services-edit-dialog span.dialog-title').text(),
			buttons: {
				'btn1'	: function(){
					servicesObj.sendServiceEditRequest();
				},
				'btn2'	: function(){
					$(this).dialog('close');
				}
			}
		});
		uiDialogButtonsFix($('#services-edit-dialog'));
		//Загружаем параметры для редактирования
		this.serviceEditParamsLoad();
	},
	/**
	 * Получение параметров для редактирования услуги
	 * @param {Object} detailsContElem Элемент, в который сохраняются загруженные параметры
	 */
	prototype.serviceEditParamsLoad = function()
	{
		var serviceId = this.serviceId;
		var serviceType = this.serviceType;
		var loaderElem = $('#service-edit-loader');
		//Отправляем данные на сервер
		$.ajax({
			type: 'POST',
			url: '/ppc/services/edit.params/format/html/',
			data: 'serviceId='+serviceId+'&serviceType='+serviceType,
			beforeSend: function(){
				$(loaderElem).show();
			},
			success: function(result) 
			{
				$(loaderElem).hide();
				//Сохраняем в dom полученные данные
				$('#services-edit-dialog-content').html(result);
			},
			complete: function(){
				$(loaderElem).hide();
			}
	 	});
	},
	/**
	 * Отправка данных для запроса на редактирование услуги
	 */
	prototype.sendServiceEditRequest = function()
	{
		//Получаем массив из данных формы + данные по изменяемой услуге
		var formData = this.getServiceEditFormData();
		//Отправляем данные на сервер
		var dialogContainer = $('#services-edit-dialog');
		var loaderElem = $('#service-edit-loader');
		var servicesObj = this;
		if(!formData)
		{
			alert(messages['ppc']['services']['persNotSel']);
			return false;
		}
		$.ajax({
			type: 'POST',
			url: '/ppc/services/edit.request/format/json/',
			dataType: 'json',
			data: formData,
			beforeSend: function(){
				$('#services-edit-dialog-content').html('');
				//Перемещаем диалог на центр экрана
				servicesObj.serviceEditDialog.dialog("option","position",'center');
				$(loaderElem).show();
			},
			success: function(result) 
			{
				//Заменяем кнопки диалога на одну кнопку OK
				var buttons	= {
					'Ok': function() {
						$(this).dialog('close');
					}
				};
				servicesObj.serviceEditDialog.dialog('option','buttons',buttons);
				$(loaderElem).hide();
				//Отображаем сообщение об успешной отправке запроса
				$(dialogContainer).find('div.services-edit-dialog-success').show();
				
			},
			complete: function(){
				//Заменяем кнопки диалога на одну кнопку OK
				var buttons	= {
					'Ok': function() {
						$(this).dialog('close');
						$(dialogContainer).find('div.services-edit-dialog-success').hide();
					}
				};
				servicesObj.serviceEditDialog.dialog('option','buttons',buttons);
				$(loaderElem).hide();
				//Отображаем сообщение об успешной отправке запроса
				$(dialogContainer).find('div.services-edit-dialog-success').show();
			}
	 	});
	},
	/**
	 * Получение данных с изменениями по услуге из формы
	 */
	prototype.getServiceEditFormData = function() 
	{
		//Формируем массив данных из формы для отправки
		var formData = {};
		formData['data'] = {};
		//Общие данные
		formData['data']['serviceId'] = this.serviceId;
		formData['data']['serviceType'] = this.serviceType;
		formData['data']['serviceCaption'] = $('#services-list-line-'+this.serviceType+'-'+this.serviceId).find('td.pcc-base-table-name').text();
		//Поля формы
		var serviceEditForm = $('#service-edit-form');
		formData['data']['fields'] = {};
		var j = 0;
		$(serviceEditForm).find('select').each(function(){
			formData['data']['fields'][j] = {
				'name'		:		$(this).attr('name'),
				'value'		:		$(this).find('option:selected').val(),
				'caption'	:		trim($(this).parent().parent().find('td.service-edit-param').text())
			};
			j++;
		});
		$(serviceEditForm).find('input:text').each(function(){
			formData['data']['fields'][j] = {
				'name'		:		$(this).attr('name'),
				'value'		:		$(this).val(),
				'caption'	:		trim($(this).parent().parent().find('td.service-edit-param').text())
			};
			j++;
		});
		//Список персон
		formData['data']['persons'] = {};
		if ($(serviceEditForm).find('input[name="persons"]').length) 
		{
			var i = 0;
			var personsElems = $(serviceEditForm).find('input[name="persons"]:checked');
			if (personsElems.length == 0) {
				return false;
			}
			$(personsElems).each(function(){
				formData['data']['persons'][i] = {};
				formData['data']['persons'][i] = $(this).val();
				i++;
			});
		}
		//Комментарий
		formData['data']['comment'] = $(serviceEditForm).find('#service-edit-comment').val();
		return formData;
	}
}


/**
 * Класс для работы с услугами и оплатой
 */
with (Payments = function(){
}) {
	/**
	 * Выделение всех услуг по выбранному способу оплаты
	 * @param {Object} methodSysName
	 */
	prototype.paymentSelAllServForMeth = function(methodSysName){
		$('#services-payment-select-form input.payment-radio-' + methodSysName).attr('checked', 'checked');
	}
	
	/**
	 * Диалог для запроса на формирование документа
	 * @param string methodSysName
	 * @param string type
	 */
	prototype.showDocRequestDialog = function(methodSysName, type){
		//Если ни одна услуга не оплачивается с помощью данного метода оплаты
		if (!($('#services-payment input.payment-ch-' + methodSysName).length)) {
			return false;
		}
		this.methodSysName = methodSysName;
		var services = {};
		//Если идёт формирование для всех услуг, где выбран данный способ оплаты
		if (type == 'all') 
		{
			$('#services-payment input.payment-ch-' + this.methodSysName).attr('checked', 'checked');
		}
		//Проверяем, выбрана ли хотя бы одна услуга
		var selectedServicesElems = $('#services-payment input.payment-ch-' + this.methodSysName + ':checked');
		if ($(selectedServicesElems).length == 0) {
			alert(messages['ppc']['payment']['servicesNotSel']);
			return false;
		}
		//Получаем список выбранных услуг и считаем сумму по выбранным услугам
		var i = 0;
		var summ = 0;
		var servicesPaymentTable = $('#services-payment-table');
		$(selectedServicesElems).each(function(){
			services[i] = {};
			var curNameParts = $(this).attr('name').split('-');
			services[i]['sys_name'] = curNameParts[0];
			services[i]['id'] = curNameParts[1];
			summ = summ+parseInt($(servicesPaymentTable).find('#services-payment-row-'+services[i]['sys_name']+'-'+services[i]['id']).find('td.pcc-base-table-summ').text());
			i++;
		});
		this.summ = summ;
		var formattedSumm = sprintf('%01.2f',summ);
		//Отображаем блок с суммой и записываем в него полученную сумму
		$('#payment-dialog-summ').removeClass('hidden').text(formattedSumm);
		this.requestServices = services;
		//Открываем диалог
		this.docRequestDialogOpen();
	}
	
	/**
	 * Инициализация и показ диалога для запроса на формирование документа
	 */
	prototype.docRequestDialogOpen = function(){
		//По умолчанию скрываем все блоки в диалоге кроме указанных
		var dialogContainer = $('#payment-request-dialog');
		$(dialogContainer).find('#payment-dialog-body div[id!="payment-dialog-lang-line"][id!="payment-dialog-summ-line"]').addClass('hidden');
		//В зависимости от типа документа, отображаем элементы
		$(dialogContainer).find('#payment-dialog-body').removeClass('hidden');
		$(dialogContainer).find('#payment-dialog-body-elements').removeClass('hidden');
		//Сбрасываем настройки языка и отменяем события для списка
		var langSelectElem = $(dialogContainer).find('#payment-dialog-lang');
		$(langSelectElem).find('option:first').attr('selected', 'selected');
		$(langSelectElem).unbind('change');
		/*
		 * Если метод оплаты - безнал, то отображаем строку для ввода названия компании и
		 * назначаем событие для списка с выбором языка
		 */
		if (this.methodSysName == 'bank') 
		{
			$(dialogContainer).find('#payment-dialog-company-line').removeClass('hidden');
			//Если выбран английский язык
			if ($(langSelectElem).find('option:selected').val() == 'en') {
				$(dialogContainer).find('#payment-dialog-attention-line').removeClass('hidden');
			}
			//Событие при смене языка
			$(langSelectElem).change(function(){
				if ($(this).find('option:selected').val() == 'en') {
					$(dialogContainer).find('#payment-dialog-attention-line').removeClass('hidden');
				}
				else {
					$(dialogContainer).find('#payment-dialog-attention-line').addClass('hidden');
				}
			});
		}
		/*
		 * Если метод оплаты - онлайн, то отображаем предупрреждение о переходе на другой сайт и выбор
		 * метода оплаты
		 */
		if (this.methodSysName == 'online') 
		{
			$(dialogContainer).find('#payment-dialog-online-conf-line').removeClass('hidden');
			$(dialogContainer).find('#payment-dialog-online-method-line').removeClass('hidden');
		}
		//Ссылка на объект
		var paymentObj = this;
		//Определяем высоту диалога в зависмости отметода оплаты
		var height;
		switch (this.methodSysName) 
		{
			case 'auth' : height = 200; break;
			case 'bank' : height = 300; break;
			case 'online' : height = 450; break;
		}
		//Открываем диалог
		this.docRequestDialog = $(dialogContainer).dialog({
			width: 500,
			height: height,
			position: 'center',
			modal: true,
			resizable: false,
			draggable: false,
			autoOpen: true,
			title: $(dialogContainer).find('#payment-dialog-title-' + paymentObj.methodSysName).text(),
			buttons: {
				'btn1': function(){
					//Назначаем событие в зависимости от метода оплаты
					var lang = $(langSelectElem).find('option:selected').val();
					//При онлайн оплате открываем новое окно, в котором делаем редирект на оплату
					if (paymentObj.methodSysName == 'online') 
					{
						var onlineMethod = $(dialogContainer).find('#payment-dialog-online-method-line').find('input[name="payment-dialog-online-method"]:checked').val();
						//Ограничение на сумму при онлайн оплате
						if (onlineMethod==1 && paymentObj.summ > 100000)
						{
							paymentObj.showOnlineWarningDialog();
							return false;
						}
						//Определяем выбранные параметры для оплаты
						var onlineUrl = '/ppc/payment/online.pay/lang/'+lang+'/method/'+onlineMethod+'/summ/'+paymentObj.summ+'/';
						var newWin = window.open(onlineUrl, 'paymentWin');
						newWin.focus();
						$(this).dialog('close');
					}
					//При запросе счёта или авторизационного письма, отправляем запрос на сервер
					else {
						paymentObj.processDocRequest();
					}
				},
				'btn2': function(){
					$(this).dialog('close');
				}
			}
		});
		//Fix для перевода кнопок диалога
		uiDialogButtonsFix($('#payment-request-dialog'));
	}
	
	/**
	 * Отображение диалога с сообщением об ограничении на сумму при онлайн оплате
	 */
	prototype.showOnlineWarningDialog = function()
	{
		$('#payment-online-warning').dialog({
			width: 700,
			height: 'auto',
			position: 'center',
			modal: true,
			resizable: false,
			draggable: false,
			autoOpen: true,
			buttons: {
				'Ok': function(){
					$(this).dialog('close');
				}
			}
		});
	}
	
	/**
	 * Обработка запроса на формирование документа
	 */
	prototype.processDocRequest = function(){
		//Ссылка на объект
		var paymentObj = this;
		//Данные для отправки
		var request = {};
		request['request'] = {};
		request['request']['method_sys_name'] = this.methodSysName;
		request['request']['lang'] = $('#payment-dialog-lang option:selected').val();
		request['request']['company'] = $('#payment-dialog-company').val();
		request['request']['services'] = this.requestServices;
		var loaderElem = $('#payment-dialog-loader');
		//Отправляем данные на сервер
		$.ajax({
			type: 'POST',
			url: '/ppc/payment/doc.request/format/json/',
			dataType: 'json',
			data: request,
			beforeSend: function(){
				//Cкрываем все блоки в диалоге
				$('#payment-request-dialog').find('#payment-dialog-body div[id!="payment-dialog-lang-line"][id!="payment-dialog-summ-line"]').addClass('hidden');
				$(loaderElem).show();
			},
			success: function(result){
				//Отображаем строку с результатом
				$('#payment-request-dialog').find('#payment-dialog-success-line').removeClass('hidden');
				//Заменяем кнопки диалога на одну кнопку OK
				var buttons = {
					'Ok': function(){
						$(this).dialog('close');
					}
				};
				paymentObj.docRequestDialog.dialog('option', 'buttons', buttons);
			},
			complete: function(){
				$(loaderElem).hide();
			}
		});
	}
	
	/**
	 * Обновление списка с документами
	 * @param string docType Тип документа
	 */
	prototype.updateDocsList = function(docType)
	{
		//Определяем блок с прелоадером в зависимости от типа документа
		var loaderElem;
		if (docType == 'invoice') 
		{
			loaderElem = $('#invoices-loader');
		}
		if (docType=='auth_letter')
		{
			loaderElem = $('#auth-letters-loader');
		}
		$.ajax({
			type: 'POST',
			url: '/ppc/payment/docs.list/format/json/',
			data: 'type='+docType,
			cache: false,
			beforeSend: function(){
				$(loaderElem).show();
			},
			success: function(result){
				//Если не было ошибок, заменяем список док-тов сгенерированным html
				if (result.status==1)
				{
					if (docType=='invoice')
					{
						$('#payment-docs-invoices').html(result.content);
					}
					if (docType=='auth_letter')
					{
						$('#payment-docs-auth-letter').html(result.content);
					}
				}
				else
				{
					alert(result.messages);
				}
			},
			complete: function(){
				$(loaderElem).hide();
			}
		});
	}
	
	/**
	 * Выбор способа оплаты для услуги
	 * @param int serviceId
	 * @param tring serviceSysName
	 */
	prototype.setServicePayment = function(serviceId, serviceSysName){
		var paymentTable = $('#services-payment-table');
		var selectedMethodElem = $(paymentTable).find('input[name="' + serviceSysName + '-' + serviceId + '"]:checked');
		var paymentMethod = $(selectedMethodElem).val();
		//Отправляем данные на сервер
		$.ajax({
			type: 'POST',
			url: '/ppc/payment/set.payment/format/json/',
			data: 'serviceId=' + serviceId + '&serviceSysName=' + serviceSysName + '&paymentMethod=' + paymentMethod,
			success: function(result){
				//Формируем checkbox для выбранного метода оплаты
				var methodChElem = '<input type="checkbox" value="' + paymentMethod + '" class="payment-ch-' + paymentMethod + '" name="' + serviceSysName + '-' + serviceId + '">';
				//Добавляем checkbox в соответствующую ячейку
				var selectedCell = $(selectedMethodElem).parent();
				$(selectedCell).html(methodChElem);
				//Удаляем все radio на данной строке, а также кнопку выбора метода
				var curLine = $(selectedCell).parent();
				$(curLine).find('input[name="' + serviceSysName + '-' + serviceId + '"]:radio').remove();
				$(curLine).find('div.payment-method-select').remove();
			}
		});
	}
}
/**
 * Класс для работы с услугами и оплатой
 */
with (User = function(){}) 
{
	/**
	 * Отображение формы для редактирования профиля
	 */
	prototype.showProfileEditForm = function()
	{
		this.profileDetailsTable = $('#user-profile-details-table');
		//Получаем данные для заполнения формы
		var data = {};
		$(this.profileDetailsTable).find('td.detail-value-editable').each(function(){
			var detailName = $(this).attr('id').replace('profile-details-','');
			data[detailName] = $(this).text();
		});
		//Заполняем форму
		this.fillProfileEditForm(data);
		//Показываем модальное окно
		loadPopup($('#profile-edit-form-container'));
	}
	
	/**
	 * Заполнение формы для редактирования профиля данными
	 * @param {Object} data
	 */
	prototype.fillProfileEditForm = function(data)
	{
		this.profileEditForm = $('#profile-edit-form');
		$(this.profileEditForm).find('input[type="text"]').each(function(){
			var curElemName = $(this).attr('name');
			$(this).val(data[curElemName]);
		});
		$(this.profileEditForm).find('input[name="title"]').removeAttr('checked');
		$(this.profileEditForm).find('input[value="'+data.title+'"]').attr('checked','checked');
	}
	
	/**
	 * Процесс редактирования профиля
	 */
	prototype.profileEditProcess = function()
	{
		var profile = {};
		//Ссылка на текущий объект
		var userObj = this;
		//Получаем данные из формы
		var data = {};
		$(this.profileEditForm).find('input[type="text"]').each(function(){
			var fieldName = $(this).attr('name');
			data[fieldName] = $(this).val();
		});
		data['title'] = $(this.profileEditForm).find('input[name="title"]:checked').val();
		profile['userData'] = data;
		profile['userId'] = $(this.profileEditForm).find('#user-id').val();
		//Отправляем данные на сервер
		$.ajax({
			type: 'POST',
			url: '/ppc/user/profile.edit/format/json/',
			dataType: 'json',
			data: profile,
			beforeSend: function(){
				$('#profile-edit-loader').show();
			},
			success: function(result){
				//В случае успеха добавляем новые значения в таблицу и закрываем форму
				if(result.status==1)
				{
					$(userObj.profileDetailsTable).find('td.detail-value-editable').each(function(){
						var detailName = $(this).attr('id').replace('profile-details-','');
						$(this).text(data[detailName]);
					});
					disablePopup($('#profile-edit-form-container'));
				}
				//В случае ошибки выводим сообщение
				else
				{
					alert(result.messages);
				}
			},
			complete: function(){
				$('#profile-edit-loader').hide();
			}
		});
	}
	
}

/* ------------------------------------------| Общие функции  |---------------------------------------------*/
/**
 * Инициализация кадендаря
 */
function calendarInit(element,minYear,maxYear)
{
	$(element).datepicker({
			showOn: 'button',
			buttonImage: '/img/calendar.gif',
			buttonImageOnly: true,
			dateFormat: 'dd.mm.yy',
			changeMonth: true,
			changeYear: true,
			yearRange: minYear+':'+maxYear
	});
	//Устанавливаем параметры для изображения, вызывающего календарь
	$(element).next().addClass('form-calendar').attr('title','Select date').attr('alt','Select date');
}


/**
 * Диалоги с подробной информацией
 */
function showInfo(section)
{
	//Для ie6 и определённых секций выставляем фиксированную высоту
	if (detectIe6() && section=='payment')
	{
		height=500;
	}
	else
	{
		height='auto';
	}
	//Заголовок для диалога
	var title = $('#'+section+'-help').find('div.dialog-title').text();
	//Отображаем диалог
	$('#'+section+'-help').dialog('destroy');
	$('#'+section+'-help').dialog({
		width : 600,
		height: height,
		title : title,
		position : 'center',
		modal: true,
		resizable : false,
		draggable : true
	});
}


/* --------------------| Функциии, вызывающиеся в момент $(document).ready для разных разделов |----------------*/
/**
 * Функции инициализации для разделов
 */
var ppc = {
	//Информация о заказе
	orderInfo :
	{
		init : function()
		{
			//Объект класса для работы с услугами
			servicesObj = new Services();
			//Инициализация диалога для просмотра деталей по услуге
			servicesObj.detailsDialogInit();
			//Таблица со списком услуг
			var servicesTable = $('#services-list-table');
			//Раскраска строк таблицы
			tableHighlight($('#services-list-table'));
		}
	},
	//Персоны
	persons :
	{
		init : function()
		{
			//Календарь для выбора даты рождения и даты истечения срока действия паспорта
			var curDate = new Date();
			var curYear = curDate.getFullYear();
			calendarInit('#person-birthday',curYear-150,curYear);
			calendarInit('#person-passport-exp-date',curYear-150,curYear+150);
			//Объект класса для работы с персонами
			var personsObj = new Persons();
			/*
			 * События для ссылок
			 */
			//Ссылка для добавления персон
			$('#person-add-link').click(function(){
				personsObj.showActionForm('add');
			});
			//Установка событий для каждой строки таблицы персон
			$('#persons-list-table tr').each(function() {
				personsObj.setEvents(this);
			});
			//Ссылки в форме редактирования/добавления
			var personsFormLinks = $('#persons-form-links');
			$(personsFormLinks).find('span.form-clear-link').click(function(){
				clearForm('#persons-form');
			});
			$(personsFormLinks).find('span.form-add-link').click(function(){
				personsObj.processPerson();
			});
			$(personsFormLinks).find('span.form-save-link').click(function(){
				personsObj.processPerson();
			});
			$(personsFormLinks).find('span.form-cancel-link').click(function(){
				//Скрываем модальное окно
				var modalElem = $('#persons-form-container');
				disablePopup(modalElem);
				//Сбрасываем поля формы
				personsObj.resetActionForm();
			});
			//Событие для списка с выбором гражданства
			$('#person-citizenship').bind('change',function(){
				personsObj.citizUpdate();
			});
		}
	},
	//Методы оплаты
	payment :
	{
		select: 
		{
			init: function()
			{
				paymentsObj = new Payments();
				//Отправка формы по ссылке
				if ($('#services-payment-select-next').length) 
				{
					var servicesPaymentForm = $('#services-payment-select-form');
					$('#services-payment-select-next').click(function(){
						$(servicesPaymentForm).submit();
					});
				}
				/*
				 * События для ссылок 
				 */
				//Выделение всех услуг по выбранному способу оплаты
				$('#services-payment span.services-payment-select-all').click(function(){
					var methodSysName = $(this).attr('id').replace('services-payment-select-all-','');
					paymentsObj.paymentSelAllServForMeth(methodSysName);
				});
				//Раскраска строк таблицы
				tableHighlight($('#services-payment-select-table'));
				//Табы для счетов/писем авторизации
				$('#payment-docs').tabs({
					collapsible: true
				});
			}
		},
		index : 
		{
			init: function()
			{
				paymentsObj = new Payments();
				var paymentsTable = $('#services-payment-table');
				/*
				 * События для ссылок 
				 */
				//Диалог для отправки запроса на документ
				$(paymentsTable).find('span.service-payment-get-link').click(function(){
					var curId = $(this).attr('id').replace('payment-dialog-link-',''); 
					var curParams = curId.split('-');
					paymentsObj.showDocRequestDialog(curParams[0],curParams[1]);
				});
				//Раскраска строк таблицы
				tableHighlight($('#services-payment-table'));
				//Табы для счетов/писем авторизации
				$('#payment-docs').tabs({
					collapsible: true
				});
				//Ссылки для обновления списков счетов и авторизационных писем
				var paymentDocsCont = $('#payment-docs');
				$(paymentDocsCont).find('#invoices-refresh-link').click(function(){
					paymentsObj.updateDocsList('invoice');
				});
				$(paymentDocsCont).find('#auth-letters-refresh-link').click(function(){
					paymentsObj.updateDocsList('auth_letter');
				});
			}
		}
	},
	/*
	 * Пользователь: профиль
	 */
	user :
	{
		/*
		 * Профиль 
		 */
		profile : 
		{
			init: function()
			{
				userObj = new User();
				/*
				 * События для ссылок
				 */
				$('#user-profile-edit').click(function(){
					userObj.showProfileEditForm();
				});
				//Ссылки в форме редактирования профиля
				var profileEditFormLinksBlock = $('#profile-edit-form-links');
				$(profileEditFormLinksBlock).find('.form-clear-link').click(function(){
					clearForm('#profile-edit-form');
				});
				$(profileEditFormLinksBlock).find('.form-save-link').click(function(){
					userObj.profileEditProcess();
				});
				$(profileEditFormLinksBlock).find('.form-cancel-link').click(function(){
					disablePopup($('#profile-edit-form-container'));
				});
			}
		}
	}
}
/* --------------------|  Автозагрузка функций общих для всех разделов |----------------*/
$(document).ready(function(){
	//Шрифты
	Cufon.replace('.muriad');
});