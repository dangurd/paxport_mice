var sysSettings = {
    //Ключ для карт google (conference.su)
    gmKey : 'AIzaSyCh5VV-7IGMf_3mmoEat4QmXjbL49l_8kA',
    // Загружаемая версия карт google
    gmApiVers : '3.8'
};

/* ----------------------------------| Персоны |-----------------------------------------------------------------*/
/**
 * Добавление строки с пустыми полями по персоне
 */
function addPerson()
{
	var personsForm = $('#persons-form');
	// Вычисляем номер добавляемой строки
	var lastRow = personsForm.find('.new-person:last');
	var newPersonNum
	if (lastRow.length) 
	{
		var lastPersonNum = parseInt(lastRow.attr('id').replace('new-person', ''));
		newPersonNum = lastPersonNum + 1;
	}
	else
	{
		newPersonNum = 1;
	}
	// Загружаем контент формы для персоны с сервера
	$.ajax({
		type: 'POST',
		url: '/process/new.person/',
		data: 'num=' + newPersonNum,
		success: function(result){
			personsForm.append(result);
			$('#new-person-add').show();
			// Выбор специализации из списка значений
			$('#new-person' + newPersonNum + ' select.spec-type').change(function(){
				var curValue = $(this).find('option:selected').val();
				if (curValue == 0)
				{
					return true;
				}
				$(this).parent().parent().find('.spec-val').val(curValue);			
			});
			$('#new-person' + newPersonNum +' .badge-line input').tooltip();
			if ($.browser.msie && !detectIe9()) 
			{
				$('#new-person' + newPersonNum +' select.ie-width-fix').fixSelect();
			}
		},
		complete: function(){
			$('.person-loader').hide();
		}
	});
	
}

/**
 * Удаление контейнера с формой добавления новой персоны
 * @param int personId
 */
function deleteNewPersonForm(personNum)
{
	var addedPersons = $('#added-persons table tr.added-person').length;
	var newPersonsForms = $('#new-persons .new-person').length;
	if (addedPersons == 0 && newPersonsForms < 2)
	{
		return;
	}
	if (addedPersons != 0 && newPersonsForms == 1)
	{
		$('#new-person-add').hide();
	}
	$('#new-person' + personNum).remove();
}

/**
 * Удаление персоны
 * @param int personId
 */
function deletePerson(personId)
{
	if (confirm(messages['reg']['all']['personDelConf'])) 
	{
		$.ajax({
			type: 'POST',
			url: '/process/delete.person/format/json/',
			data: 'personId=' + personId,
			success: function(result){
				if (result.status==1)
				{
					var addedPersons = $('#added-persons table tr.added-person').length;
					if (addedPersons == 1)
					{
						$('#added-persons-cont').remove();
						//addPerson();
                        window.location.reload();
						return;
					}
					$('#added-person' + personId).remove();	
				}
				else
				{
					alert(result.messages);
				}
			}
		});
	}
}

/**
 * Изменение персоны
 * @param int personId
 */
function editPerson(personId)
{
    if (confirm(messages['reg']['all']['personEditConf']))
    {
        $.ajax({
            type: 'POST',
            url: '/process/edit.person/format/json/',
            data: 'personId=' + personId,
            success: function(result){
                if (result.status==1)
                {
                    var addedPersons = $('#added-persons table tr.added-person').length;
                    if (addedPersons == 1)
                    {
                        $('#added-persons-cont').remove();
                        //addPerson();
                        window.location.reload();
                        return;
                    }
                    $('#added-person' + personId).remove();
                }
                else
                {
                    alert(result.messages);
                }
            }
        });
    }
}


/**
 * Добавление заказа визовой поддержки для персоны
 * @param int personId Id персоны
 * @param int mode Тип добавления: 0 - при регистрации, 1 - из ЛК
 * @param int visaTypeId Id типа добавляемой визы
 */
function personAddVisa(personId,mode,visaTypeId)
{
	$.ajax({
		type: 'POST',
		url: '/process/add.visa/format/json/',
		data: 'personId='+personId+'&visaTypeId='+visaTypeId,
		success: function(result) 
		{
			if (result.status == 1) 
			{
				var personRow = $('#added-person' + personId);
				//Скрываем ссылку для добавления визы
				$(personRow).find('.persons-visa-add-link').addClass('hidden');
				$(personRow).find('.persons-visa-required').removeClass('hidden');
				$(personRow).find('.persons-visa-required .person-visa-type-str').text(result.visaStr);
				//Меняем сообщение о визовой поддержке на выбранный тип визы
				$(personRow).find('.persons-visa-not-required').addClass('hidden');
				//Если заказ визы идёт не из ЛК, то отображаем ссылку для отказа от визы
				if (mode != 1)
				{
					$(personRow).find('.persons-visa-refuse-link').removeClass('hidden');
				}
			}
			else
			{
				alert(result.messages);
			}
		}
 	});
}

/**
 * Удаление заказа визовой поддержки для персоны
 * @param int personId Id персоны
 */
function personRefuseVisa(personId)
{
	$.ajax({
		type: 'POST',
		url: '/process/delete.visa/format/json/',
		data: 'personId='+personId,
		success: function(result) 
		{
			if (result.status == 1) 
			{
				var personRow = $('#added-person' + personId);
				$(personRow).find('.persons-visa-not-required').removeClass('hidden');
				$(personRow).find('.persons-visa-add-link').removeClass('hidden');
				$(personRow).find('.persons-visa-required').addClass('hidden');
				$(personRow).find('.persons-visa-refuse-link').addClass('hidden');
			}
			else
			{
				alert(result.messages);
			}
		}
 	});
}


/* ----------------------------------| Номера |-------------------------------------------------------*/
/**
 * Загрузка списка гостиниц города
 */
function loadCityHotels(cityId)
{
	$.ajax({
		url: '/process/city.hotels/format/json/',
		data:
		{
			city : cityId
		},
		success: function(result) 
		{
			if (result.status == 1 && result.hotels) 
			{
				var cont = '',
					h = result.hotels;
				for (var i = 0; i < h.length; i++)
				{
					cont += '<option value="' + h[i]['id'] + '">' + h[i]['name'] + '</option>';
				}
				$('#hotel-name').html(cont);
				getRoomsAndPrices();
			}
		}
	});
}

/**
 * Заказ номера
 */
function addHotelRoom()
{
	if (!$('#persons-block input:checked').size()) {
		alert ('Не выбран ни один гость');
		return false;
	} 
	
	var hotelOrderForm = $('#main-form-step2');
	//Заказ на проживание
	var hotelOrder={};
	hotelOrder['order'] = {
		'hotelId'			: 			$(hotelOrderForm).find('#hotel-name option:selected').val(),
		'roomId' 			: 			$(hotelOrderForm).find('#hotel-rooms option:selected').val(),
		'accommodation' 	: 			parseInt($('#hotel-accommodation option:selected').val()),
		'startDate'			:			$('#hotel-sdate').val(),
		'endDate'			:			$('#hotel-edate').val(),
		'comment'			:			$('#hotel-comment').val()	
	};
	//Завтрак
	var breakfActiveCont = $(hotelOrderForm).find('#hotel-breakfast-active');
	if($(breakfActiveCont).is(':visible'))
	{
		hotelOrder['order']['breakfast'] = $(breakfActiveCont).find('#hotel-breakfast option:selected').val();
	}
	else
	{
		hotelOrder['order']['breakfast'] = 0;
	}
	//Дополнительная кровать и тип кровати
	var extraBed;
	var bedType;
	//Если выбрано двухместное размещение, то определяем выбранный тип кровати, а также необходимость доп. кровати
	if (hotelOrder['order']['accommodation'] == 2) 
	{
		if ($('#hotel-extra-bed').attr('checked')) 
		{
			extraBed = 1;
		}
		else 
		{
			extraBed = 0;
		}
		bedType = $('#hotel-bed-type').val();
	}
	else
	{
		extraBed = 0;
		bedType = '';
	}
	hotelOrder['order']['extraBed'] = extraBed;
	hotelOrder['order']['bedType'] = bedType;
	//Получаем список выбранных персон
	hotelOrder['order']['persons'] = getHotelPersons(hotelOrder['order']['accommodation'],extraBed);
	//Если персоны не выбраны
	if (!hotelOrder['order']['persons'])
	{
		alert(messages['reg']['all']['hotelMissParams']);
		window.location= '/step1/'; 
		return false;
	}
	//Отправляем данные на сервер
	hotelRoomSave(hotelOrder);
}
/**
 * Получение списка выбранных персон при заказе номера
 * @param int accommodation
 * @param int extraBed
 */
function getHotelPersons(accommodation,extraBed)
{
    //var hotelOrderForm = $('#main-form-step2');
	//Определяем количество персон на основе выбранного типа размещения
	var setType = accommodation;
	if(extraBed==1)
	{
		setType++;
	}
	//Объект, содержащий выбранные персоны
	var persons={};
	//Получаем список выбранных персон
    for(i=1;i<=setType;i++)
    {
        persons[i] = {};
        persons[i]['id'] = $('#persons-block input:checked').eq(i-1).val();
        //Если персона не выбрана в одном из списков
        if (persons[i]['id']==0)
        {
            return false;
        }
        persons[i]['name'] = trim($('#persons-block input:checked').next().eq(i-1).text());
    }
	
	var persons_count = $('#persons-block input:checked').size();

	if (setType > persons_count) {
		return false;
	}
   
	return persons;
}


/**
 * Передача данных по заказу номера на сервер для проверки и сохранения в БД
 * @param {Object} hotelOrder
 */
function hotelRoomSave(hotelOrder)
{
	var loaderElem = '#hotel-loader';
	var loaderContainer = '#ordered-hotel';
	$.ajax({
		type: 'POST',
		url: '/process/add.hotel.room/format/json/',
		dataType: 'json',
		data: hotelOrder,
		beforeSend: function(){
			ajaxLoaderContainer('show', loaderElem, loaderContainer);
		},
		success: function(result) 
		{
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
			var status = parseInt(result.status);
			//Если в результате ошибка, то выводим её
			if (status == 0) 
			{
				alert(result.messages);
			}
			//Если записи успешно добавлены, добавляем строку
			else
			{	
				var emptyOrderElem = $('#hotel-empty-order');
				//Добавляем новую строку (без prepend плывёт верстка)
				if ($(emptyOrderElem).is(':visible')) 
				{
					$('#ordered-hotel').prepend(result.content);
				}
				else
				{
					$('#ordered-hotel').append(result.content);
				}
				//Скрываем блок, сообщающий об отсутствии заказов
				$(emptyOrderElem).addClass('hidden');
				//Отменяем выбор персон
				hotelPersonsListsReset();
				//Очищаем поле с комментарием
				$('#hotel-comment').val('');
				//Меняем текст ссылки
				//$('#hotel-add-more-text').removeClass('hidden-imp');
				//$('#hotel-add-text').addClass('hidden-imp');
                //Скрываем контейнер заказа
                $('#order_form_cont').hide();
				//Скроллинг к таблице заказов
				var scrollToCont = document.getElementById('hotel-comment');
				scrollToCont.scrollIntoView(true);
			}
		},
		complete: function(){
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
		}
	});
}

/**
 * Получение списка номеров для гостиницы с ценами на размещения дял указанного периода 
 */
function getRoomsAndPrices()
{
	//Формируем запрос на номера
	var roomsRequest = {};
	roomsRequest['request'] = {
		'hotelId'		:	$('#hotel-name').find('option:selected').val(),
		'sDate'			:	$('#hotel-sdate').val(),
		'eDate'			:	$('#hotel-edate').val(),
		'accommodation'	:	$('#hotel-accommodation').find('option:selected').val()
	};
	//Если выбрано двухместное размещение и доп. кровать
	if (roomsRequest['request']['accommodation']==2 && $('#hotel-extra-bed').attr('checked'))
	{
		roomsRequest['request']['extraBed'] = 1;
	}
	else
	{
		roomsRequest['request']['extraBed'] = 0;
	}
	//Отправляем запрос на сервер
	$.ajax({
		type: 'POST',
		url: '/process/rooms.prices/format/json/',
		dataType: 'json',
		data: roomsRequest,
		success: function(result) 
		{
			//Если не было ошибок
			if (result.status == 1) 
			{
				//Сохраняем список номеров в DOM
				$('#hotel-block').data('roomsList',result.rooms);
				//Обновляем список номеров и поле со стоимостью проживания
				updateRooms();
			}
			else
			{
				alert(result.messages);
			}
		}
	});
}

/**
 * Обновления списка номеров с ценами
 */
function updateRooms()
{
	var hotelRoomsSelect = $('#hotel-rooms');
	//Получаем id выбранного номера
	var selectedRoomId = hotelRoomsSelect.val();
	//Получаем список номеров из DOM
	var rooms = $('#hotel-block').data('roomsList');
	//Проходимся по полученным ранее номерам и формируем контент для списка номеров
	var roomsSelectCont = '';
	for (i in rooms)
	{
		//Выделяем выбранный ранее номер
		var selected = '';
		if (rooms[i]['id'] == selectedRoomId)
		{
			selected = 'selected="selected"';
		}
		var roomOption = '';
		roomOption = '<option value="'+rooms[i]['id']+'"'+selected+'>';
		roomOption += rooms[i]['name']+' ('+rooms[i]['dayPriceText']+')</option>';
		roomsSelectCont += roomOption;
	}
	//Заменяем контент списка
	$('#hotel-rooms').html(roomsSelectCont);
	//Отображаем цену размещения для номера
	getRoomPrice();
}

/**
 * Отображение цены размещения в выбранном номере на основе данных, полученных ранее с сервера
 */
function getRoomPrice()
{
	var hotelRoomsSelect = $('#hotel-rooms');
	var selectedRoomId = hotelRoomsSelect.val();
	//Если номера ещё не загружены (страница в исходном состоянии), то загружаем их
	if(!$('#hotel-block').data('roomsList'))
	{
		getRoomsAndPrices();
		return false;
	}
	//Получаем список номеров из DOM
	var rooms = $('#hotel-block').data('roomsList');
	//Определяем стоимость выбранного номера и выводим её
	var curRoomPrice = rooms[selectedRoomId]['summ'];
	var priceAmountCont = $('#hotel-price-amount');
	var priceByRequestCont = $('#hotel-price-by-request');
	//Если стоимость не по запросу
	if(curRoomPrice != 0)
	{
		$(priceAmountCont).show();
		$(priceByRequestCont).hide();
		$(priceAmountCont).find('span').text(curRoomPrice);
	}
	//Если по запросу
	else
	{
		$(priceAmountCont).hide();
		$(priceByRequestCont).show();
	}
	var breakfActiveCont = $('#hotel-breakfast-active');
	var breakfNotActiveCont = $('#hotel-breakfast-not-active');
	var breakfNotAvCont = $('#hotel-breakfast-not-av');
	var breakfNote = $('#hotel-price-note');
	//Если затрак не доступен для заказа
	if (rooms[selectedRoomId]['breakfastAv'] == 0)
	{
		$(breakfNotAvCont).show();
		$(breakfActiveCont).hide();
		$(breakfNotActiveCont).hide();
		$(breakfActiveCont).find('option:last').attr('selected','selected');
	}
	//Если затрак доступен для заказа
	else
	{
		$(breakfNotAvCont).hide();
		//Если завтрак включён в стоимость, то не даём его выбирать
		if (rooms[selectedRoomId]['breakfastIncl'] == 1)
		{
			$(breakfActiveCont).hide();
			$(breakfNotActiveCont).show();
			$(breakfNote).hide();
		}
		else
		{
			//Если до этого выбор завтрака был скрыт, выбираем первый элемент списка
			if(!$(breakfActiveCont).is(':visible'))
			{
				$(breakfActiveCont).find('option:first').attr('selected','selected');
			}
			$(breakfActiveCont).show();
			$(breakfNotActiveCont).hide();
			$(breakfNote).show();
		}
	}
	//Проверяем, какие есть варианты размещения для номера
	var accom = rooms[selectedRoomId]['accomodation'];
	var roomAccSelect = $('#hotel-accommodation');
	//Если для номера возможно только одноместное размещение
	if (accom == 1)
	{
		//Если в списке для выбора доступно двухместное размещение, удаляем данный элемент
		if ($(roomAccSelect).find('option[value="2"]').length)
		{
			$(roomAccSelect).find('option[value="2"]').remove();
		}
		hotelAccUpdate();
	}
	else
	{
		//Если в списке нет пункта с выборов двухместного размещения, добавляем его
		if (!$(roomAccSelect).find('option[value="2"]').length)
		{
			var doubleAccElem = '<option value="2">'+$('#hotel-accommodation-2').text()+'</option>';
			$(roomAccSelect).append(doubleAccElem);
		}
	}
	//Проверяем возможность заказа доп. кровати для номера
	var extraBedAv = rooms[selectedRoomId]['extraBedAv'];
	var selectedAcc = $(roomAccSelect).find('option:selected').val();
	//Если выбрано 2-х местное размещение
	if (selectedAcc == 2)
	{
		//Если доп. кровать не доступна для заказа
		if (extraBedAv == 0)
		{
			/*
			 * Если до этого была выбрана доп. кровать, отменяем её выбор, загружаем заново
			 * прайсы
			 */
			if ($('#hotel-extra-bed').attr('checked'))
			{
				$('#hotel-extra-bed').attr('checked' , false);
				getRoomsAndPrices();
			}
			$('#hotel-extra-bed-label').hide();
			$('#hotel-extra-bed').hide();
		}
		//Если доп. кровать доступна для заказа
		else
		{
			$('#hotel-extra-bed-label').show();
			$('#hotel-extra-bed').show();
		}
	}
}

/**
 * Инициализация диалога для просмотра детальной информации по гостинице
 */
function hotelDetailsDialogInit()
{
	var dialogCont = $('#hotel-details-dialog');
	hoteldetailsDialog = $(dialogCont).dialog({
		width 		:	680,
		height		:	600,
		position 	:	['center',100],
		modal		:	true,
		resizable 	:	false,
		draggable 	:	true,
		autoOpen 	:	false,
		title		: 	$(dialogCont).find('#hotel-details-dialog-title').text()
	});
}
/**
 * Загрузка и отображение диалога с детальной информацией по гостинице
 */
function showHotelDetails(hotel_id)
{
    var dialogCont = $('#hotel-details-dialog');
    var loader = $(dialogCont).find('#hotel-details-loader');
    if (hotel_id==null) {
        var hotelId = $('#hotel-name option:selected').val();
    } else {
        var hotelId = hotel_id;
    }


    //Скрываем карту
    $('#hotel-map').hide();
    //Очищаем блок с контентом в диалоге
    $(dialogCont).find('#hotel-details-dialog-content').html('');
    //Отображаем диалог
    $(dialogCont).dialog('open');
    //Получаем информацию по гостинице
    $.ajax({
        type: 'POST',
        url: '/process/hotel.details/format/json/',
        data: 'hotelId='+hotelId,
        beforeSend: function(){
            $(loader).show();
        },
        success: function(result)
        {
            $(loader).hide();
          //  $('#hotel-map').show();
            //Отображаем полученную информацию
            $(dialogCont).find('#hotel-details-dialog-content').html(result.content);
            //Если необходимо, отображаем карту с гостиницей
            if (!hotelMapObj || !hotelMapObj.hotel || !$('#hotel-map').length)
            {
                return true;
            }
            //Если карта уже была иницализирована
            if (hotelMapObj.map)
            {
                //Отображаем гостиницу
                hotelMapObj.showHotel();
                //Делаем её resize
                google.maps.event.trigger(hotelMapObj.map, 'resize');
            }
            //Если карта ещё не была инициализирована
            else
            {
                //Инициализация карты
                hotelMapObj.gmLoad('hotelMapObj.hotelMapInit');
            }
        },
        complete: function(){
            $(loader).hide();
        }
    });
}

/**
 * Обновление элементов при смене типа размещения
 */
function hotelAccUpdate()
{
	var accommodation = parseInt($('#hotel-accommodation option:selected').val());
	var setType = accommodation;
	if (accommodation==2)
	{
		$('#hotel-bed-details').removeClass('hidden');
		$('#hotel-persons-line2').removeClass('hidden');
		var extraBed;
		if ($('#hotel-extra-bed').attr('checked')) 
		{
			$('#hotel-persons-line3').removeClass('hidden');
			setType++;
		}
		else 
		{
			$('#hotel-persons-line3').addClass('hidden');
		}	
	}
	else
	{
		$('#hotel-bed-details').addClass('hidden');
		$('#hotel-persons-line2').addClass('hidden');
		$('#hotel-persons-line3').addClass('hidden');
	}
	//Если ещё не было заказов и выбранное размещение соответствует количеству персон, а также не выбрана доп. кровать то распределяем персон по спискам
	var personsCount = $('#hotel-persons-all option').length-1;
	if ($('#ordered-hotel tbody.hotel-order').length==0 && setType==personsCount && !($('#hotel-extra-bed').attr('checked')))
	{
		for (i = 1; i <=setType; i++) 
		{
			var personsList = $('#hotel-persons'+i);
			$(personsList).find('option').removeAttr('selected');
			$(personsList).find('option:eq('+i+')').attr('selected','selected');
			var selectedElemVal = $(personsList).find('option:selected').val();
			$(personsList).find('option[value!="'+selectedElemVal+'"][value!="0"]').remove();
		}
	}
	//Возвращаем списки выбора персон в исходное состояние
	else
	{
		hotelPersonsListsReset();
	}
}

/**
 * Контроль выбранных персон
 * @param {Object} selectElement Список, в котором изменилось значение
 */
function hotelPersonsSelection(selectElement)
{
	//Номер списка, в котором был сделан выбор
	var selectElementNum = parseInt($(selectElement).attr('id').replace('hotel-persons',''));
	//Значение выбранного элемента списка
	var selectElementVal = $(selectElement).find('option:selected').val();
	//Если выбран элемент по умолчанию
	if(selectElementVal==0)
	{
		return false;
	}
	//Выбранный тип проживания
	var accommodation = parseInt($('#hotel-accommodation option:selected').val());
	//Для одноместного размещения функция не нужна
	if (accommodation==1)
	{
		return false;
	}
	var setType = accommodation;
	if ($('#hotel-extra-bed').attr('checked')) 
	{
		setType++;
	}
	//Проходимся по всем спискам выбора персон (кол-во зависит от выбранного размщения)
	for (i = 1; i <=setType; i++) 
	{
		//Если список не тот, с которым мы работаем
		if (i!=selectElementNum)
		{
			//Удаляем в списках выбранный в текущем списке элемент
			$('#hotel-persons'+i+' option[value="'+selectElementVal+'"]').remove();
		}
	}
}

/**
 * Возвращение списков выбора персон в исходное состояние (список по умолчанию берётся из скрытого элемента)
 */
function hotelPersonsListsReset()
{
	//Получаем список по умолчанию из скрытого элемента
	var defaultHotelPersonList = $('#hotel-persons-all').html();
	//Копируем список по умолчанию во все списки
	$('#hotel-persons-block select').each(function(){
		$(this).html(defaultHotelPersonList);
	});
}

/**
 * Удаление заказа на проживание из таблицы заказов
 * @param int serviceId
 */
function deleteHotelOrder(serviceId)
{
	var hotelOrdersTable = $('#ordered-hotel');
	$(hotelOrdersTable).find('#hotel-order'+serviceId).remove();
	var ordersCount = $(hotelOrdersTable).find('tbody.hotel-order').length;
	//Если нет заказов
	if (ordersCount == 0) 
	{
		//Отображаем строку, сообщающую об отсутствии заказов
		$(hotelOrdersTable).find('#hotel-empty-order').removeClass('hidden');
		//Меняем текст ссылки для добавления номера
		$('#hotel-add-more-text').addClass('hidden-imp');
		$('#hotel-add-text').removeClass('hidden-imp');
		//Если нет заказов, то распределяем персоны по номерам
		hotelAccUpdate();
	}
}


/**
 * Класс для вывода карты гостиницы
 */
function HotelMap(){
	this.hotel = null,
	this.map = null,
	this.hotelsMarkers = [],
	this.infoWindow = null
}
HotelMap.prototype = {
	/**
	 * Загрузка скриптов для google maps
	 */
	gmLoad : function(callback)
	{
		var gmKey = '';
		//Если скрипты уже не загружены, то загружаем
		if(!$('script[src*="maps.google.com"]').length)
		{
            alert ('not loaded!');
			var script = document.createElement('script');
			script.type = 'text/javascript';
			script.src = 'https://maps.google.com/maps/api/js?v=3.8&sensor=false&region=RU&key=' + gmKey + '&language=' + sysLang;
			if (callback && callback != '')
			{
				script.src += '&callback=' + callback;
			}
			document.body.appendChild(script);
		}
	},
	/**
	 * Инициализация карты с гостиницей
	 */
	hotelMapInit : function()
	{
		var selfObj = this;
		//Инициализация карты с заданными параметрами
		var myOptions = {
			zoom: 14,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		var map = new google.maps.Map(document.getElementById('hotel-map'), myOptions);
		this.map = map;
		google.maps.event.trigger(this.map, 'resize');
		//Создаём объект всплывающего окна InfoWindow
		selfObj.infowindow = new google.maps.InfoWindow({ maxWidth : 350 });
		//Скрываем всплывающее окно при клике на карту
		google.maps.event.addListener(selfObj.map, 'click', function(){
			if (selfObj.infowindow)
			{
				selfObj.infowindow.close();
			}
		});	
		//Отображаем маркер с гостиницей
		selfObj.showHotel();
	},
	/**
	 * Отображение маркера с гостиницей на карте
	 */
	showHotel : function()
	{
		var selfObj = this;
		//Удаляем имеющиеся маркеры
		if (selfObj.hotelsMarkers.length)
		{
			for (i in selfObj.hotelsMarkers)
			{
				selfObj.hotelsMarkers[i].setMap(null);
			}
			selfObj.hotelsMarkers.length = 0;
		}
		//Устанваливаем центр карты
		var mapCenter = new google.maps.LatLng(selfObj.hotel.coords[0], selfObj.hotel.coords[1]);
		this.map.setCenter(mapCenter);
		//Формируем объект маркера, добавляем на карту
		var hotelLatLng = new google.maps.LatLng(selfObj.hotel.coords[0], selfObj.hotel.coords[1]);
		var marker = new google.maps.Marker({
			position: hotelLatLng,
			map : selfObj.map,
			title : selfObj.hotel.name
		});
		selfObj.hotelsMarkers.push(marker);
		//Формируем объект окна, всплывающего при нажати на маркер
		var infowindowText = '<div style="font-size:13px; margin:3px 0 10px 0;text-align:left;" class="bolder">' + selfObj.hotel.name + '&nbsp;&nbsp;';
		infowindowText += '<img style="margin-bottom: -0.2em;" src="/img/' + selfObj.hotel.stars + 'stars.gif" alt=" " width="88" height="16" /></div>'
		infowindowText += '<div style="text-align:left;">' + selfObj.hotel.address + '</div>';
		//Создаём всплывающее окно для маркера
		selfObj.recHotelInfoWindow(marker, infowindowText);
	},
	/**
	 * Создание всплывающего окна для маркера гостиницы
	 */
	recHotelInfoWindow : function(marker, content)
	{
		var selfObj = this;
		google.maps.event.addListener(marker, 'click', function() {
			selfObj.infowindow.setContent(content);
	    	selfObj.infowindow.open(selfObj.map, marker);
	    });
	}
}

/* ----------------------------------| Транспорт |-------------------------------------------------------*/

/**
 * Заказ транспорта
 */
function addTransport()
{
	//Заказ на транспорт
	var transportOrder = {};
	transportOrder['order'] = {
		'transpServiceId' 		: 	$('#transfer-type option:selected').val(),
		'transpServiceName' 	: 	$('#transfer-type option:selected').text(),
		'transpTypeId' 			: 	$('#transport-type option:selected').val(),
		'transpTypeName' 		: 	$('#transport-type option:selected').text(),
		'date'					:	$('#transport-date').val(),
		'time'					:	$('#transport-time').val(),
		'number'				:	$('#transport-number').val(),
		'comment'				:	$('#transport-comment').val()
	};
	//Определяем категорию транспорта, а также был ли выбор вокзала для услуги, на основе этого считываем нужные параметры
	var transportCategory = parseInt($('#transport-services-category option[value="'+transportOrder['order']['transpServiceId']+'"]').text());
	var transportStationSelect = parseInt($('#transport-services-station-select option[value="'+transportOrder['order']['transpServiceId']+'"]').text());
	if(transportCategory == 2)
	{
		transportOrder['order']['train_car'] = $('#transport-car').val();
	}
	else
	{
		transportOrder['order']['train_car'] = '';
	}
	if (transportStationSelect == 1)
	{
		transportOrder['order']['station'] = $('#transport-station option:selected').text();
		transportOrder['order']['stationId'] = $('#transport-station option:selected').val();
	}
	else
	{
		transportOrder['order']['station'] = '';
	}
	//Определяем, заказана ли бесплатная услуга
	transportOrder['order']['isFree'] = parseInt($('#transport-services-isfree option[value="'+transportOrder['order']['transpServiceId']+'"]').text());
	//Получаем список выбранных персон
	transportOrder['order']['persons'] = getSelectedPersons('#transport-persons-block');
	//Если персоны не выбраны
	if (!transportOrder['order']['persons'])
	{
		alert(messages['reg']['all']['transportMissParams']);
		return false;
	}
	//Город
	transportOrder['order']['cityId'] = window.curCityId;
	//Отправляем данные на сервер
	transportSave(transportOrder);

    $('#transfer_form_cont').hide();
	
}

/**
 * Передача данных по заказу транспорта на сервер для проверки и сохранения в БД
 * @param {Object} transportOrder
 */
function transportSave(transportOrder)
{
	var loaderElem = '#transport-loader';
	var loaderContainer = '#ordered-transport';
	$.ajax({
		type: 'POST',
		url: '/process/add.transport/format/json/',
		dataType: 'json',
		data: transportOrder,
		beforeSend: function(){
			ajaxLoaderContainer('show',loaderElem,loaderContainer);
		},
		success: function(result) 
		{
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
			var status = parseInt(result.status);
			//Если в результате ошибка, то выводим её
			if (status == 0) 
			{
				alert(result.messages);
			}
			//Если записи успешно добавлены, добавляем строку
			else
			{	
				//Скрываем строку, сообщающую об отсутствии заказов
				$('#ordered-transport tr.empty-order-row').addClass('hidden');
				//Добавляем строку с заказом
				$('#ordered-transport').append(result.content);
				//Меняем текст ссылки
			//	$('#transport-add-more-text').removeClass('hidden-imp');
			//	$('#transport-add-text').addClass('hidden-imp');
				//Очищаем поля
				$('#transport-number').val('');
				$('#transport-time').val('00:00');
				$('#transport-car').val('');
				$('#transport-comment').val('');
				//Отменяем выбор персон
				$('#transport-persons-block input').removeAttr('checked');	
				//Скроллинг к таблице заказов
				var scrollToCont = document.getElementById('transport-comment');
				scrollToCont.scrollIntoView(true);
			}
		},
		complete: function(){
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
		}
	});
}

//Устанавливаем события для списков выбора транспортной услуги, типа транспорта
function transportListsEvents()
{
	var trasfersList = $('#transfer-type');
	//При обновлении страницы выбираем 1 услугу в списке
	$(trasfersList).find('option:first').attr('selected','selected');
	/*
	 * Блокируем календарь, если первая выбранная услуга - бесплатная
	 */
	var selectedService = $(trasfersList).find('option:selected').val();
	var transpTypesLine = $('#transport-types-line');
	var transportIsFree = parseInt($(transpTypesLine).find('#transport-services-isfree option[value="'+selectedService+'"]').text());
	//Если услуга бесплатная, то ставим фиксированную дату и блокируем её выбор, скрываем строку с выбором типа транспорта
	if(transportIsFree == 1)
	{
		$('#transport-date').datepicker('disable');
	}
	/*
	 * Обновление строки с максимальным количеством персон при выборе транспорта, котроль выбранных персон
	 * (если больше максимального, то убираем выбор последних).
	 */
	$('#transport-type').bind('change',function(){
		//Выбранный тип транспорта
		var transportTypeId = $(this).find('option:selected').val();
		//Получаем максимальное кол-во персон из скрытого списка и записываем его в нужный элемент
		var transportMaxPersons = $('#transport-types-max-persons option[value="'+transportTypeId+'"]').text();
		$('#transport-max-persons-span').text(transportMaxPersons);
		//Если количество выбранных персон получается больше, чем максимальное, то отменяем выбор "лишних" персон
		var selectedPersonsCount = $('#transport-persons-block input:checked').length;
		if (selectedPersonsCount>transportMaxPersons)
		{
			transportMaxPersons--;
			$('#transport-persons-block input:gt('+transportMaxPersons+')').removeAttr('checked');
		}
	});
	/*
	 * Контроль максимального количества персон
	 */
	$('#transport-persons-block input').bind('click',function() {
	//Максимальное количество персон
		var transportMaxPersons = $('#transport-max-persons-span').text();
		var selectedPersonsCount = $('#transport-persons-block input:checked').length;
		//Если количество выбранных персон получается больше, чем максимальное
		if (selectedPersonsCount>transportMaxPersons)
		{
			return false;
		}
	});
	/*
	 * Скрытие/отображение строчки с выбором вокзала и номера вагона в зависимости от типа выбранной услуги (ж/д, авиа или др.).
	 * Задание фиксированных дат и блокирование полей для бесплатных услуг.
	 */
	$(trasfersList).bind('change',function() {
		var selectedService = $(trasfersList).find('option:selected').val();
		var transpTypesLine = $('#transport-types-line');
		var transportIsFree = parseInt($(transpTypesLine).find('#transport-services-isfree option[value="'+selectedService+'"]').text());
		var transportCategory = parseInt($(transpTypesLine).find('#transport-services-category option[value="'+selectedService+'"]').text());
		var transportStationSelect = parseInt($(transpTypesLine).find('#transport-services-station-select option[value="'+selectedService+'"]').text());
		var transportFixDate = $(transpTypesLine).find('#transport-services-fixdate option[value="'+selectedService+'"]').text();
		//Если для услуги требуется выбор вокзала, то открываем строку с выбором
		var stationLine = $('#transport-station-line');
		if (transportStationSelect!=1)
		{
			$(stationLine).addClass('hidden');
		}
		else
		{
			$(stationLine).removeClass('hidden');
		}
		//Если для услуги требуется ввод номера вагона, то отображаем соответствующие поля
		var transportCarElements = $('#main-form-step3 .transport-car-elements');
		if (transportCategory!=2)
		{
			$(transportCarElements).addClass('hidden');
		}
		else
		{
			$(transportCarElements).removeClass('hidden');
		}
		//Если услуга бесплатная, то ставим фиксированную дату и блокируем её выбор, скрываем строку с выбором типа транспорта
		if(transportIsFree == 1)
		{
			$('#transport-types-line').hide();
			$('#transport-date').val(transportFixDate).attr('disabled','true').datepicker('disable');
		}
		else
		{
			$('#transport-types-line').show();
			$('#transport-date').val($(transpTypesLine).find('#transport-services-def-date').val()).removeAttr('disabled').datepicker('enable');
		}
	});
}
/* ----------------------------------| Питание |----------------------------------------------------------*/
/**
 * Смена доступных для выбора типов питания при выборе экскурсии
 */
function changeMealTypes()
{
	var curMealId = $('#meal-name option:selected').val();
	var mealPeriodsCont = '';
	var curTypeCont = '';
	///Получаем возможные даты/периоды для экскурсии
	$.ajax({
		type: 'POST',
		url: '/process/meal.types/format/json/',
		dataType: 'json',
		data: {'mealId' : curMealId},
		success: function(result) 
		{
			var mealTypesLineCont = $('#meal-type-select-line');
			//Если для банкета найдены типы питания
			if (result.count > 0) 
			{
				$('#meal-type option').remove();
				//Формируем элементы списка периодов и добавляем их в список
				for (var i in result.types) {
					mealPeriodsCont += '<option value="' + result.types[i]['id'] + '">' + result.types[i]['name'] + '</option>';
				}
				$('#meal-type').append(mealPeriodsCont);
				$(mealTypesLineCont).show();
			}
			//Если типы питания не найдены
			else
			{
				$(mealTypesLineCont).hide();
			}
		}
	});
}

/**
 * Заказ банкета
 */
function addMeal()
{
	var mealId = $('#meal-name option:selected').val();
	//Заказ на банкет
	var mealOrder = {};
	mealOrder['order'] = {
		'mealId' 		: 	mealId,
		'mealName' 		: 	ltrim(rtrim($('#meal-name option:selected').text())),
		'mealTypeId' 	: 	$('#meal-type option:selected').val(),
		'mealTypeName' 	: 	ltrim(rtrim($('#meal-type option:selected').text())),
		'comment' 		: 	$('#meal-comment').val()
	};
	//Получаем список выбранных персон
	mealOrder['order']['persons'] = getSelectedPersons('#meal-persons-block');
	//Если персоны не выбраны
	if (!mealOrder['order']['persons'])
	{
		alert(messages['reg']['all']['mealMissParams']);
		return false;
	}
	//Отправляем данные на сервер
	mealSave(mealOrder);
}

/**
 * Передача данных по заказу банкета на сервер для проверки и сохранения в БД
 * @param {Object} mealOrder
 */
function mealSave(mealOrder)
{
	var loaderElem = '#meal-loader';
	var loaderContainer = '#ordered-meal';
	$.ajax({
		type: 'POST',
		url: '/process/add.meal/format/json/',
		dataType: 'json',
		data: mealOrder,
		beforeSend: function(){
			ajaxLoaderContainer('show',loaderElem,loaderContainer);
		},
		success: function(result) 
		{
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
			var status = parseInt(result.status);
			//Если в результате ошибка, то выводим её
			if (status == 0) 
			{
				alert(result.messages);
			}
			//Если записи успешно добавлены, добавляем строку
			else
			{
				//Скрываем строку, сообщающую об отсутствии заказов
				$('#ordered-meal tr.empty-order-row').addClass('hidden');
				//Добавляем строку с заказом
				$('#ordered-meal').append(result.content);
				//Меняем текст ссылки
			//	$('#meal-add-more-text').removeClass('hidden-imp');
			//	$('#meal-add-text').addClass('hidden-imp');
				//Отменяем выбор персон
				$('#meal-persons-block input').removeAttr('checked');
				//Очищаем поле с комментарием
				$('#meal-comment').val('');
				//Скроллинг к таблице заказов
				var scrollToCont = document.getElementById('meal-comment');
				scrollToCont.scrollIntoView(true);
			}
		},
		complete: function(){
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
		}
	});
}


/* ----------------------------------| Экскурсии |----------------------------------------------------------*/
/**
 * Назначение событий для эскурсий
 */
function excursionEvents()
{
	/*
	 * Контроль максимального количества персон
	 */
	$('#excursion-persons-block').bind('click',function() {
		//Определяем максимальное количество персон
		var curExcursionId = $('#excursion-name option:selected').val();
		var withAcc = parseInt($('#excursion-with-acc option[value="' + curExcursionId + '"]').text());
		if (withAcc == 0)
		{
			return true;
		}
		var maxPersons = $('#excursion-accommodation option:selected').val();
		if ($('#excursion-extra-bed').attr('checked'))
		{
			maxPersons++;
		}
		//Если количество выбранных персон получается больше, чем максимальное
		var selectedPersonsCount = $('#excursion-persons-block input:checked').length;
		if (selectedPersonsCount > maxPersons)
		{
			return false;
		}
	});
	//Отмена выбора персон
	$('#excursion-extra-bed').click(function(){
		$('#excursion-persons-block input:checked').removeAttr('checked');
	});
}

/**
 * Смена доступных для выбора периодов/дат при выборе экскурсии
 */
function changeExcursionPeriods()
{
	var curExcursionId = $('#excursion-name option:selected').val();
	/* 
	 * Если для экскурсии необходимо выбрать тип размещения, отображаем блок для выбора,
	 * а также ограничиваем количество персон для выбора в соответствии с размещением
	 */
	var withAcc = parseInt($('#excursion-with-acc option[value="' + curExcursionId + '"]').text());
	if (withAcc==1)
	{
		$('#excursion-accommodation option').removeAttr('selected');
		$('#excursion-accommodation option:first').attr('selected','selected');
		$('#excursion-extra-bed').removeAttr('checked');
		$('#excursion-accommodation-line').css('display','block');
	}
	else
	{
		$('#excursion-accommodation-line').css('display','none');
		$('#excursion-hotel-bed-details').addClass('hidden');
	}
	//Получаем возможные даты/периоды для экскурсии
	var excursionPeriodsCont = '';
	$.ajax({
		type: 'POST',
		url: '/process/excursion.periods/format/json/',
		dataType: 'json',
		data: {'excursionId' : curExcursionId},
		success: function(result) 
		{
			$('#excursion-period option').remove();
			//Формируем элементы списка периодов и добавляем их в список
			for (var i in result.periods)
			{
				if (result.periods[i]['start_date'] == result.periods[i]['end_date'])
				{
					curPeriod = result.periods[i]['start_date'];
				}
				else
				{
					curPeriod = result.periods[i]['start_date'] + ' - ' + result.periods[i]['end_date'];
				}
				excursionPeriodsCont += '<option value="'+result.periods[i]['id']+'">' + curPeriod + '</option>';
			}
			$('#excursion-period').append(excursionPeriodsCont);
			//Если уже были выбраны персоны, отменяем выбор
			$('#excursion-persons-block input:checked').removeAttr('checked');
		}
	});
}

/**
 * Обновление элементов при смене типа размещения
 */
function excursionAccUpdate()
{
	var accommodation = parseInt($('#excursion-accommodation').val()); 
	if (accommodation==2)
	{
		$('#excursion-hotel-bed-details').removeClass('hidden');
	}
	else
	{
		$('#excursion-hotel-bed-details').addClass('hidden');
	}
	//Если уже были выбраны персоны, отменяем выбор
	$('#excursion-persons-block input:checked').removeAttr('checked');
}

/**
 * Заказ экскурсии
 */
function addExcursion()
{
	var excursionId = $('#excursion-name option:selected').val();
	//Заказ на экскурсию
	var excursionOrder = {};
	excursionOrder['order'] = {
		'excursionId' 	: 	excursionId,
		'name' 			: 	ltrim(rtrim($('#excursion-name option:selected').text())),
		'periodId' 		: 	$('#excursion-period option:selected').val(),
		'comment' 		: 	$('#excursion-comment').val()
	};
	//Параметры размещения
	var withAcc = parseInt($('#excursion-with-acc option[value="' + excursionId + '"]').text());
	if (withAcc == 0)
	{
		excursionOrder['order']['withAcc'] = 0;
	}
	else
	{
		excursionOrder['order']['withAcc'] = 1;
		var accommodation = parseInt($('#excursion-accommodation').val());
		if (accommodation > 1 && $('#excursion-extra-bed').attr('checked'))
		{
			accommodation++;
		}
		if (accommodation > 1)
		{
			excursionOrder['order']['bedType'] = $('#excursion-hotel-bed-type option:selected').val();
		}
		excursionOrder['order']['accommodation'] = accommodation;
	}
	//Получаем список выбранных персон
	excursionOrder['order']['persons'] = getSelectedPersons('#excursion-persons-block');
	//Если персоны не выбраны
	if (!excursionOrder['order']['persons'])
	{
		alert(messages['reg']['all']['excursionMissParams']);
		return false;
	}
	//Отправляем данные на сервер
	excursionSave(excursionOrder);
}

/**
 * Передача данных по заказу экскурсий на сервер для проверки и сохранения в БД
 * @param {Object} excursionOrder
 */
function excursionSave(excursionOrder)
{
	var loaderElem = '#excursion-loader';
	var loaderContainer = '#ordered-excursion';
	$.ajax({
		type: 'POST',
		url: '/process/add.excursion/format/json/',
		dataType: 'json',
		data: excursionOrder,
		beforeSend: function(){
			ajaxLoaderContainer('show',loaderElem,loaderContainer);
		},
		success: function(result) 
		{
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
			var status = parseInt(result.status);
			//Если в результате ошибка, то выводим её
			if (status == 0) 
			{
				alert(result.messages);
			}
			//Если записи успешно добавлены, добавляем строку
			else
			{
				//Скрываем строку, сообщающую об отсутствии заказов
				$('#ordered-excursion tr.empty-order-row').addClass('hidden');
				//Добавляем строку с заказом
				$('#ordered-excursion').append(result.content);
				//Меняем текст ссылки
				$('#excursion-add-more-text').removeClass('hidden-imp');
				$('#excursion-add-text').addClass('hidden-imp');
				//Отменяем выбор персон
				$('#excursion-persons-block input').removeAttr('checked');
				//Очищаем поле с комментарием
				$('#excursion-comment').val('');
				//Скроллинг к таблице заказов
				var scrollToCont = document.getElementById('excursion-comment');
				scrollToCont.scrollIntoView(true);
			}
		},
		complete: function(){
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
		}
	});
}

/**
 * Завершение процесса добавления услуг путём подключения регистрационной формы
 */
function orderFromPpcComplete()
{
	//Отправляем данные на сервер
	$.ajax({
		type: 'POST',
		url: '/process/order.from.ppc.complete/',
		beforeSend: function(){
			$('#ppc-order-loader').show();
		},
		success: function(result) 
		{
			$('#ppc-order-loader').hide();
			window.parent.location.href = '/ppc/order/finish/';
		},
		complete: function(){
			$('#ppc-order-loader').hide();
		}
	});
}


/* ----------------------------------| Другое |----------------------------------------------------------*/

/**
 * Заказ услуг типа "Другое"
 */
function addOther()
{
	//Заказ на услугу
	var otherOrder = {};
	otherOrder['order'] = {
		'serviceId' : $('#other-service option:selected').val(),
		'comment' : $('#other-comment').val()
	};
	//Получаем список выбранных персон
	otherOrder['order']['persons'] = getSelectedPersons('#other-persons-block');
	//Если персоны не выбраны
	if (!otherOrder['order']['persons'])
	{
		alert(messages['reg']['all']['mealMissParams']);
		return false;
	}
	//Отправляем данные на сервер
	otherSave(otherOrder);
}

/**
 * Передача данных по заказу услуг на сервер для проверки и сохранения в БД
 * @param {Object} otherOrder
 */
function otherSave(otherOrder)
{
	var loaderElem = '#other-loader';
	var loaderContainer = '#ordered-other';
	$.ajax({
		type: 'POST',
		url: '/process/add.other/format/json/',
		dataType: 'json',
		data: otherOrder,
		beforeSend: function(){
			ajaxLoaderContainer('show',loaderElem,loaderContainer);
		},
		success: function(result) 
		{
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
			var status = parseInt(result.status);
			//Если в результате ошибка, то выводим её
			if (status == 0) 
			{
				alert(result.messages);
			}
			//Если записи успешно добавлены, добавляем строку
			else
			{
				//Скрываем строку, сообщающую об отсутствии заказов
				$('#ordered-other tr.empty-order-row').addClass('hidden');
				//Добавляем строку с заказом
				$('#ordered-other').append(result.content);
				//Меняем текст ссылки
				$('#other-add-more-text').removeClass('hidden-imp');
				$('#other-add-text').addClass('hidden-imp');
				//Отменяем выбор персон
				$('#other-persons-block input').removeAttr('checked');
				//Очищаем поле с комментарием
				$('#other-comment').val('');
				//Скроллинг к таблице заказов
				var scrollToCont = document.getElementById('other-comment');
				scrollToCont.scrollIntoView(true);
			}
		},
		complete: function(){
			ajaxLoaderContainer('hide',loaderElem,loaderContainer);
		}
	});
}





/**
 * Завершение процесса добавления услуг путём подключения регистрационной формы
 */
function orderFromPpcComplete()
{
	//Отправляем данные на сервер
	$.ajax({
		type: 'POST',
		url: '/process/order.from.ppc.complete/',
		beforeSend: function(){
			$('#ppc-order-loader').show();
		},
		success: function(result) 
		{
			$('#ppc-order-loader').hide();
			window.parent.location.href = '/ppc/order/finish/';
		},
		complete: function(){
			$('#ppc-order-loader').hide();
		}
	});
}


/* ----------------------------------| Общие функции |-------------------------------------------------------*/

/**
 * Получения списка выбранных персон
 * @param {Object} persContainer Контейнер (элемент), в котором находятся поля для выбора персон 
 */
function getSelectedPersons(persContainer)
{
	//Получаем количество выбранных персон
	var selPersons = $(persContainer).find('input:checked');
	var personsCount = selPersons.length;
	//Проверяем данные
	if (personsCount == 0)
	{
		return false;
	}
	//Объект, содержащий выбранные персоны
	var persons={};
	//Получаем список выбранных персон
	var i=0;
	$(selPersons).each(function(){
		persons[i] = {};
		persons[i]['id'] = $(this).val();
		persons[i]['name'] = trim($(this).next().text());
		i++;
	});
	return persons;
}

/**
 * Удаление заказанной усуги
 */
function deleteService(serviceId,type,loaderElem,loaderContainer)
{
	if (!confirm(messages['reg']['all']['serviceDelConf'])) 
	{
		return false;
	}
	if (!loaderElem)
	{
		loaderElem = '#default';
	}
	//Отправляем запрос на удаление
	$.ajax({
		type: 'POST',
		url: '/process/delete.service/format/json/',
		dataType: 'json',
		data: 'id='+serviceId+'&type='+type,
		beforeSend: function(){
			if (loaderElem == 'default') 
			{
				ajaxLoader('show');
			}
			else
			{
				ajaxLoaderContainer('show',loaderElem,loaderContainer);
			}
		},
		success: function(result) 
		{
			if (loaderElem == 'default') 
			{
				ajaxLoader('hide');
			}
			else
			{
				ajaxLoaderContainer('hide',loaderElem,loaderContainer);
			}
			var status = parseInt(result.status);
			//Если в результате ошибка, то выводим сообщение
			if (status == 0) 
			{
				alert(messages['reg']['all']['deleteError']);
			}
			//Если удаление прошло успешно, то удаляем строку в таблице
			else
			{
				//Если тип услуги - проживание
				if (type == 'hotel') 
				{
					deleteHotelOrder(serviceId);
				}
				//Если тип услуги отличный от проживания
				else 
				{
					$('#ordered-' + type + '-row' + serviceId).remove();
					var ordersCount = $('#ordered-' + type + ' tr.ordered-' + type + '-row').length;
					if (ordersCount == 0) 
					{
						//Скрываем строку, сообщающую об отсутствии заказов
						$('#ordered-' + type + ' tr.empty-order-row').removeClass('hidden');
						//Меняем текст ссылки для добавления номера
					//	$('#' + type + '-add-more-text').addClass('hidden-imp');
					//	$('#' + type + '-add-text').removeClass('hidden-imp');
					}
				}
			}
		},
		complete: function(){
			
		}
	});
}


/**
 * Диалоги с подробной информацией
 */
function showInfo(section,title,dialogParams)
{
	//Для ie6 и определённых секций выставляем фиксированную высоту
	if (detectIe6())
	{
		height=500;
	}
	else
	{
		height='auto';
	}
	$('#'+section+'-help').dialog('destroy');
	var defParams = {
		width : 680,
		height: height,
		title : title,
		position : ['center',200],
		modal: true,
		resizable : false,
		draggable : true,
		open: function(event, ui) {
			$(this).scrollTop(0);
		}
	};
	if (dialogParams)
	{
		$.extend(defParams, dialogParams);
	}
	$('#'+section+'-help').dialog(defParams);
}



function step3Tooltips()
{
	$('#transport-date, #transport-time').tooltip();
	$('#transport-service-help').tooltip({
			bodyHandler: function(){
				return $('#transport-help-tooltip').html();
			},
			showURL: false
	});
	if ($('#excursion-service-help').length) 
	{
		$('#excursion-service-help').tooltip({
			bodyHandler: function(){
				return $('#excursion-help-tooltip').html();
			},
			showURL: false
		});
	}
	if ($('#meal-service-help').length) 
	{
		$('#meal-service-help').tooltip({
			bodyHandler: function(){
				return $('#meal-help-tooltip').html();
			},
			showURL: false
		});
	}
}

/**
 * Асинхронная загрузка справочной информации для указанного шага
 * @param int step
 * @param Object container
 */
function loadHelpsAsync(step, container, cityId)
{
	if (!cityId)
	{
		cityId = null;
	}
	$.ajax({
		type: 'POST',
		url: '/index/helps/format/json/',
		dataType: 'json',
		data: {step : step, cityId : cityId},
		success: function(result) 
		{
			if (result.status == 1)
			{
				$(container).before(result.content);
			}
		}
	});
}
/**
 * Скрытие/отображение формы заказа
 */
function formBodyToggle(formCont)
{
	var formBody = $(formCont).find('div.toogle-form-body');
	var toggleControls = $(formCont).find('.toogle-form-header .link');
	var toggleOpenControl = $(formCont).find('.toggle-closed');
	var toggleCloseControl = $(formCont).find('.toggle-opened');
	$(toggleControls).disableTextSelect();
	$(toggleControls).click(function(){
		if ($(this).hasClass('toggle-opened'))
		{
			$(toggleOpenControl).removeClass('hidden');
			$(this).addClass('hidden');
			$(formBody).hide();
		}
		else
		{
			$(toggleCloseControl).removeClass('hidden');
			$(this).addClass('hidden');
			$(formBody).show();
		}
	});
}





/* --------------------| Функциии, вызывающиеся в момент $(document).ready для разных шагов формы |----------------*/
/**
 * Функции инициализации для шагов
 */
var reg = {
	step1 : {
		init: function()
		{
			formBodyToggle($('#part-info'));
			var personsForm = $('#persons-form');
			// Подсказки
			personsForm.find('.tooltip-show').tooltip();
			// Добавление персоны
			$('.add-link').live('click', function(){
				$(this).parent().find('.person-loader').show();
				addPerson();
			});
			// Удаление персоны
			personsForm.find('.delete-person').live('click', function(){
				var personNum = parseInt($(this).attr('id').replace('delete-person', ''));
				deleteNewPersonForm(personNum);
			});
			// Скрытие окна с ошибками
			if ($('#persons-attention').length)
			{
				setTimeout(function() { $('#persons-attention').fadeOut(900) }, 9000);
			}

            // Генерация имени для бейджа
            personsForm.delegate('.p-fname-elem, .p-lname-elem', 'change', function(){
                var formBody = $(this).closest('.person-body'),
                    badgeName = formBody.find('.p-bname-elem');
                if (badgeName.hasClass('changed-val'))
                {
                    return;
                }
                var fname = trim(formBody.find('.p-fname-elem,').val()),
                    lname = trim(formBody.find('.p-lname-elem').val());
                if (fname != '' && lname != '')
                {
                    badgeNameStr = lname.capitalize() + ' ' + fname.capitalize();
                    badgeNameStr = badgeNameStr.translit();
                    badgeName.val(badgeNameStr);
                    badgeName.addClass('generated-val');
                }
            });
            personsForm.delegate('.p-bname-elem', 'change', function(){
                if ($(this).hasClass('generated-val'))
                {
                    $(this).removeClass('generated-val');
                    $(this).addClass('changed-val');
                }
            });
            // Генерация названия компании для бейджа
            personsForm.delegate('.p-company-elem', 'change', function(){
                var formBody = $(this).closest('.person-body'),
                    badgeComp = formBody.find('.p-bcompany-elem');
                if (badgeComp.hasClass('changed-val'))
                {
                    return;
                }
                var company = trim(formBody.find('.p-company-elem,').val());
                if (company != '')
                {
                    badgeCompStr = company.capitalize();
                    badgeCompStr = badgeCompStr.translit();
                    badgeComp.val(badgeCompStr);
                    badgeComp.addClass('generated-val');
                }
            });
            personsForm.delegate('.p-bcompany-elem', 'change', function(){
                if ($(this).hasClass('generated-val'))
                {
                    $(this).removeClass('generated-val');
                    $(this).addClass('changed-val');
                }
            });
		}
	},

    step22 : {
        init: function()
        {
            $('.lector-ab').each(function(){
                if($(this).val()=='--'){
                    $(this).parent().hide();
                }

            });

            // Смена статуса тезиса
            var abstractForm = $('#abstracts-form');

            if($)

            var abstractDoclad = function(formab,choose)
                {

                    if(choose==2 || choose==3)
                    {
                        formab.val('');
                        formab.parent().show();

                    }
                    else
                    {

                        formab.val('--');
                        formab.parent().hide();
                    }
                };


            var docDel = function(doc)
            {
                $.ajax({
                    type: 'GET',
                    url: '/async/delete/',
                    data: 'doc=' + doc.attr('data'),
                    success: function(result){
                        if(result=="ok"){
                            doc.hide();
                            doc.parent().hide();
                        }
                    }

                });
            }


          var abstractStatusCh = function(abstractCont, status)
            {
                var formBody = abstractCont.find('.abstract-body'),
                    fillAct = abstractCont.find('.fill-abstract'),
                    deleteAct = abstractCont.find('.delete-abstract'),
                    statusEl = abstractCont.find('.status-el'),
                    curStatus = statusEl.val();
                // Идёт добавление тезиса
                if (status == 1)
                {
                    formBody.removeClass('hidden');
                    fillAct.addClass('hidden');
                    deleteAct.removeClass('hidden');
                    formBody.find('.errors').remove();
                    statusEl.val(status);
                }
                // Идёт удаление тезиса
                else if (status == 2)
                {
                    // Если тезис уже был добавлен ранее, выводим подтверждение удаления
                    if (curStatus == 3 && !confirm(window.abstractDelConf))
                    {
                        console.log('return');
                        return;
                    }
                    formBody.find('input:text, textarea').val('');
                    formBody.addClass('hidden');
                    fillAct.removeClass('hidden');
                    deleteAct.addClass('hidden');
                    statusEl.val(status);
                }
            };


            abstractForm.delegate('.fill-abstract', 'click', function() {
                abstractStatusCh($(this).closest('.abstract-f'), 1);
            });
            abstractForm.delegate('.delete-abstract', 'click', function() {
                abstractStatusCh($(this).closest('.abstract-f'), 2);
            });

            abstractForm.delegate('.del-doc', 'click', function() {
                docDel($(this));
            });


            abstractForm.delegate('.thes-form', 'change', function() {
                abstractDoclad($(this).parent().parent().find('.lector-ab'),$(this).val());

            });




            $("#section0, #section1, label[for='section0'], label[for='section1']").tooltip({
                bodyHandler: function(){
                    return $('#p-section').html();
                },
                showURL: false
            });

            $("#form0, #form1, label[for='form0'], label[for='form1']").tooltip({
                bodyHandler: function(){
                    return $('#p-form').html();
                },
                showURL: false
            });

            $("#authors0, #authors1,#authors20,#authors21 ,label[for='authors0'], label[for='authors1'],label[for='authors20'], label[for='authors21']").tooltip({
                bodyHandler: function(){
                    return $('#p-authors').html();
                },
                showURL: false
            });

            $("#company0, #company1,#company20,#company21 ,label[for='company0'], label[for='company1'],label[for='company20'], label[for='company21']").tooltip({
                bodyHandler: function(){
                    return $('#p-company').html();
                },
                showURL: false
            });

            $("#city0, #city1,#city20,#city21 ,label[for='city0'], label[for='city1'],label[for='city20'], label[for='city21']").tooltip({
                bodyHandler: function(){
                    return $('#p-city').html();
                },
                showURL: false
            });


        }
    }, //step22

	step2 : {
		init: function()
		{
			//Отображение скрытие блоков заказа услуг
			formBodyToggle($('#main-form-step2'));
			//Загрузка списка гостиниц города
			$('#hotel-city').change(function(){
				loadCityHotels($(this).val());
			});
			//Смена элементов и выбор персон в зависмости от размещения
			hotelAccUpdate();
			// Отображение/скрытие строки с комментарием
			var showHotelNote = function()
			{
				var curHotel = $('#hotel-name option:selected').val(),
					priceNoteAtt = $('#cur-hotel-att');
				if (false && typeof priceNotes[curHotel] != 'undefined' && priceNotes[curHotel] != '')
				{
					priceNoteAtt.html(priceNotes[curHotel]);
					priceNoteAtt.show();
				}
				else
				{
					priceNoteAtt.html('');
					priceNoteAtt.hide();
				}
			};
			showHotelNote();
			//События при выборе гостиницы
			$('#hotel-name').change(function(){
				getRoomsAndPrices();
				showHotelNote();
			});
			//События при выборе номера
			$('#hotel-rooms').change(function(){
				getRoomPrice();
			});
			//События при смене типа размещения
			$('#hotel-accommodation').change(function(){
				hotelAccUpdate();
				getRoomsAndPrices();
			});
			$('#hotel-extra-bed').click(function(){
				hotelAccUpdate();
				getRoomsAndPrices();
			});
			//Контроль выбранных персон
			$('#hotel-persons-block select').bind('change',function(){
				hotelPersonsSelection(this);
			});
			//Всплывающие подсказки
            $('#hotel-service-help').tooltip();
			/*
            $('#hotel-service-help').tooltip({
				bodyHandler: function(){
					return $('#hotel-help-tooltip').html();
				},
				showURL: false
			});
               */

			//Инициализация календарей
			var sdateElem = $('#hotel-sdate');
			var edateElem = $('#hotel-edate');
			$(sdateElem).datepicker('option',{
				onSelect: function(selectedDate) {
					//Если дата начала больше даты окончания, то делаем дату окончания равной дате начала плюс день
					var edate = $(edateElem).val();
					var compareRes = compareDates(selectedDate,edate);
					if(compareRes==1 || compareRes==0)
					{
						var startDateParts = selectedDate.split('.');
						var startDate = new Date(startDateParts[2]+'/'+startDateParts[1]+'/'+startDateParts[0]);
						newStopDate = addDays(startDate,1);
						var month = newStopDate.getMonth() + 1;
						newStopDateStr = newStopDate.getDate()+'.'+month+'.'+newStopDate.getFullYear();
						$(edateElem).datepicker('setDate',newStopDateStr);
					}
					//Загрузка номеров и пересчёт стоимости размещения при смене дат
					getRoomsAndPrices();
				}
			});
			$(edateElem).datepicker('option',{
				onSelect: function(selectedDate) {
					//Загрузка номеров и пересчёт стоимости размещения при смене дат
					getRoomsAndPrices();
				}
			});
			//Инициализация диалога для просмотра детальной информации по гостинице
			hotelDetailsDialogInit();
       //     alert ('srep2!');
			//Отображение детальной информации по гостинице
			$('.show-info').click(function(){
                var hotel_id =$(this).parent().parent().parent().parent().parent().parent().attr('hotel_id');
                //alert (hotel_id);
				showHotelDetails(hotel_id);
			});
			hotelMapObj = new HotelMap();
		}
	},
	step3 : {
		init: function()
		{
			//Отображение скрытие блоков заказа услуг
			formBodyToggle($('div.transport-block'));
			formBodyToggle($('div.excursion-block'));
			formBodyToggle($('div.meal-block'));
			formBodyToggle($('div.other-block'));
			/*
			 * Маска для поля с временем
			 */
			$.mask.definitions['~']='[+-]';
			$('#transport-time').mask('99:99');
			/*
			 * Флаг определяющий, доступны для заказа экскурсии
			 */
			var excursionsAv = true;
			if (!$('div.excursion-block').length)
			{
				excursionsAv = false;
			}
			/*
			 * Смена select'тов при обновлении страницы 
			 */
			if (excursionsAv) 
			{
				changeExcursionPeriods();
			}
			//Сбрасываем выбор банкета
			$('#meal-name option').removeAttr('selected');
			$('#meal-name option:first').attr('selected', 'selected');
			//Устанавливаем события для списков выбора транспортной услуги, типа транспорта
			transportListsEvents();
			excursionEvents();
			//Всплывающие подсказки
			step3Tooltips();
			//Загрузка справочной информации
			loadHelpsAsync(3, $('#transport-help-tooltip'), curCityId);
			//Смена города
			$('.city-ch select').change(function(){
				var curCity = $(this).val(),
					langSwForm = $('#lang-sw-form');
				langSwForm.find('input:hidden').val(curCity);
				langSwForm.submit();
			});
		}
	},
	step5 : {
		init : function()
		{
			//Проверка checkbox'а по Пдн
			$('#main-form-step5').submit(function(){
				if (!$('#pdn-agr').attr('checked'))
				{
					alert(messages['reg']['all']['pdn']);
					return false;
				}
				return true;
			});
			//Всплывающие подсказки
			$('#contacts-password').tooltip();
		}
	}
}

/**
 * Класс для работы с гостиницами/бронирование гостиниц
 */
function Hotel(){
    this.hotels = [],
        this.hotelsData = [],
        this.expoSite = null,
        this.infoDialog = null,
        //Параметры для google maps
        this.map = null,
        this.infowindow = null,
        this.hotelsMarkers = []
}
Hotel.prototype = {
    /**
     * Загрузка скриптов для google maps
     */
    gmLoad : function(callback)
    {
        //Если скрипты уже не загружены, то загружаем
        if(!$('script[src*="maps.google.com"]').length)
        {
            alert ('not loaded');
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'http://maps.google.com/maps/api/js?sensor=false&region=RU&key=' + sysSettings.gmKey + '&language=' + sysLang;
            if (callback && callback != '')
            {
                script.src += '&callback=' + callback;
            }
            if (sysSettings.gmApiVers != '')
            {
                script.src += '&v=' + sysSettings.gmApiVers;
            }
            document.body.appendChild(script);
        }
    },
    /**
     * Подготовка координат гостиниц для google maps
     */
    completeRecHotelsCoords : function()
    {
        var selfObj = this;
        //Проходимся по гостиницам, получаем недостающие координаты с помощью геокодера
        for (var i = 0; i < selfObj.hotels.length; i++)
        {
            //Если нет координат, определяем их по адресу
            if (selfObj.hotels[i].google_coordinates == '')
            {
                if (typeof geocoder == 'undefined' || !geocoder)
                {
                    geocoder = new google.maps.Geocoder();
                }
                //Получаем адрес без индекса
                var reg = /\d{6}/
                var address = selfObj.hotels[i].address.replace(reg, '');
                var curNum = i;
                geocoder.geocode( {'address': address}, function(results, status)
                {
                    //Если координаты успешно получены, сохраняем их
                    if (status == google.maps.GeocoderStatus.OK)
                    {
                        selfObj.hotels[curNum].google_coordinates = results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng();
                    }
                });
            }
            //Разделяем строку с координатами на широту и долготу
            var coordsParts = selfObj.hotels[i].google_coordinates.split(',');
            selfObj.hotels[i].coords = {}
            selfObj.hotels[i].coords.Lat = coordsParts[0];
            selfObj.hotels[i].coords.Lng = coordsParts[1];
        }
    },
    /**
     * Инициализация карты со списком рекомендуемых гостиниц
     */
    recHotelsMapInit : function()
    {
        var selfObj = this;
        //Дополняем координаты гостиниц, приводим их в нужный вид
        this.completeRecHotelsCoords();
        var myOptions = {
            zoom: 11,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(document.getElementById('rec-hotels-map'), myOptions);
        this.map = map;
        google.maps.event.trigger(this.map, 'resize');
        //Инициализация карты с заданными параметрами
        var mapCenter;
        var expoSiteCoords = null;
        if (!this.expoSite || !selfObj.expoSite.lat || !selfObj.expoSite.lng || selfObj.expoSite.lat == 0 || selfObj.expoSite.lng == 0)
        {
            mapCenter = new google.maps.LatLng(selfObj.hotels[0].coords.Lat, selfObj.hotels[0].coords.Lng);
        }
        else
        {
            mapCenter = new google.maps.LatLng(selfObj.expoSite.lat, selfObj.expoSite.lng);
            //Отображаем метку для площадки
            selfObj.initExpoSitePoint();
        }
        map.setCenter(mapCenter);
        //Создаём объект всплывающего окна InfoWindow
        selfObj.infowindow = new google.maps.InfoWindow({ maxWidth : 350 });
        //Скрываем всплывающее окно при клике на карту
        google.maps.event.addListener(selfObj.map, 'click', function(){
            if (selfObj.infowindow)
            {
                selfObj.infowindow.close();
            }
        });


        //Расставляем маркеры с гостиницами
        for (var i = 0; i < selfObj.hotels.length; i++)
        {
            console.log(selfObj.hotels[i].name);
            //Формируем объект маркера, добавляем на карту
            var hotelLatLng = new google.maps.LatLng(selfObj.hotels[i].coords.Lat, selfObj.hotels[i].coords.Lng);
            var icon = '/img/map_markers/marker' + selfObj.hotels[i].stars + '.png';
            var marker = new google.maps.Marker({
                position: hotelLatLng,
                map : map,
                title : selfObj.hotels[i].name,
                icon : icon
            });
            var priceStr = selfObj.hotels[i].minPrice;
            if (priceStr == 0)
            {
               // priceStr = mapMessages['byRequest'];
            }
            else
            {
              //  priceStr += ' ' + mapMessages['curr'];
            }
            //Формируем объект окна, всплывающего при нажати на маркер
            var infowindowText = '<div style="font-size:13px; margin:3px 0 10px 0;text-align:left;" class="bolder">' + selfObj.hotels[i].name + '&nbsp;&nbsp;';
            infowindowText += '<img style="margin-bottom: -0.2em;" src="/img/' + selfObj.hotels[i].stars + 'stars.gif" alt=" " width="88" height="16" /></div>'
            infowindowText += '<div style="text-align:left;">' + selfObj.hotels[i].address + '</div>';
         //   infowindowText += '<div class="map-price clearfix">';
        //    infowindowText += '<div class="map-price-left">' + mapMessages['price'] + '</div>';
         //   infowindowText += '<div class="map-price-right">' + priceStr + '</div>';
         //   infowindowText += '</div>';
        //    infowindowText += '<div style="text-align:left;margin-top: 10px;">';
        //    infowindowText += '<a href="/index/index/finish/1/hotel/' + selfObj.hotels[i].id + '/" class="iw-order-link" onclick="location.href=$(this).attr(\'href\');">' + mapMessages['book'] + '</a>';
        //    infowindowText += '</div>';
            //Создаём всплывающее окно для маркера
            selfObj.recHotelInfoWindow(marker, infowindowText);
        }
    },
    /**
     * Создание всплывающего окна для маркера гостиницы
     */
    recHotelInfoWindow : function(marker, content)
    {
        var selfObj = this;
        google.maps.event.addListener(marker, 'click', function() {
            selfObj.infowindow.setContent(content);
            selfObj.infowindow.open(selfObj.map, marker);
        });
    },
    /**
     * Отображение карты со списком рекомендуемых гостиниц
     */
    showRecHotelsMap : function(mapInitFunction)
    {
        var selfObj = this;
        var dialogElem = $('#rec-hotels-map');
        $(dialogElem).dialog({
            width: 600,
            height: 700,
            position: ["center", 80],
            modal: true,
            draggable : false,
            resizable: false,
            autoOpen: true,
            title: 'Расположение гостиниц на карте',
            open : function() {
                //Инициализация карты
                selfObj.gmLoad(mapInitFunction);

                //Если карта уже была иницализирована, отображаем маркеры и делаем её resize
                if (selfObj.map || 1)
                {
                    selfObj.recHotelsMapInit();
                    google.maps.event.trigger(selfObj.map, 'resize');
                }
            }
        });
    },
    /**
     * Отображение маркера для выставочной площадки
     */
    initExpoSitePoint : function()
    {
        var selfObj = this;
        if (!this.expoSite || !selfObj.expoSite.lat || !selfObj.expoSite.lng)
        {
            return;
        }
        //Формируем объект маркера, добавляем на карту
        var siteLatLng = new google.maps.LatLng(selfObj.expoSite.lat, selfObj.expoSite.lng);
        var marker = new google.maps.Marker({
            position: siteLatLng,
            map : selfObj.map,
            title : selfObj.expoSite.name
        });
        //Формируем объект окна, всплывающего при нажати на маркер
        var infowindowText = '<div style="font-size:13px; margin:3px 0 10px 0;text-align:left;" class="bolder">' + selfObj.expoSite.name + '</div>';
        if (typeof selfObj.expoSite.addr != 'undefined' && selfObj.expoSite.addr)
        {
            infowindowText += '<div style="text-align:left;">' + selfObj.expoSite.addr + '</div>';
        }
        //Создаём всплывающее окно для маркера
        var infowindow = new google.maps.InfoWindow({ maxWidth : 350 });
        infowindow.setContent(infowindowText);
        infowindow.open(selfObj.map, marker);
    },
    /**
     * Отображение/скрытие опсиания гостиницы
     * @param int hotelId
     */
    toggleHotelCaption	:	function(hotelId, toggleAction)
    {
        if (!toggleAction)
        {
            toggleAction = false;
        }
        var hotelContainer = $('#hotel-'+hotelId);
        var captContainer = $(hotelContainer).find('div.rec-hotel-caption-inner');
        var toggleClosedLink = $(hotelContainer).find('.toggle-closed');
        var toggleOpenedLink = $(hotelContainer).find('.toggle-opened');
        //Если описание не загружено, то загружаем
        if (!$(captContainer).find('#content-loaded-'+hotelId).length)
        {
            this.loadHotelCaption(hotelId);
        }
        //Отображаем/скрываем блок с описанием
        if ((toggleAction !== null && toggleAction == 'open') || $(toggleClosedLink).is(':visible'))
        {
            $(toggleClosedLink).hide();
            $(toggleOpenedLink).show();
            $(captContainer).show();
        }
        else if ((toggleAction !== null && toggleAction == 'close') || !$(toggleClosedLink).is(':visible'))
        {
            $(toggleOpenedLink).hide();
            $(toggleClosedLink).show();
            $(captContainer).hide();
        }
    },
    /**
     * Загрузка описания и фото гостиницы
     * @param {Object} hotelId
     */
    loadHotelCaption	:	function(hotelId)
    {
        var hotelContainer = $('#hotel-'+hotelId);
        var captContainer = $(hotelContainer).find('div.rec-hotel-caption-inner');
        var loaderElem = $(hotelContainer).find('div.hotel-caption-loader');
        $.ajax({
            type: 'POST',
            url: '/hotels/hotel.caption/format/html/',
            dataType: 'html',
            data: 'hotelId='+hotelId,
            beforeSend: function(){
                $(loaderElem).show();
            },
            success: function(result)
            {
                $(captContainer).html(result);
            },
            complete: function(){
                $(loaderElem).hide();
            }
        });
    },
    /**
     * Загрузка списка гостиниц города (для карты)
     */
    loadCityHotels : function(callback)
    {
        var selfObj = this,
            loaderElem = $('.city-hotels-loader');
        var successFunc = function()
        {
            selfObj.hotels = selfObj.cityHotels;
            callback();
        };
        if (typeof selfObj.cityHotels != 'undefined')
        {
            successFunc();
            return;
        }
        $.ajax({
            type: 'POST',
            url: '/hotels/city.hotels/format/json/',
            dataType: 'json',
            beforeSend: function(){
                $(loaderElem).show();
            },
            success: function(result)
            {
                selfObj.cityHotels = result.hotels;
                successFunc();
            },
            complete: function(){
                $(loaderElem).hide();
            }
        });
    },
    /**
     * Обновление элементов при смене типа размещения
     */
    hotelAccUpdate	:	function()
    {
        var accommodation = parseInt($('#hotel-accommodation option:selected').val());
        var setType = accommodation;
        if (accommodation==2)
        {
            $('#hotel-bed-details').show();
            $('#hotel-persons-line2').show();
            var extraBed;
            if ($('#hotel-extra-bed').attr('checked'))
            {
                $('#hotel-persons-line3').show();
                setType++;
            }
            else
            {
                $('#hotel-persons-line3').hide();
            }
        }
        else
        {
            $('#hotel-bed-details').hide();
            $('#hotel-persons-line2').hide();
            $('#hotel-persons-line3').hide();
        }
        //Если ещё не было заказов и выбранное размещение соответствует количеству персон, то распределяем персон по спискам
        var personsCount = $('#hotel-persons-all option').length-1;
        if ($('#ordered-hotel tr.ordered-hotel-row').length==0 && setType==personsCount)
        {
            for (i = 1; i <=setType; i++)
            {
                var personsList = $('#hotel-persons'+i);
                $(personsList).find('option').removeAttr('selected');
                $(personsList).find('option:eq('+i+')').attr('selected','selected');
                var selectedElemVal = $(personsList).find('option:selected').val();
                $(personsList).find('option[value!="'+selectedElemVal+'"][value!="0"]').remove();
            }
        }
        //Возвращаем списки выбора персон в исходное состояние
        else
        {
            this.hotelPersonsListsReset();
        }
    },
    /**
     * Контроль выбранных персон
     * @param {Object} selectElement Список, в котором изменилось значение
     */
    hotelPersonsSelection	:	function(selectElement)
    {
        //Номер списка, в котором был сделан выбор
        var selectElementNum = parseInt($(selectElement).attr('id').replace('hotel-persons',''));
        //Значение выбранного элемента списка
        var selectElementVal = $(selectElement).find('option:selected').val();
        //Если выбран элемент по умолчанию
        if(selectElementVal==0)
        {
            return false;
        }
        //Выбранный тип проживания
        var accommodation = parseInt($('#hotel-accommodation option:selected').val());
        //Для одноместного размещения функция не нужна
        if (accommodation==1)
        {
            return false;
        }
        var setType = accommodation;
        if ($('#hotel-extra-bed').attr('checked'))
        {
            setType++;
        }
        //Проходимся по всем спискам выбора персон (кол-во зависит от выбранного размщения)
        for (i = 1; i <=setType; i++)
        {
            //Если список не тот, с которым мы работаем
            if (i!=selectElementNum)
            {
                //Удаляем в списках выбранный в текущем списке элемент
                $('#hotel-persons'+i+' option[value="'+selectElementVal+'"]').remove();
            }
        }
    },
    /**
     * Возвращение списков выбора персон в исходное состояние (список по умолчанию берётся из скрытого элемента)
     */
    hotelPersonsListsReset	:	function()
    {
        //Получаем список по умолчанию из скрытого элемента
        var defaultHotelPersonList = $('#hotel-persons-all').html();
        //Копируем список по умолчанию во все списки
        $('#hotel-persons-block select').each(function(){
            $(this).html(defaultHotelPersonList);
        });
    },
    /**
     * Получение списка номеров для гостиницы с ценами на размещения дял указанного периода
     */
    getRoomsAndPrices	:	function()
    {
        //Ссылка на текущий объект
        var hotelObj = this;
        //Формируем запрос на номера
        var roomsRequest = {};
        roomsRequest['request'] = {
            'hotelId'		:	$('#hotel-name').find('option:selected').val(),
            'sDate'			:	$('#hotel-sdate').val(),
            'eDate'			:	$('#hotel-edate').val(),
            'accommodation'	:	$('#hotel-accommodation').find('option:selected').val()
        };
        //Отправляем запрос на сервер
        $.ajax({
            type: 'POST',
            url: '/hotels/rooms.prices/format/json/',
            dataType: 'json',
            data: roomsRequest,
            success: function(result)
            {
                //Если не было ошибок
                if (result.status == 1)
                {
                    hotelObj.rooms = result.rooms;
                    //Обновляем список номеров и поле со стоимостью проживания
                    hotelObj.updateRooms();
                }
                else
                {
                    alert(result.messages);
                }
            }
        });
    },
    /**
     * Обновления списка номеров с ценами
     */
    updateRooms		:	function()
    {
        var hotelRoomsSelect = $('#hotel-rooms');
        //Получаем id выбранного номера
        var selectedRoomId = $(hotelRoomsSelect).find('option:selected').val();
        //Проходимся по полученным ранее номерам и формируем контент для списка номеров
        var roomsSelectCont = '';
        for (i in this.rooms)
        {
            //Выделяем выбранный ранее номер
            var selected = '';
            if (this.rooms[i]['id'] == selectedRoomId)
            {
                selected = 'selected="selected"';
            }
            var roomOption = '';
            roomOption = '<option value="'+this.rooms[i]['id']+'"'+selected+'>';
            roomOption += this.rooms[i]['name']+' ('+this.rooms[i]['dayPriceText']+')</option>';
            roomsSelectCont += roomOption;
        }
        //Заменяем контент списка
        $('#hotel-rooms').html(roomsSelectCont);
        //Отображаем цену размещения для номера
        this.getRoomPrice();
    },
    /**
     * Отображение цены размещения в выбранном номере на основе данных, полученных ранее с сервера
     */
    getRoomPrice	:	function()
    {
        var hotelRoomsSelect = $('#hotel-rooms');
        var selectedRoomId = $(hotelRoomsSelect).find('option:selected').val();
        //Если номера ещё не загружены (страница в исходном состоянии), то загружаем их
        if(!this.rooms)
        {
            this.getRoomsAndPrices();
            return false;
        }
        //Определяем стоимость выбранного номера и выводим её
        var curRoomPrice = this.rooms[selectedRoomId]['summ'];
        var priceAmountCont = $('#hotel-price-amount');
        var priceByRequestCont = $('#hotel-price-by-request');
        //Если стоимость не по запросу
        if(curRoomPrice!=0)
        {
            $(priceAmountCont).show();
            $(priceByRequestCont).hide();
            $(priceAmountCont).find('span').text(curRoomPrice);
        }
        //Если по запросу
        else
        {
            $(priceAmountCont).hide();
            $(priceByRequestCont).show();
        }
        //Если завтрак включён в стоимость, то не даём его выбирать
        var breakfActiveCont = $('#hotel-breakfast-active');
        var breakfNotActiveCont = $('#hotel-breakfast-not-active');
        if (this.rooms[selectedRoomId]['breakfastIncl']==1)
        {
            $(breakfActiveCont).hide();
            $(breakfNotActiveCont).show();
        }
        else
        {
            //Если до этого выбор завтрака был скрыт, выбираем первый элемент списка
            if(!$(breakfActiveCont).is(':visible'))
            {
                $(breakfActiveCont).find('option:first').attr('selected','selected');
            }
            $(breakfActiveCont).show();
            $(breakfNotActiveCont).hide();
        }
        //Проверяем, какие есть варианты размещения для номера
        var accom = this.rooms[selectedRoomId]['accom'];
        var roomAccSelect = $('#hotel-accommodation');
        //Если для номера возможно только одноместное размещение
        if (accom==1)
        {
            //Если в списке для выбора доступно двухместное размещение, удаляем данный элемент
            if ($(roomAccSelect).find('option[value="2"]').length)
            {
                $(roomAccSelect).find('option[value="2"]').remove();
            }
            this.hotelAccUpdate();
        }
        else
        {
            //Если в списке нет пункта с выборов двухместного размещения, добавляем его
            if (!$(roomAccSelect).find('option[value="2"]').length)
            {
                var doubleAccElem = '<option value="2">'+$('#hotel-accommodation-2').text()+'</option>';
                $(roomAccSelect).append(doubleAccElem);
            }
        }
    },
    /**
     * Заказ номера
     */
    addHotelRoom	:	function()
    {
        var hotelOrderForm = $('#hotel-order-form');
        //Заказ на проживание
        var hotelOrder={};
        hotelOrder['order'] = {
            'hotelId'		: 	$(hotelOrderForm).find('#hotel-name option:selected').val(),
            'roomId' 		: 	$(hotelOrderForm).find('#hotel-rooms option:selected').val(),
            'accommodation' : 	parseInt($(hotelOrderForm).find('#hotel-accommodation option:selected').val()),
            'startDate'		:	$(hotelOrderForm).find('#hotel-sdate').val(),
            'endDate'		:	$(hotelOrderForm).find('#hotel-edate').val(),
            'comment'		:	$(hotelOrderForm).find('#hotel-comment').val()
        };
        //Завтрак
        var breakfActiveCont = $(hotelOrderForm).find('#hotel-breakfast-active');
        if($(breakfActiveCont).is(':visible'))
        {
            hotelOrder['order']['breakfast'] = $(breakfActiveCont).find('#hotel-breakfast option:selected').val();
        }
        else
        {
            hotelOrder['order']['breakfast'] = 0;
        }
        //Дополнительная кровать и тип кровати
        var extraBed;
        var bedType;
        //Если выбрано двухместное размещение, то определяем выбранный тип кровати, а также необходимость доп. кровати
        if (hotelOrder['order']['accommodation'] == 2)
        {
            if ($(hotelOrderForm).find('#hotel-extra-bed').attr('checked'))
            {
                extraBed = 1;
            }
            else
            {
                extraBed = 0;
            }
            bedType = $(hotelOrderForm).find('#hotel-bed-type').val();
        }
        else
        {
            extraBed = 0;
            bedType = '';
        }
        hotelOrder['order']['extraBed'] = extraBed;
        hotelOrder['order']['bedType'] = bedType;
        //Получаем список выбранных персон
        hotelOrder['order']['persons'] = this.getHotelPersons(hotelOrder['order']['accommodation'],extraBed);
        //Если персоны не выбраны
        if (!hotelOrder['order']['persons'])
        {
            alert(messages['reg']['all']['hotelMissParams']);
            return false;
        }
        //Отправляем данные на сервер
        this.hotelRoomSave(hotelOrder);
    },
    /**
     * Получение списка выбранных персон при заказе номера
     * @param int accommodation
     * @param int extraBed
     */
    getHotelPersons		:	function(accommodation,extraBed)
    {
        var hotelOrderForm = $('#hotel-order-form');
        //Определяем количество персон на основе выбранного типа размещения
        var setType = accommodation;
        if(extraBed==1)
        {
            setType++;
        }
        //Объект, содержащий выбранные персоны
        var persons={};
        //Получаем список выбранных персон
        for(i=1;i<=setType;i++)
        {
            persons[i] = {};
            persons[i]['id'] = $(hotelOrderForm + ' #persons-block').find('#hotel-persons'+i+' [checked=true]').val();
            //Если персона не выбрана в одном из списков
            if (persons[i]['id']==0)
            {
                return false;
            }
            persons[i]['name'] = trim($(hotelOrderForm + ' #persons-block').find('label [for=hotel-persons'+i+']').text());
        }
        return persons;
    },
    /**
     * Передача данных по заказу номера на сервер для проверки и сохранения в БД
     * @param {Object} hotelOrder
     */
    hotelRoomSave	:	function(hotelOrder)
    {
        var hotelOrderServicesCont = $('#hotel-order-services');
        var loaderElem = $(hotelOrderServicesCont).find('#hotel-loader');
        var loaderContainer = $(hotelOrderServicesCont).find('#ordered-hotel');
        var hotelObj = this;
        $.ajax({
            type: 'POST',
            url: '/hotels/add.hotel.room/format/json/',
            dataType: 'json',
            data: hotelOrder,
            beforeSend: function(){
                ajaxLoaderContainer('show', loaderElem, loaderContainer);
            },
            success: function(result)
            {
                ajaxLoaderContainer('hide',loaderElem,loaderContainer);
                var status = parseInt(result.status);
                //Если в результате ошибка, то выводим её
                if (status == 0)
                {
                    alert(result.messages);
                }
                //Если записи успешно добавлены, добавляем строку
                else
                {
                    var hotelOrderServices = document.getElementById('hotel-order-services');
                    hotelOrderServices.scrollIntoView(true);
                    var emptyOrderElem = $('#hotel-empty-order');
                    //Добавляем новую строку (без prepend плывёт верстка)
                    if ($(emptyOrderElem).is(':visible'))
                    {
                        $('#ordered-hotel').prepend(result.content);
                    }
                    else
                    {
                        $('#ordered-hotel').append(result.content);
                    }
                    //Скрываем блок, сообщающий об отсутствии заказов
                    $(emptyOrderElem).addClass('hidden');
                    //Отменяем выбор персон
                    hotelObj.hotelPersonsListsReset();
                    var hotelOrderForm = $('#hotel-order-form');
                    //Очищаем поле с комментарием
                    $(hotelOrderForm).find('#hotel-comment').val('');
                    //Меняем текст ссылки
                    $(hotelOrderForm).find('#hotel-add-more-text').removeClass('hidden-imp');
                    $(hotelOrderForm).find('#hotel-add-text').addClass('hidden-imp');
                }
            },
            complete: function(){
                ajaxLoaderContainer('hide',loaderElem,loaderContainer);
            }
        });
    },
    /**
     * Удаление заказа на проживание из таблицы заказов
     * @param int serviceId
     */
    deleteHotelOrder	:	function(serviceId)
    {
        var hotelOrdersTable = $('#ordered-hotel');
        $(hotelOrdersTable).find('#hotel-order'+serviceId).remove();
        var ordersCount = $(hotelOrdersTable).find('tbody.hotel-order').length;
        //Если нет заказов
        if (ordersCount == 0)
        {
            //Отображаем строку, сообщающую об отсутствии заказов
            $(hotelOrdersTable).find('#hotel-empty-order').removeClass('hidden');
            //Меняем текст ссылки для добавления номера
            $('#hotel-add-more-text').addClass('hidden-imp');
            $('#hotel-add-text').removeClass('hidden-imp');
            //Если нет заказов, то распределяем персоны по номерам
            this.hotelAccUpdate();
        }
    },
    /**
     * Отображение диалога с инфоррмацией по гостинице
     */
    initInfoDialog : function()
    {
        var selfObj = this;
        var dialogElem = $('#hotel-info');
        selfObj.infoDialog = dialogElem.dialog({
            width: 600,
            height: 700,
            position: ["center", 80],
            modal: true,
            draggable : true,
            resizable: false,
            autoOpen: false,
            title: dialogElem.find('.dialog-title').text()
        });
    },
    /**
     * Отображение диалога с инфоррмацией по гостинице
     */
    showInfo : function()
    {
        var selfObj = this;
        if (selfObj.infoDialog == null)
        {
            selfObj.initInfoDialog();
        }
        var hotelId = $('#hotel-name option:selected').val();
        var dialogCont = $('#hotel-info');
        var loader = dialogCont.find('#hotel-details-loader');
        // Скрываем карту
        $('#hotel-map').hide();
        // Очищаем блок с контентом в диалоге
        var dialogBody = dialogCont.find('.dialog-body');
        dialogBody.html('');
        //Отображаем диалог
        selfObj.infoDialog.dialog('open');
        //Получаем информацию по гостинице
        $.ajax({
            type: 'POST',
            url: '/hotels/hotel.caption/format/html/format/html/',
            data: 'hotelId=' + hotelId + '&showName=1',
            beforeSend: function(){
                $(loader).show();
            },
            success: function(result)
            {
                $(loader).hide();
                $('#hotel-map').show();
                // Отображаем полученную информацию
                dialogBody.html(result);
                // Если карта уже была иницализирована
                if (selfObj.map != null)
                {
                    //Отображаем гостиницу
                    selfObj.showHotel();
                    //Делаем её resize
                    google.maps.event.trigger(selfObj.map, 'resize');
                }
                // Если карта ещё не была инициализирована
                else
                {
                    //Инициализация карты
                    selfObj.gmLoad('hotelObj.hotelMapInit');
                }
            },
            complete: function(){
                $(loader).hide();
            }
        });
    },
    /**
     * Инициализация карты с гостиницей
     */
    hotelMapInit : function()
    {
        var selfObj = this;
        //Инициализация карты с заданными параметрами
        var myOptions = {
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        this.map = new google.maps.Map(document.getElementById('hotel-map'), myOptions);
        google.maps.event.trigger(this.map, 'resize');
        //Создаём объект всплывающего окна InfoWindow
        selfObj.infowindow = new google.maps.InfoWindow({ maxWidth : 350 });
        //Скрываем всплывающее окно при клике на карту
        google.maps.event.addListener(selfObj.map, 'click', function(){
            if (selfObj.infowindow)
            {
                selfObj.infowindow.close();
            }
        });
        //Отображаем маркер с гостиницей
        selfObj.showHotel();
    },
    /**
     * Отображение маркера с гостиницей на карте
     */
    showHotel : function()
    {
        var selfObj = this;
        var curHotelId = $('#hotel-name option:selected').val();
        var hotel = selfObj.hotelsData[curHotelId];
        hotel.coords = hotel.google_coordinates.split(',');
        // Если координат нет, скрываем карту
        if (hotel.coords.length < 2)
        {
            $('#hotel-map').hide();
            return;
        }
        hotel.name = hotel.name_orig;
        //Удаляем имеющиеся маркеры
        if (selfObj.hotelsMarkers.length)
        {
            for (i in selfObj.hotelsMarkers)
            {
                selfObj.hotelsMarkers[i].setMap(null);
            }
            selfObj.hotelsMarkers.length = 0;
        }
        //Устанваливаем центр карты
        var mapCenter = new google.maps.LatLng(hotel.coords[0], hotel.coords[1]);
        this.map.setCenter(mapCenter);
        //Формируем объект маркера, добавляем на карту
        var hotelLatLng = new google.maps.LatLng(hotel.coords[0], hotel.coords[1]);
        var marker = new google.maps.Marker({
            position: hotelLatLng,
            map : selfObj.map,
            title : hotel.name
        });
        selfObj.hotelsMarkers.push(marker);
        //Формируем объект окна, всплывающего при нажати на маркер
        var infowindowText = '<div style="font-size:13px; margin:3px 0 10px 0;text-align:left;" class="bolder">' + hotel.name + '&nbsp;&nbsp;';
        infowindowText += '<img style="margin-bottom: -0.2em;" src="/img/' + hotel.stars + 'stars.gif" alt=" " width="88" height="16" /></div>'
        infowindowText += '<div style="text-align:left;">' + hotel.address + '</div>';
        //Создаём всплывающее окно для маркера
        google.maps.event.addListener(marker, 'click', function() {
            selfObj.infowindow.setContent(infowindowText);
            selfObj.infowindow.open(selfObj.map, marker);
        });
    }
}

/* --------------------|  Автозагрузка функций общих для всех разделов |----------------*/
$(document).ready(function(){
	//Календарь
	if ($('.datepicker').length) 
	{
		$('.datepicker').datepicker({
			showOn: 'button',
			buttonImage: '/img/calendar.gif',
			buttonImageOnly: true,
			dateFormat: 'dd.mm.yy',
			changeMonth: true,
			changeYear: true
		});	
		//Устанавливаем параметры для изображения, вызывающего календарь
		$('.datepicker').next().addClass('form-calendar').attr('title','').attr('alt','');
	}
	//Ie select fix
	if ($.browser.msie && !detectIe9()) 
	{
		$('select.ie-width-fix').fixSelect();
	}


    //TRANSFORM!!!


   //     $('form').jqTransform();



});

