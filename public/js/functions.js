/**
 * Перенаправление на главную с задержкой
 */
function autoRedirect(url,timeout)
{
	setTimeout("location.replace('"+url+"')",timeout*1000 );
	var timer=window.setInterval(function()
	{
	    $("#redirect-string").html("After "+ timeout + " seconds you will be redirected to the main page.");
	    if (timeout <= 0)
	    { 
	         window.clearInterval(timer);
	    }
	    timeout--;
	}, 1000);
}

/**
 * Проверка дат
 */
function checkDates(startDate,stopDate,delimiter)
{
	var curDate = new Date();
	curDate.setMinutes(0); curDate.setHours(0); curDate.setSeconds(0); curDate.setMilliseconds(0);
	var curTime=curDate.getTime();
	var stopDateParts = stopDate.split(delimiter);
	var stopDate = new Date(stopDateParts[2]+'/'+stopDateParts[1]+'/'+stopDateParts[0]);
	var stopDateTime=stopDate.getTime();
	var startDateParts = startDate.split(delimiter);
	var startDate = new Date(startDateParts [2]+'/'+startDateParts [1]+'/'+startDateParts[0]);
	var startDateTime=startDate.getTime();
	var daysDiff=(stopDateTime - startDateTime)/86400000;
	//Даты не в прошлом, дата начала меньше даты конца и разница по дням меньше 365
	if (startDateTime >= stop_date_time || startDateTime <  curTime || stopDateTime <  curTime || daysDiff > 365)
	{
		return false;
	}
	else
	{
		return true;
	}
}

/**
 * Сравнение дат в формате dd.mm.yyyy
 * @param string firstDate
 * @param string secondDate
 */
function compareDates(firstDate, secondDate)
{
	var firstDateTime = dateToTime(firstDate)
	var secondDateTime = dateToTime(secondDate);
	var diff = firstDateTime - secondDateTime;
	if (diff>0)
	{
		return 1;
	}
	else
	{
		if (diff == 0) 
		{
			return 0;
		}
		else 
		{
			return 2;
		}
	}
}

/**
 * Прибавление дней к дате
 */
function addDays(date, n) 
{
	// может отличаться на час, если произошло событие перевода времени
	var d = new Date();	
	d.setTime(date.getTime() + n * 24 * 60 * 60 * 1000);
	return d;
}

/**
 * Преоьразование даты из формата dd.mm.yyyy в формат unix
 * @param {Object} date
 */
function dateToTime(date)
{
	var curDate = new Date();
	curDate.setMinutes(0); curDate.setHours(0); curDate.setSeconds(0); curDate.setMilliseconds(0);
	var curTime=curDate.getTime();
	var dateParts = date.split('.');
	var date = new Date(dateParts[2]+'/'+dateParts[1]+'/'+dateParts[0]);
	return date.getTime();
}


/**
 * Проверка на IE6
 */
function detectIe6()
{
 	var browser = navigator.appName;
 	if (browser == "Microsoft Internet Explorer")
	{
	    var b_version = navigator.appVersion;
	    var re = /\MSIE\s+(\d\.\d\b)/;
	    var res = b_version.match(re);
	    if (res[1] <= 6)
		{
	    	return true;
	    }
  	}
 	return false;
}

/**
 * Проверка на IE9
 */
function detectIe9()
{
 	var browser = navigator.appName;
 	if (browser == "Microsoft Internet Explorer")
	{
	    var b_version = navigator.appVersion;
	    var re = /\Trident\/5\.0/;
	    var res = re.test(b_version);
	    if (res)
		{
			return true;
		}
  	}
 	return false;
}

/**
 * Проверка на IE
 */
function detectIe()
{
 	var browser = navigator.appName;
 	if (browser == "Microsoft Internet Explorer")
	{
	    var b_version = navigator.appVersion;
	    var re = /\MSIE\s+(\d\.\d\b)/;
	    var res = b_version.match(re);
	    if (res[1] >= 5)
		{
	    	return true;
	    }
  	}
 	return false;
}


/**
 * Текущая дата
 */
function getCurDate()
{
	var dateObj = new Date();
	return dateObj.format("yyyy-mm-dd");
}

/**
 * Ajax прелоадер
 * @param string action
 */
function ajaxLoader(action)
{
	if(action=='show')
	{
		$('#container').addClass('transparent');
		$('.loader').show();
	}
	if(action=='hide')
	{
		$('#container').removeClass('transparent');
		$('.loader').hide();
	}
}

/**
 * Ajax прелоадер с указанием класса блока-прелоадера
 * @param string action
 */
function ajaxLoaderBlock(loaderElem,action)
{
	if(action=='show')
	{
		$(loaderElem).parent().addClass('transparent');
		$(loaderElem).removeClass('transparent').show();
	}
	if(action=='hide')
	{
		$(loaderElem).parent().removeClass('transparent');
		$(loaderElem).hide();
	}
}

/**
 * Ajax прелоадер с указанием класса блока-прелоадера и блока, для которого задаётся прозрачность
 * @param string action
 */
function ajaxLoaderContainer(action,loaderElem,container)
{
	if(action=='show')
	{
		$(container).addClass('transparent');
		$(loaderElem).show();
	}
	if(action=='hide')
	{
		$(container).removeClass('transparent');
		$(loaderElem).hide();
	}
}

/**
 * Очистка полей формы
 * @param string elem Селектор формы
 */
function clearForm(elem)
{
	$(elem+' input[type!="radio"][type!="checkbox"]').each(function(){
		$(this).val('');
	});
	$(elem+' select').each(function(){
		$(this).find('option').removeAttr('selected');
		$(this).find('option:first').attr('selected','selected');
	});
}

//Размер документа по вертикали  
function getDocumentHeight()  
{  
    return (document.body.scrollHeight > document.body.offsetHeight)?document.body.scrollHeight:document.body.offsetHeight;  
}  
   
//Размер документа по горизонтали  
function getDocumentWidth()  
{  
    return (document.body.scrollWidth > document.body.offsetWidth)?document.body.scrollWidth:document.body.offsetWidth;  
}

//Отобразить модальное окно
function loadPopup(modalElem)
{
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();
	$('#mask').css({'width':maskWidth,'height':maskHeight});
	$('#mask').css({'opacity':'0.6'});
	$('#mask').show();	
	var winH =  $(window).height();
	var winW = $(window).width();
	var modalTopPos = winH/2-$(modalElem).height()/2;
	modalTopPos = modalTopPos-60;
	var modalLeftPos = winW/2-$(modalElem).width()/2;
	$(modalElem).css('top',  modalTopPos);
	$(modalElem).css('left', modalLeftPos);
	$(modalElem).fadeIn(250); 
	//Событие, закрывающее модальное окно при нажатие на любую область экрана
	$('#mask').click(function () {
		$(this).hide();
		$(modalElem).hide();
	});
	//Эмулируем position fixed для ie6
	if (detectIe6())
	{
		var offset = $(modalElem).offset();
		var topPadding = 15;
		$(window).scroll(function() {
			if ($(window).scrollTop() > offset.top) {
				$(modalElem).stop().animate({marginTop: $(window).scrollTop() - offset.top + topPadding});
			}
		else {$(modalElem).stop().animate({marginTop: 0});};});
	}	
}

//Скрыть модальное окно
function disablePopup(modalElem)
{
	$(modalElem).hide();
	$('#mask').hide();
}


function scrollToPos(pos)
{
	docHeight = getDocumentHeight();
	if (pos=='center')
	{
		window.scrollTo(100, docHeight/2);
	}
}
/**
 * Раскраска строк таблицы
 * @param {Object} table
 */
function tableHighlight(table,hover)
{
	//Если в таблице менее 2 строк
	if ($(table).find('tbody tr').length < 2)
	{
		return true;
	}
	if (!hover)
	{
		hover = 0;
	}
	var table = $(table).find('tbody');
	var oddRows = $(table).find('tr:odd');
	var evenRows = $(table).find('tr:even');
	$(oddRows).addClass('odd-row');
	$(evenRows).addClass('even-row');
	if (hover==1)
	{
		$(oddRows).mouseover(function(){
			$(this).removeClass('odd-row');
			$(this).addClass('active-row');
		});
		$(evenRows).mouseover(function(){
			$(this).removeClass('even-row');
			$(this).addClass('active-row');
		});
		$(oddRows).mouseout(function(){
			$(this).removeClass('active-row');
			$(this).addClass('odd-row');
		});
		$(evenRows).mouseout(function(){
			$(this).removeClass('active-row');
			$(this).addClass('even-row');
		});
	}
}

/**
 * Return a formatted string
 */
function sprintf() {
	// 
	// +   original by: Ash Searle (http://hexmen.com/blog/)
	// + namespaced by: Michael White (http://crestidg.com)

	var regex = /%%|%(\d+\$)?([-+#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuidfegEG])/g;
	var a = arguments, i = 0, format = a[i++];

	// pad()
	var pad = function(str, len, chr, leftJustify) {
		var padding = (str.length >= len) ? '' : Array(1 + len - str.length >>> 0).join(chr);
		return leftJustify ? str + padding : padding + str;
	};

	// justify()
	var justify = function(value, prefix, leftJustify, minWidth, zeroPad) {
		var diff = minWidth - value.length;
		if (diff > 0) {
			if (leftJustify || !zeroPad) {
			value = pad(value, minWidth, ' ', leftJustify);
			} else {
			value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
			}
		}
		return value;
	};

	// formatBaseX()
	var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
		// Note: casts negative numbers to positive ones
		var number = value >>> 0;
		prefix = prefix && number && {'2': '0b', '8': '0', '16': '0x'}[base] || '';
		value = prefix + pad(number.toString(base), precision || 0, '0', false);
		return justify(value, prefix, leftJustify, minWidth, zeroPad);
	};

	// formatString()
	var formatString = function(value, leftJustify, minWidth, precision, zeroPad) {
		if (precision != null) {
			value = value.slice(0, precision);
		}
		return justify(value, '', leftJustify, minWidth, zeroPad);
	};

	// finalFormat()
	var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) {
		if (substring == '%%') return '%';

		// parse flags
		var leftJustify = false, positivePrefix = '', zeroPad = false, prefixBaseX = false;
		for (var j = 0; flags && j < flags.length; j++) switch (flags.charAt(j)) {
			case ' ': positivePrefix = ' '; break;
			case '+': positivePrefix = '+'; break;
			case '-': leftJustify = true; break;
			case '0': zeroPad = true; break;
			case '#': prefixBaseX = true; break;
		}

		// parameters may be null, undefined, empty-string or real valued
		// we want to ignore null, undefined and empty-string values
		if (!minWidth) {
			minWidth = 0;
		} else if (minWidth == '*') {
			minWidth = +a[i++];
		} else if (minWidth.charAt(0) == '*') {
			minWidth = +a[minWidth.slice(1, -1)];
		} else {
			minWidth = +minWidth;
		}

		// Note: undocumented perl feature:
		if (minWidth < 0) {
			minWidth = -minWidth;
			leftJustify = true;
		}

		if (!isFinite(minWidth)) {
			throw new Error('sprintf: (minimum-)width must be finite');
		}

		if (!precision) {
			precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type == 'd') ? 0 : void(0);
		} else if (precision == '*') {
			precision = +a[i++];
		} else if (precision.charAt(0) == '*') {
			precision = +a[precision.slice(1, -1)];
		} else {
			precision = +precision;
		}

		// grab value using valueIndex if required?
		var value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];

		switch (type) {
			case 's': return formatString(String(value), leftJustify, minWidth, precision, zeroPad);
			case 'c': return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
			case 'b': return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
			case 'o': return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
			case 'x': return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
			case 'X': return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad).toUpperCase();
			case 'u': return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
			case 'i':
			case 'd': {
						var number = parseInt(+value);
						var prefix = number < 0 ? '-' : positivePrefix;
						value = prefix + pad(String(Math.abs(number)), precision, '0', false);
						return justify(value, prefix, leftJustify, minWidth, zeroPad);
					}
			case 'e':
			case 'E':
			case 'f':
			case 'F':
			case 'g':
			case 'G':
						{
						var number = +value;
						var prefix = number < 0 ? '-' : positivePrefix;
						var method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
						var textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
						value = prefix + Math.abs(number)[method](precision);
						return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
					}
			default: return substring;
		}
	};

	return format.replace(regex, doFormat);
}

function str_replace ( search, replace, subject ) {	// Replace all occurrences of the search string with the replacement string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Gabriel Paderni

	if(!(replace instanceof Array)){
		replace=new Array(replace);
		if(search instanceof Array){//If search	is an array and replace	is a string, then this replacement string is used for every value of search
			while(search.length>replace.length){
				replace[replace.length]=replace[0];
			}
		}
	}

	if(!(search instanceof Array))search=new Array(search);
	while(search.length>replace.length){//If replace	has fewer values than search , then an empty string is used for the rest of replacement values
		replace[replace.length]='';
	}

	if(subject instanceof Array){//If subject is an array, then the search and replace is performed with every entry of subject , and the return value is an array as well.
		for(k in subject){
			subject[k]=str_replace(search,replace,subject[k]);
		}
		return subject;
	}

	for(var k=0; k<search.length; k++){
		var i = subject.indexOf(search[k]);
		while(i>-1){
			subject = subject.replace(search[k], replace[k]);
			i = subject.indexOf(search[k],i);
		}
	}

	return subject;

}


/**
 * Trims
 */
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
function trim(string) {
        return string.replace(/(^\s+)|(\s+$)/g, "");
}
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

/**
 * Fix для перевода текста кнопок в диалогах. Применяется, так как не было найдено способа динамичесски установить текст для кнопки (jqueryui bug).
 * При этом, текст для кнопок берётся из блока с классом ui-fix-buttons, который располагается внутри блока, для которого создаётся диалог. 
 * По умолчанию текст для кнопок задаётся в виде "btn#N", где #N - порядковый номер кнопки
 * @param string dialogContId
 */
function uiDialogButtonsFix(dialogCont)
{
	//Получаем тексты для кнопок и формируем из них массив
	var i=0;
	var buttons = {};
	$(dialogCont).find('.ui-fix-buttons span').each(function(){
		buttons[i] = trim($(this).text());
		i++;
	});
	var j=0;
	//Проходимся по кнопкам в диалоге и заменяем их текст
	$(dialogCont).next().find('button.ui-button span').each(function(){
		$(this).text(buttons[j]);
		j++;
	});	
}


/**
 * Translit
 */
String.prototype.translit = (function(){
    var L = {
            'А':'A','а':'a','Б':'B','б':'b','В':'V','в':'v','Г':'G','г':'g',
            'Д':'D','д':'d','Е':'E','е':'e','Ё':'Yo','ё':'yo','Ж':'Zh','ж':'zh',
            'З':'Z','з':'z','И':'I','и':'i','Й':'Y','й':'y','К':'K','к':'k',
            'Л':'L','л':'l','М':'M','м':'m','Н':'N','н':'n','О':'O','о':'o',
            'П':'P','п':'p','Р':'R','р':'r','С':'S','с':'s','Т':'T','т':'t',
            'У':'U','у':'u','Ф':'F','ф':'f','Х':'Kh','х':'kh','Ц':'Ts','ц':'ts',
            'Ч':'Ch','ч':'ch','Ш':'Sh','ш':'sh','Щ':'Sch','щ':'sch','Ъ':'"','ъ':'"',
            'Ы':'Y','ы':'y','Ь':"'",'ь':"'",'Э':'E','э':'e','Ю':'Yu','ю':'yu',
            'Я':'Ya','я':'ya'
        },
        r = '',
        k;
    for (k in L) r += k;
    r = new RegExp('[' + r + ']', 'g');
    k = function(a){
        return a in L ? L[a] : '';
    };
    return function(){
        return this.replace(r, k);
    };
})();

/**
 * Capitalize
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}