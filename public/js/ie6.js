//min-height и min-width
function minPropsIe6()
{
	var curDocWidth = document.body.clientWidth;
	if ($('div.ppc-container').length) 
	{
		var curWidth;
		if (curDocWidth < 1000) 
		{
			curWidth = '1000px';
		}
		else
		{
			curWidth = '100%';
		}
		$('div.ppc-container').css({'width': curWidth});
	}
	if ($('div.services-list div.services-list-body').length) 
	{
		var curWidth;
		if (curDocWidth < 951)
		{
			curWidth = '900px';
		}
		else
		{
			curWidth = '100%';
		}
		$('div.services-list div.services-list-body').css({'width': curWidth});
	}
	if ($('div.top-block').length) 
	{
		var curWidth;
		if (curDocWidth < 1000) 
		{
			curWidth = '1000px';
		}
		else
		{
			curWidth = '90%';
		}
		$('div.top-block').css({'width': curWidth});
	}
}
/*
$(window).load(function(){
	minPropsIe6();
});
$(window).resize(function(){
	minPropsIe6();
});
*/