#!/usr/bin/php
<?php
/**
 * Файл, запускающий функционал по экспорту данных на шлюз организатора
 */
//Если не перданы параметры
if (!isset($argv[1]) || trim($argv[1])=='')
{
	echo 'Missing params';
	exit();
}
set_time_limit(0); 
ignore_user_abort(TRUE);
//Определяем константу, которая обозначает, что скрипт был вызван по cron
define('_CRON_',true);
require_once realpath(dirname(__FILE__)).'/../public/index.php';
//Определяем запрошенное действие
$cronAction = $argv[1];
//На основе запрашиваемого действия, запускаем нужный процесс
switch($cronAction)
{
	case 'export' : 	
		$appObj = new Welt_Applications();
		$appObj->sendCompleteApps();
	break;
	case 'synch' : 	
		$appObj = new Welt_Applications();
		$appObj->sendModifedApps();
	break;
}